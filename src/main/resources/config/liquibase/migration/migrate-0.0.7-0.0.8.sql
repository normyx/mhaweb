-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog//20200309144213_changelog.xml
-- Ran at: 09/03/20 15:44
-- Against: mha@jdbc:postgresql://192.168.0.71:5432/mysharedhomeactivities
-- Liquibase version: 3.6.3
-- *********************************************************************

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'CPX-IGHR3VJOV0A (192.168.0.26)', LOCKGRANTED = '2020-03-09 15:44:22.34' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog//20200309144213_changelog.xml::1583764948212-1::mathieu.goulene (generated)
ALTER TABLE task DROP COLUMN priority;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1583764948212-1', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog//20200309144213_changelog.xml', NOW(), 37, '8:00bd1bc91c3e28c7e1076cec9f13dc84', 'dropColumn columnName=priority, tableName=task', '', 'EXECUTED', NULL, NULL, '3.6.3', '3765063444');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog//20200309144213_changelog.xml::1583764948212-2::mathieu.goulene (generated)
ALTER TABLE task_project DROP COLUMN simplified;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1583764948212-2', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog//20200309144213_changelog.xml', NOW(), 38, '8:ec08d42add73d5c3de48c769dc7d2b05', 'dropColumn columnName=simplified, tableName=task_project', '', 'EXECUTED', NULL, NULL, '3.6.3', '3765063444');

-- Release Database Lock
UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

