-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200123094448_changelog.xml
-- Ran at: 23/01/20 10:53
-- Against: mha@jdbc:postgresql://192.168.0.71:5432/mysharedhomeactivities
-- Liquibase version: 3.6.3
-- *********************************************************************

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'CPX-IGHR3VJOV0A (192.168.0.20)', LOCKGRANTED = '2020-01-23 10:53:16.361' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200123094448_changelog.xml::1579772701940-1::mathieu.goulene (generated)
CREATE TABLE shopping_cart (id BIGINT NOT NULL, CONSTRAINT shopping_cart_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1579772701940-1', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200123094448_changelog.xml', NOW(), 34, '8:8b04f945978f4c961437cc95f6a76e42', 'createTable tableName=shopping_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '9773197496');

-- Release Database Lock
UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

