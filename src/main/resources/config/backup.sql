--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Debian 11.4-1.pgdg90+1)
-- Dumped by pg_dump version 11.2

-- Started on 2019-09-24 11:08:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 16390)
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO mhaweb;

--
-- TOC entry 196 (class 1259 OID 16385)
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO mhaweb;

--
-- TOC entry 200 (class 1259 OID 16410)
-- Name: mha_authority; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.mha_authority (
    name character varying(50) NOT NULL
);


ALTER TABLE public.mha_authority OWNER TO mhaweb;

--
-- TOC entry 202 (class 1259 OID 16430)
-- Name: mha_persistent_audit_event; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.mha_persistent_audit_event (
    event_id bigint NOT NULL,
    principal character varying(50) NOT NULL,
    event_date timestamp without time zone,
    event_type character varying(255)
);


ALTER TABLE public.mha_persistent_audit_event OWNER TO mhaweb;

--
-- TOC entry 203 (class 1259 OID 16435)
-- Name: mha_persistent_audit_evt_data; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.mha_persistent_audit_evt_data (
    event_id bigint NOT NULL,
    name character varying(150) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.mha_persistent_audit_evt_data OWNER TO mhaweb;

--
-- TOC entry 199 (class 1259 OID 16398)
-- Name: mha_user; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.mha_user (
    id bigint NOT NULL,
    login character varying(50) NOT NULL,
    password_hash character varying(60) NOT NULL,
    first_name character varying(50),
    last_name character varying(50),
    email character varying(191),
    image_url character varying(256),
    activated boolean NOT NULL,
    lang_key character varying(10),
    activation_key character varying(20),
    reset_key character varying(20),
    created_by character varying(50) NOT NULL,
    created_date timestamp without time zone,
    reset_date timestamp without time zone,
    last_modified_by character varying(50),
    last_modified_date timestamp without time zone
);


ALTER TABLE public.mha_user OWNER TO mhaweb;

--
-- TOC entry 201 (class 1259 OID 16415)
-- Name: mha_user_authority; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.mha_user_authority (
    user_id bigint NOT NULL,
    authority_name character varying(50) NOT NULL
);


ALTER TABLE public.mha_user_authority OWNER TO mhaweb;

--
-- TOC entry 204 (class 1259 OID 16447)
-- Name: profil; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.profil (
    id bigint NOT NULL,
    display_name character varying(255) NOT NULL,
    user_id bigint,
    profil_data_id bigint
);


ALTER TABLE public.profil OWNER TO mhaweb;

--
-- TOC entry 211 (class 1259 OID 16489)
-- Name: profil_data; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.profil_data (
    id bigint NOT NULL,
    photo bytea NOT NULL,
    photo_content_type character varying(255) NOT NULL
);


ALTER TABLE public.profil_data OWNER TO mhaweb;

--
-- TOC entry 198 (class 1259 OID 16396)
-- Name: sequence_generator; Type: SEQUENCE; Schema: public; Owner: mhaweb
--

CREATE SEQUENCE public.sequence_generator
    START WITH 1050
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_generator OWNER TO mhaweb;

--
-- TOC entry 214 (class 1259 OID 16507)
-- Name: spent; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.spent (
    id bigint NOT NULL,
    label character varying(20) NOT NULL,
    description character varying(200),
    amount real NOT NULL,
    spent_date date NOT NULL,
    last_update_date date NOT NULL,
    confirmed boolean NOT NULL,
    spender_id bigint,
    wallet_id bigint
);


ALTER TABLE public.spent OWNER TO mhaweb;

--
-- TOC entry 217 (class 1259 OID 16522)
-- Name: spent_config; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.spent_config (
    id bigint NOT NULL,
    label character varying(40) NOT NULL,
    wallet_id bigint NOT NULL
);


ALTER TABLE public.spent_config OWNER TO mhaweb;

--
-- TOC entry 215 (class 1259 OID 16512)
-- Name: spent_sharing; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.spent_sharing (
    id bigint NOT NULL,
    amount_share real NOT NULL,
    share integer NOT NULL,
    sharing_profil_id bigint,
    spent_id bigint
);


ALTER TABLE public.spent_sharing OWNER TO mhaweb;

--
-- TOC entry 216 (class 1259 OID 16517)
-- Name: spent_sharing_config; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.spent_sharing_config (
    id bigint NOT NULL,
    share integer NOT NULL,
    spent_config_id bigint NOT NULL,
    profil_id bigint NOT NULL
);


ALTER TABLE public.spent_sharing_config OWNER TO mhaweb;

--
-- TOC entry 209 (class 1259 OID 16476)
-- Name: task; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.task (
    id bigint NOT NULL,
    label character varying(40) NOT NULL,
    description character varying(4000),
    done boolean NOT NULL,
    due_date date,
    priority integer NOT NULL,
    task_project_id bigint
);


ALTER TABLE public.task OWNER TO mhaweb;

--
-- TOC entry 210 (class 1259 OID 16484)
-- Name: task_owner; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.task_owner (
    owner_id bigint NOT NULL,
    task_id bigint NOT NULL
);


ALTER TABLE public.task_owner OWNER TO mhaweb;

--
-- TOC entry 207 (class 1259 OID 16466)
-- Name: task_project; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.task_project (
    id bigint NOT NULL,
    label character varying(40) NOT NULL,
    workspace_id bigint
);


ALTER TABLE public.task_project OWNER TO mhaweb;

--
-- TOC entry 208 (class 1259 OID 16471)
-- Name: task_project_owner; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.task_project_owner (
    owner_id bigint NOT NULL,
    task_project_id bigint NOT NULL
);


ALTER TABLE public.task_project_owner OWNER TO mhaweb;

--
-- TOC entry 212 (class 1259 OID 16497)
-- Name: wallet; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.wallet (
    id bigint NOT NULL,
    label character varying(40) NOT NULL,
    workspace_id bigint
);


ALTER TABLE public.wallet OWNER TO mhaweb;

--
-- TOC entry 213 (class 1259 OID 16502)
-- Name: wallet_owner; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.wallet_owner (
    owner_id bigint NOT NULL,
    wallet_id bigint NOT NULL
);


ALTER TABLE public.wallet_owner OWNER TO mhaweb;

--
-- TOC entry 205 (class 1259 OID 16456)
-- Name: workspace; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.workspace (
    id bigint NOT NULL,
    label character varying(40) NOT NULL
);


ALTER TABLE public.workspace OWNER TO mhaweb;

--
-- TOC entry 206 (class 1259 OID 16461)
-- Name: workspace_owner; Type: TABLE; Schema: public; Owner: mhaweb
--

CREATE TABLE public.workspace_owner (
    owner_id bigint NOT NULL,
    workspace_id bigint NOT NULL
);


ALTER TABLE public.workspace_owner OWNER TO mhaweb;

--
-- TOC entry 3019 (class 0 OID 16390)
-- Dependencies: 197
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
00000000000000	jhipster	config/liquibase/changelog/00000000000000_initial_schema.xml	2019-09-24 09:35:10.891961	1	EXECUTED	8:b8c27d9dc8db18b5de87cdb8c38a416b	createSequence sequenceName=sequence_generator		\N	3.6.3	\N	\N	9310510834
00000000000001	jhipster	config/liquibase/changelog/00000000000000_initial_schema.xml	2019-09-24 09:35:11.319122	2	EXECUTED	8:479b887a3c03a7ee78322613cde2c00b	createTable tableName=mha_user; createTable tableName=mha_authority; createTable tableName=mha_user_authority; addPrimaryKey tableName=mha_user_authority; addForeignKeyConstraint baseTableName=mha_user_authority, constraintName=fk_authority_name, ...		\N	3.6.3	\N	\N	9310510834
20190725132118-1	jhipster	config/liquibase/changelog/20190725132118_added_entity_Profil.xml	2019-09-24 09:35:11.348786	3	EXECUTED	8:5dd25f72715d5851a9395babb85ed2fa	createTable tableName=profil		\N	3.6.3	\N	\N	9310510834
20190725132118-1-relations	jhipster	config/liquibase/changelog/20190725132118_added_entity_Profil.xml	2019-09-24 09:35:11.355236	4	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.6.3	\N	\N	9310510834
20190725132119-1	jhipster	config/liquibase/changelog/20190725132119_added_entity_Workspace.xml	2019-09-24 09:35:11.372599	5	EXECUTED	8:0a6924cb00a54be85389dc3aac1630a9	createTable tableName=workspace		\N	3.6.3	\N	\N	9310510834
20190725132119-1-relations	jhipster	config/liquibase/changelog/20190725132119_added_entity_Workspace.xml	2019-09-24 09:35:11.391714	6	EXECUTED	8:5481566f5d07ad4da732a965690e41ad	createTable tableName=workspace_owner; addPrimaryKey tableName=workspace_owner		\N	3.6.3	\N	\N	9310510834
20190725132120-1	jhipster	config/liquibase/changelog/20190725132120_added_entity_TaskProject.xml	2019-09-24 09:35:11.409335	7	EXECUTED	8:892ef1e279eba699cdb4d83871f5cd9c	createTable tableName=task_project		\N	3.6.3	\N	\N	9310510834
20190725132120-1-relations	jhipster	config/liquibase/changelog/20190725132120_added_entity_TaskProject.xml	2019-09-24 09:35:11.428351	8	EXECUTED	8:b2a5af7135b390c4659131ad1a6d806c	createTable tableName=task_project_owner; addPrimaryKey tableName=task_project_owner		\N	3.6.3	\N	\N	9310510834
20190725132121-1	jhipster	config/liquibase/changelog/20190725132121_added_entity_Task.xml	2019-09-24 09:35:11.4558	9	EXECUTED	8:6469020ce8bc5ab06e70a13562a29f1f	createTable tableName=task		\N	3.6.3	\N	\N	9310510834
20190725132121-1-relations	jhipster	config/liquibase/changelog/20190725132121_added_entity_Task.xml	2019-09-24 09:35:11.478143	10	EXECUTED	8:1433bc4bb894ead2edd929fef17448be	createTable tableName=task_owner; addPrimaryKey tableName=task_owner		\N	3.6.3	\N	\N	9310510834
20190805215757-1	jhipster	config/liquibase/changelog/20190805215757_added_entity_ProfilData.xml	2019-09-24 09:35:11.502933	11	EXECUTED	8:7ef580dae4e6bf774b5d73dd84f2d99c	createTable tableName=profil_data		\N	3.6.3	\N	\N	9310510834
20190805215757-1-relations	jhipster	config/liquibase/changelog/20190805215757_added_entity_ProfilData.xml	2019-09-24 09:35:11.509231	12	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.6.3	\N	\N	9310510834
20190808092503-1	jhipster	config/liquibase/changelog/20190808092503_added_entity_Wallet.xml	2019-09-24 09:35:11.528057	13	EXECUTED	8:d77aac57d4c671d90b93a4778ee21de9	createTable tableName=wallet		\N	3.6.3	\N	\N	9310510834
20190808092503-1-relations	jhipster	config/liquibase/changelog/20190808092503_added_entity_Wallet.xml	2019-09-24 09:35:11.54714	14	EXECUTED	8:e4869d33233e647726920c688c24ffeb	createTable tableName=wallet_owner; addPrimaryKey tableName=wallet_owner		\N	3.6.3	\N	\N	9310510834
20190808093326-1	jhipster	config/liquibase/changelog/20190808093326_added_entity_Spent.xml	2019-09-24 09:35:11.565627	15	EXECUTED	8:58669572c575fd7a50a393f849476731	createTable tableName=spent		\N	3.6.3	\N	\N	9310510834
20190808093326-1-relations	jhipster	config/liquibase/changelog/20190808093326_added_entity_Spent.xml	2019-09-24 09:35:11.571259	16	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.6.3	\N	\N	9310510834
20190808093559-1	jhipster	config/liquibase/changelog/20190808093559_added_entity_SpentSharing.xml	2019-09-24 09:35:11.588638	17	EXECUTED	8:4984e69c05662e7ffca3acb84aa6978d	createTable tableName=spent_sharing		\N	3.6.3	\N	\N	9310510834
20190808093559-1-relations	jhipster	config/liquibase/changelog/20190808093559_added_entity_SpentSharing.xml	2019-09-24 09:35:11.594886	18	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.6.3	\N	\N	9310510834
20190819092745-1	jhipster	config/liquibase/changelog/20190819092745_added_entity_SpentSharingConfig.xml	2019-09-24 09:35:11.614613	19	EXECUTED	8:e40627c5c2567361b1ac20a90b919e74	createTable tableName=spent_sharing_config		\N	3.6.3	\N	\N	9310510834
20190819092745-1-relations	jhipster	config/liquibase/changelog/20190819092745_added_entity_SpentSharingConfig.xml	2019-09-24 09:35:11.620522	20	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.6.3	\N	\N	9310510834
20190820084126-1	jhipster	config/liquibase/changelog/20190820084126_added_entity_SpentConfig.xml	2019-09-24 09:35:11.63787	21	EXECUTED	8:945da0ecd2c75835881783b26bafb66e	createTable tableName=spent_config		\N	3.6.3	\N	\N	9310510834
20190820084126-1-relations	jhipster	config/liquibase/changelog/20190820084126_added_entity_SpentConfig.xml	2019-09-24 09:35:11.643159	22	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.6.3	\N	\N	9310510834
20190725132118-2	jhipster	config/liquibase/changelog/20190725132118_added_entity_constraints_Profil.xml	2019-09-24 09:35:11.657216	23	EXECUTED	8:b35d9bf9867e1182d27f1459d76b0381	addForeignKeyConstraint baseTableName=profil, constraintName=fk_profil_user_id, referencedTableName=mha_user; addForeignKeyConstraint baseTableName=profil, constraintName=fk_profil_profil_data_id, referencedTableName=profil_data		\N	3.6.3	\N	\N	9310510834
20190725132119-2	jhipster	config/liquibase/changelog/20190725132119_added_entity_constraints_Workspace.xml	2019-09-24 09:35:11.673053	24	EXECUTED	8:95d4d8f72cb62f0b771e66aeb6f143fe	addForeignKeyConstraint baseTableName=workspace_owner, constraintName=fk_workspace_owner_workspace_id, referencedTableName=workspace; addForeignKeyConstraint baseTableName=workspace_owner, constraintName=fk_workspace_owner_owner_id, referencedTabl...		\N	3.6.3	\N	\N	9310510834
20190725132120-2	jhipster	config/liquibase/changelog/20190725132120_added_entity_constraints_TaskProject.xml	2019-09-24 09:35:11.689905	25	EXECUTED	8:7e4429eb9f8005d9aea79e4f1fbfeba3	addForeignKeyConstraint baseTableName=task_project, constraintName=fk_task_project_workspace_id, referencedTableName=workspace; addForeignKeyConstraint baseTableName=task_project_owner, constraintName=fk_task_project_owner_task_project_id, referen...		\N	3.6.3	\N	\N	9310510834
20190725132121-2	jhipster	config/liquibase/changelog/20190725132121_added_entity_constraints_Task.xml	2019-09-24 09:35:11.704888	26	EXECUTED	8:4bd36cd7c29d83d182497f646ff21ab0	addForeignKeyConstraint baseTableName=task, constraintName=fk_task_task_project_id, referencedTableName=task_project; addForeignKeyConstraint baseTableName=task_owner, constraintName=fk_task_owner_task_id, referencedTableName=task; addForeignKeyCo...		\N	3.6.3	\N	\N	9310510834
20190808092503-2	jhipster	config/liquibase/changelog/20190808092503_added_entity_constraints_Wallet.xml	2019-09-24 09:35:11.723627	27	EXECUTED	8:0276237ebc82c10e7697d7754f827921	addForeignKeyConstraint baseTableName=wallet, constraintName=fk_wallet_workspace_id, referencedTableName=workspace; addForeignKeyConstraint baseTableName=wallet_owner, constraintName=fk_wallet_owner_wallet_id, referencedTableName=wallet; addForeig...		\N	3.6.3	\N	\N	9310510834
20190808093326-2	jhipster	config/liquibase/changelog/20190808093326_added_entity_constraints_Spent.xml	2019-09-24 09:35:11.740138	28	EXECUTED	8:1476c4418d15a01c3fc791fde663b85e	addForeignKeyConstraint baseTableName=spent, constraintName=fk_spent_spender_id, referencedTableName=profil; addForeignKeyConstraint baseTableName=spent, constraintName=fk_spent_wallet_id, referencedTableName=wallet		\N	3.6.3	\N	\N	9310510834
20190808093559-2	jhipster	config/liquibase/changelog/20190808093559_added_entity_constraints_SpentSharing.xml	2019-09-24 09:35:11.755792	29	EXECUTED	8:68a9f397e6c1e6f5cc94695f73ce811d	addForeignKeyConstraint baseTableName=spent_sharing, constraintName=fk_spent_sharing_sharing_profil_id, referencedTableName=profil; addForeignKeyConstraint baseTableName=spent_sharing, constraintName=fk_spent_sharing_spent_id, referencedTableName=...		\N	3.6.3	\N	\N	9310510834
20190820084126-2	jhipster	config/liquibase/changelog/20190820084126_added_entity_constraints_SpentConfig.xml	2019-09-24 09:35:11.767613	30	EXECUTED	8:74de1da514a346d30fdcf3e54b491de1	addForeignKeyConstraint baseTableName=spent_config, constraintName=fk_spent_config_wallet_id, referencedTableName=wallet		\N	3.6.3	\N	\N	9310510834
20190819092745-2	jhipster	config/liquibase/changelog/20190819092745_added_entity_constraints_SpentSharingConfig.xml	2019-09-24 09:35:11.781772	31	EXECUTED	8:18a97351a22efe12ad779eda652d54b3	addForeignKeyConstraint baseTableName=spent_sharing_config, constraintName=fk_spent_sharing_config_spent_config_id, referencedTableName=spent_config; addForeignKeyConstraint baseTableName=spent_sharing_config, constraintName=fk_spent_sharing_confi...		\N	3.6.3	\N	\N	9310510834
\.


--
-- TOC entry 3018 (class 0 OID 16385)
-- Dependencies: 196
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- TOC entry 3022 (class 0 OID 16410)
-- Dependencies: 200
-- Data for Name: mha_authority; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.mha_authority (name) FROM stdin;
ROLE_ADMIN
ROLE_USER
\.


--
-- TOC entry 3024 (class 0 OID 16430)
-- Dependencies: 202
-- Data for Name: mha_persistent_audit_event; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.mha_persistent_audit_event (event_id, principal, event_date, event_type) FROM stdin;
1001	admin	2019-09-24 07:36:05.647	AUTHENTICATION_SUCCESS
1002	admin	2019-09-24 07:36:51.41	AUTHENTICATION_SUCCESS
1051	admin	2019-09-24 07:45:38.582	AUTHENTICATION_FAILURE
1052	admin	2019-09-24 07:45:44.081	AUTHENTICATION_SUCCESS
1053	admin	2019-09-24 07:46:27.397	AUTHENTICATION_SUCCESS
1054	mgoulene	2019-09-24 07:49:00.922	AUTHENTICATION_SUCCESS
1055	mgoulene	2019-09-24 08:08:40.801	AUTHENTICATION_SUCCESS
1056	mgoulene	2019-09-24 08:09:24.91	AUTHENTICATION_SUCCESS
1057	mgoulene	2019-09-24 08:56:03.427	AUTHENTICATION_SUCCESS
1058	mgoulene	2019-09-24 08:58:01.899	AUTHENTICATION_SUCCESS
1059	mgoulene	2019-09-24 09:00:02.898	AUTHENTICATION_SUCCESS
1351	mgoulene	2019-09-24 09:04:53.854	AUTHENTICATION_SUCCESS
\.


--
-- TOC entry 3025 (class 0 OID 16435)
-- Dependencies: 203
-- Data for Name: mha_persistent_audit_evt_data; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.mha_persistent_audit_evt_data (event_id, name, value) FROM stdin;
1051	type	org.springframework.security.authentication.BadCredentialsException
1051	message	Les identifications sont erronées
\.


--
-- TOC entry 3021 (class 0 OID 16398)
-- Dependencies: 199
-- Data for Name: mha_user; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.mha_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key, activation_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date) FROM stdin;
1	system	$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG	System	System	system@localhost		t	fr	\N	\N	system	\N	\N	system	\N
2	anonymoususer	$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO	Anonymous	User	anonymous@localhost		t	fr	\N	\N	system	\N	\N	system	\N
3	admin	$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC	Administrator	Administrator	admin@localhost		t	fr	\N	\N	system	\N	\N	system	\N
4	user	$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K	User	User	user@localhost		t	fr	\N	\N	system	\N	\N	system	\N
1101	mgoulene	$2a$10$my1unVDKakT4vUUOyXSENuGS8LPRq66KIPqWCgKAQD5iGu7cDqMuy	Mathieu	Goulène	mathieu.goulene@gmail.com	\N	t	fr	\N	63157638984752664557	admin	2019-09-24 07:47:13.331	2019-09-24 07:47:13.274	admin	2019-09-24 07:47:13.331
1102	gdiez	$2a$10$my1unVDKakT4vUUOyXSENuGS8LPRq66KIPqWCgKAQD5iGu7cDqMuy	Géraldine	Diez	geraldine.diez@gmail.com	\N	t	fr	\N	89561177201399456648	admin	2019-09-24 07:47:44.586	2019-09-24 07:47:44.584	admin	2019-09-24 07:47:44.586
\.


--
-- TOC entry 3023 (class 0 OID 16415)
-- Dependencies: 201
-- Data for Name: mha_user_authority; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.mha_user_authority (user_id, authority_name) FROM stdin;
1	ROLE_ADMIN
1	ROLE_USER
3	ROLE_ADMIN
3	ROLE_USER
4	ROLE_USER
1101	ROLE_USER
1101	ROLE_ADMIN
1102	ROLE_USER
\.


--
-- TOC entry 3026 (class 0 OID 16447)
-- Dependencies: 204
-- Data for Name: profil; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.profil (id, display_name, user_id, profil_data_id) FROM stdin;
1201	Mathieu	1101	1151
1202	Géraldine	1102	1152
\.


--
-- TOC entry 3033 (class 0 OID 16489)
-- Dependencies: 211
-- Data for Name: profil_data; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.profil_data (id, photo, photo_content_type) FROM stdin;
1151	\\xffd8ffe000104a46494600010101012c012c0000ffe1010045786966000049492a000800000001000e010200de0000001a0000000000000042455645524c592048494c4c532c204341202d204a414e554152592031333a20204163746f722047656f72676520436c6f6f6e6579206172726976657320617420746865203730746820416e6e75616c20476f6c64656e20476c6f6265204177617264732068656c64206174205468652042657665726c792048696c746f6e20486f74656c206f6e204a616e756172792031332c203230313320696e2042657665726c792048696c6c732c2043616c69666f726e69612e20202850686f746f206279204a61736f6e204d6572726974742f476574747920496d6167657329ffe103b7687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f00093c7264663a52444620786d6c6e733a7264663d22687474703a2f2f7777772e77332e6f72672f313939392f30322f32322d7264662d73796e7461782d6e7323223e0a09093c7264663a4465736372697074696f6e207264663a61626f75743d222220786d6c6e733a70686f746f73686f703d22687474703a2f2f6e732e61646f62652e636f6d2f70686f746f73686f702f312e302f2220786d6c6e733a4970746334786d70436f72653d22687474703a2f2f697074632e6f72672f7374642f4970746334786d70436f72652f312e302f786d6c6e732f2220786d6c6e733a4765747479496d61676573474946543d22687474703a2f2f786d702e6765747479696d616765732e636f6d2f676966742f312e302f2220786d6c6e733a64633d22687474703a2f2f7075726c2e6f72672f64632f656c656d656e74732f312e312f2220786d6c6e733a706c75733d22687474703a2f2f6e732e757365706c75732e6f72672f6c64662f786d702f312e302f2220786d6c6e733a697074634578743d22687474703a2f2f697074632e6f72672f7374642f4970746334786d704578742f323030382d30322d32392f222070686f746f73686f703a4372656469743d22476574747920496d6167657322204765747479496d61676573474946543a417373657449443d2231353934323231323222203e0a3c64633a63726561746f723e3c7264663a5365713e3c7264663a6c693e4a61736f6e204d6572726974743c2f7264663a6c693e3c2f7264663a5365713e3c2f64633a63726561746f723e3c64633a6465736372697074696f6e3e3c7264663a416c743e3c7264663a6c6920786d6c3a6c616e673d22782d64656661756c74223e42455645524c592048494c4c532c204341202d204a414e554152592031333a20204163746f722047656f72676520436c6f6f6e6579206172726976657320617420746865203730746820416e6e75616c20476f6c64656e20476c6f6265204177617264732068656c64206174205468652042657665726c792048696c746f6e20486f74656c206f6e204a616e756172792031332c203230313320696e2042657665726c792048696c6c732c2043616c69666f726e69612e20202850686f746f206279204a61736f6e204d6572726974742f476574747920496d61676573293c2f7264663a6c693e3c2f7264663a416c743e3c2f64633a6465736372697074696f6e3e0a09093c2f7264663a4465736372697074696f6e3e0a093c2f7264663a5244463e0affed012250686f746f73686f7020332e30003842494d04040000000001061c0250000d4a61736f6e204d6572726974741c027800de42455645524c592048494c4c532c204341202d204a414e554152592031333a20204163746f722047656f72676520436c6f6f6e6579206172726976657320617420746865203730746820416e6e75616c20476f6c64656e20476c6f6265204177617264732068656c64206174205468652042657665726c792048696c746f6e20486f74656c206f6e204a616e756172792031332c203230313320696e2042657665726c792048696c6c732c2043616c69666f726e69612e20202850686f746f206279204a61736f6e204d6572726974742f476574747920496d61676573291c026e000c476574747920496d61676573ffdb0043000a07070807060a0808080b0a0a0b0e18100e0d0d0e1d15161118231f2524221f2221262b372f26293429212230413134393b3e3e3e252e4449433c48373d3e3bffdb0043010a0b0b0e0d0e1c10101c3b2822283b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3bffc00011080264019803012200021101031101ffc4001c0000010501010100000000000000000000000102030405060708ffc4004c100001030204030406060705060603010001000203041105122131064151132261710714328191a1152342b1c1d1525462729293f024333482e117355373a2b216434463c2f1647483a3ffc4001a010003010101010000000000000000000000010203040506ffc40026110101000202020202030003010000000000010211032112312241325104137123336142ffda000c03010002110311003f00c3c2b159f09ac6d442e361ed32fa10bd878778820c52918f6bc1b8f82f19aea29a82adf4d3b6ce69d3c47556705c66a307ab12c649889efb3af8ad6c994dc672eabdec6a858b80e3906274ac7b1e1d70b6962d4210848042108043ec9f25e67e90db7aca2ff9b65e9a762bcd7d208fed3447ff007c04e7b832fc5d870db72d1b7c96d2c8e1e1fd89be4b5d053d0421091842108010842005148a55148b3e5fc558fb42762bc97d33ff00bae94fff00923fed2bd68ec5793fa67ff73407a54b7fed72e1c7fecc7fd6d7f0af3ea17b04201236d561d74bdbd63dc36bd8246d4bdadb35ca3b6975e9edc73a2644f0dd2c9d044e95db298c594eaa2e5a4dc9131f242eccd5a74f5f1ca32bf42aa35832ea144f888ef346ca7cb626537da7c46271378cddbe0aa53c7690664e15121696e6d5425d235e0b89b85525d69b5c66ba6dfab87b0154aa694b41214f4b5a447672b40b6669b5b55c9bcb0bdb96ee567e1514beb0000ba46d24c0ddd759546f149521ce6ec574cdad8a4683a6aab2933f9516efbacfa8927a787526cb92ab7e7aa738f32bb3c566a7346fef0b86dc2e1657e7793e2af871d5abc676d9a1843e2cda68b45ad6363bf35814558636965f42ae36b3bba9519e3654deae91e213b9c4b41d164bb757e57b5cf2ebe8a093b39068355bf1f51ae07523c9eeee9d2c8f8a4bdf450c2eecdda1b2b0e2d9c59164976db1d59a58f5bed60bb46a02a1ab9f67e8add1539638e6f67922b2301fdd1a9518dc71cb51959d18236b6c42bd00b8b00a08a2bc62eadd3b2c565c9974e7deea43a35363173aa74bb28cbc305d633b2f4657411b981c6d7e4a5c374a723982b3aa2774aed49b0d95dc3a4196dd56b9cb303b7716a5370843c5d0b0952f6ee2ee148b1281d246dcb20d5ae1c8af29aaa69a8e77c1330b5ecebcd7d0ce6870208b82b89e30e12657426681a1b237569b7c97ad8e5e3fe3b2e3b79ff0fe3d360b560dc985c7bc072f15ec78362f0e234ac918f0e046e1784cf04b4d3ba199858f61b1056c70df11cd825506b9c5d4ee3a8fd1f15ae58cb37112e9ee4859d856290e214ec7c6f0e0e170415a2b16810842401d8af39e3e6e69a93c2a1abd18ecbcf38f47d6d2f8d433ef47d965f8d75bc3ff00e09be4b596570fff00826f92d54c4f4108424610842004210801432a994332cb97f1abc7da22742bc9fd335fe8487ffd96ff00dae5eae765e55e990138130f2150cbfc1cb871bff263feb6b3e15e43494a67d6e89e31168a28ea248b469b26be473f52755e9d9db8ec7498461ed929bb4d36bdd67d486f68434f34fc3715753d398cf4202809cefbacb3f6c72ea81a052c3245d9b83ceea07b480aa4af234052c71d9e33744e5ada8718ce80abd4bd94edb386ab2d6961f102db93c95f24e9beec9d25a88a389966f45529ab1d4f2b737b37d54952f71b859e774b0c773b678cf2f6eaea1d4d3d13e56919836e15086bcba1cb7d40592da878664cc6d6b26472161dd19714a2e0b35b532c872971b2a6acb99db116e69869dcdd4ecaf1b24d1e3d4471e8eba99e5c5b705332e549da58d93f63ddd99988164e8ceaa70c8a56fed28bb27349e68dc5de8b97552440b5d74c21c1974432722a6fa4e5bd2f9a86b5a2ca28e512c8557702ed029e96221d72b2b8c936cee5645d1c82b70c7a5d54f6485b942c8a4a5be61b2e6ca5ac632e5367d934442516bf3d5154334c5adeaa2739f011e2892eba5e979f8109298b9a407725910bdd475263934b1b2d8a6c4660d0d1b2cfc4a9bb571945b31decaf1ce5f8e45b8bec92391a2c42160c524f0eb7240e4853782efaad71e3b66f6faed35ec6c8d2d70d0a72177b6705c65c22dac8ccf000d95becb873f02bcc668a4825745334b1ed36703c97d13246d9585ae1a15c0719707fad30d4d3372ccd1a78f81558e5712cb1db92e18e259705a96c72bc9a671fe0ff0045ec186625157d3b5ec7837170415e012c6f864747234b5ed3620ee1747c29c512e0d50c8677934ce36b9fb1fe8b4cb1df719cba7b52154a0ae8ab606c8c707022e0856d64d42f3ff4802c299dd2a19f7af40e4b80f4843fb3c07a4ecfbd2fb82faaea787ffc137c96b2c7e1d37a16f92d84c4f41084240210840084210614332994332cf97f1563ed095e5fe97403c3cfbf29e3fbcaf4f5e5fe975e7e80744d6973a49d960353a5d79f8ffd98ff00adefe35e3462698eed1deba80dc1d775af4d865792dece8e7998ed43991bb4eb7d3456a7e13c7a59cbe3c35c5a45c16bda41f9fc97aae460b1f94abb0383dc07557dfc11c46c844aec2de1a7f6d971e62f70abbf04c6284fd7e1f50ccba925ba0f7a9cb1dc4658eda15384b9b446504682eb9a71bb8ada7e2389be1752b20792f16366dcac57b1d1b8b5ed2d70dc1162118e3e231c7418c2f706b772b769689d053e771b0235bac6a67e49daeb2d6c42bdd2d336366dcd4f27e8f2aa5593c772d60546f7375623a29a6371cd5b66141a2f2380f32899638c5e385537419a3cccd5404106c742b6a364101b17b48e764b538532a23ed6075ca5393f6d6e1bf4c9865ca45f92b6e95b23081baa6fa69637963d8410a6642e6ea53ca637b73deba452bafb2854d20ef15629689d36a468abca63374a59229341be8af402e3bc2e9b52c653bacd1aa92099bd9d9c45f9a9bf28a9dd2d46411dc2a71465c7bbb2b92358e06c742a7a1a504ded759f94c31aae4f448296cccce164c6ccd351946c34576aeec659a360b1a275aa45f9bb55187ce5b5cfee356404b45910c8f630d9d60b46a296318736769b9f058eda86e521478df458e3da68642e9f52a599bda1ba828876b212169fab968d428cf2f1a2cd55463084e7c6e36bf35604443b655eaea3b168d39a896e57a4eb7e8ada36bda4585d0a218888c5ca13f1e5fa5cc728faa10842f49d0132589b330b5c2f74f420de71c69c20660eaba46012b790fb43a2f38735cd25af05ae1a1046a0afa2a68593465ae00dd79971b7093a37babe8d9deddec1f687e6af0cbc7aa9ca6fb8cbe10e2b9309a86d2d43c9a771ee93f63fd17ae51d64757107b5c0dc6ebe79d2ebafe10e2d930f9994756f3d913663c9dbc0abcf0df7112e9ec0b83f4843fb030f4999ff0072ed28eb23aa8839ae06e1719e907fddc3fe6b3fee0b1fb697f1ae8b86cff616f92d9589c326f46df25b6989e8210848c210847b0108e6b3f15c6f0dc120edb11ac8e9da7d969d5cef2035280d05998be2b87e1517695f5905334edda3ec4f90dcfb97018efa5732c32438344ea5b0ff00115000711fb2dd40f7fc170126235b8a561923a196be6905def739c49ea4bddb7c92bc7e53544ba7a4621e93a8636bbd4e07c801b092a1dd8b5de21a0179f92e3f11e21c7f1499d2d3e0cf7b2da48d84c4c1e399dde23ccaa74984e2b19323a4a7c3dad23b94ac05ff00c66e7e6ac3b0a8f10cce96696a40dc3e673c3be29e3c3863e8ee76b323a9c76673bfb3b6f7b97f6ec06fe65ea570c6a26f6952d95ccfd18da2417f1c8f24fbc2d26f0ce09a3a6a76439bd9ecfbce36e9b24fa0786a36348a6a9693a979a9747f2175aa18f4750d89ef1055b09782d74334e5847907b775ac2aeb638de724ef636c3b40dcd97efba826e1de149edd95754c45c740670e1f03a95561c06ae926b61d56e945c96805a730f10e36bfc11b0d91352d7309b37da198faab5cd27c4106cab3b87e9aa2a1ce10d2d547c9ac95cd737fca6e3e4167bea7148278dd5383d442f6ed2c2d7343c788b9175a31630c7d9b250cc72eaecf016b99e479a02b1e12c3ea1f97eb28cbbd9395aeca7a1b7e4167e25c215f87e6742e82b6102f9e17eb6fdd3aaeaa9314c2eb5e032aa373b62d99ae693efd8ad13494b016b9f1cb4a0ea24f6e3fe21b7bc28cb0945ee69e4934f3c19acc2cb69622c6ea9493544a4e77b8af4dc6704a6972b6ba9da1b3387655d4e6f113adb301ecfdcb99abc03d4a631c9116b8e8d7170735def5978f8fd15de9c9863af7375620afa8a67771e4b47d93b296b4069cb18b92aa8a77daf64e5994ec4cb4da8eae2ad602e68cca0a9205ec1186c0d206aa0aa95b1cce638ec792c3c3e5d33bbcb2361a674f269aadb6c429e98e962a1c1dd1bdd98594f893c36321ab0e5cee59f8aecd573d54ecf537dc051c843469ba491ae0339fb4544493baf4319d2a751629dc5cecb75d0504168b32c2a167d6871daebaaa40dec34e4b87f9596ba88e4bb9a67d78cad3d5613185f2fbd6c62d25ae07354a9612e19ba2be1be386d18aed4d418b0d3197f25910479efaa2b27749216f21c9322739842df1c6cc76bd6a3430f97b09f29e6ba4f6da1f6d172b282d6891bb8d56de0d89b676764fd1dcd7273f1f94f22ce7daf9681aac3c5a68d87deba0af8fb3a72f6ecb8caf91d3545ba28fe371ef3dd4e33b412cae9353b210e616b05d0bd4c7d74d9f62a108526108420050d55332a622c7806e14c8450f22e33e147d0cefada48eec3ac8d03e6171a4e9e0be85afa28eb60731e01365e41c5bc31261550fa88187b127bcd1f67c569867aeaa72c7ee2ef07716be925650d63fb8748de4fc8ad5e31c4a1adc2dcd0e19c3dbefef05e6de2a6f599a42c1248e7004684f8abb86ea65d4d3db7863fc237c96eae7f858de8d96e8ba058df6d27a084212f6023742f2ef4a7c7b361730e1dc2cbc544acbd4c8cf69ad2346b4f2279948e4dac719fa53a5c39d2e1b82cec92a5872c9516ccd8cf30de448ebb0f15e692577ae574b3d4623256d4c835909bea7c5c34f72c2a7a1cef39f4cbed46c3d3912568c6e9a363dd470b1b081a3c36e1be249e7e415c17f50f6414f0b9b2c94e2592da19a575b37e2569c3897ab46e2f0f68cd63d947a13ceda8f995957a9ae799247ba5b7b72e6c8c07f789fb93ae24958f0fbb63161d9b1ce0d1e17b0f8055b2d2fcdc45894aeece86820840d3b59dc2573be1a05565acc7ea985adc6c345ac2389b95be5a0486b239dc580ddbcfb6398fb86ca66494393fb454d431a00bb4b5ae6fbac8d8d294ecc5e95c2a269649fb32330848207890158c364c36b9f761a764e4fb3331c331e63723e4b62930fc06d9c62b24521e6f8c8cbe6468a43c1784e266f4b8c52be5712790fb8828d99ecc323863649f4646f00588cd95b7f7587cd432c4c0dcecc0a99b979173e3b9fde0f57a1e17c770965e181d3c43fe14ef77c894da6af313dc258e68a466ed04dfded23f240ac6189c149290fa4c430f711a3a96b3b51fc2edfe2b470cc66590914d8a36b241bc15acecdc7c2eb426661b8c347ac53d3555dbbf679246ff981d162d7f0eba96613e153c92160efd2558d6dd1afe63cd2d93a18861f8bdf3d10657463585d76bbc83c5be054cda19646b9f83d759d9acfa7a8bb4b0f8dbf25870073db09ceea7905b2c8e3df835d9dd5aba3a71257892e194d8b519067603764cd3b11e046c4228d31e7825138884926175ee24643fdd4a7a11ecb81f8a6be965a8a7115530c134968f20d59981bdd9e1e6ba264d4f8947251d6c21ec7775d1bf437f03d7982aabe84b2a061f552493c6f36a7a970d763dc75bed0e479a5687058a70ef615adab8e48fb398dcb05fbaee63e2b1f11a5969d9a46ed2d9865208bf82f44930bab99f554352e0e7349ecdeeb77b4d1df8f9ac78302a97e112d3c9554f5ae796b5cc6bc1735b7bebadf7b2c2e37cb7f4571ede7cca89612435c5a98f7ba579738dc95a35d8354411b666b1ce67b2f0778cdc800fc1548a965cd77308b2d778cec6b5db5b088bb2873dd3eb64cda7555639dec608d97f2434973aee375c571be7e558f9545530134d61cb55996d6cb6a495a0652abb2904d2e660165be19ea76b9975b10372c63aad9a09ecdcae3ee59ce0d8859232a430870d0ac3931fec8ca6ed58c46132ca0b7517561d4e29a8b31d090a086a1af9083e6afe33330d03325af96c40595f2eb15cc75db9477d63dc7c5119cceca9f1c792e4a9208b5bd97a16c917d58925ccc88f92869dd90768c765705a0fa70e8de0fe8ac895bd99ca0acf8ecca68e3a1a7c61d574c627f4b2c77c61b50ecdd5261d7ed4eaa7ad89c2a1d6e6dba998cc33b222dd5509a5ceeb0f64210f8cb792174cd6ba5ef6fb1508424a0842100210841854315c322c4299cd734136f8abe840786f1370ecb83d4ba4634f604ff00095823423cd7bd63983c38952bdae60371623aaf19c7b049706ab731c09849ee3bf05a619fd567963aee3d6784cde8d9fba1746b98e0f75e8a3fdc1f72e9d45f6b9e82108dca9356adad870ea39ab2a5e190c2d2e71257ce588630daac4aa6a99d8beaeb267492d54ecce1bfb2c074b0160b7bd2971d498b628ec268261ea14ceb38b7512386e4f5b725c7e18d756b6473c18a01ed4a00cc7c012344e6bdab5ae97e81d593b8b69a8a37907bd254656b1a3a92741f02b6051c5344d757e2b13c8d7246cfab07c8d8159bdac2e93b311bb28176c4db8637c4f52adc53c71465c2289c4e833306fefdd394697860b83d4bd8f75754d439becb416656f901b2922e1ec19ae21d2cb2176fda46c3f0b10ab32a2a4b1ad2c74cf03616681f0d7e2aa56635554af31fae535381a16c6c3211ef00dbe28dd3f17434fc3b82b4b4b69a9c3ae75958e65fcb70ac3b04a68dc40a5a6b1e4de7ef0b84938a711792f6620d932fd80e2d27dc56952f14e292b1ad95d6e6eedec03c746b86a0fc53d978ba534986d3dc4987fabb9baf691f790fc170ac4439e4d34b7366ba3195e0f982b329f8adbdab5b50c324634cc5b95ccf3b6e3c6ca59a8e93112f9e8a5752d5589718bed7811b14bc85c749be88abc208928e59dcc1b81316ab1f4dc156f6418931af9766b2ad97bfeebdbcfdea851f12d660f2b23c54f6948f7646d5345fb23e3e0b76ba8e9712a67e47e474a2cd959ec977228d8b2a9d56050563054d3bd91c8c1f56f7bce669e99da76fdebaa2cc51f4723b0fc7e8df15c68f8cdf4fd2bec79ea1677d218bf0fd63a1ac7930137ed72dee3a1ebef5d0bea69f13a6752ccd69690d2d0ee608e47a2363c58188453e058993dbe7a599a248657ec47e8bbdd7f35a787620f9991623100caac3e610c9d9381ede99fa823f777f7152494c22c25f4d560c914531ecb30b9744e17cbe6d20ae7699d2e0988be181ad960ec8989e6f77b1a09fbb4296ce62ea2b6a994f8bb03c8033868b8d1e09b107a16b81b1e856f53e59a27f696765706c9fb4342d70f1fc970dc453f6863a880b1cd9de2461dcb41b170f3bb6fef5d06198935ed8227bbeaaae37464db51a8b7bf5296c5c57713864635d3b8074b42f0f040d5f11e7eebdfe2b8f8e683d6a6a86ca1904a1ae21d15c1713aeb63cc2ed1d52d95945eb16c921753cd61ae470235f7ae120821a692a2099f24c692a2fd88716dc3730ebb1363ef4e7b2d74bf5340d9ccbf58ded6789c2e0dcbf98bdb43636f15c657d59a47f612c5a900870d88ea1771410c2599231d8d538e62c73bb809fb890b3b88301a7ac123fb374138787398dd5cc71d0dff65da1ba59e32f74bcab8fa79048eced1600292c426d264d582322fa5dceb956fb2cda2e6cfaae5caeeb3250e71365a985459184bb97348da501c3379a4aa91f044447a66d12f3df5178e3a9daa563c35c5c4d81f9acf7cc5c74b809d2e791fdf2548c85adb02352b7c64c62bad74b14e6edf1b2b15136688349df926c506566651e474921f0585d5bb2b766e4045ca4612c769a94b3bfb36868dd240f04abfada2d4b33e430900ea5653af9b5d56c88f337cd6655c6592edbaae1b37a5637b2c1264376eeb4227f6a4b9fae9659b0b0db556e00e2f0d1cca3924a9ca76b0ea3126c10addcc0c0f3b2161e59c4fa7d48842176ba42108400842106108423406e173fc4bc3d0e2947234b2f987c0ae812117163b241ccf0a534b470b60941ccc01b7eaba751b60631f980b1522365ad05cafa45c79f80709cf342e0d9e72218cdf6bee47b9754bc53d2c6391625c57478546fbd3d0b099728bdde4f780f7002fe252563ede7501a174991f4ddacb7b5dc5d606fe0755b4d6d456ceda5a78d91c50b32924580eb66a653d3c74ec6bbd5c4723af9626ea6de2792b6d95b4f13b30125b668d0bcf4f24f6d244ae64143188c5e4908d75f69de3e0a4926a3c1e03355486a2722e231a5cefbf21b6cb3a0904524f5958e178ec4306bde71dbdc16562358fa8a937a8692cbdc3873e696d5a5aacc6ea3107eb9e18efecc5dd1efebef4c86480f70cf57dedbbc00bfc15486a23cdf5c233d375a507aa18ecd708c91b9ef27b1a2f62d9891514d989df46b88f95d359855397b8c0d75f9b18eb11fe52b5e968e6232e78a78efa35cc23e6365a7161af75b4b81ae570cd63e0edd4dce1cc2b9810d5d39c999ef65fbcc7ee0782b31ba661faced04e07d54cc394b872b8fd21d5754ec2c48cb168bedba57e12e746d8df1df2fb242cef2369c6c6a6ad7d532482a69d8fb8cb33241dd78ebe1d55cc25d2618c34ec7be4a66ea18edda3c0f31656e4c38b661298aceca01b73b2b11504b107300223777873cbe08f34de3435979e3f55a88bb58f2fda1671bf8f50a9c7867aa98c472973a1d01eade5a2e9a2c3dd20682331039a5930f7345f29d0d92bc8738d972c2fad8442ef6f307b4f4ca2c41f30567c98619692268d1f15c34f50ed08f82e963a1746fcc4dcfdca6344d20656ec7753791538f4e46b30ccb032306cd8dc431bb922ca4a7a39226d2b466cd0f79f61a6e09f9347c5749261dda3f35ef9798e6520a020925c5c7c5133178d932b2a9d14663cf99cdb91979e7b8f9058cfa7ab9b1374ae75388c4b790651ad85aee3bf55d5ba12cbddce71e7e2a9d453cd20cf4d3cb1dcf79a40b3bc0dd6b8e5bac33c351428e00d0194cf8cc87d96bf693ad89fb95c392aea98f9602d99ac314f166fef23dadeedc29e089b67412442297a3dbf5720f03c8f929a43331adcd139e5bed5c5dcd23637e7a2d6fa61a7944b87cb87e273d1cbed43216077e901b1f82b861ecd85ebaae24c33b6ae76294ce0f686b498ed7cc39f911afb973d5601be5eeb0ede0bcfe7b665a73f24d56155d548c700372a4873cec05c6e02af8980db1573087365665beeba30c65c651de8c387e778202749424168636ee5a6eb422e46cb4f0fa78aa0e7005c2ad6fd5563d3120a77b22b4ad21529a46b242d6efccaec710a56084d8002cb89c40daadcd6f21651e10b3aaf35de6e9d0b6c92ee2d04852358ecb7013b753484a652dd95699c24372144f94b5f94a7c7a9d5398f8f65a32d6d95ca001cf19bdca199819a2645298de08d2c8bf28d31c36dcab8c98b2f2284c6d6b268407100a1616e5be8fc5f50a1085dd1a84210820842114c210840084211ec042109065f1162070bc0eaaa98ecaf6b0863ad7ca4f3f76ebc02a67a56d6545748fece491d95ac6b331036d2fcf9927995eafe96312347c3f1d1b1ed0eab9035daead68b1d078916f7af10a99cbeef9859ad75dad1d7fd545b77a6d849e3b5d755ea2281a5bda1ef171bb8f99fc955a898bd8f20b9ee7380019c87f5653d340f6657bc58e525d7d9aa9cef6c9188b2de3cd76b4123e3d4aa93ecf2ab2646369a06cb2169909790dd6c361755990b66a831c6d79793bc8db9f82bb151ba48a1fab01ac610c046fde256fe1987b5ac33cac1db13a9ebe2b2cb3d35c70db3a83871d2baef948681fa037e816f52f0f51464111b74dfbb6bad2a684b40b856dad0392e7bc96ba31e3910c140c67b0c0d0b423a668e42e9b1056a36e9a051bad3c611b03398b293b104dec9c1a4eea668296d5a883d5848eb91eceaa46520161a5b753c6148d68bdd54a8b8a30c703986a6c96ce7b85b40391eaa6cae16364e6b6d74f65a406217b100f8a56c0ddada153db9f2406d94ed5a47d9868cb6f9285cc17170ad1f6755138dce8105629cf035cdb8b059d3d383b0703b5c15aef1d06eabc8c17b91a730ab1cb559e586e33a31da31b1104f36bc8b903fae4acd3b993b1ac99fde370d91bcbfae8a8d6c869660f7026304090036ff304b25c07cd01690fb76a396becbc781e6bb31cb71c59636545591b7b60d7b72ccdd090747ebd3deb89c5e0f57ab9183417b80392ee67b57d26427eb98eeba82365cbf11091f474ef93db60b3ee2c7c3e2b2e7c778ed8f2e3bc5c562adb80554a0a874130d4d95bad2e9a4c8d17513294c76cc0dcaac32930d5658cdcd34e7abf58a7b377beab77012585c1c572f9c45139d7b80765af84d63f396b86516b9bf45332ef67e3a6d6315cc8a9dc2e2e4597250c5eb12ba422f73ba7e2b881aa95d63a6c14f8744e14a1d626e5195e861f2c95a79a9db288ee2e374fee3602e1ee2b2ea34a87b88d6e9eda8cd108c1372abc3adab7e57482a646c93ddba68a48f4199543dd71bf24f6cae203792dae2cee3d249662f364a348c9eaa070b39497fabb25ad2a5b8fa0c9dcd70b1d90a363492845c712afa2dbe9230e23593e45483d2361877980f8af24bab343472e215021886a773d1745c243f2af55ff00689867fc61f14bfed130cb7f7e3e2b8f8bd1ed74918789b7fd84f3e8eebb9cdff42cfe27f275bfed0f0c3ff9edf8a43e90b0d06ddbb7e2b903e8f6b81fefbfe851bb806bda7bb20fe147c47cdd81f489870ffce1f3513bd23e1e367dfdc571efe05c4c6cf69ff2950bf8371566cd69f884fe03793b07fa4ba31ec871f715049e93a2d990bcfb971cfe17c599ff00a707c8aacfc13138bdaa2974e89eb0fd97957b660588c988c0d95c2c08bd96bae7784aeda28c39a5a720d0f92e886ea17f4f20f4a1309718a76c925dcccee6b08b00d04346be79979d4f030d51963bbd911b37a17fff007f72ebb8e267e2dc513c8d734c2d96ed793a060dbe26e7deb9f7c6c918e738905a75e9e3fe8b197b757a86cb1134b4f0b06b62e7b8fdbfeb557b0dc159086cb50d01a2e5b98a58e95c5b1c9339cc606b6cd06c4e9fd68b6292211ebd8dedb9905cabb758a24de48e4a78da5ad66b9b5f056e0840635a45fde8730b45dd6b8d00b2b1047b03bae1caeddd8cd2789ba6aa760279f24300691e4a468d6fceca5a6d2c2d03456e317d9528c59de2ae40eeeeba1283580d20037174f173cc5826b48cc4dbe4acb0b3bd70341d53d2764630d86c148c8cb7a78292320b9baedcd482d9fc07cd578a7c91f64e360d4bd99b297b4630df96e892a5cf6d9a2eedf408d4296a00c25d64a4653aa048f1ab9876beca27ced27700f3516c69374e71d77519b6a3e09a6517dc14d73c03ba9b55a348f8a8de2e9c4dd0ee49a74c8c418d75fb404b482d72c786bbe8fab6c2f03b39886b1db8daf63e07ef5d1cf1878b5b559b5383b2ada63cb940d72f42ba38ab9b9645698fabbc3f30ece4370e07d9b722b3f1a73aa707abfab25cd8f3380e5975bfe2a6cd2d3034b54ced18f16bbb420ff5f7aad11719cc31bf2d9bddb806e0f23d46b65b59d69cbadb8ca18d81c65935ba91ce8279f2066dcd56af97d549899bdcb48fd1703623dca0a595cc01e371bae6b85fcab9b1eb2d35a6a285e18c040cdbe8a2c4cb30fa2b348ed243974e8157a3f58aa91eebdf5d0745571d99d255470ffc368bf99d5698f1f71a5d695227679005d4617246ca76c6ff00d3f92c6c32803acf705a33658c59ba68b3e5ce6f518e1c9e34625818a8cef80eb7b8589061d5314f96561165b54b8aba98d9c6edbdb55af4d514d5c761995e39e5269db8e38e5dc70959118aa1cd2a069ca6eb7f8a68453d4895a3bae5cf8049b0dd75617cb1639e3acb47125ce53988e41bab54d86c85b98b775ab1e181d10b8d5639f3632ea224df51874ecea8534b1f62f732db1422f7db0b757b748574bc10d071671fd90b9a5d3f028be2921f00bbf2f4b9edecb4ac68a767746ca6cade81329c5a9d9e4a45cd5b1b91bfa211d9b3f44272100ceca3fd009beaf11fb014a8404068a9ddbc63e0a376194a778c7c15b4206d0c14b1d3fb02c9f238b23739adcce0346f529ea0ac884d473445c5b9984661b8d374e8fb784632e63f10962a6699679e67385db68e3b93af8e89b05108ec1a33ccf1a9b5ec3aab8fa68e0ed672eed262ecad711c81e5e6a6a66ba3638bcd9d94171e7e01618475674ca6a1631dda4eeccf77b20f256c825c1a0680ed74ac89cf6dd8d25ced05b929678e38231133dafb649e6ab3f45c6874eeb7402eac44dcaf37e7f254f31748df05722ef3893b85c793b62db01df6f3520df9dd46d207bb652b1d7202951ed61bdc354accfa00069e29182e09b29a3eee88348c6caefb56f25288dc402d7dadbe895a01b0d94806d6d89d5390a9f1c4db9bb9f7b6f753b61bb8e67bac0724c880ed05c5b4d1587581b2b45decd646003627de539f339c067b9d1160403f8ea98e191da93e48f507ba6971cb63add4134ad61199b7bf3b292dad893645811622e3c5675a45791ad22f96de4a32ce87e2ac3d84ebc942f1a78a8519cd3f42344c170ef04e26c45b62aa22a2919adec9ed89b2b41b5cdac1d7fbd0e78b1040213e95c333b4d46edeab7e373f2fa65e2d877add248c3dd9d8deebadcc6d75c934bcb1ee9a3001616b85b563ad624782f47aa686e57017045c15ca62b465b24d246c69046702db91bae872c79c71244c6d7b191e51999da968fb2e76faf9827deb3da0c70bb30d16a71b47ea98e42630447240c732fcc6bf0e8a8c2e6cf00046a56796e48e6ca7fc9b3f079e48dee0362e541f7acc5a576f779b2d38da29229652346b4959983bc3b116079f69561bbba32fc5d1b299d4f0816546a64ef6a79aeb311a68c61c2569d32ae12ba7ef900ecb1bc7f3d31d7696a62394d8f8a30f9df048d901d8ea9d44ff00598483b96d82a4d97b294b4ad26375a7a18fa9a75989d2b314c3096eae02e0accc0f86c49387cc76e4ac6118935a0c4f3a11a2bf1d518a477666de2b0cf96e18e93cdad6d66a60869e3ecdad17f259f98c6c713cb653194cafbb8aab8856450c2e69206965c585cae5d3971cacbb60d7b9a6a1c47342a55731925bb79217b1861f1832c7caedd5d82ea780f5c4e5f20b96d5757c05fef396fd02edcbd17dbd960fee59e49ea383fb967929173360842100244a84008424402aad893cb30daa78dc42fb5bad95954b1805d84d4b40be68c8b787347d09ee3c8eb29b2e48c32edb875efa93e3ef53543435cd61d49d5e00d4e9b2252f7543ae4e48581ef246be0d03ccfc957a290d45517bee4b1a0b73789dff00ae8b2c5d3934a3bc10974966bdda5c72feb6f72a3533120b9da13a35a3752caf3248e68d5ad36d7eefebaaa9339a2606e0bada6ba04b3f4ae39d889c4b869a0d16843a81adfdcb3e23bdceeb4a8db617bae7b36ea9749dad3945f752b1aee8a58d99bc54e21006ea74af23597cb622da291ad7176847c139ac21a0e890bf2f85f44b47b4cc6bc9b5c1561ac9343dd040e8abc3212ed2fb2b8c7029c82d2b738d45ae9f792fb5d3c1bb77b2906526db78a7a4da8b33dc6c469e0354e7820920e9e4a50d1aebee4d24663d069ef4f45b41d99758dede364764e206bef53b9ed048bebd2ca23281d14d8a94c730b4779da289d18e4fdfaa98bc11751bac08279a9d2b6af90b77b79a6bfcb6565d1df505412312d16d4df2daff00727125b1899b7b34ebe5f9264cdd76d514b398dc47b4dd88e602d70f6c793d6d78cbda52b646b8653a86f4ea166d4319239ec76ae60f88b2943c40f7440dd9ed34fe0a17fd5d54529376116246d65d5f4e3fb79e71bda0f53c35c3398db9cb9edd6fece87ddafb973b4cc717003605741c6cf8e5e249dac2076766b9a46a1c00d6fd0eeb01af746eee858e56eb4e4cefcd631695b1e1ef887b4e02e7c2eb9fa72f6cec733da06eb72aa9a5ab85ee2dd6da2cda188c15604cdb596bc1678e976edd51c59cfc3045203ece8b8fab738ca4f52ba3aaa987b2686ed654df4b154c76035dd172932da7536a385d476776f31a853d6525e77386ce19828d946e825cc3656679096332eed163e495ce6fa698f2493555e89afed00bea16f8392205c7521615354363aad745ad24a2401a0f25c9cf2dca167f2ba873ea8318482b9eaf9e4a89083720157ea29e66c835ee15662c3632731db75585c38be545c7c63160a72757b50b5aabb28416b5a10b5feccb2ecf1cb191a6ba8e0436c52403a05cbae9781cdb167fee85e9e5e98fdbdaa9f5819e4a451536b4ecf252ae6ad821084008421002108400992c62589ec70d1ed20a7eeb86c738a2bcd7c94d86ca20869de5ae903439cf70df7bd85d4e594c676bc38f2ceea399a98fd599511bb66c87b4eaeb5c00b3e92511c06620137bbc36fd3453e2d5524af99d54f7fd777a42c6585af7be9b2ca15e200d7b8b5b1b43e591bb8d36683d7658e35d3634a5a88a91833127a8fd277f5f72861064b3df6cce3a954e9a69312a9cf2b406b4795b4dbef5a8230dbd85bc12ceaf086bb4985b45a54e7b388b88ccee97b2ce73b2ca1d6e4153c471e8e9d81a6239dda0697588d3729638ecf2cb4df93176d3bbb36164b2e978e33988f78518e258a3932cb1f6561de04dddf01f8ae465ae8eaa10da730c44eaf73e225ee3e079285b254c6c74991ee17fb1ddf8dd6be38b1f2c9e83163b4b338069d6f6ef1b2bbda40fc8e123751b037b2f2392b6b2290b8319d835d77b633988f152b31d39fb586b243a6a1cccb6f994784d2a7259edeb6c73001de02fc8e89cda8036bdfc579753f134ada81339ef98e5ef3ed9729f05bf47c50e99aded43838684dee0acae0d71e47771549ca6e745335e7202341ceeb9ba2c62399c5bb386965af14d98778efd165669b4ed7ccc41f6be0a1339648e39b436d141da37d9b9b7c552aba96444b9cfb36ca7755a8befa925d7b9b14d325fbd9bdc572957c46d86373da3211b667004fb9659e308c35c2a2476a7760cc07c2cb5c70db2cb3d3d10cf1b058bc03d364d1571bc96b48b8f1dd79b338b2391c4b1fddbf77b588d94551c4b276cecd1cd2bcdcb5d4f206c64fbb92d271b1bc95e8cec629630eed1f92c6c4393a3c469aa4598fbdf6e575e7031692a035ee8a36c839b63d2c7f48df53ee5353639350ca0981ae85c7eb1a366f8b53feb85fd95de4c05b3059938733bcd24106f70aec35b154c4dec9e1ed237b5912c39d841d8ac6cf1ad65f28a705597b6cfb1b00e6387cd5a71192cc680dbd80373a7b96130cb154bdadbb4b08363fd6c412174107723cce77749bdafbf9ae99e9c997b79ef1944c38ed498bd9cf702f73b0bdc725cfc71fd6016baeab1d7498862334ae7cae6b490d6c9a65f21d162414ef3883465397f158678d976e6cf1d5db4e0a6cb112597002a15786c753ac4def2ec20a68df4996dad965430b28ebaf2fb04aae3ca5de27e534e427a2969df964691d6ea58ac1a0f33a2dfe2baca3f550c8b2994dac42e618e7102cb3cf1fd30deba597305940e68254f1dc8ef20b35594ba67e99553138480b47356daf2d6349292ae4ecdc05b9a4aa6de9b333a5d6fbf2936ece2d59b6cd186d6c1a817b26cc1d4d76958983e23253ce1ae26cba0ae736683b46f9acf9b0d5d36cf59e3b8c4ab7677142490e637e485a637534e0b32db6bdeba2e0a36c60f8b47deb9de6b7f834db191e2dfc57a997a5fdbdba97fc3b3c94ca1a4ff000ecf2532e6ad82108400847bd25c75402a44666f50933b4e9990039ed8d8e7b8d9ad0493e0be7cc66a712188b64a2aa923eedc906c3537d4735eef8cbf26075ef076a693fed2bc85b4d1bccd7d4edee0b9f9af71e87f0bd655054553ea29e39ac1cf6b3bd93673b99b7458588c72c95629a26164313039c6db972e9994a3b26002ed1a2a13c6e15ee6308cacb124ec0f41eefbd4f1dddd0e6924d9d8552b98082497bbbce279ad47dc5baa8b0f746c9adbdb72772add68680246816b6e967f90c27c5858acdd9b1ad6b8f684fd93aac6a682492a33ba105c4ece199695561c66a96481ed7b5c3a0007bd5d6b1987c2647b086b06a5add156f50a4dd59a3a0ee82e8a21fba1697d1f405bfda032c46e5d65e7b89716d7d4ca63c38ba08af60e68ef3bf259788619890a68aaea5d2cbda6f7b92d4e616fba57298fa9b7a156615c36c7e7fa4a9e99f6b02d9da0fdeb9fabe1fc1daf2e67114563b8be87dfb2e4e096a2285f4cc86321f7b9740d73f516b071171ee5dd70fe078e5360b0cd0d437eb6ee7d254b41665f3e46caae1a9bd94e4b6eb4869f8483e1cf057473308d1cd20843b872ae905db20201beeadbe9e8993b5cc63b09ab20122273723bff00895759362023cb213383b3e368d7dcb2bbfdb79d455c29d253ce1a5adcdd775da51f79a2f7f82e47b32250e7372b81d42eb70c6e68dbcf459e4bc565cc75c902de4b9cc766744d706bb70bab9223d9920ae57178def9886b3391a35a799512f677d3903875756e62c84bae75711f8a6bf85a62c0e9aa22880fd37582e8dd1628d8dac631b1b9e6cc01b9dee3cf2b42c8670d7d3729f5892560b1bcb2bf33dc7c1bb342de4b59e5668986708504d265389d348ee6c64a1c7ef5d1d3706d1d380637c874def75e5f5187d4e1b25453494ec7c8d25ae748d24b3f775b6bd53f01a1c56b3148e928aa6685fa97bd8f23201b9365b787eab9ff00b3f78bd2a7e1c60710d95f94ebad9529b048e9584df30e4f24fc2cb0ea310e2ac02611cd55eb911f64c8dcff001e616c60fc49062b786a29cc7381a8e47c9657ca3592647e19533c1581c1c08ce2edd801b6cbb48835cdb8dbee5c754d1c82673e17800e970775d3e079cd13592c8d73801a8163ef4b2bb9b2c66ae94f15a66b6291e3578617007423c94d853c9c3cba591a4b05c9be96e6ae5742c998e88db51efb2e7ab21acfa3628e397b239bbd61a9005ade4afcb58231e3f2e4d1b8fc9475ada7aea3b5a5058e23675b62a8515346242f2068a6ae8cd2e15431968b667107dc1328487316732bfd7b61fcbc663c9662d481a00b82b3317a7ed2225bbabf1bf2b5549a60f2585678df938de7f5ed91d524124e53656a18dac8aeee8b5717c3435cf99a37d56455c9929881e4af937b988baf6ab355904966d75630e99d565cdb6a150647da59aafd033d49ce7f275ade0aae18eb4261e53754716972481bcc14b4f389e220f4b29315804f52e70dc0505245d9bec557c7c1ae32cc750e30363901dacba0a00c9a972937d2cb0b10616341053f09ac744ec8e26c8d5b375bf163e3d53eb00827732c84fc4d81e7b406e84492a33b65d350eeaf611881c32b9b519730d885454f45492d7543608bda3d792f4eb99e994de9330e8e9dad707823f64a90fa4dc37abbf84ae669fd1d574d1079a8b03d1aa5ff0066b59fac3bf842c6f8af79375fe93a846c5e7fca542ff49f4dca390f935639f46d59ca777f0a4ff66f59ff0019dfc28f88de4d07fa501f629de7cc2ad27a4ea83a3299df10ab9f4715c3698ff0289de8ef1217b480ff00911f12f92493d24622ef66103cdcb6b84389310c66b241396b5ad22c015ce3b803151f69bef695bfc1d8057e0f5b27ac3416b88208ba2f8fd0f93d02be03538554d3dae6585edf882bcae068736479dcb03bde57af47ecb5796d5d38a5aba9a768d1b2be31e41e6df25c7cf3d57a3fc4cb5e510471da26c62c0db9ac4a86c9f4816e66ddd1977bc1b03f2f92daa801add09079ac9ab045548c73c070005c1f646e7f059f15f936e69bc5350c51cdb38e9cf6d55e7d24e5966491bc7478b5d51a36e502c085a40bc00d69ef1f804b2bd8c674a670faaa691c6385a58ede3cc0b4fe2151adc32bf12021100820bddc1d21ef780e8174d18ec9999ee2e3ccb93acdacbe705ac3cc6e54f92a62e7a97862929ded9990373dbeb23dec7a8ea1598a96474e590cd935bb9ae1bf805a13e190c643a9e29daf1b3a390dfe04ace92796071ed68ea1e76be4d154c8fc1a1143561e0763496cf7b924696b724d31d430bbb6981b5ac1a2c34f15423adac9059946f7349ddc2c54cd82aa520c905c5fd9cc7ef4ee5697848b2e9a91e045240d7b7f448d94d0be8e11962bc41c756b5ba2af161f2b2fdd318e6dcf7ba7be9db10ccedc6de0a2e4731fd2ad58649503236c39adec3465634740b14b73480db5e765bd879007bb52a6ae74bf20bc4483c962f671fade791b982da7690bbc965b19da3ca8f555adc56a86d31973b8e791ba0b721e0aab5d46e3d9b06404dcf9ab72d338bafbf92a736182469cae730f22dd2cb6c6f5d32b35ec938aa0e3d847054b48d0482dadd3db4f5fdb9ece929e0cc066c8edfaf254c51e2f4ee0e82565401b35c3291ef5663aec5468ec327cdcc8734fe2abcec2984bdc4cfc265a884b260d6b8e879dd60cbc2d1d054b6784b83ef98b1b7208f2e4ba58dd894e7bd1181a77326a7e4ae43419012f719731bf6975173578698f038be26865310eb6872e83e0b5289950d8c8632403997b6cafb202c1763ee10e73e317b5be6a7cbe8bc7ed42ba199d1e66101ec19add6dafe0a29621360cf9626dc452024f406e3efb2b95151923edaf6ca75f25152b03e8268daecd1c80dafcac6ff82d77f04633fe4723c5938a7a6a08767657388f82ad85ca1e2c08d028fd24bcc18c51b6d669a5b8fe23fe8b2701af05e1a4ebb257f091e7ff002b2df2d75a1ddc59552e709090b4dbab5412d28702563ab6f4e5f6c0c4ab5c6231df75ced5bc9631bbddcafe3cf314e5a0ecb19d3660dbf25d18636f747be96e381ed667e654d0d5b1cf31ee02aeead7180b582d61baab46c2d7e771df75525d5b5b4b319a74b4d867acb5cf3d345ced6e6a798b0e85a6c576182ced7c392e0ac5e28c2a4edc54c4dd0e8e0b3c32933d554ba65544a65801df451523c0209dc2b94348678bb376e1327c39f4af04ec56fb9e9befbdc5eec84d01b6b742930b70c8587dc85865b97a196132bb5c2b67853fdf6cf258a4eab67857fdf91f915eb5f4e17b950b47aa3341b2b161d157a03fd919e4acae6ad8961d1194744a8402656f408cade812a100dc8cfd1093b265ef9427a1002f36c505b88ab63e950e3af8d8fe2bd2579c63f6838b2b093606469f8b1ab1e5f4e9fe35f9562d5973deed6d73a28a5a2fa873c0fad75c971e5756663966b9d6e55b9809a07e96e6b9b1e9df9fad33299b95a02bf4e01ef11b2cf6bac6d6b7257a1d40603a6e555f6cf1f4b1acdabb460d875538d080146d56616f3b25a54f6963612c1b5f9a53167e5af92961603c9590c3626c3453635db3fd54ee7927886cdd4eaae38372ff005aaaee3671b04b41090d6f2b79aa157675edb81b2bb2925c492a94ae00944856a189a2de2b5a8b4002ce8185da81a2d4a38ec754ea655c703d9b80e8b3da32dcad42cbb6ea8c91104a8b17288ac6c1c2e9e69c3af6d9246c3e6a56f8a25166d5db4f92e4809cd8c1e7b1d0ab400237095b103ab404c690b58e1a873b5533411cbe09c18010db6c9f908d91a1d22734381b12d77551dcbaed70d6df14e9039b73751b882c37dc6c889aa734625bc5b8702a3c2278e405b1ec068a781d7ad638dac013aaa9850cb3bac32b6e6de4aadeb458c9bb5c37a51933e3748d2758e96dff0051fc972787ca60a90efb2ba1f4a2f238969edf6a8da7fea70fc17334c2ec1e216f27c63c9fe477c963bca0c4e3a8840bea9b8962d1d2c2e25c01b68b95c2e6742f22e556c767924999727290b2f0be71cddaad7d6baae673ceb98dd5788173ad6ba841d574382d1c52b733f6b2e9bf19d17e2cb2cb3437aea536571b6566eaf6271b2390960d392cea18dd3d50e62ea31bb9e5578df2bb74dc374d2019de6d65b789c6d9a91cd6d8922ca935cda1c3c6b636b92a850e2cf9267894f75c6c171c973b7269aacac344f157ba17722b52b5bdd6e7f24b51086d489d8351d12d54ad9e20085be596e3597e3b6735ae8240e6ec79212f6a41b1d4212dd18f2f4d03badae13ff7ec7fba5629dd6bf0b3b2e3d0f915ebdf4e57ba61ff00e119e4acaad87eb48cf25657356c1084200421220150842005e7dc6f46f8b1b6cec06d5310703fb4cd08f816af415cef19d1f6f8336a5a3bd492079fdd3a3bef07dca3926f16bc3978e71c54645444d7b9badadaa7533bb3748c70201dae9cc0e0deef2d42ad52f9642031baae4b74f464dab4ba4a46e0156a9dddcbf554e627390ed0a9e2758009d445e63f33b4d82bf0bb601674255d8dfa78a56b5c234a120380e4a774a1ad3aea3aace64c40ba492b08e6344b6bf1dad4b26522e4595492a58d3a15995588e67886305ef77205490b0b1b779d52eceea1f5130e6ed5c6c354f7c01d16bb959d8892d9e1901b35bba9c627086805e2e7455ad32bdadd3598dcab5295b6f1586ca9619058ad9a2aa8c0b13a9d884092b4ae00b6d75567b65239d9486a19604117e6aacd3b491b0ea951a31cd7b181cd75947156ea5af1cf7534b334d39d792a0da77881a5eeef7829d2e5d7b6b32463ec5a54c0df9dacb12298b1d6370afc5387106e3cac85b40680e8909198780dfaa844fa25320736fb146d3a24a46c79aa933ada295d29b9be9d3c55399e49d10567648817cce683625a403e69d045ea71c9da6fb0b0dd328cb5b524bb5d3457aa2312be23b11bf9f245462f1ef4973769c5c22e74f4b130dba905dffc962d18bc43c11c4d880c5389f10ad69bb1f396b0f56b7badf900929496c375dbaf8c8f2392f9725a7f6bd84c4f8a82bea04f181cc1dd4551297487cd577c976d92c71fb64647de7aea28088a8cb81e4b9665f368b6e91cf34e6e4dba279cda72c76af894f9ae2eadf0cd2932768e1dd1aacf7c0ea8a9ca0dc5f55d55240da1c3ae45890b9b97298e1e33ecf1c74c6e24c4dc241046ef359b4f57a582ad5f319eb2479eb60a0617070cbbae9e3e293091b63969d6525707c403fc93cb733c060bacaa58677c60e470bf82eaf08c380803e4373b95cb9592a6e537f1614b4f91fde16050b62be06544d919b379842cf706948eab53869d6c720596775a5c3a6d8e537995eddf4cdef186ff00836792b4a9e186f44cf25717356c108488054210801084200514d032a20920945e391a58e1d4116522101e5f2412d055494731bba079613d7a1f78b14d7375056d719513a9b126d701f5552d0d71e8f02df316f82c07540630dcf2e6b8b39aba7abc77cb19546a1f9e6939006c88ddd4f928ad23a213bdb6654173e227ed341cb7f8829c1d6b782349fb5e8e417d0ea1598e5df559f1105da9dd4f72d360764ab5c570d5860d566d5e22f7bc4508cd23f403f155ab6a26f618097b8d805670ca36d392f97bf3b85dcefc9131565c9a4d454feaad25e73caff69dfd72568bee35d8263cbae05b4d944490ecce207883babd32b454c7eb2c2d246a35bf45c7e27c392544ddfac9045b86daf6f7aec1c1fa8692db9d54523592b1d1b80cdc8db5553a4ddfd394a1a9aac21a20a99dd2c00f7243a96f82e82931d68b7d635c3910555a8a07c9196c915da46e173cfe1ec43b72fa27b80e9cbe08b8ca78e5af6eece380b6ec1e66eb1f16c5b1fa98c8c229c35b7b191d6cc7f741fbd57c1702c42394495ce74a7903ecb7dcbaa8612c1ec826fcd4f8c9ecee76de953026e30f8187140186d7cb704fbecb62471234be890b9ad6816ca6e9ce3a10479d94d54b6fb5398168ce2e6c9d054022f99493b730161ef58d5323a8a60e7e91b8ee3929d2b1c9d0327240be9e6a6ed09e6b161ac0e1ed7bd5c6cb980d5458d36b4f933102ea079bbae99dae6d1293a2719e54fa704cde1649c4d883f0ce1bafad67b7142433c1ceee83f1374ea4204fb5eedfc563fa40ad6c5c293d383deaa919181e01d98ffdbf34e7e519dbe385af180c2c70d345a31bdada70527641cd3a6a15621c0e4e45775af235aec8c699e72c1ccabcec1beabb4cea289ad86f20df92926af7ba3c97b059f95df4c6ded4df108da7cd3db5c6288b1a77164c0eedaed0aacd1ba37d9cb4c66fdaf1b5d2f0fd1198891c2fcc95a38d54362a62c053703aa822c2f3170072ae6b14c4dd5552eca4e40e5c338f2e4e5bff008711d2e13595f3110c4e75cef65d9e09c10c84366ac01cededc824e19c76860a3c8ecad75b9a9710e358d99a3845fc96bc99e79750f53edb15430da28b210d690b066c560327671cb66f40b94c471a9eba426e402a846f904ad249f8a53f8f6cf953dc77d1bda59dc3ed2143834466858e22c07542e4b8ddf49b976ac775a180bb2e354c7f69679d15dc18db17a6fdf5f41535ef5851bd133c95de4a860e6f42cf257d73d6c44a848902a108400842440084210115552c15b4cfa6a9884913c59cd2b9697d1f504b382faeab105f584168cc3a66b5edf35d7242a6e32ddd5e39e78fe35e7dc79470d1cf863608db142c81f1318d1a34348b0f9ae5c1d00e6bb7f48d01750d054df48e67467fcc2fff00c570c4dec3a15867f93af8aef089e224bacaec71dc5c778fdca9406d2782d2a6cae97c3aacaba31acf969e415b1badddd4edcd5c748d8c026c79792d09a983c6fe20aa35345db348ed0c771b84e54e484d531ed23527c12c7331e72bc1f0f15cc62c31ac1334ec61aa8473b6b6f158f171d5466b3e883add1e6e3e4b598db3719e5949ede88d78d438826da152c41ae69234d7e2b90a3e308e68c17524a2e796baad8838a2849cb2b648ddb9cedb1535a49b6e08f30b23d58dc9236d9329311a4a9178e5611e7aabe1d15ad7ba7bd1785578e36dedb752a63100435a74f04e7186db81e698d9626db23c123c54da7313f2106e4dcec025ececfb7250f6c1a4dcdfadcaab5d8cd151459eaab2181bc9cf7017f21cd49ef4b4e2c693bf82a954c8aa637425a1d7f6964b31b93199bb3c229a592222deb52b4b183c86e55f868eae99dfda26ed8f321b96c8d6bd96f6c9a16cd4b53252b8e6119ee93cc15bb0b498c69af825f546b9e24cba9d1586c223673f7aced6b3d22603988f14f3b592968cd9b64c26e2e9c4d55acc569b096367a99446d71201ebe4b83c731e7e393ba5735cc8630442c3c8753e255ef48138924a6a626c6369937e64dbf05ce5296184e676cb493536f3ff0091cb6fc3e95a270b1baa934ad12687656aa488da72f3541b1f68edd6d877dd725c8e33193468d9123488412752acc74cd63012555a89db9c8b68340af1d5bd2a691432981f988bd915138988b0b5944e7663b590d6971b05aebec69347572c71f66d71b2815a8e99a05dc6e99244d69b8532e3be8b70b4e4b4137b256133bed7b04a1a3b22d6dee558a2c3ea2a0da28cdbaa9b67b1f8f753c546c2d53434b0b240e92c00eaad478356b1ba9546aa8a78a4fae79b5d73e57775b2f3dba7a7c568e9200d6b86810b8fa821b0d813ba13c38b705c23a32ad616e0dc4e98ff00ee0550deea482530d4325b7b0e057a753a7bfe0a6f42df25a2bcf309f48187d351b592c9675b6b1570fa4bc2f9497f715cf71ad658ee122e14fa4cc3793cfc0a8dde932807371f2052f1a3ca3bf48bcedde93e907b2d7ff0950bbd2843ca279ff2a7e347947a55c75485edfd20bcb26f49ce20f674f27c82a137a47c424f622b79b91e14bca3d83b58ef6cc9e0af2ee13e24c4317c45e2a1e0345b40bd3e2fee9be495c7472ece48764a848dce71c53f6fc2d50ee703992b7dc6c7e44af3169b7e2bd9b12a415d87cd48ff006276963adbd8af16904904ae8a56e5918e2c78b6c41b15cfcb3b75f05f8d8b10bbbd6f15a94efb10458eb658314d620bb45ab4920d2c2fcd64e98e8a26e78ec4a64ac0d245882a1a29c9ee8055894976c361ad929ed597a537465ce21c2ed772b2e7b11e0fa47d43aae918d8a522c5b6d1dfeaba76ddc4dcaaf3b8b5d97c375732b2f48b258c8a5c3290039a10c7b6563c823a5974188f0ce1f5869ea8b5b70eb3c586bcc2a658d90836bbadba9a3a8a98c88db21cade44dc2d3ca56738efd5453705513b10a79609248a1918eed18c36ef0d8a59b856ba1a695d435cf718c660d95df8ad6a7ae9864ced690d24dc7885646205b1480465d76100036ba2d87ae58c88383a4908f58c46537e4dd0050c7c1318a5b54564d24c41ef66cb6e9a05b8313949b989addb4cf755a5abab99c407e51fb2149f8f25fbd30ea385a828e963ed257caf0e697173c927995850f04c357888acaaef35ba323234b5c917ea5768da6b9ccfd7c4a5191a728d82573d7a5787eeecc86861a681b1c400cbe09e232e04bc5d5802e335ae90ecb3daac37b301a34513881ba91ceca6dcacabbde0e86e566d65e91bf4690a071e5b5d3e6758650b1f1ec4c61983d455e6b3d832477e6f3a0fcfdcae32ceebb705c5d5beb78a544ad3dd0eecdbe4dd16451b9ee6d8936495b522a1cd68d4f356618c4705fa85d17ac34f1adf2b6d55aa9333b2857306c1ea3129f2c62cd1b9296830e7564e5c46975dd70fd34744d23281a272cf4cb7b7318d60b261749dabce9b2e41c4971257a1f19548a96b211a81de2179fccdc9210ab8acdd91785ed1a730d92004a9190b88badad5dd1cd909215b8d8d736ee1af25563600fb15a5470f6cf161cec1639dfd0924eda584e05eb6f04d8379aeaa0a482922c8c6816e6aa50c6ea58031a6c7994f754dc6eb1b96fd30cf2b4b512068365c7f104eeed3c2eba5a89846c2f7bb92e2f16ab1535272ec14f163bcf6309ba63ed352b729d46a850d2939c8e485d36f85d3a2eabd2dbc1d8c3b689a3deac33817167ef907b8af6b104636604eecd83ec85af9e43c63c659e8f71476ef68ff2953b7d1cd6f399d7fdc5ebf91bd0250d6f447953f18f211e8dab0ef33bf853ff00d9ad55f59dfee685eb761d12587447951e31e4dfecd67ff8f20ff28487d1b54729dff05eb761d1161d023ca8d47903bd1b558da771ff00284d3e8e6b40fef5dfc2bd86c3a04587408f2a3c63cd386784eb707c40c8f7666badf66cbd222bf64dbef64ecade812a56ece4d112a44a90472b991c6f7c8e6b236b49739c6c1a39925789e378be1b8ce335b5784b9d253894333ed9dd61770f02ae7a66e357768785f0f98b4340756bda7da2756c7e5ccf981d550c27016d0fa3ea1aa2d2d9a790d44aee795fdd6fc834ff0098a2f1f9636fe9a71e7e3948ce6bcc6e208e775a349520383b368392c890b81b38f780d396814b0cd908702083e2b96c76caeba9273a39aedd6a47267617755c952566a00d0dfe0b7e927cccb12a2b4956dc75b8e6abcadcc6dbe8a627336c34511d4efef441547b57c136c483cd5886b1addec45f9a74ac6bd8096dc7350369db9acc6f8837569f4d28eb610def7c9586d653bdbdd783cb4596c8356b43868360137d5dad7dc388bee06c96d5db5454c4ddc823c10eac8eddc68f0bacf8e9ec0963c90473533586df85b54ad1ba9fd65ee71174f886601d651c71343b6b9e6adb1ba7e4a6d393f69186de491c05ae92e41d123cdf452ad2bcb20b697549f2917f052cf28b10392a32bc16df7772d522a49660dd6e355e7fc71889acad650c0f261a6d5e793a43bfc36f8aeaabab044ecad78ce4116decb92c6e9e060ed1b6b9dfcd6d87576e0fe472ff00f11cfd353f7b3b868ad3dfda3c44cd6fa26098362206e12e0fdfae05dc96b65bdd7166eb70aa56d2d202e1a90afc5501ac7596656d76463228c6cdd55ec2207cf0873c6eb197796a3297bd30b1594cb54fbf2165cbd63476c6cbafc770d96173e5634969d4ae4a56de4d55f0cb8dbb563d53608f36e15b7b0363b04e822cadba494f25572de46a8058dfaaea300a36ba212bb4b0bae79b0173b6d16d0ac741406184f788b2ab65e9797ad3526ae632e33e9e6b36a31c86006ceb9f05cdd44d519c891eef8aac4df7463c3f76a271feda95f8dcf57768eeb7ef596841042de6331f4d24916a885e42109f87817250b9b96fc8df5e2108e6b7504895080122364c7cac60bb9d6403d2acaaac7e86901ed2768b78ac89f8ff000984dbd6184fef0415b1d5a55c60f48d85df499bfc4aed2f1be1750401336e7c427aa371d2a155a6c4a96a85e395a7dead0b1d4246161f17f10c5c2dc35578ac995cf8c658187edc87468f8ea7c015b4f7b636b9ef706b5a2ee24d801d4af9dfd2971b0e29c6852d0c99b0ca1244446d2bfed3fcb90f0f34e4dd1b71f18aac6f19636490c9555b50039eed4b9ef76e7de57d1b361d04b861a16b6d008bb00072681616f2b05e07c0f18938d709045c0a86bbe1afe0be8aa5f66c576f1e33c2b2b7e4f1cc56964a0ab9639459f13887f89ebefd0aa11ca1a4b1db01f15e8fc77c3e25886290b74003261e1f64fe0bcdea6174521201cdcd79d9e1e3969e8e197963e517a09ef6ea16f50549700d71b0e4b92a79f2ea6fa2d5a0aa2d78693aac32c74d65dbb18dc2c004fcb7f154292a848d0ce6afb6dcb6096972985b61b6a13035a1da36c4f3e6aec6d0fd48dc28dcd6b5c000090a76ad7da23138fb0741d53bb073858375eb656621636b0d06aac3037283641c528e99fa10092ac085cd1adefcd5a1e02c96e6f6e490578e237d76dd481b97a8ba940dc9b209cad161748f66869df928dfae84696dd3cec7525579e4019a9dc249529ece2fca46dc97398f639060d442a5eccf217658623f69c37bf80d2fe6b5659dc64314409276f05c3fa47706c9864003acd89efcc46849758ebfe5fb96bc586f2ed97367e38db18231e9e69df34eebc8f75c902c3dca2adc40d447baca45d74ff563bdbcb4ec98ecb470c8dcea8bb79ac75b18154e49ecee49e73a1674e89f4cf74ad6bdba95d86194823a46e9b05ced39f58ad601a80175b01ece26b765cbc38cddac71f6cec55b13299e2402d6b1baf2c9a179ad7b5a0968768bd2b889f7a72d0eb66d172b2b60a789ce205ecb6b7576bfb54a7a50e837d42cea96f672917492623235e729b02546e265bb895131b3ba726d3c7334308b6b648c908782abc4d70dd48744f52516998886bbbc150566a9d7d028e2a77486cb7c6eb1ece59276894ed687457539a16b5b726e55ea2c27d606aeb05197263a12f95d467531c9f142ddfa3e969477de2fe08585e496fa5eb4fa812a8e19993c61ec20829eba8c7242153c42b1b494ee7b8d801741a0c5b1aa7c329dd2cb235a1a3995e7188717e278e551a6c2c16b2f6ed0858bc498ed463f8a7630b89843b2b45fda3d577dc1dc35152d2b1ef60bdae4db72af5aeeb3deeea32f0ce0592bad36233493b8ef9ce9f05d2537046170b00ec19fc21748d6b582cd1609ca76ad473153c0d854cd23d5dbf05c6e3fe8edf4ad74d871208d721d8fe4bd6532589b2b0b5c2f74a5b06a3c0a8f1ac5306a82ced1ed319b3a39392f5be12c726c5a863925616b9c362b98e3ce161265ada6601231c03ec3da6a8319e34a4e05e1e6d3536597189e3faa8b710823db7fe039f92bb7ca2663aaa7e98f8ddcc69e17c3a6b39c01ae918761ca3f7ee7dc3aaf1871ba9aa2a25aa9e49e791d2cb2b8be47b8dcb9c4dc92540ae4d1dad6e16a934bc5385cd7b06d54773e05c015f48d39b3c85f2d31ee8e46bda6ce69041e857d3382d73712c2e8ebd9b54c2d93c891723e2ba786fc6c6797b6c18e39a17c32b43e39016b9a7982bcbb8a7871f855618c0cf0486f13cf31d3cc2f51614caea0a6c528df4954ccd1bb63cda7a8f158f371f946fc5c9e17ff001e073c062974b5b707aa7d34d675f7b6e7a2e8f89f866a30a9cb5e33c6ed59201a3c7e1e4b98918e8dcf034b1dbaae1b2ceabb7df71d1e1d57da00090483a2e8619065bdc5bcd70b4d50f64b6171e17d9743435c0b43738b8b9b1e6a32c74ac6ba2865b0d77dc2697973ef7d6eaab2a1a6306e0bb925eded6cda75b2cda5ad282c49274becad30b7502da2c8a7ac8dadb077786de2adc752dcd9b3dc1dd2a72ed7da1aef64d923dc4bb4034549f3d81c8eb1ea374e6cf985c937b6d7d1252d977229ae7e97be835b2ae6a034dc926ea37d4b5cdebe1d5209dd2743a0d564e235995a40be626c00e6a5aaac0d61caeb2a714665944ef36b6c1549f68ca9f041d95dc7571dca67a42e1e8ab7d16d3d731a3d670c93b5b81a963df670f9b4ff00955960b1d415d454527acf046334075cd4b3305fc586cba7826f75cdfc9fc647cbc853081ae60735de6135d0386daae8d570ed1ab34ae11bb35f75098de3eca035c0b74214e537346ef386ea5a64bbcea0684ae82b717869da0e70345e731d6ba960195c4395597109ea0fd64848e8b978f1ca319e9d06278ebab6a6cc2723741e2b32a277cdddd6c9b454e64372ad4d1066802bb85f634cf344d7589f355aa4f6560d3badb8a8dcf84bb524aa15540d8d86490edd54e396afc9a6397d4370f709486b82bb518710039a0eaa1c218c74a0f25d0564f1b23cad01ceb7c1659efcfa171fdb92969246bc661a2b11c6182c1599b3c8e25dcba28ad65573b66ab9f3bbba2117b04b255494b17d59dd35c6ce5154bb3476e88c66ec18dd533d624a804b8dcf342aa242c69039a16fe1fa6ddbdff8278abd6a36c133ad237422ebd018f0f68703a15f3b61b5d261f5d1d430db29d7c42f71e1dc45b5b44c20deed042df39a5e37e9b4b80f48f8b3a970d3046eb3e63905b90e6bbd7e8c2bc83d25cc5d89d3c67601c54633762b2bd31b84e9454e34cb8b88c5fdebdc686210d231a0725e35c08e68c61e09d4b45be2bdaa13785a7c15e7ed381e8084285952ac4e21e2cc1b85e93d6315aa11170fab85bde924fdd6fe3b78af19e2bf4bb8d6399e970c270ba23a7d5baf33c78bf9790f8944c6d2b5e91c73c7981f0ec13531959598965223a48fbd95dc8bcecd03a6ebe7aaba89ab6aa4aaa991d2cd2b8b9ef77325479ae49bea7524f348b6c71d15bb34b41d937294f485569262f6cf4458b0ace1a7d0b9d7928652d039e476a3e79978b5aebb2f4598c7d15c5cca67bad15730c275d03b769f88b7bd5f15d65a197a7be354a0a899ab6ea40b4a223aca3a7c4295d4d55187c6eebcbc42f28e2de14a9c0e5edc0ed28dc49130fb27a397ae593658d9344e8a68db246f16731c2e0858e7c7326d867717cfbde6bb721c362acc151235c0dc070d8f22baae2be009b0f2facc19ae9a98ea6989bba3fddea3c1716d734b8c7a348362d3b85c796171baaea994cbb8e820c62cd19bbafe40ec55b188891b99aed6dadce8b9bb92d03768d9a126776a6273858dec1ca3fae5f4d3c9d4c75cd74760411b792b14f5a00b3e41d005c67ae165ed23c3af73a5934e2794dc3a6b8bd8e897f52a64ee9b5967db30ca77d129c4032fa80d3b0bae23e9725b6cd21b6f7235437112f7f743c9e77b9b25fd53f63cabb3fa5010ebe9ae80aac7152e36dc83b02b9c8e492475b31ef1f795af454ce195a45ddbf923c7185bad0883e73ded4f2f05a713088c1b0b9e4a3a5a70c04937fb95b02c3c16795e8e4ed1bef1d34d265be48dc6de365ddd052de82a2075aefbb0e9fb365c44b97b5a3a675cfacd545191e19813f2057a2528fa92efd27b8fccae9e09ac2b9bf937d3e426130bcb1c2c412084e712d3d4722b5b8de80e17c6d8cd206e56b6adee68e8d71ccdf910b2227070c8e3a15d51c67072703aa88b5cd759381b734c929c8e1622e12763107020bbcae9330d519829f184d4a4c420862c8f8df7fd216296a2ae1905e37389e84596567b146645c25163aac2eaa07c218e91a1dd090136be8d9564b00eef8735cc67b8b14f64cf8cf724737c8d9617f8fbbbd9ce977110ec31a23887d611bf409282aa491b964249dee5557d43e5fef1e5ff00bc6e9d14c62f65a12fe9ba196eafbb572616a885702417c76f1054c668a46f75e2fd0ae5cb8b3c6fa639635049604954a77f20ac4efb02a966b9d79ad78f1d9c867665daa158cbddd10baee27e75d2f35e91e8e7142ea614ee76b13b2fbb92f3cab8bb1ab9a223563cb7e6b7f81aa8c18c98f368f6dfe0af2ee2b7abb7b7b8e68afd42f1ff004931918ac0fe45ae1f35ebb4ceed299a7c179efa4bc2dd2510aa636e6176636e9cd618f5946b97a709c3d5bea38bc5213604e52bdd309aa6d551b1c1d7d17ced2c8ca6687cd208c1d5a3ed1f20b4e2f4a58d61d462970c10c3616ede56677fb81d07cd6d94dfa463d57bf55d5535052beaab2a22a7823177c92b835ad1e65793f16fa68b17d1f0b4773b1ae999a7f91877f377c179762fc418a639376b8a6235156f06e04afbb5be4dd87b82cd7389e764a61fb56d66b6b6a6baa9f555b512d44ef377cb2bcb9c7de5552ee884815915bb20e8866c10744112fd117d10522001a68a5827929aa23a885c5b244f0f6387220dc28909c0fa8387b148f19c1293128ec054c41e403ec9fb43dc6e16a0d97957a1ac78cb475781cafef539ede0bfe813670f71b1ff00315eaad3a2d6f7d8c7f4508b212a4a46f68734b5c342b87e28e0ea0c4e5749ad3557d89d837f070e6177645d54aca6151196db51b2352f54f1b65e9e1d88e195d834bd9e211111ecca966b1bbdfcbdeaabef7df2b8ecf0742bd76782d9a19d81cc3a39ae17042e4715e0c68cd36124007534cf3dd1fba79796cb9b9382cef174e3cd3d64e21f33a339658c69bdf74c12c6fd1920bab7534cf864753cf0bdb9776bdbdf6fe6154fa3b382f8de1cceadfeb45cfbb3dba268e1b1d01f2d2eacc0fd0803292a2a6c2a691c1b1871bf4d16a45814f080e333b35b4e8a6e70f4b74108706bed7791cb92dba361d490013a11cd65d352d4c2419034f2cc0596c5333291a1d5656ca6d38c77000a6b58d88f24ca769001ba9b4cc5ced1a39aceae455a389d5bc6984d334ddb4f9aa64f0e43ef5e991b72b005c8f07e0f276f2e373f71d50d0d8a32350c17b13e64dd760bbf0c7c7091e7f3e532cfa7cf9e9be87d578e5954d6d8565231ee3d5cd25bf735abcf23b0d57b4fa7bc39d2d16115ec6926392489c47420387fda578a34eab6c7d39eadb0e60080330db4ba8ad63b7c511bac54ce6e61986e374c90a4ba759308e68075f5497497ba4dd00fcc80e4d45f9201d7d2e9d9bc547742024be9ba7076962a3ba5cdcd30933022ced4263d8db5c6a3ee4dbe88052b8ca60196d940bf884248a57c130730f8d8ec50b2ca65be8498fdbbae218fb2c7ab1bff00b974bc3937638ed33b6049695638b5b97892ab61b1249b00b9c38d370f99b2d2812ccc376bcfb0d3e5cd6b2ef1458fa21d8ce1f8360c2bb13ac8a92003db90dae7a01b93e0179471afa5a38a45250e034fd8c2e195f57334191e3f65bb34789d7c02f3cc571bc471aa815189564b5323459b9dddd60e8d1b01e4b3cb8b94cc3f6d3696498b9e5ce717b8ee49b92a2738bb746a939ab481ba3c108402254205d00376b7429535bb94ef7a01a51e095211a2012c842101bbc1d8dffe1fe28a2af7b8885afc935bf41da1f86fee5f4943207301690411a11cd7ca0365f41fa32c59f8c706533a476696909a67ff0096d97fe9216b8deb45f6ec414a135a538216c4f589ff00e349fc4535d34c7ff3a4fe229a05d5d830b9268848e9191871b3739f6965bd0664ec7cc3bd23f37225c5517c350c3ab9d6ea0ae824c3648e66c232be476ed69d92cd864b0b33bf296dec4b4dec8f30e4ab30e82b5a054b03c8d9c46a3c8ac89b85678b34d4d4ec95bcdcd68cd6f10bd42a7078de637410c6d1905ee6d728adc39ac92330b58c0406efcd46571c8e6567a79353ca298f652441963c8585d69c796460b0d2dd6e176753c3f057ccf8e7a763a460bb9f7b1f8acda9e1699f342289d031ad8010cb65cd6bea4f327a9586584be9ae3c9f5582c88376d8a94464f327cf92d4182b8b5d92aa17f644098b81688efcf51a8f24d9b0ec8c85f0ccc9e39df91ae682def7420ecb2b8d8de652fda9b4bc01671db5d50c6cb5b3368e373bbded93f65bcfe3b2d3970697d5a47534ac9e5885cc6d045fad89b03e6af613840a2a5124f231b2cc6ee7104dfc0780578e1deea32e49274b71cb344c0c64d200d161df29feb551ff001e4fe3295f4c58fca64659adccf76cd68f35c971156555652c6709a864d472cbd8b9f1121fda1fb241b581e5d56ce53f8b718a0aaa21453cc6ae46481e23cd98022e353b0d09f15c0bb0ea02e25b434e013a0ecdba7c974c7018292592f88d2d4cd467354530b8d07b41ae3a388e8a862d55495554d7d1c0218c300203436e6e75b0f727b0c7fa368795153ff29bf9270a1a46ed4b00f28c29ee910101c3e8bf5483f96127d1f45fa9c1fca1f92b291015fe8ea1fd4e9ff94dfc91f47d0fea74ff00ca6fe4ac2120aff4750fea74ff00ca6fe48fa3a87f53a7fe537f2561080aff004750fea74ffca6fe48fa3e87f53a7fe537f2561080aff47d0fea74ff00ca6fe48fa3e8bf5383f94dfc9584202bfd1f45fa9c1fca1f923e8fa2fd4e0fe537f2561080ae70ea13bd1d3ff29bf92158420391c6f882b31caf92aea5cc0e79bf67136cd6ff005e2b28b9ce3a94896cb6934927e09c07541082754006c9a478276a521401a24b6a96dcd0804284bcd0100dfb694ec93edfb93bc5006c842392013ee479a2c8b7b90065e8bd0fd0ff00109c33898e1533ff00b3624dc801d9b28d5a7dfa8f785e7aa6a79a48268e681e592c4e0f8de376b81b82ab1bdf655f5a08da79278859d16570c6371f10f0ed0e2acb03511832347d978d1c3e20ad90a32dcba38e6c340d805af1c624a280be374a5ba8eccede6b215c6cd4bd9b016cb1bda2c4c6477bc564a5aa791d2627219632c71659ad2a373cb29a66b68dd1b4e8e25db155eaaaccd335ecccdc82cd24eaa19eb657b72cb2b9c3a202bf1a6213e1986c7530464b8b4303ada309e67fae8b2b8e2bf14a1ada49e9249990881a5cf0dbb735f99daeaa71b62554ec329a9bb5776323ce6675b5adf7ae7c63d595b143438a5754c940c2333230d2e006d6befef29fa0f43c6b19f54e18931982519eb228db1348166b8efe7cfe0a8d3e3d506681b2451b9aec20d43ad706f63a792e4f1fc7692b70da0c2f0d64eda4a369379c0ccf71e7a123afc5630aca904115128223ecc77ce8cfd1f2f0483d2f09c528b1ac2aa8b05f2967691bc6add4d95da5a0a6752d3c4d886474ce3aeb6d06d75e4b1d44d131cc8e57b1af20b835c4036dafe4ba7a0e3430d07675313e4999ec39a6c1de27a1fbd31b77b13c35b33bb06c5132275dce1a8d3992a1a47c38950433425f333bc1af880237d8df9af31afe21c4f119fb59eae500021ac6bc86b41d0857998c61d35350f6d2e21493d1c4231eac1ae63ac7dab122c4df5483b2c5f102e657e1f152366aa6d3b6414d203df603a8d2c49b6ab93754621261320a6c2a9a82292a22687b6ec73a407bb6cc797354718c77d726a4346fa968a3616b2795ff005ae24dc9246de016755e21595e5a6aeaa69cb7d9ed1e5d6f24c3a7101c6aaaa29b17c1fd56ad91bdefad89a581ae68bdde3d920db71d5722ad498ae212d37ab495b50f876ecdd292df82a97400912a06e8044a94b7a24480489509822108480421080108420042122015084203cf3925d1081b2d9212a02100837478276e90f24022399425e6806a39a0ee96c8067db09f6d130fb606e9e80446a8472401bee912f242004e09bcfc938203d6bd09f1064a8abe1e9a43964fed34f73f6868f6fbc58fb8af666ea17cb9c295536158b53e32c06f4d202db73fd2f95c7bd7d3b4755156d2c5534ef0f866607b1c39828e49d4a53db01588222eced73752d16f79099eab51ff00024fe029fd956650dece6b0d40b1d162b3cd3c40076639483f6b4bdfad956969a31376643c39c1ce06e2c2d7dfe0a7c95d7be5a8bf9395399b561cf66598349d458d8aac7d873fc714b0b68237c65e1d04e584388398381d76fd9f9aab261b4730c3eb0411b62a3a78dd58d6b40ce3b30f693d4b8ddbf059fc6f8a321c4d9453d5b622d89af7c724996ee2490483e077f15cf1c7612c730e2ac2d7b5ad737d605886fb20ebb0e5d11f61d86218161a6b272e94c524cf9df132304358185c000d0c208eeebde160546dc1f098710c8c6d538b3106528cef6104f3710596b7873f92e5bff11de3923fa6bb9292646fad68f277245f54dfa7a2cf9fe96666ed3b4bfac0be7fd2df7f1522f6ea6b6970c661d50e651bc48da185e1dda0d1c5f6bdb2fc7f05cca1b8e97c7d9b716bb3218f28a8d329ddb6bede0a035b4805cd54207fcc08902c22eab0c4288ed5901fff00a0fcd0710a26e86b29c1f195bf9a61610ab8c4284ffeb20fe6b7f341c4686f6f5da7bffcd6fe69682c2156fa46847feb29ff009adfcd1f4950febb4ffcd6fe680b29556fa428bf5c83f9adfcd28c42889d2ae03fff0041f9a616c2690a31574c76a88bf8c270a881ceb09a3246fde09e88bb212e78493f5acd37ef04c7cb1301719a3ca35be61a25aa0a8500afa222e2ae0b7fcc0835f4607f8b83f981069d0abfd2343faed3ff0035bf9a3e91a1fd769ff9adfcd20b09157388d0febb4ffcd6fe68fa4687f5da7fe6b7f3405842adf49507ebb4ff00cd6fe68fa4a83f5da7fe6b7f3405942adf49507ebb4ffcd6fe684070f6483729c939ad922f64ba5927242360b74244b7d10022e8be882804484f54e49ad900cfb69c9add5e53d101108d905009b25ba0ee8400a58217544cd89877dcf41cca8895ad4117ab424bb4924defc8744c3469c323636168b31a32af5df4538c9a9c2a6c1a77de5a339e2bee6271d3e06ff10bc7e23676bb13a15bbc298dbf06e23a2c44bacc0fec67e86371b1f8687dcaf5e52e297d0890a1a411706e129d972b401674facaff0035a01674a6ef7f9ad78fd87cf5e945c66f48f8a876d1f64c1e0044d5ccc54c352badf4b10b61f4895ae1ff009d0c321f3ecc0fc1724d9881a2647f62c6fd9ccef92708a31abec6dc86ca3ed0bc1bb8f8a5ba0923a5ca2c001e4af60981e21c49888a5a281d26419e5701dd637c4f8eca951d254e25570d25244e96a2a1e191c6dddc4fe0be8de0be0ea6e13c05b47191255cb67d4ce07f78fe83f646c3e3cd3ea4dd379354701d65347da5540f63180dc35ba05818960d0dc411901d7d095f4a4b414ef6164801cd7d09dd70b8ef0b5150cf6650c4629ae03b2ead3d2eaa78e5e9a4cf7d5781be27c523a391b95edd084e6539702ebea576d8df085448f7c6c6b7b760bc4fbff783a15c830989ee8a56398f61b39ae1620a5669194d2a88243cb4523210db5f75680cc0006e9b2461a345295770d3452d3d2cb532321a76b9f3486cd6377714d0c25d7705d3700c024e3ac2330bb5b2b9e41fd9638a726e86453c81f0b241bdac42b79d99db94d8f8f25363f42cc2b1ea9a78f48647b9d18e9de370ab88dae9882795c2aa553091e1c6ced7929639afed32fa6aa26820dbe09ce366e84dbe6912957d2d213da44f3049cda5bdd3f0d965bad94b4917b7c56ad4da52e0eb778e96d135984c66191f25f46121a39e895818412ab92617511b338171d39855082dd1c2c7c54a89b2694e29852504895224021084b61612591cd0ad239a0a3746e8011ee4881b20179210109ec0f7a0ec8011640461d949b8dd3eed76c521682985a41d12ee1a5b244c121d8a92f717dd397608354b647252410baa666c2cd0b8ea7a0ea824d434c65776ce6dd8c3a789ff0045a2def1fbd4ee63616b6088656b45822d95a2c01cc740a926127d96fd9209b29d84169613a58a8d913834876f6d4dd2c42cfbf43f04e07bff000362df4c70a51cee7669626f632fef374bfbc58fbd746bc8fd1462a29315a8c25cefabaa6f691eba676fe63ee5eb8b2e49acb7fb563e88b3e76e595c3a9bad055aae3d9fee29617554f9fbd314663e3ece76928e270f811f82e24349d97a0fa6e84b38b2826b68fc3da01ea448ff00cc2e018e25ba7bd690a93296ee4270bb886804926c1a06a4a9a8e86ab10aa6d35153c9533bb6646db9f7f4f35ec3c07e8e598286e2b8a864d5e47d4c635641e3e2ef14e4d929fa31e16a8c0b1b8ab314a7c9593c65ac89c3585ae17bf838fc82f625cf3a9e492be0aa1f600cc7c5a57420dc5c29e59e95194d8627be490873657bf2768002eb836e7b0562ae8db5744629b539753e239a8ea3b0a7ac635f148e6ccf0e19185c03fa9b6db05a00dda0916b8d945cacd50e0316c1dc7ea9fa48dd637db42b89c6f85e9b19d5d7a6ad8f4ed40f6bc1c39f9af6ca9a18aa5859236e39750b9dc4b859f29cf0b839c363b1f7f55b4ce6436f069b8571da67b9aca61501a6d789e09f81d5665452d753e6ede8ea210ddcc9138597b55760b530383a585cc76d7b68ef7a5a0a89292521c098de324919d9cd3e09789ea5787b6a19cddf35d6fa34227e3aa20d6975a398e83d9ee1d4af67a6e1bc26a29d8daac1282aa303b8f96958e363e36bad3a3c070ac22291b85e1d4d47da7b6608834bbcc84a65aa9b1e41e91387e56364c41a2e6078948eac75838fb880571d4ee692d7f30be80c7709656e1e5a63cc5ad20b7f49a770bc127a27e1788d5e1ef04985f6693cdbb83f0215defb2be92c80defc88bdae98e3999b6aa620480122d76dfe09994f32a52a9d992edac095a14f0de3ca79ef73a5946c8c106c343b2bd0b0000017bf4e68063dd773439a3d9b683621452e13455acb36c1d7d8eeacc80860b8f64db7485a01b0be72343d1018359c31511b4be9de1e07d976eb126826a793b39e3746ee8e165dfb24737527334f30754540a5a883254c22465af95edbdbcba29b0f6f3c2916a63b86b70cc48c51126191824889df29ff005b8596b3ab08421484e848956a91e484974bc900895221006896e93642016e8293ef420048764ba7540d100c7353412d3a290ea130a2c33daf1cfe0b570d6764dce410e76e6df259b4f0bde1d2b7411d9daf9add899308da5ed6b9a4daec16213c77aed352e675c12351cec98250666bcfb37eef929a52e32b99769193436f8aafd8e5eeee1be2a893324ceed45814e06f2137163ba646c006bb72bf252377b1df96880bd8656c9415b4f5d11735f03c38653ae87f2bafa0e8ea9b510c6e0e0e6c8c0f8de367b48b83e6be7589a3969f82f64f4775e310e176d248fbc943218af7d437769f9dbdc8cfbc7fc39edd9a6b9a1cd2d3b150c529738c525848ddff0068750ac2e7f4b70be907845bc5180ba26766caea6766a791f701b72330247223f05e7b827a23aaaaa802bf118fb11edb695a4b9d6e40b8003cd7b7628f0ca2932b6ef90646e9d79a4c369a186998c8c5ac35f15b6fe3b0c4c138570dc169841474ac8221a96b7573cf573b72b6d94e5e6fb0577237a04a0594ff0065fa0ae60063cad1600dd4b1691b474d13d20163e6a2db41c842120134b414e420232c05a5ae01cd3c8eaa854e038754837a76b1c79b345a684e5b3d054a284d340d835ee0b0f2f353969236522116eeec2bcd15c5c05e27e94b09fa3b1fa7ae89b68ea9bd99b751a8fc7e0bdd1719e927036e2fc2f53923bcd0b7b58bcc6bfd79ad30cbea969e2f4fa0ecc91dd3f229ee2d0351baa74b28223906a34255f963cc5de042b41b1169b3730f356a3764701c8ea2e16696b9aeb8b85236795803438defa2034465b7781b5b44462373372072551d88b4444bc5ec09dfc157c3f1a8a6688e4cb0bc69ed5c1f7a035fba492082069aa6bc822c2d6d8754d2483a1160771adc24b97333016b5ca46c3e318725451c97f6a12ddf6b1ff55cd2eb38b876987504ba66639cd3ef03f25c9acb2f6a810842469909a0a5b6bbad125e5a2127cd0805424bf9a50500237412125d00a8488f34017f146bb942426c800929f4f4efa87d80d3aa752d23ea9f668eedd7454b87b699a036c5db9206815482d434f40d6c0f07bad2c22e55864ae742c20774b6f64f7ddc1cd06c08b5d2888363cad232dade29a4d2d0c78d2e40b036e4a2b68ac39b768b74e964c91a00d3c9010b799254add0efaf21d5300d06d729cd1ae849d50134648b9b85ddfa31c50d37103a8deeb475b1580e59dba8f9665c232f97deb4b0bad930faea6ad8cddf4f2b6403ad8ea3de139df41f404f0991a1cc39646ead725826ed5ba8caf6e8e6f4441347534f1cf13b3472b43da7a822e12ba26b9e1e0969f0e6b9ff00f2b445345dbcdafb2c1a79a635a607d86cadb5a1a2c1248cccdf15532fa056b8385c272af138b5d94ab0a6cd50121d52a12010842004210801084200421080157ab85b3d3bd8e1704105584845c1089d5d87cd18be1cec1f88b10c39cdd2394960fd876a3faf04fa77bdec2c713ddca2e472b2ea7d2f61feab8dd1e251b74958629081cf71f8fc571f4f335b3090939740ed7915d28ab12c5a1b5ee3aa832d85acae3da43cb48bdcf5dd40e6e5232dc0dee912a4ad04116d4e8164d4618e04be01cf4016f16df4d081bec9b66969d45c7c3dc968d894d8b56510313bbcc1ed34f45bf4d54e9a06cad176bb6d76556ae861aa63bec4a350e1a6aa7c32164148d81c40702493b2021c75e26c03d9b18e46bb5f78fc5726bb8c4a164f80d5b58db16b335fa8041fb82e1d6797b5408421419c94149cfcd0b523afe283a9488f14028205d17488d48f2402a34d022fa0f9a10470b2447248740800956a8f0f92a5c1ce1959e29709a5655d49123800d17b755acd95d4f218c372009c856add3d3b626e5600d03776c87bee7203a1dcdad750b5ce7b2ce76fd548791363a59511edb65b58689c2f6b902c428b30bdcebd12b88df71c804c1e4ea0f24c711936372807301e235e490dc78dfaa400d05d39bbe82e3aa4dac6d703902976e9a26120be83e2a68dc06dc957bddd71d13d8fb697b9280f68f46f8afaff000c8a57baf250bfb237fd1ddbf2d3dcbaf5e3de8d713f51e24f557bfeaeb63319b9d33b756fe23debd8565c93e5bfdaf1f410842ccd0cacb7782744fccdb1dd3c8b85010637df92a9dcd05842406e2e9548084210021084008421002108400842101c1fa4cc14e2b8154358cbcb1b4ba3f31a8f98b7bd78a51be39213706c5857d2d8ad309e0736d72e6e9e6365f39e2d45f44f11565235b95824cf183c98ed7f123dcba31bb89abe7318e3734923b30544f02ffb3c8dee968ad230c79810c36b5f91d4293b36dac09194ea84aa3ad9f6d13351b5ba152b8069b01bf824cbafcc202290388b0d3751461d98dce9e0adb23b6a05afef53b200597b01e4805a2a5326689d25c48c7348eb7165c19041b1dc2f4489dd9c8c769cd70989c7d962954cb5836675bcaeb3cd51550842cd454a910b422a1010981e2952236402a51629a9c35dd00208b845bc51cec823a099d4f3b64693a1d6dcc2e89af655c6d7137e608dd73056a6135595dd93b71ab512eae85688fa97653a9d93db7e66c9f233b46dedb0d3451836162469beaad25cd99a34b58ee9fa91de077be89a05c8bec9ed69077d873e68053636b1b6964bb6809b240058744add5a3a72f0400346f4f240d6e0841245c5f5f0436f7eba734c1cd274209f3b6a9cd395c0f2e6981d67dada94e6ea6f736bee80bb49592515543530e9253c8d91847506ebe84a2ab8ebe860ac88de39e36c8df222ebe758dc1bb9f0b755ec5e8d311157c3028dc476943218edfb2756fde47b94e7378ff8ac5d8a108582826bdb985939080862758e52a65148db1ce13da7336e9dfd83908424021084008421002108400842101154373446db8d5786fa56c34d26354d8835a4364bc4f3e7de6fff0025eec45c11d579cfa4fc24d770fcae68bbe369737cdbdefb811ef5af1dfa2af2aa1780e0733af700db98ff00ed6948c00919859dadf6581453004386ad016f308742c36d5bed5d6950af2340235d3a73b27c718e8764e959fa22fa7248c392d9b4f9ea904a2306d752442c091b936b040b904e9a58d86e9a01ccfb3ae6fb1481da077539bcecb8fe248cc78ed45c019f2bb4db568fc575af2585b7d0dc6a0f25ce71742e6d6c32dee1f165bf5b1ff50a72f4a8e7d0842c94548950ac0421099146a8d76489500253e292e8dd305e5e28e684140214ac7ba3787b4d88293c1214a874947542581ae06da6df7a739a33666001bbdcac6c36a0453e479eebb6f02ba16bdb2461a06855cbb4d358328b837f04e2466049e4a105cc759da1e691d28b5c9dfc132486c741a83d13c775a2c06aa38bd9b9f703c92b892466dfa72402dda4d86bcee949371ae84f44c07bc74b59385b6075e42c807dada6ba6c9cdd06a6c07dca36924273083ddbea362984cd3fd05db7a34c53d53890d23ddf575d1960d74cedef37e571ef5c3b0800b49d55ba1a99282aa1ac84d9f0482561ea41ba35be8e3e8d42ad45571d751415711bc73c6d91be445d595ccb0842100845c28dbdc796f252a63c734e03d09ad3709c90084210021084008421002108400b13882984d4330cb7b0ce0792db55aad81ecd763a15785d50f97eb290e178bd5d03b68657069bfd93ab4fc085ad41331d0b89be637dcdb656bd2361bea1c491545acd9986327ab9874ffa4b7e0b330e90075dc49cab7a8ad0d0c561bedf20a179b6b71e255b25b7200ee9171655a566808f8052446cb62e1be9a15235e09d09d75dd5232586ba7827098585ec42416e49406125d7eeac3e22963a9a189cc06f13edaf2047fa05a0650e36e43c552ada664d45306819ac4fbc6a965dc39edcca1010b2d2c210854412a44a8012a44a1303c508468982846c5093740090a5488045b986d597c61b7ef0d0dd61a96099d04a1e396e3aa52ea8ae8ea05c02d3af3f04c8da7b400f2f92928cc733411a8237bee9ce8844ebee08dd6883b602db943afd2c0a33661702c06da28cbae74274402b8e973f725162798f2515ce6dc13cae13da4107c0d90135ecddfdc392507bc35df74d075d06a9c0f575bf04c1c0e97beea66ee2fa5b416510b585fe2a4d36b5bf1407affa30c545670ebe85cebbe824c82ffa0ed5bf88f72ed978c7a3ac5061fc550c4e368eb986177ef6edf98b7bd7b3acb9277bfdae04210b330908b84a84046d363652263b4374e1b260a84212010842004212201508420051ccdcd190a448ed5a513d8793fa58c2bb6c21d54d6ddf016cc0f9775df237f72f30a2975045ec4ec0afa038b30f656e133c4f176d8877eeb858fdebe7881aea3a87c128efc4f2c70e7706df82ea9dc4d74b1caf39737207cac51282e6e5041e8a1a67660c739a7d9274ea15800dc0dada12798dd4a5992b6ff674db555dcf0d70d77ebc968d4301b9bf7b7597334b5c4d893e4900f7df4b9365117c8d76605304b6201d7add485c1cdb12506c1999d9ccf6743a215bc4d833b246db51636f042c6f4b504212aa2221084a1950912aa20952254c04be1f349a5b7411636400508472fb9008912a45343470bac313fb12743ecf9ae83bb34037cd7f782b8f0483706c42e8b0cac6cec05ded0d1dafcd5e17e93626777058e9f8a85f2dee0683652543c389734661d5556b4c9f15449e3ef90361d14ac196fa6a9236646dc1ff005529680cbdbe0500adb91e27729c49bdec4a42473df9a35b123ffb4c1d9b9dfa27b4eb73af54c1a6a49f72735c41b8b9037364059827969e68e68486c90b83d87a106e3ee5f426175f1629865357c27eaea226c83c2e365f3b877991e4bd67d156282a787e6c39ceefd14bdd1fb0fd47cf328ce6f1562eed0842c54108420108b8404a84008421002109a5e038379940392734a90a004a912a00421080a35d036689cc77b2f69695f3a719d13b0de2da8bb749ed2dbc7677cc1f8afa5266e6615e33e9830c314d4b8931bb3cb1c7c1dfea3e6b7c2ee158e3e8a52f61039fb3cbe2b45a1e627388fb2081d161d049721b98800dc2dc832f6634240b7de9a0e797684eb71adbc9529a10fd06b72740af004b1b63602e0dd40f3624d8efa91b20316a295d7ee68aa667466c56e4ac1a81d35d151a8a537d3ee48e56655b7b5a77682edd50a47c4e67d9bf5f1429b8ed4c742b5f46d7fea551fca77e493e8daffd4aa3f94efc949ab2159fa36bff0052a8fe53bf247d1b5ffa9547f29df92502b25563e8daff00d4aa3f94efc91f46d7fea551fca77e4985742b3f46d7fea551fca77e48fa36bff52a8fe53bf24c95f9209bfe69f2c12c0e0c9a27c6e22e03da41b7bd3130108420110842408a7a5a8753cc1e36fb43a850a4537aecdd444f6cb1b40b6a2ed2398439b94ddbb2cac2eb0b1dd838800fb07a1e8b75995f16dbad65df6cef4634920e6f7242fe5aeff149ac64b41f7dd3339b8b6c983da466b10a468eba7450b2eff645ca9c6a473b2001a585f51af9a50ee5b5d2d85928d1a9848cd08bbb6f15d4fa3ac50e1bc5b042f75a3ad6981ffbdbb7e62def5c98dee7e2a56cb2412b2785d9648dc2461e8e06e11fe87d26854708c4598b613498845ecd4c4d92dd2e351ee2af2e6bd5d3408421002108402254876420154043ccfab0641b3afadf4e5f153a677f3f2cbf3403d225420112a44a801084880470be9d570fe92f0bfa43856a835bdf8da5cdf31de1f77cd772565e33009a82761170584fc16985ec3e67a4758b5c1dcb45bd4f28ec8806f7d0f3d560cd4fea3884f48491eaf3399f03a7cacb529260e1b817d34e6b5456831c1c74791fa40fdc9928765d1b7e6754e2436306e4bb73e68170e2d0dbe8371af9244ac458137df928799041b93b59592d0e610ed08e8a09402743727920209636b81eedbc2c84d792d245f4f14241b8b4e9e9a967c19ef10c82a5b5314664cf9810e0fd0340f01d5735ff008870afd6bfff00377e4a68f8ae86285d1475c5ac73db21b46ebe66dec6f6be972b18d1d99a2c22a257cd4b0c429e37c9186c9da4777e52581c4bcf74d8ebdddb5b29a9786e19eb28ea5f4a1f4d2319dac34af748dce5ee6e8e0490d005c9bf85f55c83b8f63748d93e90602d739d66d286b5c5c2ce2e01b675c69add42fe33a592b22ab7578ed61cbd996c040665d80686d80f0b241d253d252368a5f59a131362648d96a25739ae12eb958c17b1fb37045f7d9257c387c544d98d09a7cf2b4c0ced489658ac6ee7024817d2c40ebb85cf8e37a6f556529ab81f14608607d1b5c5a09b9b12cbee7aa4ade35a5c409353570b9ce398bdb4618e27c4b580a67d3a5928e91b8b5453d361c673d9c4e8a39247767182c0e7b9ee041e7be835f24f8e9b087cd51d9d2e6a28df27695723dc320b77047af78df910495ccc7c750466622ba3776ed636412528787068b3742d3b25ff00c754e60101a9a63182e7069a0610d277b7734f72567e931cb715ff00bce3ff00923fee72c5bad3e20aca7aeaf64b4d2676088349b11adcf5f359975a401225401704dc69e2804421080122542540048371a15bd86d6f6d100eb07374778f8ac052d3ceea79448d3b6e3a846375459b7472b86fbf9f355da1ce36d94d4ee6d444d7b4dc3b9f4f05239993be396eb541d087003c94adb5b5d135874bf33f24fd4db52990bee4028b037bebe6999b29b14ba3ac36eb748d230b4e9ceda9b278be5009176fdc982dbfc92df623e3d130f58f44f8aface0b5186487bf452e660ffdb7ea3e79be4bbf5e1de8fb14fa338c69839d68aac1a67eba5ddab7fea007bd7b8acb9277b5c0842166610842010ec81b2100585900a8421002108400842100244a90a010eca0a96873751a1162a72a29b562ac7d87cddc7b47f47f19548d84cd6bc69cfd93ff006fcd51a3974b2ec3d32d0f6789d1d6803bc5cc27cc5c7dc570b41280e17d2c355bd45746d901a7b9bbb5b9f0b252e3da036d05f5b6eaa4335996cba5cebb9d55a05a626824070d48e69111d76bb53a6b73ba8c3980b5db9279295d6319206a45f537559ce398380b8f1080248337813c8a12195beff142038a4210b1ad0239210a404210ac04a84260210841047342106108420824421002108534da5854cf648f8c1eedaf6f15bf1fd63066d6fba10b5c7f18cefb33d9bdbaa56925aeb93dd3a78210a801de3aea9c361e484240e3bfb8256f3f7a10982ba47c404ec716c91383d8472208b15f485348e969a291d6cce6071f3b210b3cff0018ac532542164a08421002442100a8421002108400842100882842010eca37fb25085503cb3d32c4c76031484779b2b2c7faf32bc7e95c7b51aa10b74d6d52126465c9d8fdeb458db4675276dca109248e6875eea39761e37ba1080a530dfc0e8842101fffd9	image/jpeg
1152	\\xffd8ffe100984578696600004d4d002a000000080006011a00050000000100000056011b0005000000010000005e012800030000000100020000013b00020000000f0000006602130003000000010001000082980002000000190000007600000000000000480000000100000048000000014d616e66726564205765726e657200004d616e66726564205765726e65722028474e552d46444c290000ffe10b95687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f003c3f787061636b657420626567696e3d27efbbbf272069643d2757354d304d7043656869487a7265537a4e54637a6b633964273f3e0a3c783a786d706d65746120786d6c6e733a783d2761646f62653a6e733a6d6574612f2720783a786d70746b3d27496d6167653a3a45786966546f6f6c20392e3734273e0a3c7264663a52444620786d6c6e733a7264663d27687474703a2f2f7777772e77332e6f72672f313939392f30322f32322d7264662d73796e7461782d6e7323273e0a0a203c7264663a4465736372697074696f6e207264663a61626f75743d27270a2020786d6c6e733a64633d27687474703a2f2f7075726c2e6f72672f64632f656c656d656e74732f312e312f273e0a20203c64633a6465736372697074696f6e3e0a2020203c7264663a416c743e0a202020203c7264663a6c6920786d6c3a6c616e673d27782d64656661756c74273e4d6f6e6963612042656c6c756363692061742074686520576f6d656e262333393b7320576f726c6420417761726473203230303920696e205669656e6e612c20417573747269613c2f7264663a6c693e0a2020203c2f7264663a416c743e0a20203c2f64633a6465736372697074696f6e3e0a203c2f7264663a4465736372697074696f6e3e0a3c2f7264663a5244463e0a3c2f783a786d706d6574613e0a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a3c3f787061636b657420656e643d2777273f3effdb0043000403030403030404030405040405060a07060606060d090a080a0f0d10100f0d0f0e11131814111217120e0f151c151719191b1b1b10141d1f1d1a1f181a1b1affdb0043010405050605060c07070c1a110f111a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1affc000110800fc00b203012200021101031101ffc4001d000000070101010000000000000000000002030405060708000109ffc4004510000102040403050505060405050100000102030004051106122131074151132261719108143281a1234252b1c1152433d1e1f009165362436382a2f12526347292b2ffc4001a010002030101000000000000000000000003040102050006ffc4002f110002010402010205020603000000000000010203112131041241136114223251a1059123337181b1d152f0f1ffda000c03010002110311003f00da884c1a047891a40c08203475a04047a018184c41204263942c2f7b799b408e8927e14f58af38a9c5cc3dc26a1fed2c473003cedc4acba46671f5db44a47ebb0e710dd8b24de89cbf372f2c82b98772240bdc2af105acf1b700d01d2dd5b15d2a50a6d9c3b369cc917b6c35de3e7bf143da3718f121c99967671ca5d25d27f756156514fe152c6b6ea05879c52ee339d45564a95cc91a9f9981f7fb04e896cfa5fc43f6bee1ae0e91979aa2cfab154e2c12d4bd37e13d73b86c123c753144bdfe2058a9d98cd4fc11446654abba97271e53997c55a0bfca320b692b5a90120829f86f09cbeb41b22c948f088ecc8b246d393ff103c4695de7f00d2df689d989e75b55bcc820fa4589853dba30555de6dbc574eabe192ab02ffbb266994f99412b1e7963e75a27143452ad0bd89b27456b7f1315ef245d28b3ec8e13c6140c7321eff836bf2188252d752e52612b28f05247793f3021f888f8d341c4354c375662af86a7e6e955368ddb9a9378b4e83e247c63c0dc7846a5c1fedcd89a5a9adca632a6ca55675b21227d80182e0ff988d82bc53607a08baa8bc94942cae8dde440488c7caf6d89c4ebfe5ad3c1e4c055edb33163ff00b65571ff0039304ec853d7a7f73601801178a07833ed1b31c58c5ced0cd18c821a96ed94e1701beb6b691a06d1c9dc2c64a6ae828a600441c4748091162c10a105284282982d49de38813e5f08e8332c747103801062531e2530684c41c8f0581b738f4a6f600da04078433e2ec594ac09862a988f11be25e9b4e64bae9fbcb3f7509eaa51b003c620b2c90be3671928fc16c2a6a7514267eb13576e974dcf654cb9d55f85b4eea3f21a98f9858cf18d6f1e57e72b98bea0e546a732b2a2a528e4693c90da764a46c00878e27711aafc54c6350c4d88d7926263b92b2c93dc939607b8d27c86a4f337310271774136b93a0bc2d29766329755600e3a90a3b81f84437cccc155c0b81e10378d8dae6f7d4f41098850fbbaf2f08e28d9ccaca1435b28f8ed05a9415bdee3e90e32587aa7534a9c919375e6902e5691a69d21f64f87d5ba8392e914d98425e364a949b03d479f847768af24a84de91156c0239110a9a40d2fa0f1dbd62db9ae06bcccae7927d4d4d24fc36ba09e8798f38aeeb187e7f0fcd995ab30659eb9c8abf71cf230285584de1869d0a94d5e484694adb4e84149e4607dd72f6bdc0d6dae90525c75956550b0bf3107a939ae5a3623423fbeb046090f941c432b20ccdc956a54cd32fb3fba3e1565cb3a0e8af141d88dc6e21cd5aa01bef63a6b10e290b4f7b43cc743d443dd1a694a64cb38a0548d506d6d3988e4ed811e5d0ecbd48efc9a8bd8b25fb4e2456ddff4e9e81eaa31baad189fd88182bc5d8a9e3a944b349fccc6da22188e8af1ff009682c88f088191780d8c5c602950050838a600a112409c8d778e8111ac747151c52348340d23c40832debd220903b037e51877db93892ece5728fc3ea73e7dda4528a8551093a29e503d920fff0054dd56eaa11b6ea55097a3d3672a5515a5b9593616fbaa3c9284951fca3e3e631c51378cb15d77114f3ab7262a536e4ca947719cd903c8272803a080d5765618a6bc8c330a2aed003ddd7616bf87ccc2352492be7940b79985ca6d2066b5f2ec07a0fd4c792cce849d72dc9f9ed01bd825869520852f991f9f48534fa719b9865821575ac24dba5e0d6e57316cab5cf756bd3fb1131e1bd3533b89a582d0a5252331006c4ebf969159cbac5b2d4a1de69173615c1d28c5318494e62946a75dceba748b129d85d2948711dd4e87524ea3ce0743a58c8dd9b194e97e76113496974a43693a248fba098c09ce4decf5318462b430228c90950c809be6d07ac47f12609a6e24927652ab2a875a50b02520149e441e5167b321da0ccdb65d37b5b6b4113549ecc9253a11a1b6d144e4b2897d5e1984b881c3d9ec0d514a1fcd354c7d444b4ddb9fe05f43f9c4498cc9d08d4682ff00506371e31c31238829d334da9349719994142faa4f250f1118bb1061d9cc2f5b9ba5546ea765d7f66e81fc56cfc2b1e279f888d9e3d7f563d5ecc1e5f1fd29768e98db30dd90952526d7b74d63d939ac812e2376cf787e21035e5711655c85a75b75e7f4b18670b54b3ee2146f719bce1af61166f1f619c8ecf62a9a45f2381a4a547ae5da3662931f39fd87f892ce1de24cd611aa2c224f13b45126ea8d83538819929f25a411e60758fa3283990955ac08d47430cc342aa0a185a019638a74836d6809821c12a1052a0e5414b8938208d63a05a4744903a274102d875263c48b6fac0ad7f38a9c523ed6589ce1be0b569a69dc93153cb288b684a544050f4bc7cc30e1ec52a26c16ee9e39446e0f6f1af3a8a3d1a9697025972635473514a4a89f0b5e30bba7b26a56f7d1a2adf62a30ad4cc86e2ad041eb72e53617dc807a04d87e7030efeef34a400543ba34f9436878adcdfba90373e307cabb9a49049d56edd5e405cfe7f48a33856aca56d26e7314d858f2e5f48b63831485b93f315053776f3290dacf23b7f7e514d89851792a4fc6744dba9d2353f0c6929a5e1c91615f196c2d47adf5853932eb0b7dcd0e0c3b54edf62e1a2b0853680bb2404dcda2672324cad2921bcdded2fa7d39c42e82db8128424ea3617d22c2a5ada97b079417b03e1e5eb18e6dcf085824d0e379140651d07e9086af2791abb69041d0dc787287654f34064694949fc2a3fd988d624c50dd1298fbee364a536ba3c4effdf8476f0055ee45a7992e38a4a93de3b784662f6857284b9990535516155b69d0cad96d41472136ef5b6b1d6c627389b19e2ce25d49749a27fe914c232a94c0b2de077eff0021e5ca14a78358170de1f9c455d2ccdcfcdb2a42e6e696014ac83aa2fb6b6d7730e528aa325293cfd80d572ad1718a3263ce16c8b5c106e05f63bff310967db4abb27db1dd55881fed836654a05d69c502f377214068a293afe57f9c276ddcecadbb9b25574f803a88d7f7303d984d2ea537499f93a8531e5313f24f226259d49b143ada829041eb7023ed2605c512f8df06d0b124990a97ac48b33602458254b402a4ebd1578f8a69191e20750a11f507d886baaabf0169d28e2cacd26a13923726e72858711f201db41e0f209e62689b69aefb40488345ce6bee0eb005418109d505a85e0f2982d51270988d4e9f48e837e71d1c4605f98246a6d042e7db41b5f586c9b9f2a395b3610dab775bad5648d54620948c33edbb889153c5d489064a8a9969f797affa8b0948b7806cfac65c9e55d6903921200e961b45cfed3f5113fc609f424e632e1095781cb9adf2cd1483ebed1c5ac6a2ff00ac26f2db1c784909f3d8be47e2cb6f210ac3804b2120ea841569caf0ded92b0edcfde51fac1e55649b6a4929b1e400feb1c51683e9d30da2a32cb99366d0e052edadc037b7e91a0a998ef174ca19186b0bbca4e54e453ad158d3c06de66282c369686209154ca42d969d4ad408d0811a571371ce4b03c9b32d43954cd4eba33969232a109dae4fe509d7cc924ae697115a0e4e5647871cf15a596a2eca4a488f84b6e4969d7727eb132c0fc4ac4536fa19c48c4ba104a7ed12729f1d3c22bdc158f78b7c52abc9c861d91a4ca267738965d4505a69d284a94acae2af72024c491baed5fdf9341e26e1714c9e7886d89e69b0969c5936195cf84dce82c7c2d730bd5a5251cc50f50ad4e72b2936681a5d4bf69a4fba2d2b2a1a293be9110e224cad8a7f63307b4eddc4a4254773c81f0868e17d4e72913cec8d512a4cc4b3c5041b8b806c22538f24da9c98909998476b2a8980b710537ba763f42633d2eb3b31e93fb19bf19714e6f0b4d26878384bcd569f396626965296d82ab580512005789361ce23ac604c6b8f70bd7b1399f90aa3d4b596a75872694e2d3dd0b050e025b29ca46de516b62de072eacdda45e929a2ebeb9821994282a2b1b15151cc9b04e96e51e51b82d554d15ea12cd41aa5baff006deeed38596d4ad3bcb02c156d2d989b5b48d4a752941197569d5a92de0c7c1c506d209caa42faec3a181b2acae202b4ee91f2dc7eb17171cf852ce016a4df906c764b177881ba898a60abbc0df404ebe1fd98769cd548dd193569ca8cacce70655a75d754fa18df9fe1f158cf84b1c5288484c95525a6d26fa9ed992950f2fb11eb181a601cea3a13707e83f50635efb044fad9c538e64527f8d479699b78b6f94fe4e41a1f5026b0cfa148712b6c2d3adf78e5690c6c4f2983a1ba548040fa42d6ea087536cda8d219016142d7684ae3c0080bae154265de24804663531d09c8d4e91d1c4584b7b6a6124c2eccbaeaf64a5445f648b6fe260f5a4aae927972f186cc4d33eed489d2dd839d9e541e83ac4175b3e61f17aa46a7c4cc4f38365ceba77bdb503f48af0a880af2fd62478b6a1fb52bf559d569ef0fa9407866311c7080c9001bdd27e9088dcb626971985b9aadf9c1c4852c5b6ed2c3c80fe91e3232a0937eee91e323329bb8b7755eb167b06b448309e1f9baecdcc0a7a4a9d6826f6e57b9bc58133c17c4ea69354652c5456a052eb041ba2fa0fef9426e072c7ed699d74717af95bf946c1a1a185b69ec9036f92ffa46657af2a73b23678fc6552926cce38038615da1d5a56b14fc94cabb45494be580eb89ba4a4948e4aca48b906d78be29582e644c09ac433733505873b63fb45f53eab83704a09ca2dcb4d22c995610db798232ac8dd3a1b741d04364f8ed1975b6c64076b73f3eb0acabca43b0a318e8ae165c6eb4fcf1ee971fbf77a721136a8acce5216b402503be05a2135b9914e986d8741ceeaf4b44eb0fa0ae40b3d9a9c4a91c869006fc8cf5495847439848425b23337f122fba4f81e512b96980e5c071cb11d41d47f62202970d1aadd83c957bb937ca91aa0df7899a5d656d85b4bbd8dd29b6a7afd221b772bd5344278cd8565f13e0aab4932d76937eeab5b4a3c96351f58f9de8d50de7ee90a2855f9728fa6f3cf34b2e28f79bb04e9adf9da3e74f10e926818df115308ca1aa83aa401f8547327e8a8d3e14eedc4cafd469da3197f6199d45d0caff0015aff236fd634e7b0ccd16f8b159689003d879d41ea4f6c9b46650734a67db2827f58bebd8ca71329c79a536b50026e9b3cc049fbc4379c0ff00b63523f518ef47d1b79ccae22dcc5be763fca0943aa438483d2024df281b5b38f2e5fac79b927a9d3ca1a003bcb4d07058ef0a142e21850e1428110ef2ef7689173ac490c114ebb4740b5e9f48e882a55b53e37e02a63810fe22935386c0252e8249be8348f7889889895c0359ac36abb4dd326261bb9b5c86ac9faa84643e13e1dc2d8af1d52e994bc2b3c1b41eddc999a4654809db7dee6d178fb53d7daa170d6629d2c034baaf63288006cde74a9cfa200f9c0dcb0caf1a72aaeecc093a482093ff0008951f1beb0deb399a076e77e90baa8b2a2b234492120741726114c0cb26803724fa6d0aad0f3d84150ec1000b03fcbfac18d5c32e287dc64dbc09dbfbf082d49b169034d0983944372645c5d76bfadc0f4498b1089d708ea1ee35b0806c57d7a46b8c2f3a12ddd2b572ef5f9fce30e61a9c3215a95712b171bd8f8c6a2c21884644070a945405ac6323970f9ae7a5fd3e57a5d5f8344c8cea5c48249bdb6bef063f30da058d946fe510ca7d4b32012b36236d8885fefc950efa8117b0178ccbe479c3c95ee379a9a929c7a7d726e3c1598b795248cd7d2f61a5c73da1af0cf13b12d3e5e659acd25c907081d97633299842b9e8522e34e462d2f7f1aa345dc72d7582196a994f9957bdfbb30fb82e942884957ca0ea69ab3442bdf043a99295baf571ea9ce4fbac4b3a7ff8dd904a7281a6a4662a27e407289ff62b4d9c6d4a414ee0ab4f48433b5b936dd0db3db3ef0b8506d00e53d09bfe511d9de2250e49dece7e78498cc15f6e9c9a7af947494a7e08eb259b0f93d34b926f2a895215755fc4ee078462be3a597c449d9902ddbb2da8f9816fe51b067671baa368f765a1e6dd48585037052632771f657b1c5ca50d521a4804f8886b858a9910e7fcd4195b4bf7a45e4ea7ec89f43fca2c2f67fab9a3719f03cea54536a806c91a7f110a411ff7457326a25a50075285a7e90e585ea468b5ea3551ad552336ccc8ff00a1c4abf48d9bd99e7a39b1f605365f790732556ef7fb790811fa444a89c44c3b54aeff009729b3cdbf524361d0c20dc86942e93e562210621e33e0ac2d567e955badcbcacf316ed1b52ac537da1b15724b6c9c983e59eecd56da1be9d5295ac48b33b4d790fcb3c90a42d26e0830c2e63fa0b78b938544ea155d523b4f7606ea09eb6e913739b48b03b61e11d1177713c830eada7261216da8a543a11a47441c3760bafe15c4d29fb47097b9aec027ecd212b41e846e3e71937dae2be66ab944a505a4a4296ee4dca500900fcc83e91324e26a64bfb44219c10a4b4cb94f705604bead9701052481a661afac67be3557bfcc1c4ea83e856769a01b6d37f840163fa9f9c02a4b1609c7cb7ec54b541741569aaed60773fd984f38425b4dbee8d3c614ccd9e5b3c8039c69fdf8425a812a2ae43e1f281066260333c84efdcb6b0a26f2fbab2a4837528903c122c3ff00e8c172e9bbce2ba20da3ca8af286d00dc25a16f33de3fa477923c08642604bcf36b51b24aac4c682c2d36b328daaf7ca077bc23391d46b16df0bf1534bc9233ab01d40cb627e24f2301e541b8f643ffa756e93e8fc9a470eceadc653f11d40d0ded121aa7be49ab3a5a5bf95bcc10da6e4f9088261c7cb0f24377ca75b5f7116d4bce313b208ed41ed1b1649e634d63024ad23d336da29da9e3dc452f3219450e6e932eb36ed16b6cad64f2cd9ac8fce1ea9f48c435e9340949095922a58cefae67b45b83c72dff38944fd1a4eb295cbcca2c7eeaf686c90c1b3d20a2990980c826f728bdcf5d080218538a5a094e718ab3c003832a1272eebf5aaca655790f68995ee28db902493f486fc2b86641ea9bd36b96332945929726945d5ebbdf36809e82252ce089a9a505d4e79e775d50ca0360f99dc9f9c3e4a52d9a7b01965b094a4dac36b473a8ad6456ad44d59644c6525a4d0e7bb212004651a0b0f211943da0d09fdaedad26e54124ff28d55537d32ed0cca1aa957d7c231ef19eb0d56abaf9965666a5021b5106e0a8924fe905e227ea5cc9e63fe0b456f4e3df6afa8cd63e460727705293c89b8f2dc413204a976f1da15343f7d5057259f95e362479e878369fb21b26a788710e259c25d7a5e49a964ac8bd82136b7a0115dcdf0faafc68c4dc43c49465a1c1233cb421b50fe2e5d001f210f3ecd58e58c3384b1fb332e21b71329ef32e09b5c94a811ea923d22eaf647a3991e170a84c0b4c55a71c995df7209d0fa416198a14af4d4a7d7fab294e0071b66787556386717adc451cb85092f5ef2ab1ba4ff00b7a748b0382130d639f680c798c5b21e92956d2c4ab9fedb72f48f7da6b8232f354e9bc6587508626a550573ad8164b881b9f3827d9b928c25c0ec578a9eee198ed9c4a8e9dd426c3f28bf9b0b414e325096964afb1471c4b1896b2d36b250dcf3e9490ae41c223a33a3acae75d5ccbaa256f28b8af33a9fce3a2bd855d49b34870764e9f28fbcdd15c5cc94dcd46a3f8ec2ea4a0f33cbc2f144d527dca8e20aa4ead23b579d75645fe1b9d07c8691ab3184a53b86bc3f9c93a1b6dcb204b8966549162a2af8d44f905464265c512e2d23bcbcda1f2bc0678376843d38584eb69210d2b91205f7b6b0df3a8285a33e99ae75f38717bbb2ec01aebfdfd212d57bf30ddb51a81e26f60221176b014d33664757576bf80de1baa6a2a71c3e36f9da1f1c6bb371a692077136bf8f388fceab39bdef9944c4c7644f08410630fb92cf21f97596dd6cdd0a4ee0c00a7a47083ec5af6d17b608e22eac3156fb374212b6ddb77163af845ef46c4f2d312e8536ea4e97de323e097999c7954f9add2096543700ef6f9f2f1897a1351a23c8ecde5b4856895837413fa46256a2bb5b47abe2f21ce9a72358c8541a5baddd4929bdd249bd8c4b24e61928bab2f869ac64da27122a14b5a535064bad0fbedea3d39458548e2d53dd012eccb62fd556fa1849d29458faeb33421765db19bb5483bd8728619c9f6596f74d86fadf726f78ace678ab484214b54e36a20696d49f4880e2ee29cf4cb265e84d06dc78d83ae721cd444746126ed62b25182bb0ee33f12572285d328ab4aa75482a7163665274b9f1e82339cda9469eacc7317564a947524daf12cab4996e90a79f716fcc3d9dc79e59ba9649dcfa69e11119c0448a4116d49f9911ab462a3846272a4e5b1ae9c6cabf91f4bff2854bb26782beead0955bc2c213c90caa493e1fac289805265d640fe16536f0da1c7b32a3a2434da93b232d3c1b754da5d694cbe8074536a1adfc8807e463e9c70b294d50787987a49ab00cc836acc398223e5b49cd04bb98a42c2d1a856a08e63d2f1b17809c68a8cb60e7e8b579254e2f0eb0dbb2b32a72c5d93717911a1df29b249f089a4ecf27554add8b3fda9f101a0706ab210ac8f541499640bebde207e578af7884070ffd936934841eca62a0cb2c91b12564157eb12cc4b8e70b63eac4dd0b1dd0dd4d369443ad3ef11d8b8f04e6cb7ea21b31862ea3e3aa4d2a4dfc2c6a7252321efe52e3a9436ca3509df7245e18c37733e6d3bbf6b188c2458779223a349b15ae1738cb6b5e0ccaa524123ba6c48f38e815843aafbff91bfda46bea058a6b2a516da6421449d0b8b24abd122df38cf126a05c1f786729b79a7fac589c73ac8a9e2f9a4055d287540049bd827ba3ea1515ac8a8864b8afbae249f5dfd2072cb67a3d591ddd5328413ae748079f481ad90e4db0a70df22d4481d6133e4b4fbff8500a81b731b7ce0e54c67466d957528f8922dfac559cb218f1cab4b84eaa0b708e82d61f9c45a6cdde29e480043d393256b7144dc252103d2ffa4313e332dc201deff28bc760ea3c0581bc0529d7c2068babe512ec2f859da9153e480949b2730dcef7f944ca6a0aeca429ba8ec8418765dd95a93331aa5d41bdba0f1f18ba64c267a4d0ab077624581bfca20d4fa0bcc541f0f04e76c9075bf289a60e0db06558a96643730c871b583b1bd8c67d67db26df0e3e9fca0dec3897144cba14c7903947f48f59c0c5577675b0e290ab6642732478c5b6fd093354c5a187c0ca014f736f485f48604aa0768a714b23bc09005ff9466baad68d98c532a3182262a6ea112692f5b54902c123c79420ae513f643ed30fa92e3e124af20d0f4f945eb32f4bb72eb5a9a40b9b69dd315f5669f2b51a83eebca21a42720257a0037378e8556de499c2e8aa310b2a4d056b511a80078ea74881cfcbadaa4b4eba328528dbd22f0c494395461e4ccb60a194207679cdcac951d6dcb78a7b182942464a5daef38f139529dced6fac68d09767831f971b26c8d292519330b7d9206fcc8bc1ef10b63500948047e5f91fa428c4b2e996a95419468192847cc000c27ca56e25b4eee24240eba43b7ba4cc951b368594b93f7d7b24ba8e709cc84117ce0f2f3dc459dc389b9f553d72e82c999a3be26e510f3c1aed52143b5952ae695a47c0798044549233b31499c6de40c8eb4ad963c6e445fd47c3ec632926710e1e9c664aa01595c7943b5ccab6a8753a248e96b100c0a73f4ddde8629d355535e4d6986e8581f89b832604a4b2972d52576f352cf1fb66976b149f2dafe10e550e0ae13a84b4bcbb8c3edb2ccaa65825b7080b6c6c0f5119bb0e6279ec199555a53f871f4ad2b336c12fc83c41d095a45d1e4b02dd62f6a671b6459966ddaf4c493b2797319c967d196dd4826df58343934de1e05aa70a5b4ae3933c09c0b2ecb6d22909cada4245c7202d1d08d5ed21c2c4a885634a78503a8b28fe91d0cde1ec27e8aff8fe0f9f78e275535896a05c567752b21c5116bac1d7ea61965943b07afd414ebd4c755a64ccd4665f2a4abb6716ab816bdd5780a16512ef1005894800f32042fe06e4f2279c559bcdbe65d95e42ff00ce0b4af24b904df5d4c7b38acf2ee58dc919bf2feb095770d213b67886b072790d974f69a1bf7ae611be8c8b4850b7dd3e5d61c19414cb38e0d085000f96b0198289a6526d9556d6dcf944ac10d5f01344a63952a9b32ad24a96b5dade1b98d0b873090a55173b8b4f6c1449046853b7a1b8d629be1d32578a2552a39579826fcb717fa468771eb10cb60a82c16c8ea41dbd00f484b9127dac68f120942e426a14e799a897929eeb882826f7cd73fd7e8624d3786d68a7cb3694e554a20766e0d3cc18945330ba669932f3ca294ba73a560025b23657cb9c4970fb3ef12eb65fcaa71a259994116b2b6f4da1494f181e846cc8d60eae97b24a4ddc3894db53cfa44e9d930a62c2f908d0a4ea22055fa13b499d1392082100e6c97e5cf5eb138a54fa2768bdb20952bb3b8cdd6df9c275179468d3771b2632b4d384a73a90824136d3d62169684c38a0f0ee039943f17843a4f56a5a74e565d01973fde05ec75bf4208d47286e67b04cd76aecd36cb285ea4281d074e662229a415b427e21309630e069252928612a5003fdd14548b23126349443564c8521bed5e709d343727c35b0f944f78b18f1b986bf63d0d2e3f37340363323ed14906c001b8171b9b5fd4c46534c181f024fbb3012aa84f00852af7efab97c813af5bc69508b8433b7a31f932539dbc2cbff00440eb0f22a93b529b693d9b33332b523c119ee3e9685f48a77bfcd61c6c14833b505b08f1d11bfcc8f586fa8a449d1e41aff008cf24b8b27a13a0f403d626dc3591fda98df01c8a506cc4cae71ceeeb64a5275f0ba3eb0ec9da3fbff0083369c7e6cfb7e58a715f0c2a1213790325c6af64a86f115954620c1d32e2a9afcd49e7fe2042880ae9711b59f936de986814a0826c42869092a7842893aabcf5359591d05afe9082e434acd1a12a116eeb063c771e6294a4b62b352976cdc59b76c3c468223f2f47a955e66d21233336f386e74d093b93ca3622787f8525de2efec3962bd6c57756be44c2d6a86da72a69d2adcbb5f7121036f48bfc528fd2817c239bcc999551c1ac68b425429aca4285ec5c1a4746b3ff2a93a9df9f704740fe367ec5fe0699889f55d42c7606144c1c9209ca2cad3ea3fac2049ed4a47e3501ea61ce6d59d4d22f61daf4d85a358c4b891e6c644a6e6c5205fadf7848e9b3b94fdc1ac2d5d8be9680b8b1b5fce1b57de717e2ab7d6389f03ae5c94a4e61aa9c17f0bdf586d41285942a24756652891cc05926d63e1ca2341575249dd3a18859459e192cc089cd5c6942e95020820ed6d634f51641536da264e4292e2af992139556fd6d19530cbc599d0ea4ea82156eb1a6b0b62249944272a94a292a26db1241b7a5e33b937b9b1c54ba93c9495702cad56486c051401a9ea2e7c21214392936aa8c980a4dd41e45fe3403bdfa8de0b66bb9829c6fe259cb0f14f0c29a0876c126d99276319d7b647ed7c0ae7a5da9d63b3b7d93ad858246a5245c1f4311cc38b6a9eccdc8ce381010e1ecee774ea61c67109a63f6965a04aaec12917b0f0f4e50d357a64cbad19ea7b656eb57391a375280dec39e9ca230f1e02c2fe4aeeb183e6e7a6a72af86aa89a685be73b13412eb2e2badf707c7a4446a187718f639dea8d124db7d453da4a32b2a0073048b0f58b326eb3499a7a591489b613ef0b29532bb149e5a0ebe1bde14e2296a5d325de9aac54a5e5a56559c8865c50092ae7606c6fe10ca9ca36c7e08708bdbfc90dc25c3ea561ba1aeb536b72a154796734e3b73e76f0d7e7151e26ab2f12d6d3232e4ae4251ccc481bf2f5e43ce240fd7ab5c4798a7e11c06cbf32979cecc06d046759be8917ba8d86e6df21ac394b6086f075057559db213909652a58515288f8956dcf40341d49d61a8de1f34de588cfad4f929fd2b654f885f54dd50209012da7281c875f48ba7d9f28ea44ecfe299a6ca5a0d2a4e433731b2d43e82fe714ed0e8b338c311b3212b742a61656f39fe9377ba8fa46b4c39410cc94b48d31b2c53e59b0d329e6a03f524dc9eb17e44d420a02dc4a6ea4dd47a24ec54bde1e2f2ed648b017d204b9b7df58ecae45fd61d2470be5427de179529b5920758796a9b2f2aab64048d2f192ddcd6ea90c0cd296405cc907984a61e2559466d506d7b5a1716ee0948d0758219012e7302236768525951371a785e3a3dcd7d967e423a2f6067cdb64e5532a1619569878986c36fa730b917cde971f9c33e537640ef1cc2fca1ee649d6c7e32ab7d07e91e88f2c86b7886a7d81716240e90dae5d0b36dc2efe86144dba173e6cad12e2403f4fd20a994e49b5a4f2262494f649aa6f76d40614924839afadf5b123f588be6d35b122c987b91599ac3af300d96d9361d74d3f2b7ce18adf660ec39c4225e6cc93e15643ee4cdce8942556037b1d44690c0f2686649297920abba7293a002daf977aff003b4669c26efefea970092e8ba6dd46e3ea0fca351d1db4cb3d4c7dc59b76484f67c8dc5813f4b467f2166c6cf0dde372792b40012141a00ab529b5b7dcfa439a29094b2824848df783d89d0b0852c1f805cf87842c42d5d9a6e9b922e41e578c97135a371a9ea532f32e4bcc02b65d1955a906de7ca0999138c4d2fb42a9a69db29b71b652929ca90354a1205c017bdb5b927587b4b1a8502143342950752dde5dc0dbf625b732dca55d6d1cb187a264bcad94762bc0d2789aa6e3f3f4333009ccb7a5925a5acf42536d7c557f388faf85f85a55599cc273b509e1a9f789b75c424f9150bdbc6f1a156e32a9a0d341d65496904b4b566208165595a66048cdb699adcb54b5194bb4e12a524a859091cd5cafa6d04f56717d53fc83e94e6bb4a3fb95152a99352b2486291431212f2b6bb6c349683a4ee54a1cb5ea49b98ae78c5373ef893939b712da148d196fe14802e45be623480a7a65649c42dcbb7aad677d6daee6323713311375ac5d3cb91482cb1fbb4b817ef926f7fa81e421ae3fcf53fa0b72a5d297bb2c0e03e0b069af555c6fbd36f7669246cd24f2f3378d332526c4a842025296c5ae40b7a445786f87050f0ad26548ba9a964056961988ba8f9dc92625e9732a45b5d35300ad3739b61a8d3e94d44509d0e9620aae078080bce586636ea6d09fb652c009ef781808515b96cb709d07426041ac284af28d48ef1d40309dd364a8a959527527a08f4937ee68398e660b78e76c802c76b5a2e91462a496ca41049168e86f0955be35ff00fb8e8b7f70563e79a05dd96b1b9cc34eb73073b32a521685dc10ab0f0b9848dbb95d65606a0836bc0e6d23bcb41b5d43f28df3cbfb8df7ccf83f895785353459d6dd4dc85a7527ac256f5713a6dca15ce2d4a642149ef170dbd22de4aad0a68ce04226db57c2b482078c14eb03b37549ef249d088f245876eb3b008378085a96d2db26f6d840dec3c563228a0bea93add39c41f86650836e8ad0fd0c6b24a56691384a54da9b6cb89e6414ea353e5f48cb184648d4f14d2a46e13dace34e157e14a2ea27d046ba906ccc50945d073a9b73b4b8dd4a19426df3fac29c9da35382af064d6490a2cb0563e1362907c2fac3c34a4dc937e62d9af0d72c7b3284ad445b4d74e56061c517fba6d6defd7ce325e19b02d6920d8ea81b8237ebb4296ca137538b04ab9f8437373685a05fbc8cc2e41daf0cb2188d9949d7a8f3ce86a725c92c17b679abe8abc4bc9c48e6e59b7f2d97d93ed9ce851d2dfd3ac231312d38cd3dd4cacca5530c2663b4986cb494aaea4a9a47dd5804039f7b286820a9c9d4a9b5b534d38f34b07b32c90b51b8e9b902fe315fe32e22268345c3c664d4e414cd35f4cd05217771eedca6c3b64274c884926e7555868041a9d35284afbf02956728d485b5e7f6c09f8c18da4f0cd1a65aceda5e75052129577afb0d7af87ac663e1b515ec638f69b2ee6a90e99a98f048373ea748438c7144de2aa8aa6a716e6426eda56bcca3d0abc6d611a07d9fb022a834c76b1526489e9c502014925b6c6c9f3d6fff00886d254293fbb14727c9aeada45f52e112ac8693aa11b01ca0a0e660a2ab905455ea7941656a39bb85090ab586e4c7ae2c8b20685475bfe919c6ba0c4a8a42528558e6bdfa0e90626c1277bdf9c2548192e48255a9b9832e424731b455e4ab0c20909b0ca46c60a52ac6da0e6493061b6ba9b0d2da9f9c14a1dcb8207ca2eae56c145c4dcf789f1eb1d030176161a7947454a60f9d2dabbe9e563e9037dd056d81b6aae9e5040363a18f177cc0a86c2da47a5b1e4d33d6d1651737093787865954da9b200590917eb73b9bc3421f190364002f751eb0f94f9f9193ccf2df4a54903220a7324dbf101adfa45249d8253b5f23c4ac836f28356090bba15c89de21ab0a969e75a7374a8a4906f0eefd794ea142452b69253954ea8f788df4e90caf23e07920db6f38ac22d6c2d49c5dade0b4f82b85d751afbf5279a3eeec24b0daadf7956cc479274f9c68f945fbc4f89797199861d0e3a949d88f811ebde3e422b9e16d15f387a51cf792cc938da484b2dd96abea4151e773b81ac5c348926e46510d32c80da6e2c9d335cea49dcf998cdad3ed236f8f054e0921d90d96d04286649172a235b810a92b0b4a927517d094d8010986529cc0248d0180a1d56a42b648008d611638986ccc9a94bcf2ce65591650fbaa88562fc37315c92432fb0fadd697fbabcc10975a5936012ad8dcdb43a7589ba5f045b702144acd4bc854a9b3d3cdb73126c4d36669970f70b64e52af028be71e2817d22d07692226ed16d1425513c4dc2027a9d832b935392b353ce535537272859989a534d053dd9a89252ca3be14b1940c97511748882d5b863c51c40a4cf55e8b556d872d92767d45799b3619cad6b24b62e9bb8744a569512126f1b8197a5e5da9a6a624a6259d97a94dc94e4aa5f6bb1614961b6db6029693695714a6dd4ad432875490ee60bcd1cdc855304a2aa8f7a909f989b756c36e4a4a34f4d4c540b292975d6d44669e712a71b5369ee06eea71494245f6d452479d9547279337613f667630b4e3b37891e54cd529b3deeb3326fa120b4e14071b712904852140900df741ea22e4979444a3496da480d8f958c4c6bb3f2d38de2453b4b476f2b36cd3e5273b30db6c5895994484fc6594a49ce74fb429174804c65c55b4d3a5fc233795f5effedcd6e1caf4b56ffc42370916ef585cea04166c91ddeedcdf5e70379eb2ac028026dd3eb095c512125673280d6e2e0c263e8f73151006d73a1dc7ca0e43963651398db4b4264929ef148d7730a40ba6f9546e9b69fca251cc508d1b02e55aee440568ee6b7d473b6a3a4168405274040e64f86bb405c5650a1716e5e3e51706d8414db7041e91d049009272a47fd51d15067cf14be05ee14401af28e5bca76c8d0239240bfd600e687fe981303ed63d29e4fcd89253b0d7bdc8ad4504b9a652156009d2c6154a600abced42664e59b61c718ca4e77c20588b8e5121c35dd025c13d932c76ad8fc249b1f96a7d4c4b70fa89c435078ff112b4241f0ecc1fce3ac36a11b10ea6709a75e7569abcd2649bb654a99fb7ca7aab6d210cae0d451eb6e50f19b8897969f4a7dd6a6d1cc86d57ee9bf20763e71a052d2436b5daea4dac4f90fe6622f53a7cb542a3292738d25e965b6a19142e00d4dbcae2f1d28271b20918c532cfc23849ca253e5e5bb6289669294308d5494a39107637def13644bf6695595a917001d3fb3146606ac4f61aae0a0c84cbae52b205b6c3eb2b0d6a9d11ae89ef6d17acbcc28ba84d9202d249b263cfd54e32699af092681ad000d5200f0f184ca6fb273537493a007687655bb349ca9ef2413a7584ca48baec2dde034e900bdc35c6d500dea74293f7469ff88f42db710a44ca73a149295050b820f58326db0da5d5a4ab33672a75d8184530a2db872ffa615af524c734594891ab144e4bc8b05e336dcfcb256d0a8b12ad4e266658a523b09b955d8bc92940415a141652945ee520c39e2cc494caebcdb9fe6a989b6e6a5ca96dcb61d6e76690c38901726871e692d36d1b77bb50a74e5485aac2c62ad29450824ef7fca02e3eb2db59acaba6daf286a3cba918db62cf894a52ecb1fb7fa154c56672ad38a7e65a6e4a5c294b6a4da50210b70dd6a5280014e28daea0120001290120080198dae7bc0fc3cc0844a23284e5005ca6d05382caf21610aca729becc72318c1758e854a7ca96024e86e34d6390502c57996ae563b986e4ac929f383db5905074f849b78f58a9616a59dee458f23d214a50b1948274e5b5c4148d557db68528398a39036d22f1052933882754a6d63620f942259ec96aef7c66c3fdb6d2f0ac92a514dca4241b58dbac267d20bcabeb6493af95e2e81dc4a51a9fe27ca3a12875761afd23a248b9ffd9	image/jpeg
\.


--
-- TOC entry 3036 (class 0 OID 16507)
-- Dependencies: 214
-- Data for Name: spent; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.spent (id, label, description, amount, spent_date, last_update_date, confirmed, spender_id, wallet_id) FROM stdin;
10000	WE Douarnenez	\N	51.7799988	2018-03-25	2018-03-25	t	1201	1301
10001	Courses Géraldine	\N	18.6100006	2018-03-28	2018-03-28	t	1201	1301
10002	Ikks shopping	\N	187.300003	2018-03-29	2018-03-29	t	1202	1301
10003	Ikks remboursement	\N	187.300003	2018-03-29	2018-03-29	t	1201	1301
10004	Pain	\N	2.5	2018-03-29	2018-03-29	t	1201	1301
10005	Especes	\N	80	2018-03-31	2018-03-31	t	1201	1301
10006	Pain	\N	2.70000005	2018-04-01	2018-04-01	t	1201	1301
10007	Courses semaine ense	\N	22	2018-04-01	2018-04-01	t	1202	1301
10008	1 vin +6bieres	\N	14.5	2018-04-01	2018-04-01	t	1202	1301
10009	Vin	\N	21.8999996	2018-04-06	2018-04-06	t	1201	1301
10010	Apero	\N	10.5	2018-04-07	2018-04-07	t	1201	1301
10011	Resto	\N	58.6599998	2018-04-07	2018-04-07	t	1201	1301
10012	Boucher vendredi	\N	15.4300003	2018-04-06	2018-04-06	t	1202	1301
10013	Maison tellier	\N	39.2000008	2018-04-08	2018-04-08	t	1201	1301
10014	Pain	\N	4.1500001	2018-04-11	2018-04-11	t	1201	1301
10015	Courses drive	\N	31.5599995	2018-04-11	2018-04-11	t	1201	1301
10016	Bilan remboursement	\N	230	2018-04-12	2018-04-12	t	1202	1301
10017	Chlorophylle saledi	\N	9.75	2018-04-14	2018-04-14	t	1201	1301
10018	Picard	\N	18.8999996	2018-04-14	2018-04-14	t	1201	1301
10019	Boucher	\N	30.3299999	2018-04-14	2018-04-14	t	1202	1301
10020	Liquide	\N	80	2018-04-15	2018-04-15	t	1201	1301
10021	Courses semaine ense	\N	47.5	2018-04-07	2018-04-07	t	1202	1301
10022	Olives pour Yeu	\N	4.55000019	2018-04-14	2018-04-14	t	1202	1301
10023	Huitres	\N	6	2018-04-22	2018-04-22	t	1201	1301
10024	Boulangerie	\N	5.05000019	2018-04-22	2018-04-22	t	1201	1301
10025	Resto	\N	118	2018-04-21	2018-04-21	t	1202	1301
10026	Cine	\N	11	2018-04-22	2018-04-22	t	1201	1301
10027	Café nespresso yeu	\N	33.7999992	2018-04-24	2018-04-24	t	1202	1301
10028	Courses semaine ense	\N	73.5999985	2018-04-20	2018-04-20	t	1202	1301
10029	Vin 3bt +bieres	\N	27.7999992	2018-04-20	2018-04-20	t	1202	1301
10030	Course	\N	177.460007	2018-04-28	2018-04-28	t	1201	1301
10031	Courses	\N	60.6300011	2018-04-30	2018-04-30	t	1201	1301
10032	Courses petit dej	\N	15	2018-04-27	2018-04-27	t	1201	1301
10033	Courses	\N	116.5	2018-05-02	2018-05-02	t	1202	1301
10034	Liquide	\N	40	2018-05-02	2018-05-02	t	1202	1301
10035	Parking	\N	34.2000008	2018-05-04	2018-05-04	t	1201	1301
10036	Cadeau parents	\N	54	2018-05-04	2018-05-04	t	1202	1301
10037	Resto	\N	46.9000015	2018-05-05	2018-05-05	t	1201	1301
10038	Courses	\N	32.0800018	2018-05-05	2018-05-05	t	1201	1301
10039	Pot midi	\N	6.80000019	2018-05-06	2018-05-06	t	1201	1301
10040	Restaurant dimanche	\N	40.5999985	2018-05-06	2018-05-06	t	1202	1301
10041	Vin blanc	\N	9.5	2018-05-06	2018-05-06	t	1202	1301
10042	Pot midi	\N	6.0999999	2018-05-07	2018-05-07	t	1201	1301
10043	Apéro dimanche soir	\N	24.5	2018-05-06	2018-05-06	t	1202	1301
10044	Cinéma	\N	9.80000019	2018-05-08	2018-05-08	t	1202	1301
10045	Cinéma	\N	11	2018-05-10	2018-05-10	t	1201	1301
10046	Vin samedi soir	\N	9.5	2018-05-12	2018-05-12	t	1202	1301
10047	Saucisses	\N	14.5	2018-05-12	2018-05-12	t	1201	1301
10048	Courses samedi soir	\N	7	2018-05-12	2018-05-12	t	1202	1301
10049	Épilation	\N	3.5999999	2018-05-12	2018-05-12	t	1202	1301
10050	Courses semaine à 2	\N	45.4000015	2018-05-04	2018-05-04	t	1202	1301
10051	Vin soirée berdaguer	\N	12.9499998	2018-05-18	2018-05-18	t	1202	1301
10052	Courses apéro berdag	\N	7.80000019	2018-05-18	2018-05-18	t	1202	1301
10053	Courses pique nique	\N	13.1999998	2018-05-18	2018-05-18	t	1202	1301
10054	Courses	\N	11.4799995	2018-05-19	2018-05-19	t	1201	1301
10055	Apero	\N	29.2999992	2018-05-19	2018-05-19	t	1201	1301
10056	Pot	\N	7.80000019	2018-05-20	2018-05-20	t	1201	1301
10057	Fromager halles	\N	13.6999998	2018-05-20	2018-05-20	t	1202	1301
10058	Charcuterie halles	\N	14.8999996	2018-05-20	2018-05-20	t	1202	1301
10059	Cine	\N	11	2018-05-21	2018-05-21	t	1202	1301
10060	8 btls Jules Verne	\N	52.1500015	2018-05-26	2018-05-26	t	1202	1301
10061	Boucher	\N	36.5099983	2018-05-26	2018-05-26	t	1202	1301
10062	Courses semaine à 2	\N	21.5	2018-05-22	2018-05-22	t	1202	1301
10063	Vin + bières semaine	\N	16.2000008	2018-05-22	2018-05-22	t	1202	1301
10064	Cinéma	\N	9.60000038	2018-06-03	2018-06-03	t	1202	1301
10065	Karting	\N	29	2018-06-09	2018-06-09	t	1201	1301
10066	Boucher samedi soir	\N	38.4799995	2018-06-09	2018-06-09	t	1202	1301
10067	Courses semaine à 2	\N	92.9000015	2018-06-04	2018-06-04	t	1202	1301
10068	Argent paloma	\N	18	2018-06-21	2018-06-21	t	1201	1301
10069	Boucher	\N	21.9500008	2018-06-21	2018-06-21	t	1201	1301
10070	Vin	\N	12.1000004	2018-06-21	2018-06-21	t	1201	1301
10071	Sucre bio	\N	3.29999995	2018-06-22	2018-06-22	t	1202	1301
10072	Remboursement	\N	200	2018-06-22	2018-06-22	t	1201	1301
10073	Play gel	\N	3.8499999	2018-06-23	2018-06-23	t	1202	1301
10074	Boucher	\N	14.3000002	2018-06-23	2018-06-23	t	1202	1301
10075	Courses semaine à 2	\N	81	2018-06-18	2018-06-18	t	1202	1301
10076	Resto la prison	\N	59.2999992	2018-06-30	2018-06-30	t	1201	1301
10077	Glace	\N	9	2018-06-30	2018-06-30	t	1201	1301
10078	Apero	\N	18	2018-06-30	2018-06-30	t	1201	1301
10079	Courses samedi soir	\N	12	2018-07-07	2018-07-07	t	1202	1301
10080	Boucher samedi soir	\N	72.3899994	2018-07-07	2018-07-07	t	1202	1301
10081	Cadeau Paloma	\N	14.8999996	2018-07-07	2018-07-07	t	1202	1301
10082	Parking	\N	18.6000004	2018-07-16	2018-07-16	t	1201	1301
10083	Courses semaine ense	\N	69.5	2018-07-01	2018-07-01	t	1202	1301
10084	Gâteaux +vin martine	\N	18.7000008	2018-07-18	2018-07-18	t	1202	1301
10085	Courses semaine à 2	\N	57.2000008	2018-07-17	2018-07-17	t	1202	1301
10086	Courses vacances	\N	113	2018-07-20	2018-07-20	t	1202	1301
10087	Courses	\N	224.710007	2018-07-23	2018-07-23	t	1201	1301
10088	Boucher	\N	29.8999996	2018-07-24	2018-07-24	t	1201	1301
10089	Courses	\N	26.8299999	2018-07-26	2018-07-26	t	1201	1301
10090	Courses	\N	128.740005	2018-07-27	2018-07-27	t	1201	1301
10091	Musée soulages	\N	22	2018-07-26	2018-07-26	t	1202	1301
10092	Short celio	\N	23.3999996	2018-07-21	2018-07-21	t	1202	1301
10093	Courses	\N	32.9199982	2018-07-30	2018-07-30	t	1201	1301
10094	Autoroute	\N	15.5	2018-08-01	2018-08-01	t	1201	1301
10095	Picnic	\N	8	2018-08-01	2018-08-01	t	1201	1301
10096	Foués mauves	\N	20	2018-08-02	2018-08-02	t	1202	1301
10097	Resto	\N	71	2018-08-03	2018-08-03	t	1201	1301
10098	Super u +viande	\N	13.6999998	2018-08-04	2018-08-04	t	1202	1301
10099	Marché indien+pain	\N	18.5	2018-08-05	2018-08-05	t	1202	1301
10100	Crepière	\N	23.8999996	2018-08-06	2018-08-06	t	1202	1301
10101	Apéro mauves +rosé	\N	29.3999996	2018-08-02	2018-08-02	t	1202	1301
10102	Bière goudale rhum	\N	4.30000019	2018-08-06	2018-08-06	t	1202	1301
10103	Courses semaine ense	\N	56.2000008	2018-08-07	2018-08-07	t	1202	1301
10104	Péages	\N	37	2018-08-08	2018-08-08	t	1201	1301
10105	Paul	\N	6.4000001	2018-08-08	2018-08-08	t	1201	1301
10106	Luna	\N	26	2018-08-08	2018-08-08	t	1201	1301
10107	Boisson avion + bus	\N	17	2018-08-08	2018-08-08	t	1202	1301
10108	Cafe	\N	16.2000008	2018-08-08	2018-08-08	t	1201	1301
10109	Café	\N	6.5999999	2018-08-08	2018-08-08	t	1202	1301
10110	Pain	\N	6.19999981	2018-08-08	2018-08-08	t	1202	1301
10111	Courses	\N	24.0699997	2018-08-08	2018-08-08	t	1201	1301
10112	Vin	\N	22.3799992	2018-08-08	2018-08-08	t	1201	1301
10113	Pot	\N	15.5	2018-08-08	2018-08-08	t	1201	1301
10114	Cafe	\N	6.9000001	2018-08-09	2018-08-09	t	1202	1301
10115	Lunch	\N	37.2999992	2018-08-09	2018-08-09	t	1201	1301
10116	Croisiere	\N	50	2018-08-09	2018-08-09	t	1201	1301
10117	Pot	\N	16.7999992	2018-08-09	2018-08-09	t	1201	1301
10118	Courses	\N	37.9799995	2018-08-10	2018-08-10	t	1201	1301
10119	Poisson	\N	48.7999992	2018-08-10	2018-08-10	t	1201	1301
10120	Saumon	\N	15	2018-08-10	2018-08-10	t	1201	1301
10121	Cafe	\N	6.9000001	2018-08-10	2018-08-10	t	1202	1301
10122	Grec	\N	23.2000008	2018-08-10	2018-08-10	t	1201	1301
10123	Resto nepalais	\N	32	2018-08-10	2018-08-10	t	1201	1301
10124	Vin	\N	20.8700008	2018-08-10	2018-08-10	t	1202	1301
10125	Cafe	\N	6.5999999	2018-08-10	2018-08-10	t	1202	1301
10126	Pain	\N	6.19999981	2018-08-10	2018-08-10	t	1202	1301
10127	Cafe	\N	6.9000001	2018-08-11	2018-08-11	t	1201	1301
10128	Tram	\N	4.4000001	2018-08-11	2018-08-11	t	1201	1301
10129	Cafe	\N	6.9000001	2018-08-12	2018-08-12	t	1201	1301
10130	Musée	\N	24	2018-08-12	2018-08-12	t	1201	1301
10131	Dej	\N	8.64000034	2018-08-12	2018-08-12	t	1201	1301
10132	Cafe	\N	6.9000001	2018-08-12	2018-08-12	t	1201	1301
10133	Bateau	\N	8.80000019	2018-08-12	2018-08-12	t	1201	1301
10134	Piscine Sauna	\N	28	2018-08-11	2018-08-11	t	1201	1301
10135	Saumon	\N	16	2018-08-09	2018-08-09	t	1201	1301
10136	Bifton	\N	40	2018-08-13	2018-08-13	t	1201	1301
10137	Boulangerie	\N	9.60000038	2018-08-13	2018-08-13	t	1201	1301
10138	Glaces	\N	17.7999992	2018-08-13	2018-08-13	t	1201	1301
10139	Courses	\N	16.6000004	2018-08-13	2018-08-13	t	1201	1301
10140	Pizza	\N	5	2018-08-14	2018-08-14	t	1201	1301
10141	Tram	\N	4.4000001	2018-08-14	2018-08-14	t	1201	1301
10142	Bus	\N	11	2018-08-14	2018-08-14	t	1202	1301
10143	Coke	\N	3	2018-08-14	2018-08-14	t	1202	1301
10144	Diner	\N	19.4500008	2018-08-14	2018-08-14	t	1201	1301
10145	Parking	\N	60	2018-08-14	2018-08-14	t	1201	1301
10146	Péages	\N	37	2018-08-15	2018-08-15	t	1201	1301
10147	Vanola traiteur	\N	20	2018-08-16	2018-08-16	t	1202	1301
10148	Dessert et pain	\N	6.9000001	2018-08-16	2018-08-16	t	1202	1301
10149	Vin +bieres	\N	12.1499996	2018-08-16	2018-08-16	t	1202	1301
10150	Virement rembourseme	\N	300	2018-08-16	2018-08-16	t	1202	1301
10151	Petit casino course	\N	36.5299988	2018-08-25	2018-08-25	t	1202	1301
10152	Cave à vins	\N	52.0999985	2018-08-25	2018-08-25	t	1202	1301
10153	Gâteau	\N	7.0999999	2018-08-25	2018-08-25	t	1201	1301
10154	Cine	\N	11	2018-08-26	2018-08-26	t	1201	1301
10155	Pot avec catherine	\N	51.7999992	2018-08-31	2018-08-31	t	1201	1301
10156	Boucher	\N	10.2299995	2018-08-30	2018-08-30	t	1202	1301
10157	Courses semaine à 2	\N	39	2018-08-25	2018-08-25	t	1202	1301
10158	Boucher week end	\N	25.8299999	2018-08-31	2018-08-31	t	1202	1301
10159	Courses repas we	\N	28	2018-08-31	2018-08-31	t	1202	1301
10160	Framboises	\N	6.5	2018-08-31	2018-08-31	t	1202	1301
10161	Vanola	\N	57.5	2018-09-06	2018-09-06	t	1201	1301
10162	Cadeau Mathilde	\N	25	2018-09-08	2018-09-08	t	1202	1301
10163	Boucher vendredi	\N	19.1000004	2018-09-07	2018-09-07	t	1202	1301
10164	Huitre	\N	16	2018-09-09	2018-09-09	t	1201	1301
10165	Snack	\N	9	2018-09-09	2018-09-09	t	1202	1301
10166	Repas	\N	17	2018-09-09	2018-09-09	t	1201	1301
10167	Boisson	\N	3	2018-09-09	2018-09-09	t	1202	1301
10168	Glaces	\N	7	2018-09-09	2018-09-09	t	1202	1301
10169	Cine	\N	11	2018-09-09	2018-09-09	t	1201	1301
10170	Cadenas	\N	5.80000019	2018-09-15	2018-09-15	t	1201	1301
10171	Courses	\N	26	2018-09-15	2018-09-15	t	1201	1301
10172	Apéro chez Mathilde	\N	10.5	2018-09-08	2018-09-08	t	1202	1301
10173	Courses semaine à 2	\N	60.7000008	2018-09-08	2018-09-08	t	1202	1301
10174	Traiteur	\N	35	2018-09-22	2018-09-22	t	1201	1301
10175	Péage	\N	6.80000019	2018-09-22	2018-09-22	t	1201	1301
10176	Tarte citron	\N	7.5999999	2018-09-22	2018-09-22	t	1201	1301
10177	Courses	\N	18.5	2018-09-22	2018-09-22	t	1201	1301
10178	Cinéma	\N	9.60000038	2018-09-23	2018-09-23	t	1202	1301
10179	Concert stereolux ad	\N	43.2000008	2018-09-26	2018-09-26	t	1202	1301
10180	Cinéma	\N	5.5	2018-09-26	2018-09-26	t	1202	1301
10181	Café deca moulu	\N	4.4000001	2018-09-28	2018-09-28	t	1202	1301
10182	Gomme à daim	\N	5.05000019	2018-09-28	2018-09-28	t	1202	1301
10183	Brosse à daim	\N	5.25	2018-09-30	2018-09-30	t	1202	1301
10184	Courses semaine à 2	\N	35	2018-09-30	2018-09-30	t	1202	1301
10185	Resto	\N	51.7999992	2018-10-06	2018-10-06	t	1201	1301
10186	Reveil	\N	30	2018-10-06	2018-10-06	t	1201	1301
10187	La belle iloise	\N	16.5	2018-10-06	2018-10-06	t	1201	1301
10188	Amazon	\N	28.4500008	2018-10-07	2018-10-07	t	1201	1301
10189	Cine	\N	11	2018-10-07	2018-10-07	t	1201	1301
10190	Parking	\N	6.80000019	2018-10-07	2018-10-07	t	1201	1301
10191	Ciné	\N	11	2018-10-09	2018-10-09	t	1201	1301
10192	Ciné	\N	5.5	2018-10-14	2018-10-14	t	1201	1301
10193	Resto	\N	38	2018-10-14	2018-10-14	t	1201	1301
10194	Parking	\N	5.5	2018-10-14	2018-10-14	t	1201	1301
10195	Boucher	\N	16	2018-10-11	2018-10-11	t	1202	1301
10196	Hôtel pornic-remb	\N	0	2018-10-18	2018-10-18	t	1202	1301
10197	Vin semaine à 2 2btl	\N	15.6000004	2018-10-12	2018-10-12	t	1202	1301
10198	Courses semaine à 2	\N	86.5	2018-10-12	2018-10-12	t	1202	1301
10199	Caviste 6 btles	\N	54.4500008	2018-10-19	2018-10-19	t	1202	1301
10200	Crêperie	\N	37.7999992	2018-10-20	2018-10-20	t	1201	1301
10201	Pot	\N	15	2018-10-20	2018-10-20	t	1201	1301
10202	Maison de la presse	\N	28.7999992	2018-10-21	2018-10-21	t	1202	1301
10203	Crêperie	\N	46.2000008	2018-10-21	2018-10-21	t	1201	1301
10204	Resto le retz	\N	76	2018-10-23	2018-10-23	t	1201	1301
10205	Resto	\N	51.4000015	2018-10-25	2018-10-25	t	1201	1301
10206	Cine	\N	6.5	2018-10-25	2018-10-25	t	1201	1301
10207	Viande	\N	21.4599991	2018-10-27	2018-10-27	t	1201	1301
10208	Courses+boissons sam	\N	31.7999992	2018-10-28	2018-10-28	t	1202	1301
10209	Courses semaine à 2	\N	52.9000015	2018-10-28	2018-10-28	t	1202	1301
10210	Parking	\N	4.80000019	2018-11-03	2018-11-03	t	1201	1301
10211	Delivroo	\N	35	2018-11-03	2018-11-03	t	1201	1301
10212	Apéro	\N	7	2018-11-03	2018-11-03	t	1201	1301
10213	Super u	\N	28.9300003	2018-11-03	2018-11-03	t	1201	1301
10214	Viande	\N	16.6100006	2018-11-03	2018-11-03	t	1201	1301
10215	Cine	\N	16	2018-11-04	2018-11-04	t	1201	1301
10216	Chocolats cadeau	\N	9.5	2018-11-11	2018-11-11	t	1202	1301
10217	Boucher	\N	12.6999998	2018-11-08	2018-11-08	t	1202	1301
10218	Produit épilation	\N	4.5	2018-11-06	2018-11-06	t	1202	1301
10219	4 revues archi	\N	33.2000008	2018-11-10	2018-11-10	t	1202	1301
10220	Courses semaine à 2	\N	54.5999985	2018-11-06	2018-11-06	t	1202	1301
10221	Train	\N	97.2699966	2018-11-17	2018-11-17	t	1201	1301
10222	Parking aeroport	\N	37.9000015	2018-11-19	2018-11-19	t	1201	1301
10223	Vin	\N	20.2299995	2018-11-23	2018-11-23	t	1201	1301
10224	Revues	\N	18.8999996	2018-11-23	2018-11-23	t	1201	1301
10225	Pot	\N	7.82999992	2018-11-23	2018-11-23	t	1201	1301
10226	Metro	\N	4.05000019	2018-11-23	2018-11-23	t	1201	1301
10227	Courses semaine à 2	\N	34	2018-11-28	2018-11-28	t	1202	1301
10228	Inde et vous	\N	34.4000015	2018-11-30	2018-11-30	t	1201	1301
10229	2 thés mariage frère	\N	16	2018-12-01	2018-12-01	t	1202	1301
10230	Huîtres +pain	\N	8.5	2018-12-02	2018-12-02	t	1202	1301
10231	The Jasmin	\N	15	2018-12-03	2018-12-03	t	1202	1301
10232	3 bouteilles raclett	\N	29.7999992	2018-12-07	2018-12-07	t	1202	1301
10233	Boucher	\N	13.1499996	2018-12-06	2018-12-06	t	1202	1301
10234	Fromage	\N	21	2018-12-08	2018-12-08	t	1201	1301
10235	Boucher raclette	\N	22.5	2018-12-09	2018-12-09	t	1202	1301
10236	Parking	\N	3	2018-12-13	2018-12-13	t	1202	1301
10237	Courses semaine à 2	\N	73.0999985	2018-12-07	2018-12-07	t	1202	1301
10238	2 btles semaine à 2	\N	12.3000002	2018-12-06	2018-12-06	t	1202	1301
10239	Cave à vin 6btl	\N	54.5	2018-12-14	2018-12-14	t	1202	1301
10240	Produits de la mer	\N	14.4499998	2018-12-14	2018-12-14	t	1202	1301
10241	Huître et pain	\N	11	2018-12-16	2018-12-16	t	1201	1301
10242	Architecture Bois AB	\N	65.6999969	2018-12-16	2018-12-16	t	1201	1301
10243	Courses Noël +2vins	\N	34.7000008	2018-12-22	2018-12-22	t	1202	1301
10244	Courses samedi +vin	\N	22.9500008	2018-12-22	2018-12-22	t	1202	1301
10245	Pain	\N	8.69999981	2018-12-23	2018-12-23	t	1202	1301
10246	Fromage	\N	14.9700003	2018-12-23	2018-12-23	t	1202	1301
10247	Charcuterie	\N	10.6300001	2018-12-23	2018-12-23	t	1201	1301
10248	Pain arabe +mache	\N	4.19999981	2018-12-23	2018-12-23	t	1202	1301
10249	Cine	\N	9	2018-12-23	2018-12-23	t	1201	1301
10250	Apéro	\N	6.5999999	2018-12-24	2018-12-24	t	1202	1301
10251	Boucher	\N	62.0499992	2018-12-24	2018-12-24	t	1202	1301
10252	Courses semaine à 2	\N	94.5	2018-12-21	2018-12-21	t	1202	1301
10253	Saumon artisanal	\N	26	2018-12-30	2018-12-30	t	1202	1301
10254	Pharma	\N	10	2018-12-30	2018-12-30	t	1201	1301
10255	Cinéma	\N	5.5	2018-12-30	2018-12-30	t	1202	1301
10256	Courses jda	\N	74.2399979	2018-12-31	2018-12-31	t	1201	1301
10257	Cinéma	\N	11	2019-01-01	2019-01-01	t	1202	1301
10258	2 vins blancs reveil	\N	18.7000008	2019-01-02	2019-01-02	t	1202	1301
10259	Resto	\N	69	2019-01-02	2019-01-02	t	1201	1301
10260	Inde et vous	\N	21.5	2019-01-03	2019-01-03	t	1201	1301
10261	Courses samedi	\N	16.3500004	2019-01-05	2019-01-05	t	1202	1301
10262	Boucher samedi	\N	13.8299999	2019-01-05	2019-01-05	t	1202	1301
10263	Courses semaine à 2	\N	86.9000015	2019-01-02	2019-01-02	t	1202	1301
10264	2 vins semaine à 2	\N	13.5	2019-01-02	2019-01-02	t	1202	1301
10265	Pot	\N	40	2019-01-11	2019-01-11	t	1201	1301
10266	Resto	\N	54	2019-01-11	2019-01-11	t	1201	1301
10267	Courses appartement	\N	23.9400005	2019-01-12	2019-01-12	t	1202	1301
10268	HCB	\N	18	2019-01-12	2019-01-12	t	1201	1301
10269	Dejeuner	\N	27.7000008	2019-01-12	2019-01-12	t	1201	1301
10270	2 cafés	\N	6.5999999	2019-01-13	2019-01-13	t	1202	1301
10271	Traiteur marocain	\N	48.4000015	2019-01-13	2019-01-13	t	1202	1301
10272	Pain	\N	2.29999995	2019-01-13	2019-01-13	t	1202	1301
10273	Pizzeria	\N	49.5	2019-01-13	2019-01-13	t	1201	1301
10274	Vins cadeau Gregory	\N	36.7999992	2019-01-14	2019-01-14	t	1202	1301
10275	Inde et vous	\N	28	2019-01-18	2019-01-18	t	1201	1301
10276	Courses semaine à 2	\N	23.7000008	2019-01-18	2019-01-18	t	1202	1301
10277	Uber eats	\N	15	2019-01-26	2019-01-26	t	1201	1301
10278	Ciné	\N	11	2019-01-28	2019-01-28	t	1201	1301
10279	Tableau Trystan	\N	72	2019-01-11	2019-01-11	t	1201	1301
10280	Manolo	\N	10	2019-01-31	2019-01-31	t	1202	1301
10281	Delivroo	\N	38.5	2019-01-31	2019-01-31	t	1201	1301
10282	3 tisanes	\N	7.3499999	2019-02-01	2019-02-01	t	1202	1301
10283	Gâteau anniv Barth	\N	53.4000015	2019-02-02	2019-02-02	t	1201	1301
10284	Parking	\N	2.70000005	2019-02-03	2019-02-03	t	1202	1301
10285	Courses semaine à 2	\N	74	2019-02-03	2019-02-03	t	1202	1301
10286	Vin semaine à 2-2btl	\N	15.9499998	2019-02-03	2019-02-03	t	1202	1301
10287	Cine	\N	7.0999999	2019-02-09	2019-02-09	t	1202	1301
10288	Pain +huitres	\N	9.5	2019-02-10	2019-02-10	t	1202	1301
10289	Livre SCI	\N	18	2019-02-18	2019-02-18	t	1202	1301
10290	Bières + vin (2)sema	\N	25.7000008	2019-02-18	2019-02-18	t	1202	1301
10291	Courses semaine à 2	\N	95.5	2019-02-18	2019-02-18	t	1202	1301
10292	Courses anniversaire	\N	46.6500015	2019-02-19	2019-02-19	t	1202	1301
10293	Viande anniversaire	\N	20.8600006	2019-02-19	2019-02-19	t	1202	1301
10294	Remboursement	\N	140	2019-02-19	2019-02-19	t	1201	1301
10295	Vin semaine à 2 1btl	\N	5.9000001	2019-03-02	2019-03-02	t	1202	1301
10296	Courses semaine à 2	\N	50.2000008	2019-03-02	2019-03-02	t	1202	1301
10297	Pret	\N	10	2019-03-03	2019-03-03	t	1201	1301
10298	Boucher	\N	7.80000019	2019-03-08	2019-03-08	t	1202	1301
10299	Vanola	\N	20	2019-03-08	2019-03-08	t	1202	1301
10300	Capuccini	\N	8	2019-03-09	2019-03-09	t	1201	1301
10301	Courses	\N	45	2019-03-09	2019-03-09	t	1201	1301
10302	Parking	\N	5.30000019	2019-03-10	2019-03-10	t	1202	1301
10303	Cine	\N	5	2019-03-10	2019-03-10	t	1201	1301
10304	Cine	\N	5	2019-03-15	2019-03-15	t	1201	1301
10305	Deliveroo	\N	18	2019-03-15	2019-03-15	t	1201	1301
10306	Épilation	\N	7.5999999	2019-03-15	2019-03-15	t	1202	1301
10307	Huile +détachantling	\N	9.19999981	2019-03-15	2019-03-15	t	1202	1301
10308	Vin semaine à 2-2btl	\N	17	2019-03-15	2019-03-15	t	1202	1301
10309	Pourboire	\N	2	2019-03-15	2019-03-15	t	1202	1301
10310	Bip parking moto	\N	60	2019-03-15	2019-03-15	t	1202	1301
10311	Courses semaine à 2	\N	67.4000015	2019-03-15	2019-03-15	t	1202	1301
10312	Bouquet	\N	29.6499996	2019-03-17	2019-03-17	t	1202	1301
10313	Assouplissant+sucre	\N	8.46000004	2019-03-19	2019-03-19	t	1202	1301
10314	Resto	\N	87	2019-03-24	2019-03-24	t	1201	1301
10315	Vin 3 btl	\N	23.25	2019-03-29	2019-03-29	t	1202	1301
10316	Courses semaine à 2	\N	83.1999969	2019-03-29	2019-03-29	t	1202	1301
10317	Boucher	\N	29.1499996	2019-03-30	2019-03-30	t	1202	1301
10318	Shampooing 2	\N	11.1000004	2019-04-02	2019-04-02	t	1202	1301
10319	Deliveroo	\N	17.3999996	2019-04-05	2019-04-05	t	1201	1301
10320	Boucher	\N	13.3400002	2019-04-06	2019-04-06	t	1202	1301
10321	Place ciné	\N	7.0999999	2019-04-08	2019-04-08	t	1202	1301
10322	Resto	\N	77.4000015	2019-04-08	2019-04-08	t	1201	1301
10323	2 paires chaussettes	\N	11.8800001	2019-04-09	2019-04-09	t	1202	1301
10324	Nespresso	\N	30.2999992	2019-04-12	2019-04-12	t	1202	1301
10325	Cadeau parents	\N	32.7999992	2019-04-12	2019-04-12	t	1202	1301
10326	Cadeau parents	\N	29.9500008	2019-04-12	2019-04-12	t	1202	1301
10327	Brossette	\N	14.6999998	2019-04-13	2019-04-13	t	1201	1301
10328	Courses	\N	248.429993	2019-04-13	2019-04-13	t	1201	1301
10329	Vin semaine à 2btl	\N	19.7999992	2019-04-14	2019-04-14	t	1202	1301
10330	Courses semaine à 2	\N	84.5999985	2019-04-14	2019-04-14	t	1202	1301
10331	Courses	\N	96.1299973	2019-04-15	2019-04-15	t	1201	1301
10332	Boucher	\N	35	2019-04-16	2019-04-16	t	1202	1301
10333	Carte postale	\N	4.4000001	2019-04-16	2019-04-16	t	1202	1301
10334	Boucher	\N	22.5100002	2019-04-20	2019-04-20	t	1201	1301
10335	Cave Jules Verne	\N	38.5	2019-04-20	2019-04-20	t	1201	1301
10336	Pain	\N	8.25	2019-04-21	2019-04-21	t	1201	1301
10337	Courses	\N	42.5499992	2019-04-21	2019-04-21	t	1201	1301
10338	Cinéma	\N	9.60000038	2019-04-21	2019-04-21	t	1202	1301
10339	Courses semaine à 2	\N	39.9000015	2019-04-28	2019-04-28	t	1202	1301
10340	Vin 1 bt	\N	10.8000002	2019-04-28	2019-04-28	t	1202	1301
10341	Parking	\N	60.9000015	2019-05-03	2019-05-03	t	1201	1301
10342	Epicerie	\N	11	2019-05-04	2019-05-04	t	1201	1301
10343	Spar	\N	71.3700027	2019-05-04	2019-05-04	t	1201	1301
10344	Majorquine	\N	30	2019-05-04	2019-05-04	t	1201	1301
10345	Legumes	\N	3.29999995	2019-05-04	2019-05-04	t	1201	1301
10346	Cafe	\N	4.0999999	2019-05-04	2019-05-04	t	1201	1301
10347	Picnic	\N	16.2999992	2019-05-04	2019-05-04	t	1202	1301
10348	Resto	\N	45.7000008	2019-05-05	2019-05-05	t	1201	1301
10349	Resto	\N	64.6999969	2019-05-05	2019-05-05	t	1201	1301
10350	Resto	\N	23.9500008	2019-05-06	2019-05-06	t	1201	1301
10351	Courses	\N	34.9099998	2019-05-06	2019-05-06	t	1201	1301
10352	Parking	\N	2	2019-05-07	2019-05-07	t	1201	1301
10353	Cafe	\N	13.5	2019-05-07	2019-05-07	t	1202	1301
10354	Resto	\N	53.5	2019-05-07	2019-05-07	t	1201	1301
10355	Pain +parking	\N	3.5	2019-05-08	2019-05-08	t	1202	1301
10356	Musee Miro	\N	8	2019-05-08	2019-05-08	t	1201	1301
10357	Déjeuner	\N	33.2999992	2019-05-08	2019-05-08	t	1201	1301
10358	Cafe	\N	5	2019-05-08	2019-05-08	t	1202	1301
10359	Café	\N	4.4000001	2019-05-08	2019-05-08	t	1202	1301
10360	Essence	\N	23.4200001	2019-05-08	2019-05-08	t	1201	1301
10361	Parking	\N	10.3500004	2019-05-08	2019-05-08	t	1202	1301
10362	3 sandwich bocadilla	\N	11.9499998	2019-05-08	2019-05-08	t	1202	1301
10363	Virement rembourseme	\N	180	2019-05-09	2019-05-09	t	1202	1301
10364	Uber	\N	7	2019-05-11	2019-05-11	t	1201	1301
10365	Café	\N	16.8999996	2019-05-11	2019-05-11	t	1201	1301
10366	Daily syrien	\N	31.7999992	2019-05-11	2019-05-11	t	1201	1301
10367	Café et pâtisseries	\N	21.7000008	2019-05-11	2019-05-11	t	1201	1301
10368	Metro	\N	14.8999996	2019-05-11	2019-05-11	t	1202	1301
10369	Bateau mouche	\N	52	2019-05-11	2019-05-11	t	1201	1301
10370	Hotel	\N	158.759995	2019-05-11	2019-05-11	t	1201	1301
10371	Dejeuner	\N	55.5	2019-05-12	2019-05-12	t	1201	1301
10372	Musée	\N	13	2019-05-12	2019-05-12	t	1201	1301
10373	Uber	\N	7.53999996	2019-05-12	2019-05-12	t	1201	1301
10374	Petites bouteilles	\N	2.5	2019-05-12	2019-05-12	t	1202	1301
10375	Resto Maria luisa	\N	97	2019-05-14	2019-05-14	t	1201	1301
10376	Virement we end	\N	214	2019-05-14	2019-05-14	t	1202	1301
10377	Inde et vous	\N	18	2019-05-18	2019-05-18	t	1201	1301
10378	Cine	\N	11	2019-05-19	2019-05-19	t	1201	1301
10379	Thé	\N	7	2019-05-22	2019-05-22	t	1202	1301
10380	Épilation	\N	4.30000019	2019-05-24	2019-05-24	t	1202	1301
10381	Cartons +scotch	\N	10.9499998	2019-05-24	2019-05-24	t	1202	1301
10382	Vin 3 btl semaine à2	\N	24.5	2019-05-24	2019-05-24	t	1202	1301
10383	Courses semaine à 2	\N	107.699997	2019-05-24	2019-05-24	t	1202	1301
10384	Boucher	\N	12.1599998	2019-05-25	2019-05-25	t	1202	1301
10385	Passerelle de Marcel	\N	86	2019-05-31	2019-05-31	t	1201	1301
10386	Saumon	\N	7.94999981	2019-06-01	2019-06-01	t	1201	1301
10387	Charcuterie	\N	14.6000004	2019-06-03	2019-06-03	t	1201	1301
10388	Lulu rouget	\N	87	2019-06-06	2019-06-06	t	1201	1301
10389	Courses semaine à 2	\N	107.900002	2019-06-07	2019-06-07	t	1202	1301
10390	Vin 2 btls	\N	21.1000004	2019-06-07	2019-06-07	t	1202	1301
10391	Viande	\N	21.9799995	2019-06-08	2019-06-08	t	1202	1301
10392	Traiteur	\N	12.3100004	2019-06-08	2019-06-08	t	1202	1301
10393	Vin rouge	\N	9.80000019	2019-06-08	2019-06-08	t	1202	1301
10394	Courses we	\N	28	2019-06-12	2019-06-12	t	1202	1301
10395	Courses dem	\N	40	2019-06-15	2019-06-15	t	1201	1301
10396	Viande dimanche	\N	19	2019-06-17	2019-06-17	t	1202	1301
10397	Vanola	\N	20	2019-06-14	2019-06-14	t	1202	1301
10398	Courses samedi soir	\N	35.2000008	2019-06-17	2019-06-17	t	1202	1301
10399	Courses dimanche mid	\N	11.8000002	2019-06-17	2019-06-17	t	1202	1301
10400	Musée alcazar	\N	33	2019-06-18	2019-06-18	t	1201	1301
10401	Supplément bagages	\N	48	2019-06-20	2019-06-20	t	1201	1301
10402	Courses super u	\N	27.9699993	2019-06-20	2019-06-20	t	1202	1301
10403	Bus aéroport	\N	18	2019-06-21	2019-06-21	t	1201	1301
10404	Revues	\N	13.5	2019-06-21	2019-06-21	t	1201	1301
10405	Courses	\N	33.8899994	2019-06-21	2019-06-21	t	1201	1301
10406	Tapas	\N	50.2999992	2019-06-21	2019-06-21	t	1202	1301
10407	Navette aéroport sev	\N	8	2019-06-21	2019-06-21	t	1202	1301
10408	Courses semaine gars	\N	72.1299973	2019-06-22	2019-06-22	t	1201	1301
10409	Pot	\N	10	2019-06-22	2019-06-22	t	1202	1301
10410	Resto	\N	18.2000008	2019-06-22	2019-06-22	t	1201	1301
10411	Bijoux	\N	15	2019-06-23	2019-06-23	t	1201	1301
10412	Cadeaux musée	\N	11.6499996	2019-06-23	2019-06-23	t	1201	1301
10413	T-shirt	\N	45	2019-06-24	2019-06-24	t	1201	1301
10414	Resto	\N	13.6999998	2019-06-24	2019-06-24	t	1201	1301
10415	Resto	\N	34.5999985	2019-06-26	2019-06-26	t	1201	1301
10416	Casa de pilatos	\N	20	2019-06-26	2019-06-26	t	1201	1301
10417	Tapas	\N	29.6000004	2019-06-26	2019-06-26	t	1201	1301
10418	Liquide	\N	5	2019-06-27	2019-06-27	t	1201	1301
10419	Paella	\N	12	2019-06-27	2019-06-27	t	1201	1301
10420	Pot	\N	16	2019-06-27	2019-06-27	t	1201	1301
10421	Vin	\N	8.05000019	2019-06-27	2019-06-27	t	1201	1301
10422	Courses	\N	5	2019-06-27	2019-06-27	t	1201	1301
10423	Bus	\N	8	2019-06-28	2019-06-28	t	1201	1301
10424	Bus aéroport	\N	18	2019-06-28	2019-06-28	t	1201	1301
10425	Courses	\N	39.9000015	2019-06-28	2019-06-28	t	1201	1301
10426	Courses super u	\N	16.3199997	2019-06-29	2019-06-29	t	1202	1301
10427	Courses samedi	\N	8.94999981	2019-06-29	2019-06-29	t	1202	1301
10428	Vin 2 btls	\N	18.3999996	2019-06-29	2019-06-29	t	1202	1301
10429	Boucher	\N	23.5400009	2019-06-29	2019-06-29	t	1202	1301
10430	Courses	\N	46.9900017	2019-06-30	2019-06-30	t	1201	1301
10431	Produit lentilles	\N	6.98999977	2019-07-06	2019-07-06	t	1201	1301
10432	Douille	\N	4.80000019	2019-07-06	2019-07-06	t	1201	1301
10433	Boucher	\N	50.3800011	2019-07-06	2019-07-06	t	1201	1301
10434	Tartes	\N	15.1999998	2019-07-06	2019-07-06	t	1201	1301
10435	Courses super u	\N	16.4200001	2019-07-05	2019-07-05	t	1202	1301
10436	Billets bateau yeu	\N	55.4000015	2019-07-10	2019-07-10	t	1202	1301
10437	Super U	\N	38.7000008	2019-07-12	2019-07-12	t	1202	1301
10438	Insecticides	\N	56.7400017	2019-07-12	2019-07-12	t	1202	1301
10439	Boucher	\N	8.64000034	2019-07-12	2019-07-12	t	1202	1301
10440	2 bermudas	\N	55.5800018	2019-07-12	2019-07-12	t	1202	1301
10441	Courses repas Selma	\N	31.8299999	2019-07-12	2019-07-12	t	1202	1301
10442	2 dentifrices	\N	2.88000011	2019-07-12	2019-07-12	t	1202	1301
10443	Chlorophylle	\N	30.8700008	2019-07-12	2019-07-12	t	1202	1301
10444	Courses	\N	61.5600014	2019-07-13	2019-07-13	t	1201	1301
10445	Pain	\N	2.5999999	2019-07-13	2019-07-13	t	1201	1301
10446	Tarte fraises	\N	28	2019-07-13	2019-07-13	t	1202	1301
10447	Courses +pain	\N	26.2000008	2019-07-15	2019-07-15	t	1202	1301
10448	Courses +pzin6	\N	29.7999992	2019-07-17	2019-07-17	t	1202	1301
10449	Courses	\N	21.5	2019-07-17	2019-07-17	t	1201	1301
10450	Courses +pain	\N	30.9699993	2019-07-19	2019-07-19	t	1202	1301
10451	Apero	\N	16.6399994	2019-07-19	2019-07-19	t	1201	1301
10452	Resto	\N	71.4000015	2019-07-20	2019-07-20	t	1201	1301
10453	Cine	\N	5.5	2019-07-21	2019-07-21	t	1201	1301
10454	Traiteur asie	\N	14.8000002	2019-07-23	2019-07-23	t	1202	1301
10455	Courses Chlorophylle	\N	49.5999985	2019-07-24	2019-07-24	t	1202	1301
10456	Courses we	\N	10.4300003	2019-07-24	2019-07-24	t	1202	1301
10457	Courses super U	\N	29.7000008	2019-07-24	2019-07-24	t	1202	1301
10458	Parking	\N	4.0999999	2019-07-25	2019-07-25	t	1202	1301
10459	Viandes	\N	39.6300011	2019-07-25	2019-07-25	t	1201	1301
10460	Courses leclerc	\N	54.3800011	2019-07-26	2019-07-26	t	1202	1301
10461	Courses super U	\N	28.7199993	2019-07-29	2019-07-29	t	1202	1301
10462	Courses leclerc	\N	40.3699989	2019-07-29	2019-07-29	t	1202	1301
10463	Jeu de palets	\N	60	2019-07-31	2019-07-31	t	1202	1301
10464	Leclerc	\N	74.8000031	2019-07-31	2019-07-31	t	1202	1301
10465	Courses	\N	55	2019-08-02	2019-08-02	t	1201	1301
10466	Pain	\N	5	2019-08-03	2019-08-03	t	1201	1301
10467	Courses	\N	45.6599998	2019-08-03	2019-08-03	t	1201	1301
10468	Pain	\N	6.0999999	2019-08-03	2019-08-03	t	1201	1301
10469	Courses	\N	382.820007	2019-08-05	2019-08-05	t	1201	1301
10470	Autoroutes	\N	49.2000008	2019-08-06	2019-08-06	t	1201	1301
10471	Ciurses	\N	106.32	2019-08-09	2019-08-09	t	1201	1301
10472	Pizzas	\N	57	2019-08-10	2019-08-10	t	1202	1301
10473	Courses	\N	147.880005	2019-08-11	2019-08-11	t	1201	1301
10474	Château	\N	20	2019-08-12	2019-08-12	t	1201	1301
10475	Pain pdt vacances	\N	19.2000008	2019-08-16	2019-08-16	t	1202	1301
10476	Pique nique retour	\N	21.5200005	2019-08-16	2019-08-16	t	1202	1301
10477	Courses	\N	30.2600002	2019-08-16	2019-08-16	t	1201	1301
10478	Courses	\N	67.5999985	2019-08-17	2019-08-17	t	1202	1301
10479	Pain	\N	4.69999981	2019-08-18	2019-08-18	t	1201	1301
10480	Ciné	\N	7.0999999	2019-08-18	2019-08-18	t	1202	1301
10481	Pain	\N	2.20000005	2019-08-18	2019-08-18	t	1201	1301
10482	Légumes +pain	\N	12.6800003	2019-08-19	2019-08-19	t	1202	1301
10483	Peage	\N	55.4000015	2019-08-20	2019-08-20	t	1201	1301
10484	Deliveroo	\N	34	2019-08-20	2019-08-20	t	1201	1301
10485	Courses	\N	15.3999996	2019-08-20	2019-08-20	t	1201	1301
10486	Gel douche	\N	5.98999977	2019-08-20	2019-08-20	t	1202	1301
10487	Courses +pain	\N	8.35999966	2019-08-20	2019-08-20	t	1202	1301
10488	Gomettes	\N	3.5999999	2019-08-21	2019-08-21	t	1202	1301
10489	Cartons déménagement	\N	56	2019-08-21	2019-08-21	t	1202	1301
10490	Cave à vins 8bt	\N	98.8000031	2019-08-21	2019-08-21	t	1202	1301
10491	Chlorophylle +pain	\N	48.0999985	2019-08-21	2019-08-21	t	1202	1301
10492	Courses leclerc	\N	98.3000031	2019-08-22	2019-08-22	t	1202	1301
10493	Parking île d'yeu	\N	25.3999996	2019-08-27	2019-08-27	t	1201	1301
10494	Courses	\N	12	2019-08-27	2019-08-27	t	1201	1301
10495	Courses super U	\N	22.6800003	2019-08-30	2019-08-30	t	1202	1301
10496	Pot	\N	10	2019-08-30	2019-08-30	t	1201	1301
10497	Resto	\N	90.8000031	2019-08-30	2019-08-30	t	1201	1301
10498	Parking	\N	5	2019-08-31	2019-08-31	t	1202	1301
10499	Bionant	\N	16.1000004	2019-08-31	2019-08-31	t	1202	1301
10500	Courses	\N	82.4499969	2019-08-31	2019-08-31	t	1201	1301
10501	Boucher +pain	\N	45.3499985	2019-08-31	2019-08-31	t	1202	1301
10502	Courses	\N	32.8699989	2019-09-03	2019-09-03	t	1202	1301
10503	Courses	\N	39.75	2019-09-06	2019-09-06	t	1202	1301
10504	Chlorure	\N	4.5	2019-09-07	2019-09-07	t	1202	1301
10505	Pain +pourboire	\N	5.36999989	2019-09-07	2019-09-07	t	1202	1301
10506	Cine	\N	11	2019-09-07	2019-09-07	t	1201	1301
10507	Resto	\N	80.8000031	2019-09-07	2019-09-07	t	1201	1301
10508	Cine	\N	11	2019-09-08	2019-09-08	t	1201	1301
10509	Chlorophylle +pain	\N	50.4500008	2019-09-11	2019-09-11	t	1202	1301
10510	Rouleaux traiteur+PA	\N	7	2019-09-13	2019-09-13	t	1202	1301
10511	Ciné	\N	11	2019-09-11	2019-09-11	t	1201	1301
10512	Super U +pain	\N	40.7000008	2019-09-14	2019-09-14	t	1202	1301
10513	Super U	\N	61.0499992	2019-09-13	2019-09-13	t	1202	1301
10514	Boucher	\N	49.8800011	2019-09-14	2019-09-14	t	1201	1301
10515	Courses	\N	21	2019-09-15	2019-09-15	t	1201	1301
10516	Courses	\N	92	2019-09-16	2019-09-16	t	1201	1301
10517	Courses	\N	13.4200001	2019-09-17	2019-09-17	t	1201	1301
10518	Galettes	\N	5.5999999	2019-09-18	2019-09-18	t	1201	1301
10519	Pain	\N	6	2019-09-18	2019-09-18	t	1201	1301
10520	Pain	\N	5.61000013	2019-09-20	2019-09-20	t	1201	1301
10521	Café	\N	15.6999998	2019-09-20	2019-09-20	t	1202	1301
10522	Ciné	\N	5.5	2019-09-21	2019-09-21	t	1201	1301
\.


--
-- TOC entry 3039 (class 0 OID 16522)
-- Dependencies: 217
-- Data for Name: spent_config; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.spent_config (id, label, wallet_id) FROM stdin;
\.


--
-- TOC entry 3037 (class 0 OID 16512)
-- Dependencies: 215
-- Data for Name: spent_sharing; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.spent_sharing (id, amount_share, share, sharing_profil_id, spent_id) FROM stdin;
10523	0	0	1201	10000
10525	0	0	1201	10001
10527	187.300003	1	1201	10002
10529	0	0	1201	10003
10531	1.25	1	1201	10004
10533	0	0	1201	10005
10535	0	0	1201	10006
10537	11	1	1201	10007
10539	7.25	1	1201	10008
10541	10.9499998	1	1201	10009
10543	5.25	1	1201	10010
10545	29.3299999	1	1201	10011
10547	7.71500015	1	1201	10012
10549	19.6000004	1	1201	10013
10551	2.07500005	1	1201	10014
10553	0	0	1201	10015
10555	230	1	1201	10016
10557	4.875	1	1201	10017
10559	9.44999981	1	1201	10018
10561	15.165	1	1201	10019
10563	0	0	1201	10020
10565	23.75	1	1201	10021
10567	2.2750001	1	1201	10022
10569	3	1	1201	10023
10571	2.5250001	1	1201	10024
10573	59	1	1201	10025
10575	5.5	1	1201	10026
10577	16.8999996	1	1201	10027
10579	36.7999992	1	1201	10028
10581	13.8999996	1	1201	10029
10583	88.7300034	1	1201	10030
10585	30.3150005	1	1201	10031
10587	7.5	1	1201	10032
10589	58.25	1	1201	10033
10591	20	1	1201	10034
10593	17.1000004	1	1201	10035
10595	27	1	1201	10036
10597	23.4500008	1	1201	10037
10599	16.0400009	1	1201	10038
10601	3.4000001	1	1201	10039
10603	20.2999992	1	1201	10040
10605	4.75	1	1201	10041
10607	3.04999995	1	1201	10042
10609	12.25	1	1201	10043
10611	4.9000001	1	1201	10044
10613	5.5	1	1201	10045
10615	4.75	1	1201	10046
10617	7.25	1	1201	10047
10619	3.5	1	1201	10048
10621	3.5999999	1	1201	10049
10623	22.7000008	1	1201	10050
10625	6.4749999	1	1201	10051
10627	3.9000001	1	1201	10052
10629	6.5999999	1	1201	10053
10631	5.73999977	1	1201	10054
10633	14.6499996	1	1201	10055
10635	3.9000001	1	1201	10056
10637	6.8499999	1	1201	10057
10639	7.44999981	1	1201	10058
10641	5.5	1	1201	10059
10643	26.0750008	1	1201	10060
10645	36.5099983	1	1201	10061
10647	10.75	1	1201	10062
10649	8.10000038	1	1201	10063
10651	4.80000019	1	1201	10064
10653	0	0	1201	10065
10655	38.4799995	1	1201	10066
10657	46.4500008	1	1201	10067
10659	0	0	1201	10068
10661	10.9750004	1	1201	10069
10663	6.05000019	1	1201	10070
10665	3.29999995	1	1201	10071
10667	0	0	1201	10072
10669	1.92499995	1	1201	10073
10671	14.3000002	1	1201	10074
10673	40.5	1	1201	10075
10675	29.6499996	1	1201	10076
10677	4.5	1	1201	10077
10679	9	1	1201	10078
10681	6	1	1201	10079
10683	36.1949997	1	1201	10080
10685	14.8999996	1	1201	10081
10687	9.30000019	1	1201	10082
10689	34.75	1	1201	10083
10691	9.35000038	1	1201	10084
10693	28.6000004	1	1201	10085
10695	56.5	1	1201	10086
10697	112.355003	1	1201	10087
10699	14.9499998	1	1201	10088
10701	13.415	1	1201	10089
10703	64.3700027	1	1201	10090
10705	11	1	1201	10091
10707	23.3999996	1	1201	10092
10709	16.4599991	1	1201	10093
10711	7.75	1	1201	10094
10713	0	0	1201	10095
10715	10	1	1201	10096
10717	35.5	1	1201	10097
10719	6.8499999	1	1201	10098
10721	9.25	1	1201	10099
10723	23.8999996	1	1201	10100
10725	14.6999998	1	1201	10101
10727	4.30000019	1	1201	10102
10729	28.1000004	1	1201	10103
10731	18.5	1	1201	10104
10733	0	0	1201	10105
10735	13	1	1201	10106
10737	8.5	1	1201	10107
10739	8.10000038	1	1201	10108
10741	3.29999995	1	1201	10109
10743	3.0999999	1	1201	10110
10745	12.0349998	1	1201	10111
10747	11.1899996	1	1201	10112
10749	7.75	1	1201	10113
10751	3.45000005	1	1201	10114
10753	18.6499996	1	1201	10115
10755	25	1	1201	10116
10757	8.39999962	1	1201	10117
10759	18.9899998	1	1201	10118
10761	24.3999996	1	1201	10119
10763	7.5	1	1201	10120
10765	3.45000005	1	1201	10121
10767	11.6000004	1	1201	10122
10769	16	1	1201	10123
10771	10.4350004	1	1201	10124
10773	3.29999995	1	1201	10125
10775	3.0999999	1	1201	10126
10777	3.45000005	1	1201	10127
10779	2.20000005	1	1201	10128
10781	3.45000005	1	1201	10129
10783	12	1	1201	10130
10785	0	0	1201	10131
10787	3.45000005	1	1201	10132
10789	4.4000001	1	1201	10133
10791	14	1	1201	10134
10793	8	1	1201	10135
10795	0	0	1201	10136
10797	4.80000019	1	1201	10137
10799	8.89999962	1	1201	10138
10801	8.30000019	1	1201	10139
10803	2.5	1	1201	10140
10805	2.20000005	1	1201	10141
10807	5.5	1	1201	10142
10809	3	1	1201	10143
10811	9.72500038	1	1201	10144
10813	30	1	1201	10145
10815	18.5	1	1201	10146
10817	10	1	1201	10147
10819	3.45000005	1	1201	10148
10821	6.07499981	1	1201	10149
10823	300	1	1201	10150
10825	18.2649994	1	1201	10151
10827	26.0499992	1	1201	10152
10829	3.54999995	1	1201	10153
10831	5.5	1	1201	10154
10833	25.8999996	1	1201	10155
10835	5.11499977	1	1201	10156
10837	19.5	1	1201	10157
10839	12.915	1	1201	10158
10841	14	1	1201	10159
10843	3.25	1	1201	10160
10845	28.75	1	1201	10161
10847	12.5	1	1201	10162
10849	9.55000019	1	1201	10163
10851	8	1	1201	10164
10853	4.5	1	1201	10165
10855	8.5	1	1201	10166
10857	1.5	1	1201	10167
10859	3.5	1	1201	10168
10861	5.5	1	1201	10169
10863	0	0	1201	10170
10865	13	1	1201	10171
10867	5.25	1	1201	10172
10869	30.3500004	1	1201	10173
10871	17.5	1	1201	10174
10873	0	0	1201	10175
10875	3.79999995	1	1201	10176
10877	9.25	1	1201	10177
10879	4.80000019	1	1201	10178
10881	21.6000004	1	1201	10179
10883	2.75	1	1201	10180
10885	4.4000001	1	1201	10181
10887	5.05000019	1	1201	10182
10889	5.25	1	1201	10183
10891	17.5	1	1201	10184
10893	25.8999996	1	1201	10185
10895	0	0	1201	10186
10897	8.25	1	1201	10187
10899	0	0	1201	10188
10901	5.5	1	1201	10189
10903	3.4000001	1	1201	10190
10905	5.5	1	1201	10191
10907	0	0	1201	10192
10909	0	0	1201	10193
10911	2.75	1	1201	10194
10913	8	1	1201	10195
10915	0	1	1201	10196
10917	7.80000019	1	1201	10197
10919	43.25	1	1201	10198
10921	27.2250004	1	1201	10199
10923	18.8999996	1	1201	10200
10925	7.5	1	1201	10201
10927	14.3999996	1	1201	10202
10929	23.1000004	1	1201	10203
10931	38	1	1201	10204
10933	25.7000008	1	1201	10205
10935	3.25	1	1201	10206
10937	10.7299995	1	1201	10207
10939	15.8999996	1	1201	10208
10941	26.4500008	1	1201	10209
10943	2.4000001	1	1201	10210
10945	17.5	1	1201	10211
10947	3.5	1	1201	10212
10949	14.4650002	1	1201	10213
10951	8.30500031	1	1201	10214
10953	8	1	1201	10215
10955	9.5	1	1201	10216
10957	6.3499999	1	1201	10217
10959	4.5	1	1201	10218
10961	16.6000004	1	1201	10219
10963	27.2999992	1	1201	10220
10965	48.6349983	1	1201	10221
10967	18.9500008	1	1201	10222
10969	10.1149998	1	1201	10223
10971	9.44999981	1	1201	10224
10973	3.91499996	1	1201	10225
10975	2.0250001	1	1201	10226
10977	17	1	1201	10227
10979	17.2000008	1	1201	10228
10981	16	1	1201	10229
10983	4.25	1	1201	10230
10985	15	1	1201	10231
10987	14.8999996	1	1201	10232
10989	6.57499981	1	1201	10233
10991	10.5	1	1201	10234
10993	22.5	1	1201	10235
10995	1.5	1	1201	10236
10997	36.5499992	1	1201	10237
10999	6.1500001	1	1201	10238
11001	27.25	1	1201	10239
11003	7.2249999	1	1201	10240
11005	5.5	1	1201	10241
11007	32.8499985	1	1201	10242
11009	17.3500004	1	1201	10243
11011	11.4750004	1	1201	10244
11013	8.69999981	1	1201	10245
11015	7.48500013	1	1201	10246
11017	5.31500006	1	1201	10247
11019	2.0999999	1	1201	10248
11021	0	0	1201	10249
11023	3.29999995	1	1201	10250
11025	31.0249996	1	1201	10251
11027	47.25	1	1201	10252
11029	13	1	1201	10253
11031	0	0	1201	10254
11033	5.5	1	1201	10255
11035	37.1199989	1	1201	10256
11037	5.5	1	1201	10257
11039	9.35000038	1	1201	10258
11041	34.5	1	1201	10259
11043	10.75	1	1201	10260
11045	8.17500019	1	1201	10261
11047	6.91499996	1	1201	10262
11049	43.4500008	1	1201	10263
11051	6.75	1	1201	10264
11053	20	1	1201	10265
11055	27	1	1201	10266
11057	11.9700003	1	1201	10267
11059	9	1	1201	10268
11061	13.8500004	1	1201	10269
11063	3.29999995	1	1201	10270
11065	24.2000008	1	1201	10271
11067	1.14999998	1	1201	10272
11069	24.75	1	1201	10273
11071	18.3999996	1	1201	10274
11073	14	1	1201	10275
11075	11.8500004	1	1201	10276
11077	7.5	1	1201	10277
11079	5.5	1	1201	10278
11081	36	1	1201	10279
11083	10	1	1201	10280
11085	19.25	1	1201	10281
11087	7.3499999	1	1201	10282
11089	0	0	1201	10283
11091	1.35000002	1	1201	10284
11093	37	1	1201	10285
11095	7.9749999	1	1201	10286
11097	7.0999999	1	1201	10287
11099	4.75	1	1201	10288
11101	9	1	1201	10289
11103	12.8500004	1	1201	10290
11105	47.75	1	1201	10291
11107	46.6500015	1	1201	10292
11109	20.8600006	1	1201	10293
11111	0	0	1201	10294
11113	2.95000005	1	1201	10295
11115	25.1000004	1	1201	10296
11117	0	0	1201	10297
11119	3.9000001	1	1201	10298
11121	10	1	1201	10299
11123	4	1	1201	10300
11125	22.5	1	1201	10301
11127	2.6500001	1	1201	10302
11129	0	0	1201	10303
11131	0	0	1201	10304
11133	9	1	1201	10305
11135	7.5999999	1	1201	10306
11137	9.19999981	1	1201	10307
11139	8.5	1	1201	10308
11141	1	1	1201	10309
11143	30	1	1201	10310
11145	33.7000008	1	1201	10311
11147	14.8249998	1	1201	10312
11149	8.46000004	1	1201	10313
11151	43.5	1	1201	10314
11153	11.625	1	1201	10315
11155	41.5999985	1	1201	10316
11157	14.5749998	1	1201	10317
11159	11.1000004	1	1201	10318
11161	8.69999981	1	1201	10319
11163	6.67000008	1	1201	10320
11165	7.0999999	1	1201	10321
11167	38.7000008	1	1201	10322
11169	11.8800001	1	1201	10323
11171	15.1499996	1	1201	10324
11173	16.3999996	1	1201	10325
11175	14.9750004	1	1201	10326
11177	0	0	1201	10327
11179	124.214996	1	1201	10328
11181	9.89999962	1	1201	10329
11183	42.2999992	1	1201	10330
11185	48.0649986	1	1201	10331
11187	17.5	1	1201	10332
11189	4.4000001	1	1201	10333
11191	11.2550001	1	1201	10334
11193	19.25	1	1201	10335
11195	4.125	1	1201	10336
11197	21.2749996	1	1201	10337
11199	4.80000019	1	1201	10338
11201	19.9500008	1	1201	10339
11203	5.4000001	1	1201	10340
11205	30.4500008	1	1201	10341
11207	5.5	1	1201	10342
11209	35.6850014	1	1201	10343
11211	15	1	1201	10344
11213	1.64999998	1	1201	10345
11215	2.04999995	1	1201	10346
11217	8.14999962	1	1201	10347
11219	22.8500004	1	1201	10348
11221	32.3499985	1	1201	10349
11223	11.9750004	1	1201	10350
11225	17.4549999	1	1201	10351
11227	1	1	1201	10352
11229	6.75	1	1201	10353
11231	26.75	1	1201	10354
11233	1.75	1	1201	10355
11235	4	1	1201	10356
11237	16.6499996	1	1201	10357
11239	2.5	1	1201	10358
11241	2.20000005	1	1201	10359
11243	11.71	1	1201	10360
11245	5.17500019	1	1201	10361
11247	5.9749999	1	1201	10362
11249	180	1	1201	10363
11251	3.5	1	1201	10364
11253	8.44999981	1	1201	10365
11255	15.8999996	1	1201	10366
11257	10.8500004	1	1201	10367
11259	14.8999996	1	1201	10368
11261	26	1	1201	10369
11263	79.3799973	1	1201	10370
11265	27.75	1	1201	10371
11267	6.5	1	1201	10372
11269	3.76999998	1	1201	10373
11271	1.25	1	1201	10374
11273	48.5	1	1201	10375
11275	214	1	1201	10376
11277	9	1	1201	10377
11279	5.5	1	1201	10378
11281	7	1	1201	10379
11283	4.30000019	1	1201	10380
11285	10.9499998	1	1201	10381
11287	12.25	1	1201	10382
11289	53.8499985	1	1201	10383
11291	12.1599998	1	1201	10384
11293	43	1	1201	10385
11295	3.9749999	1	1201	10386
11297	7.30000019	1	1201	10387
11299	43.5	1	1201	10388
11301	53.9500008	1	1201	10389
11303	10.5500002	1	1201	10390
11305	10.9899998	1	1201	10391
11307	6.15500021	1	1201	10392
11309	4.9000001	1	1201	10393
11311	14	1	1201	10394
11313	0	0	1201	10395
11315	9.5	1	1201	10396
11317	10	1	1201	10397
11319	17.6000004	1	1201	10398
11321	5.9000001	1	1201	10399
11323	16.5	1	1201	10400
11325	24	1	1201	10401
11327	13.9849997	1	1201	10402
11329	9	1	1201	10403
11331	6.75	1	1201	10404
11333	16.9449997	1	1201	10405
11335	25.1499996	1	1201	10406
11337	4	1	1201	10407
11339	36.0649986	1	1201	10408
11341	5	1	1201	10409
11343	9.10000038	1	1201	10410
11345	0	0	1201	10411
11347	0	0	1201	10412
11349	22.5	1	1201	10413
11351	6.8499999	1	1201	10414
11353	17.2999992	1	1201	10415
11355	10	1	1201	10416
11357	14.8000002	1	1201	10417
11359	0	0	1201	10418
11361	6	1	1201	10419
11363	8	1	1201	10420
11365	4.0250001	1	1201	10421
11367	2.5	1	1201	10422
11369	4	1	1201	10423
11371	9	1	1201	10424
11373	26.6000004	2	1201	10425
11375	8.15999985	1	1201	10426
11377	4.4749999	1	1201	10427
11379	9.19999981	1	1201	10428
11381	11.7700005	1	1201	10429
11383	23.4950008	1	1201	10430
11385	0	0	1201	10431
11387	0	0	1201	10432
11389	25.1900005	1	1201	10433
11391	7.5999999	1	1201	10434
11393	8.21000004	1	1201	10435
11395	55.4000015	1	1201	10436
11397	19.3500004	1	1201	10437
11399	28.3700008	1	1201	10438
11401	8.64000034	1	1201	10439
11403	55.5800018	1	1201	10440
11405	31.8299999	1	1201	10441
11407	1.44000006	1	1201	10442
11409	15.4350004	1	1201	10443
11411	30.7800007	1	1201	10444
11413	1.29999995	1	1201	10445
11415	28	1	1201	10446
11417	13.1000004	1	1201	10447
11419	14.8999996	1	1201	10448
11421	14.333333	2	1201	10449
11423	15.4849997	1	1201	10450
11425	8.31999969	1	1201	10451
11427	35.7000008	1	1201	10452
11429	0	0	1201	10453
11431	7.4000001	1	1201	10454
11433	24.7999992	1	1201	10455
11435	5.21500015	1	1201	10456
11437	14.8500004	1	1201	10457
11439	2.04999995	1	1201	10458
11441	19.8150005	1	1201	10459
11443	27.1900005	1	1201	10460
11445	14.3599997	1	1201	10461
11447	20.1849995	1	1201	10462
11449	30	1	1201	10463
11451	37.4000015	1	1201	10464
11453	36.6666679	2	1201	10465
11455	3	3	1201	10466
11457	27.3959999	3	1201	10467
11459	3.66000009	3	1201	10468
11461	229.692001	3	1201	10469
11463	29.5200005	3	1201	10470
11465	63.7919998	3	1201	10471
11467	34.2000008	3	1201	10472
11469	88.7279968	3	1201	10473
11471	10	1	1201	10474
11473	11.5200005	3	1201	10475
11475	12.9119997	3	1201	10476
11477	15.1300001	1	1201	10477
11479	33.7999992	1	1201	10478
11481	2.3499999	1	1201	10479
11483	7.0999999	1	1201	10480
11485	1.10000002	1	1201	10481
11487	6.34000015	1	1201	10482
11489	33.2400017	3	1201	10483
11491	17	1	1201	10484
11493	7.69999981	1	1201	10485
11495	2.99499989	1	1201	10486
11497	4.17999983	1	1201	10487
11499	1.79999995	1	1201	10488
11501	56	1	1201	10489
11503	49.4000015	1	1201	10490
11505	24.0499992	1	1201	10491
11507	49.1500015	1	1201	10492
11509	12.6999998	1	1201	10493
11511	6	1	1201	10494
11513	11.3400002	1	1201	10495
11515	5	1	1201	10496
11517	45.4000015	1	1201	10497
11519	2.5	1	1201	10498
11521	8.05000019	1	1201	10499
11523	49.4700012	3	1201	10500
11525	22.6749992	1	1201	10501
11527	19.7220001	3	1201	10502
11529	19.875	1	1201	10503
11531	2.25	1	1201	10504
11533	2.68499994	1	1201	10505
11535	5.5	1	1201	10506
11537	40.4000015	1	1201	10507
11539	5.5	1	1201	10508
11541	25.2250004	1	1201	10509
11543	3.5	1	1201	10510
11545	5.5	1	1201	10511
11547	20.3500004	1	1201	10512
11549	30.5249996	1	1201	10513
11551	29.9279995	3	1201	10514
11553	14	2	1201	10515
11555	55.2000008	3	1201	10516
11557	8.05200005	3	1201	10517
11559	4.19999981	3	1201	10518
11561	3.5999999	3	1201	10519
11563	2.80500007	1	1201	10520
11565	15.6999998	1	1201	10521
11567	2.75	1	1201	10522
10524	51.7799988	1	1202	10000
10526	18.6100006	1	1202	10001
10528	0	0	1202	10002
10530	187.300003	1	1202	10003
10532	1.25	1	1202	10004
10534	80	1	1202	10005
10536	2.70000005	1	1202	10006
10538	11	1	1202	10007
10540	7.25	1	1202	10008
10542	10.9499998	1	1202	10009
10544	5.25	1	1202	10010
10546	29.3299999	1	1202	10011
10548	7.71500015	1	1202	10012
10550	19.6000004	1	1202	10013
10552	2.07500005	1	1202	10014
10554	31.5599995	1	1202	10015
10556	0	0	1202	10016
10558	4.875	1	1202	10017
10560	9.44999981	1	1202	10018
10562	15.165	1	1202	10019
10564	80	1	1202	10020
10566	23.75	1	1202	10021
10568	2.2750001	1	1202	10022
10570	3	1	1202	10023
10572	2.5250001	1	1202	10024
10574	59	1	1202	10025
10576	5.5	1	1202	10026
10578	16.8999996	1	1202	10027
10580	36.7999992	1	1202	10028
10582	13.8999996	1	1202	10029
10584	88.7300034	1	1202	10030
10586	30.3150005	1	1202	10031
10588	7.5	1	1202	10032
10590	58.25	1	1202	10033
10592	20	1	1202	10034
10594	17.1000004	1	1202	10035
10596	27	1	1202	10036
10598	23.4500008	1	1202	10037
10600	16.0400009	1	1202	10038
10602	3.4000001	1	1202	10039
10604	20.2999992	1	1202	10040
10606	4.75	1	1202	10041
10608	3.04999995	1	1202	10042
10610	12.25	1	1202	10043
10612	4.9000001	1	1202	10044
10614	5.5	1	1202	10045
10616	4.75	1	1202	10046
10618	7.25	1	1202	10047
10620	3.5	1	1202	10048
10622	0	0	1202	10049
10624	22.7000008	1	1202	10050
10626	6.4749999	1	1202	10051
10628	3.9000001	1	1202	10052
10630	6.5999999	1	1202	10053
10632	5.73999977	1	1202	10054
10634	14.6499996	1	1202	10055
10636	3.9000001	1	1202	10056
10638	6.8499999	1	1202	10057
10640	7.44999981	1	1202	10058
10642	5.5	1	1202	10059
10644	26.0750008	1	1202	10060
10646	0	0	1202	10061
10648	10.75	1	1202	10062
10650	8.10000038	1	1202	10063
10652	4.80000019	1	1202	10064
10654	29	1	1202	10065
10656	0	0	1202	10066
10658	46.4500008	1	1202	10067
10660	18	1	1202	10068
10662	10.9750004	1	1202	10069
10664	6.05000019	1	1202	10070
10666	0	0	1202	10071
10668	200	1	1202	10072
10670	1.92499995	1	1202	10073
10672	0	0	1202	10074
10674	40.5	1	1202	10075
10676	29.6499996	1	1202	10076
10678	4.5	1	1202	10077
10680	9	1	1202	10078
10682	6	1	1202	10079
10684	36.1949997	1	1202	10080
10686	0	0	1202	10081
10688	9.30000019	1	1202	10082
10690	34.75	1	1202	10083
10692	9.35000038	1	1202	10084
10694	28.6000004	1	1202	10085
10696	56.5	1	1202	10086
10698	112.355003	1	1202	10087
10700	14.9499998	1	1202	10088
10702	13.415	1	1202	10089
10704	64.3700027	1	1202	10090
10706	11	1	1202	10091
10708	0	0	1202	10092
10710	16.4599991	1	1202	10093
10712	7.75	1	1202	10094
10714	8	1	1202	10095
10716	10	1	1202	10096
10718	35.5	1	1202	10097
10720	6.8499999	1	1202	10098
10722	9.25	1	1202	10099
10724	0	0	1202	10100
10726	14.6999998	1	1202	10101
10728	0	0	1202	10102
10730	28.1000004	1	1202	10103
10732	18.5	1	1202	10104
10734	6.4000001	1	1202	10105
10736	13	1	1202	10106
10738	8.5	1	1202	10107
10740	8.10000038	1	1202	10108
10742	3.29999995	1	1202	10109
10744	3.0999999	1	1202	10110
10746	12.0349998	1	1202	10111
10748	11.1899996	1	1202	10112
10750	7.75	1	1202	10113
10752	3.45000005	1	1202	10114
10754	18.6499996	1	1202	10115
10756	25	1	1202	10116
10758	8.39999962	1	1202	10117
10760	18.9899998	1	1202	10118
10762	24.3999996	1	1202	10119
10764	7.5	1	1202	10120
10766	3.45000005	1	1202	10121
10768	11.6000004	1	1202	10122
10770	16	1	1202	10123
10772	10.4350004	1	1202	10124
10774	3.29999995	1	1202	10125
10776	3.0999999	1	1202	10126
10778	3.45000005	1	1202	10127
10780	2.20000005	1	1202	10128
10782	3.45000005	1	1202	10129
10784	12	1	1202	10130
10786	8.64000034	1	1202	10131
10788	3.45000005	1	1202	10132
10790	4.4000001	1	1202	10133
10792	14	1	1202	10134
10794	8	1	1202	10135
10796	40	1	1202	10136
10798	4.80000019	1	1202	10137
10800	8.89999962	1	1202	10138
10802	8.30000019	1	1202	10139
10804	2.5	1	1202	10140
10806	2.20000005	1	1202	10141
10808	5.5	1	1202	10142
10810	0	0	1202	10143
10812	9.72500038	1	1202	10144
10814	30	1	1202	10145
10816	18.5	1	1202	10146
10818	10	1	1202	10147
10820	3.45000005	1	1202	10148
10822	6.07499981	1	1202	10149
10824	0	0	1202	10150
10826	18.2649994	1	1202	10151
10828	26.0499992	1	1202	10152
10830	3.54999995	1	1202	10153
10832	5.5	1	1202	10154
10834	25.8999996	1	1202	10155
10836	5.11499977	1	1202	10156
10838	19.5	1	1202	10157
10840	12.915	1	1202	10158
10842	14	1	1202	10159
10844	3.25	1	1202	10160
10846	28.75	1	1202	10161
10848	12.5	1	1202	10162
10850	9.55000019	1	1202	10163
10852	8	1	1202	10164
10854	4.5	1	1202	10165
10856	8.5	1	1202	10166
10858	1.5	1	1202	10167
10860	3.5	1	1202	10168
10862	5.5	1	1202	10169
10864	5.80000019	1	1202	10170
10866	13	1	1202	10171
10868	5.25	1	1202	10172
10870	30.3500004	1	1202	10173
10872	17.5	1	1202	10174
10874	6.80000019	1	1202	10175
10876	3.79999995	1	1202	10176
10878	9.25	1	1202	10177
10880	4.80000019	1	1202	10178
10882	21.6000004	1	1202	10179
10884	2.75	1	1202	10180
10886	0	0	1202	10181
10888	0	0	1202	10182
10890	0	0	1202	10183
10892	17.5	1	1202	10184
10894	25.8999996	1	1202	10185
10896	30	1	1202	10186
10898	8.25	1	1202	10187
10900	28.4500008	1	1202	10188
10902	5.5	1	1202	10189
10904	3.4000001	1	1202	10190
10906	5.5	1	1202	10191
10908	5.5	1	1202	10192
10910	38	1	1202	10193
10912	2.75	1	1202	10194
10914	8	1	1202	10195
10916	0	1	1202	10196
10918	7.80000019	1	1202	10197
10920	43.25	1	1202	10198
10922	27.2250004	1	1202	10199
10924	18.8999996	1	1202	10200
10926	7.5	1	1202	10201
10928	14.3999996	1	1202	10202
10930	23.1000004	1	1202	10203
10932	38	1	1202	10204
10934	25.7000008	1	1202	10205
10936	3.25	1	1202	10206
10938	10.7299995	1	1202	10207
10940	15.8999996	1	1202	10208
10942	26.4500008	1	1202	10209
10944	2.4000001	1	1202	10210
10946	17.5	1	1202	10211
10948	3.5	1	1202	10212
10950	14.4650002	1	1202	10213
10952	8.30500031	1	1202	10214
10954	8	1	1202	10215
10956	0	0	1202	10216
10958	6.3499999	1	1202	10217
10960	0	0	1202	10218
10962	16.6000004	1	1202	10219
10964	27.2999992	1	1202	10220
10966	48.6349983	1	1202	10221
10968	18.9500008	1	1202	10222
10970	10.1149998	1	1202	10223
10972	9.44999981	1	1202	10224
10974	3.91499996	1	1202	10225
10976	2.0250001	1	1202	10226
10978	17	1	1202	10227
10980	17.2000008	1	1202	10228
10982	0	0	1202	10229
10984	4.25	1	1202	10230
10986	0	0	1202	10231
10988	14.8999996	1	1202	10232
10990	6.57499981	1	1202	10233
10992	10.5	1	1202	10234
10994	0	0	1202	10235
10996	1.5	1	1202	10236
10998	36.5499992	1	1202	10237
11000	6.1500001	1	1202	10238
11002	27.25	1	1202	10239
11004	7.2249999	1	1202	10240
11006	5.5	1	1202	10241
11008	32.8499985	1	1202	10242
11010	17.3500004	1	1202	10243
11012	11.4750004	1	1202	10244
11014	0	0	1202	10245
11016	7.48500013	1	1202	10246
11018	5.31500006	1	1202	10247
11020	2.0999999	1	1202	10248
11022	9	1	1202	10249
11024	3.29999995	1	1202	10250
11026	31.0249996	1	1202	10251
11028	47.25	1	1202	10252
11030	13	1	1202	10253
11032	10	1	1202	10254
11034	0	0	1202	10255
11036	37.1199989	1	1202	10256
11038	5.5	1	1202	10257
11040	9.35000038	1	1202	10258
11042	34.5	1	1202	10259
11044	10.75	1	1202	10260
11046	8.17500019	1	1202	10261
11048	6.91499996	1	1202	10262
11050	43.4500008	1	1202	10263
11052	6.75	1	1202	10264
11054	20	1	1202	10265
11056	27	1	1202	10266
11058	11.9700003	1	1202	10267
11060	9	1	1202	10268
11062	13.8500004	1	1202	10269
11064	3.29999995	1	1202	10270
11066	24.2000008	1	1202	10271
11068	1.14999998	1	1202	10272
11070	24.75	1	1202	10273
11072	18.3999996	1	1202	10274
11074	14	1	1202	10275
11076	11.8500004	1	1202	10276
11078	7.5	1	1202	10277
11080	5.5	1	1202	10278
11082	36	1	1202	10279
11084	0	0	1202	10280
11086	19.25	1	1202	10281
11088	0	0	1202	10282
11090	53.4000015	1	1202	10283
11092	1.35000002	1	1202	10284
11094	37	1	1202	10285
11096	7.9749999	1	1202	10286
11098	0	0	1202	10287
11100	4.75	1	1202	10288
11102	9	1	1202	10289
11104	12.8500004	1	1202	10290
11106	47.75	1	1202	10291
11108	0	0	1202	10292
11110	0	0	1202	10293
11112	140	1	1202	10294
11114	2.95000005	1	1202	10295
11116	25.1000004	1	1202	10296
11118	10	1	1202	10297
11120	3.9000001	1	1202	10298
11122	10	1	1202	10299
11124	4	1	1202	10300
11126	22.5	1	1202	10301
11128	2.6500001	1	1202	10302
11130	5	1	1202	10303
11132	5	1	1202	10304
11134	9	1	1202	10305
11136	0	0	1202	10306
11138	0	0	1202	10307
11140	8.5	1	1202	10308
11142	1	1	1202	10309
11144	30	1	1202	10310
11146	33.7000008	1	1202	10311
11148	14.8249998	1	1202	10312
11150	0	0	1202	10313
11152	43.5	1	1202	10314
11154	11.625	1	1202	10315
11156	41.5999985	1	1202	10316
11158	14.5749998	1	1202	10317
11160	0	0	1202	10318
11162	8.69999981	1	1202	10319
11164	6.67000008	1	1202	10320
11166	0	0	1202	10321
11168	38.7000008	1	1202	10322
11170	0	0	1202	10323
11172	15.1499996	1	1202	10324
11174	16.3999996	1	1202	10325
11176	14.9750004	1	1202	10326
11178	14.6999998	1	1202	10327
11180	124.214996	1	1202	10328
11182	9.89999962	1	1202	10329
11184	42.2999992	1	1202	10330
11186	48.0649986	1	1202	10331
11188	17.5	1	1202	10332
11190	0	0	1202	10333
11192	11.2550001	1	1202	10334
11194	19.25	1	1202	10335
11196	4.125	1	1202	10336
11198	21.2749996	1	1202	10337
11200	4.80000019	1	1202	10338
11202	19.9500008	1	1202	10339
11204	5.4000001	1	1202	10340
11206	30.4500008	1	1202	10341
11208	5.5	1	1202	10342
11210	35.6850014	1	1202	10343
11212	15	1	1202	10344
11214	1.64999998	1	1202	10345
11216	2.04999995	1	1202	10346
11218	8.14999962	1	1202	10347
11220	22.8500004	1	1202	10348
11222	32.3499985	1	1202	10349
11224	11.9750004	1	1202	10350
11226	17.4549999	1	1202	10351
11228	1	1	1202	10352
11230	6.75	1	1202	10353
11232	26.75	1	1202	10354
11234	1.75	1	1202	10355
11236	4	1	1202	10356
11238	16.6499996	1	1202	10357
11240	2.5	1	1202	10358
11242	2.20000005	1	1202	10359
11244	11.71	1	1202	10360
11246	5.17500019	1	1202	10361
11248	5.9749999	1	1202	10362
11250	0	0	1202	10363
11252	3.5	1	1202	10364
11254	8.44999981	1	1202	10365
11256	15.8999996	1	1202	10366
11258	10.8500004	1	1202	10367
11260	0	0	1202	10368
11262	26	1	1202	10369
11264	79.3799973	1	1202	10370
11266	27.75	1	1202	10371
11268	6.5	1	1202	10372
11270	3.76999998	1	1202	10373
11272	1.25	1	1202	10374
11274	48.5	1	1202	10375
11276	0	0	1202	10376
11278	9	1	1202	10377
11280	5.5	1	1202	10378
11282	0	0	1202	10379
11284	0	0	1202	10380
11286	0	0	1202	10381
11288	12.25	1	1202	10382
11290	53.8499985	1	1202	10383
11292	0	0	1202	10384
11294	43	1	1202	10385
11296	3.9749999	1	1202	10386
11298	7.30000019	1	1202	10387
11300	43.5	1	1202	10388
11302	53.9500008	1	1202	10389
11304	10.5500002	1	1202	10390
11306	10.9899998	1	1202	10391
11308	6.15500021	1	1202	10392
11310	4.9000001	1	1202	10393
11312	14	1	1202	10394
11314	40	1	1202	10395
11316	9.5	1	1202	10396
11318	10	1	1202	10397
11320	17.6000004	1	1202	10398
11322	5.9000001	1	1202	10399
11324	16.5	1	1202	10400
11326	24	1	1202	10401
11328	13.9849997	1	1202	10402
11330	9	1	1202	10403
11332	6.75	1	1202	10404
11334	16.9449997	1	1202	10405
11336	25.1499996	1	1202	10406
11338	4	1	1202	10407
11340	36.0649986	1	1202	10408
11342	5	1	1202	10409
11344	9.10000038	1	1202	10410
11346	15	1	1202	10411
11348	11.6499996	1	1202	10412
11350	22.5	1	1202	10413
11352	6.8499999	1	1202	10414
11354	17.2999992	1	1202	10415
11356	10	1	1202	10416
11358	14.8000002	1	1202	10417
11360	5	1	1202	10418
11362	6	1	1202	10419
11364	8	1	1202	10420
11366	4.0250001	1	1202	10421
11368	2.5	1	1202	10422
11370	4	1	1202	10423
11372	9	1	1202	10424
11374	13.3000002	1	1202	10425
11376	8.15999985	1	1202	10426
11378	4.4749999	1	1202	10427
11380	9.19999981	1	1202	10428
11382	11.7700005	1	1202	10429
11384	23.4950008	1	1202	10430
11386	6.98999977	1	1202	10431
11388	4.80000019	1	1202	10432
11390	25.1900005	1	1202	10433
11392	7.5999999	1	1202	10434
11394	8.21000004	1	1202	10435
11396	0	0	1202	10436
11398	19.3500004	1	1202	10437
11400	28.3700008	1	1202	10438
11402	0	0	1202	10439
11404	0	0	1202	10440
11406	0	0	1202	10441
11408	1.44000006	1	1202	10442
11410	15.4350004	1	1202	10443
11412	30.7800007	1	1202	10444
11414	1.29999995	1	1202	10445
11416	0	0	1202	10446
11418	13.1000004	1	1202	10447
11420	14.8999996	1	1202	10448
11422	7.16666651	1	1202	10449
11424	15.4849997	1	1202	10450
11426	8.31999969	1	1202	10451
11428	35.7000008	1	1202	10452
11430	5.5	1	1202	10453
11432	7.4000001	1	1202	10454
11434	24.7999992	1	1202	10455
11436	5.21500015	1	1202	10456
11438	14.8500004	1	1202	10457
11440	2.04999995	1	1202	10458
11442	19.8150005	1	1202	10459
11444	27.1900005	1	1202	10460
11446	14.3599997	1	1202	10461
11448	20.1849995	1	1202	10462
11450	30	1	1202	10463
11452	37.4000015	1	1202	10464
11454	18.333334	1	1202	10465
11456	2	2	1202	10466
11458	18.2639999	2	1202	10467
11460	2.44000006	2	1202	10468
11462	153.128006	2	1202	10469
11464	19.6800003	2	1202	10470
11466	42.5279999	2	1202	10471
11468	22.7999992	2	1202	10472
11470	59.1520004	2	1202	10473
11472	10	1	1202	10474
11474	7.67999983	2	1202	10475
11476	8.6079998	2	1202	10476
11478	15.1300001	1	1202	10477
11480	33.7999992	1	1202	10478
11482	2.3499999	1	1202	10479
11484	0	0	1202	10480
11486	1.10000002	1	1202	10481
11488	6.34000015	1	1202	10482
11490	22.1599998	2	1202	10483
11492	17	1	1202	10484
11494	7.69999981	1	1202	10485
11496	2.99499989	1	1202	10486
11498	4.17999983	1	1202	10487
11500	1.79999995	1	1202	10488
11502	0	0	1202	10489
11504	49.4000015	1	1202	10490
11506	24.0499992	1	1202	10491
11508	49.1500015	1	1202	10492
11510	12.6999998	1	1202	10493
11512	6	1	1202	10494
11514	11.3400002	1	1202	10495
11516	5	1	1202	10496
11518	45.4000015	1	1202	10497
11520	2.5	1	1202	10498
11522	8.05000019	1	1202	10499
11524	32.9799995	2	1202	10500
11526	22.6749992	1	1202	10501
11528	13.1479998	2	1202	10502
11530	19.875	1	1202	10503
11532	2.25	1	1202	10504
11534	2.68499994	1	1202	10505
11536	5.5	1	1202	10506
11538	40.4000015	1	1202	10507
11540	5.5	1	1202	10508
11542	25.2250004	1	1202	10509
11544	3.5	1	1202	10510
11546	5.5	1	1202	10511
11548	20.3500004	1	1202	10512
11550	30.5249996	1	1202	10513
11552	19.9519997	2	1202	10514
11554	7	1	1202	10515
11556	36.7999992	2	1202	10516
11558	5.36800003	2	1202	10517
11560	1.39999998	1	1202	10518
11562	2.4000001	2	1202	10519
11564	2.80500007	1	1202	10520
11566	0	0	1202	10521
11568	2.75	1	1202	10522
\.


--
-- TOC entry 3038 (class 0 OID 16517)
-- Dependencies: 216
-- Data for Name: spent_sharing_config; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.spent_sharing_config (id, share, spent_config_id, profil_id) FROM stdin;
\.


--
-- TOC entry 3031 (class 0 OID 16476)
-- Dependencies: 209
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.task (id, label, description, done, due_date, priority, task_project_id) FROM stdin;
\.


--
-- TOC entry 3032 (class 0 OID 16484)
-- Dependencies: 210
-- Data for Name: task_owner; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.task_owner (owner_id, task_id) FROM stdin;
\.


--
-- TOC entry 3029 (class 0 OID 16466)
-- Dependencies: 207
-- Data for Name: task_project; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.task_project (id, label, workspace_id) FROM stdin;
\.


--
-- TOC entry 3030 (class 0 OID 16471)
-- Dependencies: 208
-- Data for Name: task_project_owner; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.task_project_owner (owner_id, task_project_id) FROM stdin;
\.


--
-- TOC entry 3034 (class 0 OID 16497)
-- Dependencies: 212
-- Data for Name: wallet; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.wallet (id, label, workspace_id) FROM stdin;
1301	Courses quotidiennes	1251
\.


--
-- TOC entry 3035 (class 0 OID 16502)
-- Dependencies: 213
-- Data for Name: wallet_owner; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.wallet_owner (owner_id, wallet_id) FROM stdin;
1201	1301
1202	1301
\.


--
-- TOC entry 3027 (class 0 OID 16456)
-- Dependencies: 205
-- Data for Name: workspace; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.workspace (id, label) FROM stdin;
1251	Sweet Home
\.


--
-- TOC entry 3028 (class 0 OID 16461)
-- Dependencies: 206
-- Data for Name: workspace_owner; Type: TABLE DATA; Schema: public; Owner: mhaweb
--

COPY public.workspace_owner (owner_id, workspace_id) FROM stdin;
1201	1251
1202	1251
\.


--
-- TOC entry 3045 (class 0 OID 0)
-- Dependencies: 198
-- Name: sequence_generator; Type: SEQUENCE SET; Schema: public; Owner: mhaweb
--

SELECT pg_catalog.setval('public.sequence_generator', 20000, true);


--
-- TOC entry 2825 (class 2606 OID 16389)
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- TOC entry 2833 (class 2606 OID 16414)
-- Name: mha_authority mha_authority_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_authority
    ADD CONSTRAINT mha_authority_pkey PRIMARY KEY (name);


--
-- TOC entry 2838 (class 2606 OID 16434)
-- Name: mha_persistent_audit_event mha_persistent_audit_event_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_persistent_audit_event
    ADD CONSTRAINT mha_persistent_audit_event_pkey PRIMARY KEY (event_id);


--
-- TOC entry 2841 (class 2606 OID 16439)
-- Name: mha_persistent_audit_evt_data mha_persistent_audit_evt_data_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_persistent_audit_evt_data
    ADD CONSTRAINT mha_persistent_audit_evt_data_pkey PRIMARY KEY (event_id, name);


--
-- TOC entry 2835 (class 2606 OID 16419)
-- Name: mha_user_authority mha_user_authority_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_user_authority
    ADD CONSTRAINT mha_user_authority_pkey PRIMARY KEY (user_id, authority_name);


--
-- TOC entry 2827 (class 2606 OID 16405)
-- Name: mha_user mha_user_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_user
    ADD CONSTRAINT mha_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2861 (class 2606 OID 16496)
-- Name: profil_data profil_data_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.profil_data
    ADD CONSTRAINT profil_data_pkey PRIMARY KEY (id);


--
-- TOC entry 2843 (class 2606 OID 16451)
-- Name: profil profil_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT profil_pkey PRIMARY KEY (id);


--
-- TOC entry 2873 (class 2606 OID 16526)
-- Name: spent_config spent_config_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_config
    ADD CONSTRAINT spent_config_pkey PRIMARY KEY (id);


--
-- TOC entry 2867 (class 2606 OID 16511)
-- Name: spent spent_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent
    ADD CONSTRAINT spent_pkey PRIMARY KEY (id);


--
-- TOC entry 2871 (class 2606 OID 16521)
-- Name: spent_sharing_config spent_sharing_config_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_sharing_config
    ADD CONSTRAINT spent_sharing_config_pkey PRIMARY KEY (id);


--
-- TOC entry 2869 (class 2606 OID 16516)
-- Name: spent_sharing spent_sharing_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_sharing
    ADD CONSTRAINT spent_sharing_pkey PRIMARY KEY (id);


--
-- TOC entry 2859 (class 2606 OID 16488)
-- Name: task_owner task_owner_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_owner
    ADD CONSTRAINT task_owner_pkey PRIMARY KEY (task_id, owner_id);


--
-- TOC entry 2857 (class 2606 OID 16483)
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- TOC entry 2855 (class 2606 OID 16475)
-- Name: task_project_owner task_project_owner_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_project_owner
    ADD CONSTRAINT task_project_owner_pkey PRIMARY KEY (task_project_id, owner_id);


--
-- TOC entry 2853 (class 2606 OID 16470)
-- Name: task_project task_project_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_project
    ADD CONSTRAINT task_project_pkey PRIMARY KEY (id);


--
-- TOC entry 2845 (class 2606 OID 16455)
-- Name: profil ux_profil_profil_data_id; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT ux_profil_profil_data_id UNIQUE (profil_data_id);


--
-- TOC entry 2847 (class 2606 OID 16453)
-- Name: profil ux_profil_user_id; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT ux_profil_user_id UNIQUE (user_id);


--
-- TOC entry 2829 (class 2606 OID 16409)
-- Name: mha_user ux_user_email; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_user
    ADD CONSTRAINT ux_user_email UNIQUE (email);


--
-- TOC entry 2831 (class 2606 OID 16407)
-- Name: mha_user ux_user_login; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_user
    ADD CONSTRAINT ux_user_login UNIQUE (login);


--
-- TOC entry 2865 (class 2606 OID 16506)
-- Name: wallet_owner wallet_owner_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.wallet_owner
    ADD CONSTRAINT wallet_owner_pkey PRIMARY KEY (wallet_id, owner_id);


--
-- TOC entry 2863 (class 2606 OID 16501)
-- Name: wallet wallet_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.wallet
    ADD CONSTRAINT wallet_pkey PRIMARY KEY (id);


--
-- TOC entry 2851 (class 2606 OID 16465)
-- Name: workspace_owner workspace_owner_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.workspace_owner
    ADD CONSTRAINT workspace_owner_pkey PRIMARY KEY (workspace_id, owner_id);


--
-- TOC entry 2849 (class 2606 OID 16460)
-- Name: workspace workspace_pkey; Type: CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.workspace
    ADD CONSTRAINT workspace_pkey PRIMARY KEY (id);


--
-- TOC entry 2836 (class 1259 OID 16440)
-- Name: idx_persistent_audit_event; Type: INDEX; Schema: public; Owner: mhaweb
--

CREATE INDEX idx_persistent_audit_event ON public.mha_persistent_audit_event USING btree (principal, event_date);


--
-- TOC entry 2839 (class 1259 OID 16441)
-- Name: idx_persistent_audit_evt_data; Type: INDEX; Schema: public; Owner: mhaweb
--

CREATE INDEX idx_persistent_audit_evt_data ON public.mha_persistent_audit_evt_data USING btree (event_id);


--
-- TOC entry 2874 (class 2606 OID 16420)
-- Name: mha_user_authority fk_authority_name; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_user_authority
    ADD CONSTRAINT fk_authority_name FOREIGN KEY (authority_name) REFERENCES public.mha_authority(name);


--
-- TOC entry 2876 (class 2606 OID 16442)
-- Name: mha_persistent_audit_evt_data fk_evt_pers_audit_evt_data; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_persistent_audit_evt_data
    ADD CONSTRAINT fk_evt_pers_audit_evt_data FOREIGN KEY (event_id) REFERENCES public.mha_persistent_audit_event(event_id);


--
-- TOC entry 2878 (class 2606 OID 16532)
-- Name: profil fk_profil_profil_data_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT fk_profil_profil_data_id FOREIGN KEY (profil_data_id) REFERENCES public.profil_data(id);


--
-- TOC entry 2877 (class 2606 OID 16527)
-- Name: profil fk_profil_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT fk_profil_user_id FOREIGN KEY (user_id) REFERENCES public.mha_user(id);


--
-- TOC entry 2896 (class 2606 OID 16612)
-- Name: spent_config fk_spent_config_wallet_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_config
    ADD CONSTRAINT fk_spent_config_wallet_id FOREIGN KEY (wallet_id) REFERENCES public.wallet(id);


--
-- TOC entry 2895 (class 2606 OID 16622)
-- Name: spent_sharing_config fk_spent_sharing_config_profil_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_sharing_config
    ADD CONSTRAINT fk_spent_sharing_config_profil_id FOREIGN KEY (profil_id) REFERENCES public.profil(id);


--
-- TOC entry 2894 (class 2606 OID 16617)
-- Name: spent_sharing_config fk_spent_sharing_config_spent_config_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_sharing_config
    ADD CONSTRAINT fk_spent_sharing_config_spent_config_id FOREIGN KEY (spent_config_id) REFERENCES public.spent_config(id);


--
-- TOC entry 2892 (class 2606 OID 16602)
-- Name: spent_sharing fk_spent_sharing_sharing_profil_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_sharing
    ADD CONSTRAINT fk_spent_sharing_sharing_profil_id FOREIGN KEY (sharing_profil_id) REFERENCES public.profil(id);


--
-- TOC entry 2893 (class 2606 OID 16607)
-- Name: spent_sharing fk_spent_sharing_spent_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent_sharing
    ADD CONSTRAINT fk_spent_sharing_spent_id FOREIGN KEY (spent_id) REFERENCES public.spent(id);


--
-- TOC entry 2890 (class 2606 OID 16592)
-- Name: spent fk_spent_spender_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent
    ADD CONSTRAINT fk_spent_spender_id FOREIGN KEY (spender_id) REFERENCES public.profil(id);


--
-- TOC entry 2891 (class 2606 OID 16597)
-- Name: spent fk_spent_wallet_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.spent
    ADD CONSTRAINT fk_spent_wallet_id FOREIGN KEY (wallet_id) REFERENCES public.wallet(id);


--
-- TOC entry 2886 (class 2606 OID 16572)
-- Name: task_owner fk_task_owner_owner_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_owner
    ADD CONSTRAINT fk_task_owner_owner_id FOREIGN KEY (owner_id) REFERENCES public.profil(id);


--
-- TOC entry 2885 (class 2606 OID 16567)
-- Name: task_owner fk_task_owner_task_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_owner
    ADD CONSTRAINT fk_task_owner_task_id FOREIGN KEY (task_id) REFERENCES public.task(id);


--
-- TOC entry 2883 (class 2606 OID 16557)
-- Name: task_project_owner fk_task_project_owner_owner_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_project_owner
    ADD CONSTRAINT fk_task_project_owner_owner_id FOREIGN KEY (owner_id) REFERENCES public.profil(id);


--
-- TOC entry 2882 (class 2606 OID 16552)
-- Name: task_project_owner fk_task_project_owner_task_project_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_project_owner
    ADD CONSTRAINT fk_task_project_owner_task_project_id FOREIGN KEY (task_project_id) REFERENCES public.task_project(id);


--
-- TOC entry 2881 (class 2606 OID 16547)
-- Name: task_project fk_task_project_workspace_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task_project
    ADD CONSTRAINT fk_task_project_workspace_id FOREIGN KEY (workspace_id) REFERENCES public.workspace(id);


--
-- TOC entry 2884 (class 2606 OID 16562)
-- Name: task fk_task_task_project_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT fk_task_task_project_id FOREIGN KEY (task_project_id) REFERENCES public.task_project(id);


--
-- TOC entry 2875 (class 2606 OID 16425)
-- Name: mha_user_authority fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.mha_user_authority
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.mha_user(id);


--
-- TOC entry 2889 (class 2606 OID 16587)
-- Name: wallet_owner fk_wallet_owner_owner_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.wallet_owner
    ADD CONSTRAINT fk_wallet_owner_owner_id FOREIGN KEY (owner_id) REFERENCES public.profil(id);


--
-- TOC entry 2888 (class 2606 OID 16582)
-- Name: wallet_owner fk_wallet_owner_wallet_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.wallet_owner
    ADD CONSTRAINT fk_wallet_owner_wallet_id FOREIGN KEY (wallet_id) REFERENCES public.wallet(id);


--
-- TOC entry 2887 (class 2606 OID 16577)
-- Name: wallet fk_wallet_workspace_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.wallet
    ADD CONSTRAINT fk_wallet_workspace_id FOREIGN KEY (workspace_id) REFERENCES public.workspace(id);


--
-- TOC entry 2880 (class 2606 OID 16542)
-- Name: workspace_owner fk_workspace_owner_owner_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.workspace_owner
    ADD CONSTRAINT fk_workspace_owner_owner_id FOREIGN KEY (owner_id) REFERENCES public.profil(id);


--
-- TOC entry 2879 (class 2606 OID 16537)
-- Name: workspace_owner fk_workspace_owner_workspace_id; Type: FK CONSTRAINT; Schema: public; Owner: mhaweb
--

ALTER TABLE ONLY public.workspace_owner
    ADD CONSTRAINT fk_workspace_owner_workspace_id FOREIGN KEY (workspace_id) REFERENCES public.workspace(id);


-- Completed on 2019-09-24 11:08:24

--
-- PostgreSQL database dump complete
--

