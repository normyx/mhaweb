package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.TodoTemplate;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.TodoTemplateRepository;
import com.mgoulene.mhaweb.service.dto.TodoTemplateCriteria;
import com.mgoulene.mhaweb.service.dto.TodoTemplateDTO;
import com.mgoulene.mhaweb.service.mapper.TodoTemplateMapper;

/**
 * Service for executing complex queries for {@link TodoTemplate} entities in the database.
 * The main input is a {@link TodoTemplateCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TodoTemplateDTO} or a {@link Page} of {@link TodoTemplateDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TodoTemplateQueryService extends QueryService<TodoTemplate> {

    private final Logger log = LoggerFactory.getLogger(TodoTemplateQueryService.class);

    private final TodoTemplateRepository todoTemplateRepository;

    private final TodoTemplateMapper todoTemplateMapper;

    public TodoTemplateQueryService(TodoTemplateRepository todoTemplateRepository, TodoTemplateMapper todoTemplateMapper) {
        this.todoTemplateRepository = todoTemplateRepository;
        this.todoTemplateMapper = todoTemplateMapper;
    }

    /**
     * Return a {@link List} of {@link TodoTemplateDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TodoTemplateDTO> findByCriteria(TodoTemplateCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TodoTemplate> specification = createSpecification(criteria);
        return todoTemplateMapper.toDto(todoTemplateRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TodoTemplateDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TodoTemplateDTO> findByCriteria(TodoTemplateCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TodoTemplate> specification = createSpecification(criteria);
        return todoTemplateRepository.findAll(specification, page)
            .map(todoTemplateMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TodoTemplateCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TodoTemplate> specification = createSpecification(criteria);
        return todoTemplateRepository.count(specification);
    }

    /**
     * Function to convert {@link TodoTemplateCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TodoTemplate> createSpecification(TodoTemplateCriteria criteria) {
        Specification<TodoTemplate> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TodoTemplate_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), TodoTemplate_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), TodoTemplate_.lastUpdate));
            }
            if (criteria.getTodoListTemplateId() != null) {
                specification = specification.and(buildSpecification(criteria.getTodoListTemplateId(),
                    root -> root.join(TodoTemplate_.todoListTemplate, JoinType.LEFT).get(TodoListTemplate_.id)));
            }
        }
        return specification;
    }
}
