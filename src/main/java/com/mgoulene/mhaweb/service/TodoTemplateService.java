package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.TodoTemplate;
import com.mgoulene.mhaweb.repository.TodoTemplateRepository;
import com.mgoulene.mhaweb.service.dto.TodoTemplateDTO;
import com.mgoulene.mhaweb.service.mapper.TodoTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TodoTemplate}.
 */
@Service
@Transactional
public class TodoTemplateService {

    private final Logger log = LoggerFactory.getLogger(TodoTemplateService.class);

    private final TodoTemplateRepository todoTemplateRepository;

    private final TodoTemplateMapper todoTemplateMapper;

    public TodoTemplateService(TodoTemplateRepository todoTemplateRepository, TodoTemplateMapper todoTemplateMapper) {
        this.todoTemplateRepository = todoTemplateRepository;
        this.todoTemplateMapper = todoTemplateMapper;
    }

    /**
     * Save a todoTemplate.
     *
     * @param todoTemplateDTO the entity to save.
     * @return the persisted entity.
     */
    public TodoTemplateDTO save(TodoTemplateDTO todoTemplateDTO) {
        log.debug("Request to save TodoTemplate : {}", todoTemplateDTO);
        TodoTemplate todoTemplate = todoTemplateMapper.toEntity(todoTemplateDTO);
        todoTemplate = todoTemplateRepository.save(todoTemplate);
        return todoTemplateMapper.toDto(todoTemplate);
    }

    /**
     * Get all the todoTemplates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TodoTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TodoTemplates");
        return todoTemplateRepository.findAll(pageable)
            .map(todoTemplateMapper::toDto);
    }

    /**
     * Get one todoTemplate by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TodoTemplateDTO> findOne(Long id) {
        log.debug("Request to get TodoTemplate : {}", id);
        return todoTemplateRepository.findById(id)
            .map(todoTemplateMapper::toDto);
    }

    /**
     * Delete the todoTemplate by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TodoTemplate : {}", id);
        todoTemplateRepository.deleteById(id);
    }
}
