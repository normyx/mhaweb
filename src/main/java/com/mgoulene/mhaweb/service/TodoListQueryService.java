package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.TodoList;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.TodoListRepository;
import com.mgoulene.mhaweb.service.dto.TodoListCriteria;
import com.mgoulene.mhaweb.service.dto.TodoListDTO;
import com.mgoulene.mhaweb.service.mapper.TodoListMapper;

/**
 * Service for executing complex queries for {@link TodoList} entities in the database.
 * The main input is a {@link TodoListCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TodoListDTO} or a {@link Page} of {@link TodoListDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TodoListQueryService extends QueryService<TodoList> {

    private final Logger log = LoggerFactory.getLogger(TodoListQueryService.class);

    private final TodoListRepository todoListRepository;

    private final TodoListMapper todoListMapper;

    public TodoListQueryService(TodoListRepository todoListRepository, TodoListMapper todoListMapper) {
        this.todoListRepository = todoListRepository;
        this.todoListMapper = todoListMapper;
    }

    /**
     * Return a {@link List} of {@link TodoListDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TodoListDTO> findByCriteria(TodoListCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TodoList> specification = createSpecification(criteria);
        return todoListMapper.toDto(todoListRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TodoListDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TodoListDTO> findByCriteria(TodoListCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TodoList> specification = createSpecification(criteria);
        return todoListRepository.findAll(specification, page)
            .map(todoListMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TodoListCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TodoList> specification = createSpecification(criteria);
        return todoListRepository.count(specification);
    }

    /**
     * Function to convert {@link TodoListCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TodoList> createSpecification(TodoListCriteria criteria) {
        Specification<TodoList> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TodoList_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), TodoList_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), TodoList_.lastUpdate));
            }
            if (criteria.getTodoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTodoId(),
                    root -> root.join(TodoList_.todos, JoinType.LEFT).get(Todo_.id)));
            }
            if (criteria.getOwnerId() != null) {
                specification = specification.and(buildSpecification(criteria.getOwnerId(),
                    root -> root.join(TodoList_.owners, JoinType.LEFT).get(Profil_.id)));
            }
            if (criteria.getWorkspaceId() != null) {
                specification = specification.and(buildSpecification(criteria.getWorkspaceId(),
                    root -> root.join(TodoList_.workspace, JoinType.LEFT).get(Workspace_.id)));
            }
        }
        return specification;
    }
}
