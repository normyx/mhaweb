package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.ShoppingItem;
import com.mgoulene.mhaweb.repository.ShoppingItemRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingItemDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShoppingItem}.
 */
@Service
@Transactional
public class ShoppingItemService {

    private final Logger log = LoggerFactory.getLogger(ShoppingItemService.class);

    private final ShoppingItemRepository shoppingItemRepository;

    private final ShoppingItemMapper shoppingItemMapper;

    public ShoppingItemService(ShoppingItemRepository shoppingItemRepository, ShoppingItemMapper shoppingItemMapper) {
        this.shoppingItemRepository = shoppingItemRepository;
        this.shoppingItemMapper = shoppingItemMapper;
    }

    /**
     * Save a shoppingItem.
     *
     * @param shoppingItemDTO the entity to save.
     * @return the persisted entity.
     */
    public ShoppingItemDTO save(ShoppingItemDTO shoppingItemDTO) {
        log.debug("Request to save ShoppingItem : {}", shoppingItemDTO);
        ShoppingItem shoppingItem = shoppingItemMapper.toEntity(shoppingItemDTO);
        shoppingItem = shoppingItemRepository.save(shoppingItem);
        return shoppingItemMapper.toDto(shoppingItem);
    }

    /**
     * Get all the shoppingItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShoppingItems");
        return shoppingItemRepository.findAll(pageable)
            .map(shoppingItemMapper::toDto);
    }

    /**
     * Get one shoppingItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ShoppingItemDTO> findOne(Long id) {
        log.debug("Request to get ShoppingItem : {}", id);
        return shoppingItemRepository.findById(id)
            .map(shoppingItemMapper::toDto);
    }

    /**
     * Delete the shoppingItem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ShoppingItem : {}", id);
        shoppingItemRepository.deleteById(id);
    }
}
