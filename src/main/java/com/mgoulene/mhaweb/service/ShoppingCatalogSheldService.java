package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.ShoppingCatalogSheld;
import com.mgoulene.mhaweb.repository.ShoppingCatalogSheldRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogSheldMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShoppingCatalogSheld}.
 */
@Service
@Transactional
public class ShoppingCatalogSheldService {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogSheldService.class);

    private final ShoppingCatalogSheldRepository shoppingCatalogSheldRepository;

    private final ShoppingCatalogSheldMapper shoppingCatalogSheldMapper;

    public ShoppingCatalogSheldService(ShoppingCatalogSheldRepository shoppingCatalogSheldRepository, ShoppingCatalogSheldMapper shoppingCatalogSheldMapper) {
        this.shoppingCatalogSheldRepository = shoppingCatalogSheldRepository;
        this.shoppingCatalogSheldMapper = shoppingCatalogSheldMapper;
    }

    /**
     * Save a shoppingCatalogSheld.
     *
     * @param shoppingCatalogSheldDTO the entity to save.
     * @return the persisted entity.
     */
    public ShoppingCatalogSheldDTO save(ShoppingCatalogSheldDTO shoppingCatalogSheldDTO) {
        log.debug("Request to save ShoppingCatalogSheld : {}", shoppingCatalogSheldDTO);
        ShoppingCatalogSheld shoppingCatalogSheld = shoppingCatalogSheldMapper.toEntity(shoppingCatalogSheldDTO);
        shoppingCatalogSheld = shoppingCatalogSheldRepository.save(shoppingCatalogSheld);
        return shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);
    }

    /**
     * Get all the shoppingCatalogShelds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingCatalogSheldDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShoppingCatalogShelds");
        return shoppingCatalogSheldRepository.findAll(pageable)
            .map(shoppingCatalogSheldMapper::toDto);
    }

    /**
     * Get one shoppingCatalogSheld by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ShoppingCatalogSheldDTO> findOne(Long id) {
        log.debug("Request to get ShoppingCatalogSheld : {}", id);
        return shoppingCatalogSheldRepository.findById(id)
            .map(shoppingCatalogSheldMapper::toDto);
    }

    /**
     * Delete the shoppingCatalogSheld by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ShoppingCatalogSheld : {}", id);
        shoppingCatalogSheldRepository.deleteById(id);
    }
}
