package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.ShoppingCatalogItem} entity.
 */
public class ShoppingCatalogItemDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 80)
    private String label;

    @NotNull
    private LocalDate lastUpdate;


    private Long shoppingCatalogSheldId;

    private String shoppingCatalogSheldLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getShoppingCatalogSheldId() {
        return shoppingCatalogSheldId;
    }

    public void setShoppingCatalogSheldId(Long shoppingCatalogSheldId) {
        this.shoppingCatalogSheldId = shoppingCatalogSheldId;
    }

    public String getShoppingCatalogSheldLabel() {
        return shoppingCatalogSheldLabel;
    }

    public void setShoppingCatalogSheldLabel(String shoppingCatalogSheldLabel) {
        this.shoppingCatalogSheldLabel = shoppingCatalogSheldLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoppingCatalogItemDTO shoppingCatalogItemDTO = (ShoppingCatalogItemDTO) o;
        if (shoppingCatalogItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shoppingCatalogItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShoppingCatalogItemDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", shoppingCatalogSheldId=" + getShoppingCatalogSheldId() +
            ", shoppingCatalogSheldLabel='" + getShoppingCatalogSheldLabel() + "'" +
            "}";
    }
}
