package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.Profil} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.ProfilResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /profils?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProfilCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter displayName;

    private LocalDateFilter lastUpdate;

    private LongFilter userId;

    private LongFilter profilDataId;

    private LongFilter spentId;

    private LongFilter shoppingCartId;

    private LongFilter taskId;

    private LongFilter taskProjectId;

    private LongFilter todoListId;

    private LongFilter todoListTemplateId;

    private LongFilter walletId;

    private LongFilter workspaceId;

    public ProfilCriteria() {
    }

    public ProfilCriteria(ProfilCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.displayName = other.displayName == null ? null : other.displayName.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.profilDataId = other.profilDataId == null ? null : other.profilDataId.copy();
        this.spentId = other.spentId == null ? null : other.spentId.copy();
        this.shoppingCartId = other.shoppingCartId == null ? null : other.shoppingCartId.copy();
        this.taskId = other.taskId == null ? null : other.taskId.copy();
        this.taskProjectId = other.taskProjectId == null ? null : other.taskProjectId.copy();
        this.todoListId = other.todoListId == null ? null : other.todoListId.copy();
        this.todoListTemplateId = other.todoListTemplateId == null ? null : other.todoListTemplateId.copy();
        this.walletId = other.walletId == null ? null : other.walletId.copy();
        this.workspaceId = other.workspaceId == null ? null : other.workspaceId.copy();
    }

    @Override
    public ProfilCriteria copy() {
        return new ProfilCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDisplayName() {
        return displayName;
    }

    public void setDisplayName(StringFilter displayName) {
        this.displayName = displayName;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getProfilDataId() {
        return profilDataId;
    }

    public void setProfilDataId(LongFilter profilDataId) {
        this.profilDataId = profilDataId;
    }

    public LongFilter getSpentId() {
        return spentId;
    }

    public void setSpentId(LongFilter spentId) {
        this.spentId = spentId;
    }

    public LongFilter getShoppingCartId() {
        return shoppingCartId;
    }

    public void setShoppingCartId(LongFilter shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }

    public LongFilter getTaskId() {
        return taskId;
    }

    public void setTaskId(LongFilter taskId) {
        this.taskId = taskId;
    }

    public LongFilter getTaskProjectId() {
        return taskProjectId;
    }

    public void setTaskProjectId(LongFilter taskProjectId) {
        this.taskProjectId = taskProjectId;
    }

    public LongFilter getTodoListId() {
        return todoListId;
    }

    public void setTodoListId(LongFilter todoListId) {
        this.todoListId = todoListId;
    }

    public LongFilter getTodoListTemplateId() {
        return todoListTemplateId;
    }

    public void setTodoListTemplateId(LongFilter todoListTemplateId) {
        this.todoListTemplateId = todoListTemplateId;
    }

    public LongFilter getWalletId() {
        return walletId;
    }

    public void setWalletId(LongFilter walletId) {
        this.walletId = walletId;
    }

    public LongFilter getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(LongFilter workspaceId) {
        this.workspaceId = workspaceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProfilCriteria that = (ProfilCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(displayName, that.displayName) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(profilDataId, that.profilDataId) &&
            Objects.equals(spentId, that.spentId) &&
            Objects.equals(shoppingCartId, that.shoppingCartId) &&
            Objects.equals(taskId, that.taskId) &&
            Objects.equals(taskProjectId, that.taskProjectId) &&
            Objects.equals(todoListId, that.todoListId) &&
            Objects.equals(todoListTemplateId, that.todoListTemplateId) &&
            Objects.equals(walletId, that.walletId) &&
            Objects.equals(workspaceId, that.workspaceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        displayName,
        lastUpdate,
        userId,
        profilDataId,
        spentId,
        shoppingCartId,
        taskId,
        taskProjectId,
        todoListId,
        todoListTemplateId,
        walletId,
        workspaceId
        );
    }

    @Override
    public String toString() {
        return "ProfilCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (displayName != null ? "displayName=" + displayName + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (profilDataId != null ? "profilDataId=" + profilDataId + ", " : "") +
                (spentId != null ? "spentId=" + spentId + ", " : "") +
                (shoppingCartId != null ? "shoppingCartId=" + shoppingCartId + ", " : "") +
                (taskId != null ? "taskId=" + taskId + ", " : "") +
                (taskProjectId != null ? "taskProjectId=" + taskProjectId + ", " : "") +
                (todoListId != null ? "todoListId=" + todoListId + ", " : "") +
                (todoListTemplateId != null ? "todoListTemplateId=" + todoListTemplateId + ", " : "") +
                (walletId != null ? "walletId=" + walletId + ", " : "") +
                (workspaceId != null ? "workspaceId=" + workspaceId + ", " : "") +
            "}";
    }

}
