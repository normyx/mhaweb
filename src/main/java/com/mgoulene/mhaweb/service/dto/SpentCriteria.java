package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.Spent} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.SpentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /spents?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SpentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private StringFilter description;

    private FloatFilter amount;

    private LocalDateFilter spentDate;

    private BooleanFilter confirmed;

    private LocalDateFilter lastUpdate;

    private LongFilter spentSharingId;

    private LongFilter walletId;

    private LongFilter spenderId;

    public SpentCriteria() {
    }

    public SpentCriteria(SpentCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.spentDate = other.spentDate == null ? null : other.spentDate.copy();
        this.confirmed = other.confirmed == null ? null : other.confirmed.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.spentSharingId = other.spentSharingId == null ? null : other.spentSharingId.copy();
        this.walletId = other.walletId == null ? null : other.walletId.copy();
        this.spenderId = other.spenderId == null ? null : other.spenderId.copy();
    }

    @Override
    public SpentCriteria copy() {
        return new SpentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public FloatFilter getAmount() {
        return amount;
    }

    public void setAmount(FloatFilter amount) {
        this.amount = amount;
    }

    public LocalDateFilter getSpentDate() {
        return spentDate;
    }

    public void setSpentDate(LocalDateFilter spentDate) {
        this.spentDate = spentDate;
    }

    public BooleanFilter getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(BooleanFilter confirmed) {
        this.confirmed = confirmed;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LongFilter getSpentSharingId() {
        return spentSharingId;
    }

    public void setSpentSharingId(LongFilter spentSharingId) {
        this.spentSharingId = spentSharingId;
    }

    public LongFilter getWalletId() {
        return walletId;
    }

    public void setWalletId(LongFilter walletId) {
        this.walletId = walletId;
    }

    public LongFilter getSpenderId() {
        return spenderId;
    }

    public void setSpenderId(LongFilter spenderId) {
        this.spenderId = spenderId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpentCriteria that = (SpentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(description, that.description) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(spentDate, that.spentDate) &&
            Objects.equals(confirmed, that.confirmed) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(spentSharingId, that.spentSharingId) &&
            Objects.equals(walletId, that.walletId) &&
            Objects.equals(spenderId, that.spenderId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        description,
        amount,
        spentDate,
        confirmed,
        lastUpdate,
        spentSharingId,
        walletId,
        spenderId
        );
    }

    @Override
    public String toString() {
        return "SpentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (spentDate != null ? "spentDate=" + spentDate + ", " : "") +
                (confirmed != null ? "confirmed=" + confirmed + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (spentSharingId != null ? "spentSharingId=" + spentSharingId + ", " : "") +
                (walletId != null ? "walletId=" + walletId + ", " : "") +
                (spenderId != null ? "spenderId=" + spenderId + ", " : "") +
            "}";
    }

}
