package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.Workspace} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.WorkspaceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /workspaces?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WorkspaceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private LocalDateFilter lastUpdate;

    private LongFilter shoppingCartId;

    private LongFilter taskProjectId;

    private LongFilter todoListId;

    private LongFilter todoListTemplateId;

    private LongFilter walletId;

    private LongFilter ownerId;

    public WorkspaceCriteria() {
    }

    public WorkspaceCriteria(WorkspaceCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.shoppingCartId = other.shoppingCartId == null ? null : other.shoppingCartId.copy();
        this.taskProjectId = other.taskProjectId == null ? null : other.taskProjectId.copy();
        this.todoListId = other.todoListId == null ? null : other.todoListId.copy();
        this.todoListTemplateId = other.todoListTemplateId == null ? null : other.todoListTemplateId.copy();
        this.walletId = other.walletId == null ? null : other.walletId.copy();
        this.ownerId = other.ownerId == null ? null : other.ownerId.copy();
    }

    @Override
    public WorkspaceCriteria copy() {
        return new WorkspaceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LongFilter getShoppingCartId() {
        return shoppingCartId;
    }

    public void setShoppingCartId(LongFilter shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }

    public LongFilter getTaskProjectId() {
        return taskProjectId;
    }

    public void setTaskProjectId(LongFilter taskProjectId) {
        this.taskProjectId = taskProjectId;
    }

    public LongFilter getTodoListId() {
        return todoListId;
    }

    public void setTodoListId(LongFilter todoListId) {
        this.todoListId = todoListId;
    }

    public LongFilter getTodoListTemplateId() {
        return todoListTemplateId;
    }

    public void setTodoListTemplateId(LongFilter todoListTemplateId) {
        this.todoListTemplateId = todoListTemplateId;
    }

    public LongFilter getWalletId() {
        return walletId;
    }

    public void setWalletId(LongFilter walletId) {
        this.walletId = walletId;
    }

    public LongFilter getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(LongFilter ownerId) {
        this.ownerId = ownerId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WorkspaceCriteria that = (WorkspaceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(shoppingCartId, that.shoppingCartId) &&
            Objects.equals(taskProjectId, that.taskProjectId) &&
            Objects.equals(todoListId, that.todoListId) &&
            Objects.equals(todoListTemplateId, that.todoListTemplateId) &&
            Objects.equals(walletId, that.walletId) &&
            Objects.equals(ownerId, that.ownerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        lastUpdate,
        shoppingCartId,
        taskProjectId,
        todoListId,
        todoListTemplateId,
        walletId,
        ownerId
        );
    }

    @Override
    public String toString() {
        return "WorkspaceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (shoppingCartId != null ? "shoppingCartId=" + shoppingCartId + ", " : "") +
                (taskProjectId != null ? "taskProjectId=" + taskProjectId + ", " : "") +
                (todoListId != null ? "todoListId=" + todoListId + ", " : "") +
                (todoListTemplateId != null ? "todoListTemplateId=" + todoListTemplateId + ", " : "") +
                (walletId != null ? "walletId=" + walletId + ", " : "") +
                (ownerId != null ? "ownerId=" + ownerId + ", " : "") +
            "}";
    }

}
