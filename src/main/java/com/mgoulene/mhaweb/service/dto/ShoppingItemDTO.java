package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.mgoulene.mhaweb.domain.enumeration.Unit;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.ShoppingItem} entity.
 */
public class ShoppingItemDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 80)
    private String label;

    @NotNull
    private LocalDate lastUpdate;

    @NotNull
    private Boolean checked;

    @DecimalMin(value = "0")
    private Float quantity;

    private Unit unit;


    private Long shoppingCatalogSheldId;

    private String shoppingCatalogSheldLabel;

    private Long shoppingCartId;

    private String shoppingCartLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Long getShoppingCatalogSheldId() {
        return shoppingCatalogSheldId;
    }

    public void setShoppingCatalogSheldId(Long shoppingCatalogSheldId) {
        this.shoppingCatalogSheldId = shoppingCatalogSheldId;
    }

    public String getShoppingCatalogSheldLabel() {
        return shoppingCatalogSheldLabel;
    }

    public void setShoppingCatalogSheldLabel(String shoppingCatalogSheldLabel) {
        this.shoppingCatalogSheldLabel = shoppingCatalogSheldLabel;
    }

    public Long getShoppingCartId() {
        return shoppingCartId;
    }

    public void setShoppingCartId(Long shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }

    public String getShoppingCartLabel() {
        return shoppingCartLabel;
    }

    public void setShoppingCartLabel(String shoppingCartLabel) {
        this.shoppingCartLabel = shoppingCartLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoppingItemDTO shoppingItemDTO = (ShoppingItemDTO) o;
        if (shoppingItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shoppingItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShoppingItemDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", checked='" + isChecked() + "'" +
            ", quantity=" + getQuantity() +
            ", unit='" + getUnit() + "'" +
            ", shoppingCatalogSheldId=" + getShoppingCatalogSheldId() +
            ", shoppingCatalogSheldLabel='" + getShoppingCatalogSheldLabel() + "'" +
            ", shoppingCartId=" + getShoppingCartId() +
            ", shoppingCartLabel='" + getShoppingCartLabel() + "'" +
            "}";
    }
}
