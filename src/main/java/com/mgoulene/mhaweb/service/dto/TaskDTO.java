package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.Task} entity.
 */
public class TaskDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    private String label;

    @Size(max = 4000)
    private String description;

    @NotNull
    private Boolean done;

    private LocalDate dueDate;

    @NotNull
    private LocalDate lastUpdate;


    private Set<ProfilDTO> owners = new HashSet<>();

    private Long taskProjectId;

    private String taskProjectLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<ProfilDTO> getOwners() {
        return owners;
    }

    public void setOwners(Set<ProfilDTO> profils) {
        this.owners = profils;
    }

    public Long getTaskProjectId() {
        return taskProjectId;
    }

    public void setTaskProjectId(Long taskProjectId) {
        this.taskProjectId = taskProjectId;
    }

    public String getTaskProjectLabel() {
        return taskProjectLabel;
    }

    public void setTaskProjectLabel(String taskProjectLabel) {
        this.taskProjectLabel = taskProjectLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TaskDTO taskDTO = (TaskDTO) o;
        if (taskDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), taskDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TaskDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", description='" + getDescription() + "'" +
            ", done='" + isDone() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", taskProjectId=" + getTaskProjectId() +
            ", taskProjectLabel='" + getTaskProjectLabel() + "'" +
            "}";
    }
}
