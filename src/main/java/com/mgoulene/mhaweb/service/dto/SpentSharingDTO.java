package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.SpentSharing} entity.
 */
public class SpentSharingDTO implements Serializable {

    private Long id;

    @NotNull
    private Float amountShare;

    @NotNull
    @Min(value = 0)
    private Integer share;

    @NotNull
    private LocalDate lastUpdate;


    private Long sharingProfilId;

    private String sharingProfilDisplayName;

    private Long spentId;

    private String spentLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmountShare() {
        return amountShare;
    }

    public void setAmountShare(Float amountShare) {
        this.amountShare = amountShare;
    }

    public Integer getShare() {
        return share;
    }

    public void setShare(Integer share) {
        this.share = share;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getSharingProfilId() {
        return sharingProfilId;
    }

    public void setSharingProfilId(Long profilId) {
        this.sharingProfilId = profilId;
    }

    public String getSharingProfilDisplayName() {
        return sharingProfilDisplayName;
    }

    public void setSharingProfilDisplayName(String profilDisplayName) {
        this.sharingProfilDisplayName = profilDisplayName;
    }

    public Long getSpentId() {
        return spentId;
    }

    public void setSpentId(Long spentId) {
        this.spentId = spentId;
    }

    public String getSpentLabel() {
        return spentLabel;
    }

    public void setSpentLabel(String spentLabel) {
        this.spentLabel = spentLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentSharingDTO spentSharingDTO = (SpentSharingDTO) o;
        if (spentSharingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentSharingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentSharingDTO{" +
            "id=" + getId() +
            ", amountShare=" + getAmountShare() +
            ", share=" + getShare() +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", sharingProfilId=" + getSharingProfilId() +
            ", sharingProfilDisplayName='" + getSharingProfilDisplayName() + "'" +
            ", spentId=" + getSpentId() +
            ", spentLabel='" + getSpentLabel() + "'" +
            "}";
    }
}
