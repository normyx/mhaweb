package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.mgoulene.mhaweb.domain.enumeration.Unit;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.ShoppingItem} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.ShoppingItemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shopping-items?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShoppingItemCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Unit
     */
    public static class UnitFilter extends Filter<Unit> {

        public UnitFilter() {
        }

        public UnitFilter(UnitFilter filter) {
            super(filter);
        }

        @Override
        public UnitFilter copy() {
            return new UnitFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private LocalDateFilter lastUpdate;

    private BooleanFilter checked;

    private FloatFilter quantity;

    private UnitFilter unit;

    private LongFilter shoppingCatalogSheldId;

    private LongFilter shoppingCartId;

    public ShoppingItemCriteria() {
    }

    public ShoppingItemCriteria(ShoppingItemCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.checked = other.checked == null ? null : other.checked.copy();
        this.quantity = other.quantity == null ? null : other.quantity.copy();
        this.unit = other.unit == null ? null : other.unit.copy();
        this.shoppingCatalogSheldId = other.shoppingCatalogSheldId == null ? null : other.shoppingCatalogSheldId.copy();
        this.shoppingCartId = other.shoppingCartId == null ? null : other.shoppingCartId.copy();
    }

    @Override
    public ShoppingItemCriteria copy() {
        return new ShoppingItemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public BooleanFilter getChecked() {
        return checked;
    }

    public void setChecked(BooleanFilter checked) {
        this.checked = checked;
    }

    public FloatFilter getQuantity() {
        return quantity;
    }

    public void setQuantity(FloatFilter quantity) {
        this.quantity = quantity;
    }

    public UnitFilter getUnit() {
        return unit;
    }

    public void setUnit(UnitFilter unit) {
        this.unit = unit;
    }

    public LongFilter getShoppingCatalogSheldId() {
        return shoppingCatalogSheldId;
    }

    public void setShoppingCatalogSheldId(LongFilter shoppingCatalogSheldId) {
        this.shoppingCatalogSheldId = shoppingCatalogSheldId;
    }

    public LongFilter getShoppingCartId() {
        return shoppingCartId;
    }

    public void setShoppingCartId(LongFilter shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShoppingItemCriteria that = (ShoppingItemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(checked, that.checked) &&
            Objects.equals(quantity, that.quantity) &&
            Objects.equals(unit, that.unit) &&
            Objects.equals(shoppingCatalogSheldId, that.shoppingCatalogSheldId) &&
            Objects.equals(shoppingCartId, that.shoppingCartId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        lastUpdate,
        checked,
        quantity,
        unit,
        shoppingCatalogSheldId,
        shoppingCartId
        );
    }

    @Override
    public String toString() {
        return "ShoppingItemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (checked != null ? "checked=" + checked + ", " : "") +
                (quantity != null ? "quantity=" + quantity + ", " : "") +
                (unit != null ? "unit=" + unit + ", " : "") +
                (shoppingCatalogSheldId != null ? "shoppingCatalogSheldId=" + shoppingCatalogSheldId + ", " : "") +
                (shoppingCartId != null ? "shoppingCartId=" + shoppingCartId + ", " : "") +
            "}";
    }

}
