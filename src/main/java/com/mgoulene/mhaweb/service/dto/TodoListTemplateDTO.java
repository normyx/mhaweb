package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.TodoListTemplate} entity.
 */
public class TodoListTemplateDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    private String label;

    @NotNull
    private LocalDate lastUpdate;


    private Set<ProfilDTO> owners = new HashSet<>();

    private Long workspaceId;

    private String workspaceLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<ProfilDTO> getOwners() {
        return owners;
    }

    public void setOwners(Set<ProfilDTO> profils) {
        this.owners = profils;
    }

    public Long getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(Long workspaceId) {
        this.workspaceId = workspaceId;
    }

    public String getWorkspaceLabel() {
        return workspaceLabel;
    }

    public void setWorkspaceLabel(String workspaceLabel) {
        this.workspaceLabel = workspaceLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TodoListTemplateDTO todoListTemplateDTO = (TodoListTemplateDTO) o;
        if (todoListTemplateDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), todoListTemplateDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TodoListTemplateDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", workspaceId=" + getWorkspaceId() +
            ", workspaceLabel='" + getWorkspaceLabel() + "'" +
            "}";
    }
}
