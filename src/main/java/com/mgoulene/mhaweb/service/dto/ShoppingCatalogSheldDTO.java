package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.ShoppingCatalogSheld} entity.
 */
public class ShoppingCatalogSheldDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    private String label;

    @NotNull
    private LocalDate lastUpdate;

    @NotNull
    private Boolean isDefault;

    @NotNull
    private Boolean isDeleted;


    private Long shoppingCatalogCartId;

    private String shoppingCatalogCartLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getShoppingCatalogCartId() {
        return shoppingCatalogCartId;
    }

    public void setShoppingCatalogCartId(Long shoppingCatalogCartId) {
        this.shoppingCatalogCartId = shoppingCatalogCartId;
    }

    public String getShoppingCatalogCartLabel() {
        return shoppingCatalogCartLabel;
    }

    public void setShoppingCatalogCartLabel(String shoppingCatalogCartLabel) {
        this.shoppingCatalogCartLabel = shoppingCatalogCartLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = (ShoppingCatalogSheldDTO) o;
        if (shoppingCatalogSheldDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shoppingCatalogSheldDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShoppingCatalogSheldDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", isDefault='" + isIsDefault() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", shoppingCatalogCartId=" + getShoppingCatalogCartId() +
            ", shoppingCatalogCartLabel='" + getShoppingCatalogCartLabel() + "'" +
            "}";
    }
}
