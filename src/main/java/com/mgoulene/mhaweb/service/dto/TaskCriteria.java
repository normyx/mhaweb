package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.Task} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.TaskResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tasks?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TaskCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private StringFilter description;

    private BooleanFilter done;

    private LocalDateFilter dueDate;

    private LocalDateFilter lastUpdate;

    private LongFilter ownerId;

    private LongFilter taskProjectId;

    public TaskCriteria() {
    }

    public TaskCriteria(TaskCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.done = other.done == null ? null : other.done.copy();
        this.dueDate = other.dueDate == null ? null : other.dueDate.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.ownerId = other.ownerId == null ? null : other.ownerId.copy();
        this.taskProjectId = other.taskProjectId == null ? null : other.taskProjectId.copy();
    }

    @Override
    public TaskCriteria copy() {
        return new TaskCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public BooleanFilter getDone() {
        return done;
    }

    public void setDone(BooleanFilter done) {
        this.done = done;
    }

    public LocalDateFilter getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateFilter dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LongFilter getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(LongFilter ownerId) {
        this.ownerId = ownerId;
    }

    public LongFilter getTaskProjectId() {
        return taskProjectId;
    }

    public void setTaskProjectId(LongFilter taskProjectId) {
        this.taskProjectId = taskProjectId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TaskCriteria that = (TaskCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(description, that.description) &&
            Objects.equals(done, that.done) &&
            Objects.equals(dueDate, that.dueDate) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(ownerId, that.ownerId) &&
            Objects.equals(taskProjectId, that.taskProjectId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        description,
        done,
        dueDate,
        lastUpdate,
        ownerId,
        taskProjectId
        );
    }

    @Override
    public String toString() {
        return "TaskCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (done != null ? "done=" + done + ", " : "") +
                (dueDate != null ? "dueDate=" + dueDate + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (ownerId != null ? "ownerId=" + ownerId + ", " : "") +
                (taskProjectId != null ? "taskProjectId=" + taskProjectId + ", " : "") +
            "}";
    }

}
