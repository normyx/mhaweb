package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.TodoTemplate} entity.
 */
public class TodoTemplateDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    private String label;

    @NotNull
    private LocalDate lastUpdate;


    private Long todoListTemplateId;

    private String todoListTemplateLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getTodoListTemplateId() {
        return todoListTemplateId;
    }

    public void setTodoListTemplateId(Long todoListTemplateId) {
        this.todoListTemplateId = todoListTemplateId;
    }

    public String getTodoListTemplateLabel() {
        return todoListTemplateLabel;
    }

    public void setTodoListTemplateLabel(String todoListTemplateLabel) {
        this.todoListTemplateLabel = todoListTemplateLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TodoTemplateDTO todoTemplateDTO = (TodoTemplateDTO) o;
        if (todoTemplateDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), todoTemplateDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TodoTemplateDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", todoListTemplateId=" + getTodoListTemplateId() +
            ", todoListTemplateLabel='" + getTodoListTemplateLabel() + "'" +
            "}";
    }
}
