package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.ProfilData} entity.
 */
public class ProfilDataDTO implements Serializable {

    private Long id;

    
    @Lob
    private byte[] photo;

    private String photoContentType;
    @NotNull
    private LocalDate lastUpdate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProfilDataDTO profilDataDTO = (ProfilDataDTO) o;
        if (profilDataDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profilDataDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProfilDataDTO{" +
            "id=" + getId() +
            ", photo='" + getPhoto() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
