package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.SpentSharingConfig} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.SpentSharingConfigResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /spent-sharing-configs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SpentSharingConfigCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter share;

    private LocalDateFilter lastUpdate;

    private LongFilter spentConfigId;

    private LongFilter profilId;

    public SpentSharingConfigCriteria() {
    }

    public SpentSharingConfigCriteria(SpentSharingConfigCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.share = other.share == null ? null : other.share.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.spentConfigId = other.spentConfigId == null ? null : other.spentConfigId.copy();
        this.profilId = other.profilId == null ? null : other.profilId.copy();
    }

    @Override
    public SpentSharingConfigCriteria copy() {
        return new SpentSharingConfigCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getShare() {
        return share;
    }

    public void setShare(IntegerFilter share) {
        this.share = share;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LongFilter getSpentConfigId() {
        return spentConfigId;
    }

    public void setSpentConfigId(LongFilter spentConfigId) {
        this.spentConfigId = spentConfigId;
    }

    public LongFilter getProfilId() {
        return profilId;
    }

    public void setProfilId(LongFilter profilId) {
        this.profilId = profilId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpentSharingConfigCriteria that = (SpentSharingConfigCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(share, that.share) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(spentConfigId, that.spentConfigId) &&
            Objects.equals(profilId, that.profilId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        share,
        lastUpdate,
        spentConfigId,
        profilId
        );
    }

    @Override
    public String toString() {
        return "SpentSharingConfigCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (share != null ? "share=" + share + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (spentConfigId != null ? "spentConfigId=" + spentConfigId + ", " : "") +
                (profilId != null ? "profilId=" + profilId + ", " : "") +
            "}";
    }

}
