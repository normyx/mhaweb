package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.TodoTemplate} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.TodoTemplateResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /todo-templates?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TodoTemplateCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private LocalDateFilter lastUpdate;

    private LongFilter todoListTemplateId;

    public TodoTemplateCriteria() {
    }

    public TodoTemplateCriteria(TodoTemplateCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.todoListTemplateId = other.todoListTemplateId == null ? null : other.todoListTemplateId.copy();
    }

    @Override
    public TodoTemplateCriteria copy() {
        return new TodoTemplateCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LongFilter getTodoListTemplateId() {
        return todoListTemplateId;
    }

    public void setTodoListTemplateId(LongFilter todoListTemplateId) {
        this.todoListTemplateId = todoListTemplateId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TodoTemplateCriteria that = (TodoTemplateCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(todoListTemplateId, that.todoListTemplateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        lastUpdate,
        todoListTemplateId
        );
    }

    @Override
    public String toString() {
        return "TodoTemplateCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (todoListTemplateId != null ? "todoListTemplateId=" + todoListTemplateId + ", " : "") +
            "}";
    }

}
