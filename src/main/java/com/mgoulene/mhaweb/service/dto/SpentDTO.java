package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.Spent} entity.
 */
public class SpentDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 30)
    private String label;

    @Size(max = 200)
    private String description;

    @NotNull
    private Float amount;

    @NotNull
    private LocalDate spentDate;

    @NotNull
    private Boolean confirmed;

    @NotNull
    private LocalDate lastUpdate;


    private Long walletId;

    private String walletLabel;

    private Long spenderId;

    private String spenderDisplayName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public LocalDate getSpentDate() {
        return spentDate;
    }

    public void setSpentDate(LocalDate spentDate) {
        this.spentDate = spentDate;
    }

    public Boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public String getWalletLabel() {
        return walletLabel;
    }

    public void setWalletLabel(String walletLabel) {
        this.walletLabel = walletLabel;
    }

    public Long getSpenderId() {
        return spenderId;
    }

    public void setSpenderId(Long profilId) {
        this.spenderId = profilId;
    }

    public String getSpenderDisplayName() {
        return spenderDisplayName;
    }

    public void setSpenderDisplayName(String profilDisplayName) {
        this.spenderDisplayName = profilDisplayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentDTO spentDTO = (SpentDTO) o;
        if (spentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", description='" + getDescription() + "'" +
            ", amount=" + getAmount() +
            ", spentDate='" + getSpentDate() + "'" +
            ", confirmed='" + isConfirmed() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", walletId=" + getWalletId() +
            ", walletLabel='" + getWalletLabel() + "'" +
            ", spenderId=" + getSpenderId() +
            ", spenderDisplayName='" + getSpenderDisplayName() + "'" +
            "}";
    }
}
