package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.SpentSharingConfig} entity.
 */
public class SpentSharingConfigDTO implements Serializable {

    private Long id;

    @NotNull
    @Min(value = 0)
    private Integer share;

    @NotNull
    private LocalDate lastUpdate;


    private Long spentConfigId;

    private String spentConfigLabel;

    private Long profilId;

    private String profilDisplayName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getShare() {
        return share;
    }

    public void setShare(Integer share) {
        this.share = share;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getSpentConfigId() {
        return spentConfigId;
    }

    public void setSpentConfigId(Long spentConfigId) {
        this.spentConfigId = spentConfigId;
    }

    public String getSpentConfigLabel() {
        return spentConfigLabel;
    }

    public void setSpentConfigLabel(String spentConfigLabel) {
        this.spentConfigLabel = spentConfigLabel;
    }

    public Long getProfilId() {
        return profilId;
    }

    public void setProfilId(Long profilId) {
        this.profilId = profilId;
    }

    public String getProfilDisplayName() {
        return profilDisplayName;
    }

    public void setProfilDisplayName(String profilDisplayName) {
        this.profilDisplayName = profilDisplayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentSharingConfigDTO spentSharingConfigDTO = (SpentSharingConfigDTO) o;
        if (spentSharingConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentSharingConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentSharingConfigDTO{" +
            "id=" + getId() +
            ", share=" + getShare() +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", spentConfigId=" + getSpentConfigId() +
            ", spentConfigLabel='" + getSpentConfigLabel() + "'" +
            ", profilId=" + getProfilId() +
            ", profilDisplayName='" + getProfilDisplayName() + "'" +
            "}";
    }
}
