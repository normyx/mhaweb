package com.mgoulene.mhaweb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.mhaweb.domain.ShoppingCatalogSheld} entity. This class is used
 * in {@link com.mgoulene.mhaweb.web.rest.ShoppingCatalogSheldResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shopping-catalog-shelds?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShoppingCatalogSheldCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private LocalDateFilter lastUpdate;

    private BooleanFilter isDefault;

    private BooleanFilter isDeleted;

    private LongFilter shoppingCatalogItemId;

    private LongFilter shoppingCatalogCartId;

    public ShoppingCatalogSheldCriteria() {
    }

    public ShoppingCatalogSheldCriteria(ShoppingCatalogSheldCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.isDefault = other.isDefault == null ? null : other.isDefault.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.shoppingCatalogItemId = other.shoppingCatalogItemId == null ? null : other.shoppingCatalogItemId.copy();
        this.shoppingCatalogCartId = other.shoppingCatalogCartId == null ? null : other.shoppingCatalogCartId.copy();
    }

    @Override
    public ShoppingCatalogSheldCriteria copy() {
        return new ShoppingCatalogSheldCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public LocalDateFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public BooleanFilter getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(BooleanFilter isDefault) {
        this.isDefault = isDefault;
    }

    public BooleanFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(BooleanFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LongFilter getShoppingCatalogItemId() {
        return shoppingCatalogItemId;
    }

    public void setShoppingCatalogItemId(LongFilter shoppingCatalogItemId) {
        this.shoppingCatalogItemId = shoppingCatalogItemId;
    }

    public LongFilter getShoppingCatalogCartId() {
        return shoppingCatalogCartId;
    }

    public void setShoppingCatalogCartId(LongFilter shoppingCatalogCartId) {
        this.shoppingCatalogCartId = shoppingCatalogCartId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShoppingCatalogSheldCriteria that = (ShoppingCatalogSheldCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(isDefault, that.isDefault) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(shoppingCatalogItemId, that.shoppingCatalogItemId) &&
            Objects.equals(shoppingCatalogCartId, that.shoppingCatalogCartId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        lastUpdate,
        isDefault,
        isDeleted,
        shoppingCatalogItemId,
        shoppingCatalogCartId
        );
    }

    @Override
    public String toString() {
        return "ShoppingCatalogSheldCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (isDefault != null ? "isDefault=" + isDefault + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (shoppingCatalogItemId != null ? "shoppingCatalogItemId=" + shoppingCatalogItemId + ", " : "") +
                (shoppingCatalogCartId != null ? "shoppingCatalogCartId=" + shoppingCatalogCartId + ", " : "") +
            "}";
    }

}
