package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.ShoppingCatalogSheld;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.ShoppingCatalogSheldRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldCriteria;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogSheldMapper;

/**
 * Service for executing complex queries for {@link ShoppingCatalogSheld} entities in the database.
 * The main input is a {@link ShoppingCatalogSheldCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShoppingCatalogSheldDTO} or a {@link Page} of {@link ShoppingCatalogSheldDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShoppingCatalogSheldQueryService extends QueryService<ShoppingCatalogSheld> {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogSheldQueryService.class);

    private final ShoppingCatalogSheldRepository shoppingCatalogSheldRepository;

    private final ShoppingCatalogSheldMapper shoppingCatalogSheldMapper;

    public ShoppingCatalogSheldQueryService(ShoppingCatalogSheldRepository shoppingCatalogSheldRepository, ShoppingCatalogSheldMapper shoppingCatalogSheldMapper) {
        this.shoppingCatalogSheldRepository = shoppingCatalogSheldRepository;
        this.shoppingCatalogSheldMapper = shoppingCatalogSheldMapper;
    }

    /**
     * Return a {@link List} of {@link ShoppingCatalogSheldDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShoppingCatalogSheldDTO> findByCriteria(ShoppingCatalogSheldCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShoppingCatalogSheld> specification = createSpecification(criteria);
        return shoppingCatalogSheldMapper.toDto(shoppingCatalogSheldRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShoppingCatalogSheldDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingCatalogSheldDTO> findByCriteria(ShoppingCatalogSheldCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShoppingCatalogSheld> specification = createSpecification(criteria);
        return shoppingCatalogSheldRepository.findAll(specification, page)
            .map(shoppingCatalogSheldMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShoppingCatalogSheldCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShoppingCatalogSheld> specification = createSpecification(criteria);
        return shoppingCatalogSheldRepository.count(specification);
    }

    /**
     * Function to convert {@link ShoppingCatalogSheldCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ShoppingCatalogSheld> createSpecification(ShoppingCatalogSheldCriteria criteria) {
        Specification<ShoppingCatalogSheld> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ShoppingCatalogSheld_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), ShoppingCatalogSheld_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), ShoppingCatalogSheld_.lastUpdate));
            }
            if (criteria.getIsDefault() != null) {
                specification = specification.and(buildSpecification(criteria.getIsDefault(), ShoppingCatalogSheld_.isDefault));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildSpecification(criteria.getIsDeleted(), ShoppingCatalogSheld_.isDeleted));
            }
            if (criteria.getShoppingCatalogItemId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCatalogItemId(),
                    root -> root.join(ShoppingCatalogSheld_.shoppingCatalogItems, JoinType.LEFT).get(ShoppingCatalogItem_.id)));
            }
            if (criteria.getShoppingCatalogCartId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCatalogCartId(),
                    root -> root.join(ShoppingCatalogSheld_.shoppingCatalogCart, JoinType.LEFT).get(ShoppingCatalogCart_.id)));
            }
        }
        return specification;
    }
}
