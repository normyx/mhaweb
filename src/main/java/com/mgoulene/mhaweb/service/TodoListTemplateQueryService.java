package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.TodoListTemplate;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.TodoListTemplateRepository;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateCriteria;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateDTO;
import com.mgoulene.mhaweb.service.mapper.TodoListTemplateMapper;

/**
 * Service for executing complex queries for {@link TodoListTemplate} entities in the database.
 * The main input is a {@link TodoListTemplateCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TodoListTemplateDTO} or a {@link Page} of {@link TodoListTemplateDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TodoListTemplateQueryService extends QueryService<TodoListTemplate> {

    private final Logger log = LoggerFactory.getLogger(TodoListTemplateQueryService.class);

    private final TodoListTemplateRepository todoListTemplateRepository;

    private final TodoListTemplateMapper todoListTemplateMapper;

    public TodoListTemplateQueryService(TodoListTemplateRepository todoListTemplateRepository, TodoListTemplateMapper todoListTemplateMapper) {
        this.todoListTemplateRepository = todoListTemplateRepository;
        this.todoListTemplateMapper = todoListTemplateMapper;
    }

    /**
     * Return a {@link List} of {@link TodoListTemplateDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TodoListTemplateDTO> findByCriteria(TodoListTemplateCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TodoListTemplate> specification = createSpecification(criteria);
        return todoListTemplateMapper.toDto(todoListTemplateRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TodoListTemplateDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TodoListTemplateDTO> findByCriteria(TodoListTemplateCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TodoListTemplate> specification = createSpecification(criteria);
        return todoListTemplateRepository.findAll(specification, page)
            .map(todoListTemplateMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TodoListTemplateCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TodoListTemplate> specification = createSpecification(criteria);
        return todoListTemplateRepository.count(specification);
    }

    /**
     * Function to convert {@link TodoListTemplateCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TodoListTemplate> createSpecification(TodoListTemplateCriteria criteria) {
        Specification<TodoListTemplate> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TodoListTemplate_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), TodoListTemplate_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), TodoListTemplate_.lastUpdate));
            }
            if (criteria.getTodoTemplateId() != null) {
                specification = specification.and(buildSpecification(criteria.getTodoTemplateId(),
                    root -> root.join(TodoListTemplate_.todoTemplates, JoinType.LEFT).get(TodoTemplate_.id)));
            }
            if (criteria.getOwnerId() != null) {
                specification = specification.and(buildSpecification(criteria.getOwnerId(),
                    root -> root.join(TodoListTemplate_.owners, JoinType.LEFT).get(Profil_.id)));
            }
            if (criteria.getWorkspaceId() != null) {
                specification = specification.and(buildSpecification(criteria.getWorkspaceId(),
                    root -> root.join(TodoListTemplate_.workspace, JoinType.LEFT).get(Workspace_.id)));
            }
        }
        return specification;
    }
}
