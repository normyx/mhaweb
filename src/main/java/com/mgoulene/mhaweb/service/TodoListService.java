package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.TodoList;
import com.mgoulene.mhaweb.repository.TodoListRepository;
import com.mgoulene.mhaweb.service.dto.TodoListDTO;
import com.mgoulene.mhaweb.service.mapper.TodoListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TodoList}.
 */
@Service
@Transactional
public class TodoListService {

    private final Logger log = LoggerFactory.getLogger(TodoListService.class);

    private final TodoListRepository todoListRepository;

    private final TodoListMapper todoListMapper;

    public TodoListService(TodoListRepository todoListRepository, TodoListMapper todoListMapper) {
        this.todoListRepository = todoListRepository;
        this.todoListMapper = todoListMapper;
    }

    /**
     * Save a todoList.
     *
     * @param todoListDTO the entity to save.
     * @return the persisted entity.
     */
    public TodoListDTO save(TodoListDTO todoListDTO) {
        log.debug("Request to save TodoList : {}", todoListDTO);
        TodoList todoList = todoListMapper.toEntity(todoListDTO);
        todoList = todoListRepository.save(todoList);
        return todoListMapper.toDto(todoList);
    }

    /**
     * Get all the todoLists.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TodoListDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TodoLists");
        return todoListRepository.findAll(pageable)
            .map(todoListMapper::toDto);
    }

    /**
     * Get all the todoLists with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<TodoListDTO> findAllWithEagerRelationships(Pageable pageable) {
        return todoListRepository.findAllWithEagerRelationships(pageable).map(todoListMapper::toDto);
    }

    /**
     * Get one todoList by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TodoListDTO> findOne(Long id) {
        log.debug("Request to get TodoList : {}", id);
        return todoListRepository.findOneWithEagerRelationships(id)
            .map(todoListMapper::toDto);
    }

    /**
     * Delete the todoList by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TodoList : {}", id);
        todoListRepository.deleteById(id);
    }
}
