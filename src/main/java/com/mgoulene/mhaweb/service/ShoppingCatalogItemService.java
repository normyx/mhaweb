package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.ShoppingCatalogItem;
import com.mgoulene.mhaweb.repository.ShoppingCatalogItemRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShoppingCatalogItem}.
 */
@Service
@Transactional
public class ShoppingCatalogItemService {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogItemService.class);

    private final ShoppingCatalogItemRepository shoppingCatalogItemRepository;

    private final ShoppingCatalogItemMapper shoppingCatalogItemMapper;

    public ShoppingCatalogItemService(ShoppingCatalogItemRepository shoppingCatalogItemRepository, ShoppingCatalogItemMapper shoppingCatalogItemMapper) {
        this.shoppingCatalogItemRepository = shoppingCatalogItemRepository;
        this.shoppingCatalogItemMapper = shoppingCatalogItemMapper;
    }

    /**
     * Save a shoppingCatalogItem.
     *
     * @param shoppingCatalogItemDTO the entity to save.
     * @return the persisted entity.
     */
    public ShoppingCatalogItemDTO save(ShoppingCatalogItemDTO shoppingCatalogItemDTO) {
        log.debug("Request to save ShoppingCatalogItem : {}", shoppingCatalogItemDTO);
        ShoppingCatalogItem shoppingCatalogItem = shoppingCatalogItemMapper.toEntity(shoppingCatalogItemDTO);
        shoppingCatalogItem = shoppingCatalogItemRepository.save(shoppingCatalogItem);
        return shoppingCatalogItemMapper.toDto(shoppingCatalogItem);
    }

    /**
     * Get all the shoppingCatalogItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingCatalogItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShoppingCatalogItems");
        return shoppingCatalogItemRepository.findAll(pageable)
            .map(shoppingCatalogItemMapper::toDto);
    }

    /**
     * Get one shoppingCatalogItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ShoppingCatalogItemDTO> findOne(Long id) {
        log.debug("Request to get ShoppingCatalogItem : {}", id);
        return shoppingCatalogItemRepository.findById(id)
            .map(shoppingCatalogItemMapper::toDto);
    }

    /**
     * Delete the shoppingCatalogItem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ShoppingCatalogItem : {}", id);
        shoppingCatalogItemRepository.deleteById(id);
    }
}
