package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.ShoppingItem;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.ShoppingItemRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingItemCriteria;
import com.mgoulene.mhaweb.service.dto.ShoppingItemDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingItemMapper;

/**
 * Service for executing complex queries for {@link ShoppingItem} entities in the database.
 * The main input is a {@link ShoppingItemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShoppingItemDTO} or a {@link Page} of {@link ShoppingItemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShoppingItemQueryService extends QueryService<ShoppingItem> {

    private final Logger log = LoggerFactory.getLogger(ShoppingItemQueryService.class);

    private final ShoppingItemRepository shoppingItemRepository;

    private final ShoppingItemMapper shoppingItemMapper;

    public ShoppingItemQueryService(ShoppingItemRepository shoppingItemRepository, ShoppingItemMapper shoppingItemMapper) {
        this.shoppingItemRepository = shoppingItemRepository;
        this.shoppingItemMapper = shoppingItemMapper;
    }

    /**
     * Return a {@link List} of {@link ShoppingItemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShoppingItemDTO> findByCriteria(ShoppingItemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShoppingItem> specification = createSpecification(criteria);
        return shoppingItemMapper.toDto(shoppingItemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShoppingItemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingItemDTO> findByCriteria(ShoppingItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShoppingItem> specification = createSpecification(criteria);
        return shoppingItemRepository.findAll(specification, page)
            .map(shoppingItemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShoppingItemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShoppingItem> specification = createSpecification(criteria);
        return shoppingItemRepository.count(specification);
    }

    /**
     * Function to convert {@link ShoppingItemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ShoppingItem> createSpecification(ShoppingItemCriteria criteria) {
        Specification<ShoppingItem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ShoppingItem_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), ShoppingItem_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), ShoppingItem_.lastUpdate));
            }
            if (criteria.getChecked() != null) {
                specification = specification.and(buildSpecification(criteria.getChecked(), ShoppingItem_.checked));
            }
            if (criteria.getQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantity(), ShoppingItem_.quantity));
            }
            if (criteria.getUnit() != null) {
                specification = specification.and(buildSpecification(criteria.getUnit(), ShoppingItem_.unit));
            }
            if (criteria.getShoppingCatalogSheldId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCatalogSheldId(),
                    root -> root.join(ShoppingItem_.shoppingCatalogSheld, JoinType.LEFT).get(ShoppingCatalogSheld_.id)));
            }
            if (criteria.getShoppingCartId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCartId(),
                    root -> root.join(ShoppingItem_.shoppingCart, JoinType.LEFT).get(ShoppingCart_.id)));
            }
        }
        return specification;
    }
}
