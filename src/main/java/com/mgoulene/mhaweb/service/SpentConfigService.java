package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.SpentConfig;
import com.mgoulene.mhaweb.repository.SpentConfigRepository;
import com.mgoulene.mhaweb.service.dto.SpentConfigDTO;
import com.mgoulene.mhaweb.service.mapper.SpentConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SpentConfig}.
 */
@Service
@Transactional
public class SpentConfigService {

    private final Logger log = LoggerFactory.getLogger(SpentConfigService.class);

    private final SpentConfigRepository spentConfigRepository;

    private final SpentConfigMapper spentConfigMapper;

    public SpentConfigService(SpentConfigRepository spentConfigRepository, SpentConfigMapper spentConfigMapper) {
        this.spentConfigRepository = spentConfigRepository;
        this.spentConfigMapper = spentConfigMapper;
    }

    /**
     * Save a spentConfig.
     *
     * @param spentConfigDTO the entity to save.
     * @return the persisted entity.
     */
    public SpentConfigDTO save(SpentConfigDTO spentConfigDTO) {
        log.debug("Request to save SpentConfig : {}", spentConfigDTO);
        SpentConfig spentConfig = spentConfigMapper.toEntity(spentConfigDTO);
        spentConfig = spentConfigRepository.save(spentConfig);
        return spentConfigMapper.toDto(spentConfig);
    }

    /**
     * Get all the spentConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SpentConfigs");
        return spentConfigRepository.findAll(pageable)
            .map(spentConfigMapper::toDto);
    }

    /**
     * Get one spentConfig by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpentConfigDTO> findOne(Long id) {
        log.debug("Request to get SpentConfig : {}", id);
        return spentConfigRepository.findById(id)
            .map(spentConfigMapper::toDto);
    }

    /**
     * Delete the spentConfig by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SpentConfig : {}", id);
        spentConfigRepository.deleteById(id);
    }
}
