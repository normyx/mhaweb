package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.Spent;
import com.mgoulene.mhaweb.repository.SpentRepository;
import com.mgoulene.mhaweb.service.dto.SpentDTO;
import com.mgoulene.mhaweb.service.mapper.SpentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Spent}.
 */
@Service
@Transactional
public class SpentService {

    private final Logger log = LoggerFactory.getLogger(SpentService.class);

    private final SpentRepository spentRepository;

    private final SpentMapper spentMapper;

    public SpentService(SpentRepository spentRepository, SpentMapper spentMapper) {
        this.spentRepository = spentRepository;
        this.spentMapper = spentMapper;
    }

    /**
     * Save a spent.
     *
     * @param spentDTO the entity to save.
     * @return the persisted entity.
     */
    public SpentDTO save(SpentDTO spentDTO) {
        log.debug("Request to save Spent : {}", spentDTO);
        Spent spent = spentMapper.toEntity(spentDTO);
        spent = spentRepository.save(spent);
        return spentMapper.toDto(spent);
    }

    /**
     * Get all the spents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Spents");
        return spentRepository.findAll(pageable)
            .map(spentMapper::toDto);
    }

    /**
     * Get one spent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpentDTO> findOne(Long id) {
        log.debug("Request to get Spent : {}", id);
        return spentRepository.findById(id)
            .map(spentMapper::toDto);
    }

    /**
     * Delete the spent by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Spent : {}", id);
        spentRepository.deleteById(id);
    }
}
