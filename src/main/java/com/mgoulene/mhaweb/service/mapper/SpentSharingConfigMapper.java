package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.SpentSharingConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SpentSharingConfig} and its DTO {@link SpentSharingConfigDTO}.
 */
@Mapper(componentModel = "spring", uses = {SpentConfigMapper.class, ProfilMapper.class})
public interface SpentSharingConfigMapper extends EntityMapper<SpentSharingConfigDTO, SpentSharingConfig> {

    @Mapping(source = "spentConfig.id", target = "spentConfigId")
    @Mapping(source = "spentConfig.label", target = "spentConfigLabel")
    @Mapping(source = "profil.id", target = "profilId")
    @Mapping(source = "profil.displayName", target = "profilDisplayName")
    SpentSharingConfigDTO toDto(SpentSharingConfig spentSharingConfig);

    @Mapping(source = "spentConfigId", target = "spentConfig")
    @Mapping(source = "profilId", target = "profil")
    SpentSharingConfig toEntity(SpentSharingConfigDTO spentSharingConfigDTO);

    default SpentSharingConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        SpentSharingConfig spentSharingConfig = new SpentSharingConfig();
        spentSharingConfig.setId(id);
        return spentSharingConfig;
    }
}
