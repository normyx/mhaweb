package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.ProfilDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProfilData} and its DTO {@link ProfilDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProfilDataMapper extends EntityMapper<ProfilDataDTO, ProfilData> {



    default ProfilData fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProfilData profilData = new ProfilData();
        profilData.setId(id);
        return profilData;
    }
}
