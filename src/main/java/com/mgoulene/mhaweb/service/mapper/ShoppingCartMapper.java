package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.ShoppingCartDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShoppingCart} and its DTO {@link ShoppingCartDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShoppingCatalogCartMapper.class, ProfilMapper.class, WorkspaceMapper.class})
public interface ShoppingCartMapper extends EntityMapper<ShoppingCartDTO, ShoppingCart> {

    @Mapping(source = "shoppingCatalogCart.id", target = "shoppingCatalogCartId")
    @Mapping(source = "shoppingCatalogCart.label", target = "shoppingCatalogCartLabel")
    @Mapping(source = "workspace.id", target = "workspaceId")
    @Mapping(source = "workspace.label", target = "workspaceLabel")
    ShoppingCartDTO toDto(ShoppingCart shoppingCart);

    @Mapping(target = "shoppingItems", ignore = true)
    @Mapping(target = "removeShoppingItem", ignore = true)
    @Mapping(source = "shoppingCatalogCartId", target = "shoppingCatalogCart")
    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(source = "workspaceId", target = "workspace")
    ShoppingCart toEntity(ShoppingCartDTO shoppingCartDTO);

    default ShoppingCart fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setId(id);
        return shoppingCart;
    }
}
