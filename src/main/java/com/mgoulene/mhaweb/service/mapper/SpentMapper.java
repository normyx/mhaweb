package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.SpentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Spent} and its DTO {@link SpentDTO}.
 */
@Mapper(componentModel = "spring", uses = {WalletMapper.class, ProfilMapper.class})
public interface SpentMapper extends EntityMapper<SpentDTO, Spent> {

    @Mapping(source = "wallet.id", target = "walletId")
    @Mapping(source = "wallet.label", target = "walletLabel")
    @Mapping(source = "spender.id", target = "spenderId")
    @Mapping(source = "spender.displayName", target = "spenderDisplayName")
    SpentDTO toDto(Spent spent);

    @Mapping(target = "spentSharings", ignore = true)
    @Mapping(target = "removeSpentSharing", ignore = true)
    @Mapping(source = "walletId", target = "wallet")
    @Mapping(source = "spenderId", target = "spender")
    Spent toEntity(SpentDTO spentDTO);

    default Spent fromId(Long id) {
        if (id == null) {
            return null;
        }
        Spent spent = new Spent();
        spent.setId(id);
        return spent;
    }
}
