package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.SpentSharingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SpentSharing} and its DTO {@link SpentSharingDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class, SpentMapper.class})
public interface SpentSharingMapper extends EntityMapper<SpentSharingDTO, SpentSharing> {

    @Mapping(source = "sharingProfil.id", target = "sharingProfilId")
    @Mapping(source = "sharingProfil.displayName", target = "sharingProfilDisplayName")
    @Mapping(source = "spent.id", target = "spentId")
    @Mapping(source = "spent.label", target = "spentLabel")
    SpentSharingDTO toDto(SpentSharing spentSharing);

    @Mapping(source = "sharingProfilId", target = "sharingProfil")
    @Mapping(source = "spentId", target = "spent")
    SpentSharing toEntity(SpentSharingDTO spentSharingDTO);

    default SpentSharing fromId(Long id) {
        if (id == null) {
            return null;
        }
        SpentSharing spentSharing = new SpentSharing();
        spentSharing.setId(id);
        return spentSharing;
    }
}
