package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShoppingCatalogCart} and its DTO {@link ShoppingCatalogCartDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ShoppingCatalogCartMapper extends EntityMapper<ShoppingCatalogCartDTO, ShoppingCatalogCart> {


    @Mapping(target = "shoppingCatalogShelds", ignore = true)
    @Mapping(target = "removeShoppingCatalogSheld", ignore = true)
    ShoppingCatalogCart toEntity(ShoppingCatalogCartDTO shoppingCatalogCartDTO);

    default ShoppingCatalogCart fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShoppingCatalogCart shoppingCatalogCart = new ShoppingCatalogCart();
        shoppingCatalogCart.setId(id);
        return shoppingCatalogCart;
    }
}
