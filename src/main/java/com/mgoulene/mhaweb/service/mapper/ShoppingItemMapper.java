package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.ShoppingItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShoppingItem} and its DTO {@link ShoppingItemDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShoppingCatalogSheldMapper.class, ShoppingCartMapper.class})
public interface ShoppingItemMapper extends EntityMapper<ShoppingItemDTO, ShoppingItem> {

    @Mapping(source = "shoppingCatalogSheld.id", target = "shoppingCatalogSheldId")
    @Mapping(source = "shoppingCatalogSheld.label", target = "shoppingCatalogSheldLabel")
    @Mapping(source = "shoppingCart.id", target = "shoppingCartId")
    @Mapping(source = "shoppingCart.label", target = "shoppingCartLabel")
    ShoppingItemDTO toDto(ShoppingItem shoppingItem);

    @Mapping(source = "shoppingCatalogSheldId", target = "shoppingCatalogSheld")
    @Mapping(source = "shoppingCartId", target = "shoppingCart")
    ShoppingItem toEntity(ShoppingItemDTO shoppingItemDTO);

    default ShoppingItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShoppingItem shoppingItem = new ShoppingItem();
        shoppingItem.setId(id);
        return shoppingItem;
    }
}
