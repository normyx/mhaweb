package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShoppingCatalogItem} and its DTO {@link ShoppingCatalogItemDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShoppingCatalogSheldMapper.class})
public interface ShoppingCatalogItemMapper extends EntityMapper<ShoppingCatalogItemDTO, ShoppingCatalogItem> {

    @Mapping(source = "shoppingCatalogSheld.id", target = "shoppingCatalogSheldId")
    @Mapping(source = "shoppingCatalogSheld.label", target = "shoppingCatalogSheldLabel")
    ShoppingCatalogItemDTO toDto(ShoppingCatalogItem shoppingCatalogItem);

    @Mapping(source = "shoppingCatalogSheldId", target = "shoppingCatalogSheld")
    ShoppingCatalogItem toEntity(ShoppingCatalogItemDTO shoppingCatalogItemDTO);

    default ShoppingCatalogItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShoppingCatalogItem shoppingCatalogItem = new ShoppingCatalogItem();
        shoppingCatalogItem.setId(id);
        return shoppingCatalogItem;
    }
}
