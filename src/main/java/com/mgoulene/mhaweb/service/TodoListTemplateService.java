package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.TodoListTemplate;
import com.mgoulene.mhaweb.repository.TodoListTemplateRepository;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateDTO;
import com.mgoulene.mhaweb.service.mapper.TodoListTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TodoListTemplate}.
 */
@Service
@Transactional
public class TodoListTemplateService {

    private final Logger log = LoggerFactory.getLogger(TodoListTemplateService.class);

    private final TodoListTemplateRepository todoListTemplateRepository;

    private final TodoListTemplateMapper todoListTemplateMapper;

    public TodoListTemplateService(TodoListTemplateRepository todoListTemplateRepository, TodoListTemplateMapper todoListTemplateMapper) {
        this.todoListTemplateRepository = todoListTemplateRepository;
        this.todoListTemplateMapper = todoListTemplateMapper;
    }

    /**
     * Save a todoListTemplate.
     *
     * @param todoListTemplateDTO the entity to save.
     * @return the persisted entity.
     */
    public TodoListTemplateDTO save(TodoListTemplateDTO todoListTemplateDTO) {
        log.debug("Request to save TodoListTemplate : {}", todoListTemplateDTO);
        TodoListTemplate todoListTemplate = todoListTemplateMapper.toEntity(todoListTemplateDTO);
        todoListTemplate = todoListTemplateRepository.save(todoListTemplate);
        return todoListTemplateMapper.toDto(todoListTemplate);
    }

    /**
     * Get all the todoListTemplates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TodoListTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TodoListTemplates");
        return todoListTemplateRepository.findAll(pageable)
            .map(todoListTemplateMapper::toDto);
    }

    /**
     * Get all the todoListTemplates with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<TodoListTemplateDTO> findAllWithEagerRelationships(Pageable pageable) {
        return todoListTemplateRepository.findAllWithEagerRelationships(pageable).map(todoListTemplateMapper::toDto);
    }

    /**
     * Get one todoListTemplate by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TodoListTemplateDTO> findOne(Long id) {
        log.debug("Request to get TodoListTemplate : {}", id);
        return todoListTemplateRepository.findOneWithEagerRelationships(id)
            .map(todoListTemplateMapper::toDto);
    }

    /**
     * Delete the todoListTemplate by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TodoListTemplate : {}", id);
        todoListTemplateRepository.deleteById(id);
    }
}
