package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.Task;
import com.mgoulene.mhaweb.repository.MySHATaskRepository;
import com.mgoulene.mhaweb.repository.TaskRepository;
import com.mgoulene.mhaweb.service.dto.TaskDTO;
import com.mgoulene.mhaweb.service.mapper.TaskMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Task}.
 */
@Service
@Transactional
public class MySHATaskService extends TaskService {

    private final Logger log = LoggerFactory.getLogger(TaskService.class);

    private final MySHATaskRepository mySHATaskRepository;

    public MySHATaskService(TaskRepository taskRepository, TaskMapper taskMapper,
            MySHATaskRepository mySHATaskRepository) {
        super(taskRepository, taskMapper);
        this.mySHATaskRepository = mySHATaskRepository;
    }

    /**
     * Get all the tasks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TaskDTO> findAllWhereTaskProjectIdWithEagerRelationships(Pageable pageable, Long taskProjectId) {
        log.debug("Request to get all Tasks from a taskProjectId : {}", taskProjectId);
        return mySHATaskRepository.findAllWhereTaskProjectIdWithEagerRelationships(pageable, taskProjectId)
                .map(taskMapper::toDto);
    }

}
