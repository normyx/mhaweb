package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.repository.TaskProjectRepository;
import com.mgoulene.mhaweb.service.dto.TaskProjectDTO;
import com.mgoulene.mhaweb.service.mapper.TaskProjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TaskProject}.
 */
@Service
@Transactional
public class TaskProjectService {

    private final Logger log = LoggerFactory.getLogger(TaskProjectService.class);

    private final TaskProjectRepository taskProjectRepository;

    protected final TaskProjectMapper taskProjectMapper;

    public TaskProjectService(TaskProjectRepository taskProjectRepository, TaskProjectMapper taskProjectMapper) {
        this.taskProjectRepository = taskProjectRepository;
        this.taskProjectMapper = taskProjectMapper;
    }

    /**
     * Save a taskProject.
     *
     * @param taskProjectDTO the entity to save.
     * @return the persisted entity.
     */
    public TaskProjectDTO save(TaskProjectDTO taskProjectDTO) {
        log.debug("Request to save TaskProject : {}", taskProjectDTO);
        TaskProject taskProject = taskProjectMapper.toEntity(taskProjectDTO);
        taskProject = taskProjectRepository.save(taskProject);
        return taskProjectMapper.toDto(taskProject);
    }

    /**
     * Get all the taskProjects.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TaskProjectDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TaskProjects");
        return taskProjectRepository.findAll(pageable)
            .map(taskProjectMapper::toDto);
    }

    /**
     * Get all the taskProjects with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<TaskProjectDTO> findAllWithEagerRelationships(Pageable pageable) {
        return taskProjectRepository.findAllWithEagerRelationships(pageable).map(taskProjectMapper::toDto);
    }

    /**
     * Get one taskProject by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TaskProjectDTO> findOne(Long id) {
        log.debug("Request to get TaskProject : {}", id);
        return taskProjectRepository.findOneWithEagerRelationships(id)
            .map(taskProjectMapper::toDto);
    }

    /**
     * Delete the taskProject by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TaskProject : {}", id);
        taskProjectRepository.deleteById(id);
    }
}
