package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.ShoppingCatalogCart;
import com.mgoulene.mhaweb.repository.ShoppingCatalogCartRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogCartMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShoppingCatalogCart}.
 */
@Service
@Transactional
public class ShoppingCatalogCartService {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogCartService.class);

    private final ShoppingCatalogCartRepository shoppingCatalogCartRepository;

    private final ShoppingCatalogCartMapper shoppingCatalogCartMapper;

    public ShoppingCatalogCartService(ShoppingCatalogCartRepository shoppingCatalogCartRepository, ShoppingCatalogCartMapper shoppingCatalogCartMapper) {
        this.shoppingCatalogCartRepository = shoppingCatalogCartRepository;
        this.shoppingCatalogCartMapper = shoppingCatalogCartMapper;
    }

    /**
     * Save a shoppingCatalogCart.
     *
     * @param shoppingCatalogCartDTO the entity to save.
     * @return the persisted entity.
     */
    public ShoppingCatalogCartDTO save(ShoppingCatalogCartDTO shoppingCatalogCartDTO) {
        log.debug("Request to save ShoppingCatalogCart : {}", shoppingCatalogCartDTO);
        ShoppingCatalogCart shoppingCatalogCart = shoppingCatalogCartMapper.toEntity(shoppingCatalogCartDTO);
        shoppingCatalogCart = shoppingCatalogCartRepository.save(shoppingCatalogCart);
        return shoppingCatalogCartMapper.toDto(shoppingCatalogCart);
    }

    /**
     * Get all the shoppingCatalogCarts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingCatalogCartDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShoppingCatalogCarts");
        return shoppingCatalogCartRepository.findAll(pageable)
            .map(shoppingCatalogCartMapper::toDto);
    }

    /**
     * Get one shoppingCatalogCart by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ShoppingCatalogCartDTO> findOne(Long id) {
        log.debug("Request to get ShoppingCatalogCart : {}", id);
        return shoppingCatalogCartRepository.findById(id)
            .map(shoppingCatalogCartMapper::toDto);
    }

    /**
     * Delete the shoppingCatalogCart by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ShoppingCatalogCart : {}", id);
        shoppingCatalogCartRepository.deleteById(id);
    }
}
