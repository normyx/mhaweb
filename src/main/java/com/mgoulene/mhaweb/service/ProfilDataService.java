package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.ProfilData;
import com.mgoulene.mhaweb.repository.ProfilDataRepository;
import com.mgoulene.mhaweb.service.dto.ProfilDataDTO;
import com.mgoulene.mhaweb.service.mapper.ProfilDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ProfilData}.
 */
@Service
@Transactional
public class ProfilDataService {

    private final Logger log = LoggerFactory.getLogger(ProfilDataService.class);

    private final ProfilDataRepository profilDataRepository;

    protected final ProfilDataMapper profilDataMapper;

    public ProfilDataService(ProfilDataRepository profilDataRepository, ProfilDataMapper profilDataMapper) {
        this.profilDataRepository = profilDataRepository;
        this.profilDataMapper = profilDataMapper;
    }

    /**
     * Save a profilData.
     *
     * @param profilDataDTO the entity to save.
     * @return the persisted entity.
     */
    public ProfilDataDTO save(ProfilDataDTO profilDataDTO) {
        log.debug("Request to save ProfilData : {}", profilDataDTO);
        ProfilData profilData = profilDataMapper.toEntity(profilDataDTO);
        profilData = profilDataRepository.save(profilData);
        return profilDataMapper.toDto(profilData);
    }

    /**
     * Get all the profilData.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ProfilDataDTO> findAll() {
        log.debug("Request to get all ProfilData");
        return profilDataRepository.findAll().stream()
            .map(profilDataMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one profilData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProfilDataDTO> findOne(Long id) {
        log.debug("Request to get ProfilData : {}", id);
        return profilDataRepository.findById(id)
            .map(profilDataMapper::toDto);
    }

    /**
     * Delete the profilData by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProfilData : {}", id);
        profilDataRepository.deleteById(id);
    }
}
