package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.ShoppingCatalogCart;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.ShoppingCatalogCartRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartCriteria;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogCartMapper;

/**
 * Service for executing complex queries for {@link ShoppingCatalogCart} entities in the database.
 * The main input is a {@link ShoppingCatalogCartCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShoppingCatalogCartDTO} or a {@link Page} of {@link ShoppingCatalogCartDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShoppingCatalogCartQueryService extends QueryService<ShoppingCatalogCart> {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogCartQueryService.class);

    private final ShoppingCatalogCartRepository shoppingCatalogCartRepository;

    private final ShoppingCatalogCartMapper shoppingCatalogCartMapper;

    public ShoppingCatalogCartQueryService(ShoppingCatalogCartRepository shoppingCatalogCartRepository, ShoppingCatalogCartMapper shoppingCatalogCartMapper) {
        this.shoppingCatalogCartRepository = shoppingCatalogCartRepository;
        this.shoppingCatalogCartMapper = shoppingCatalogCartMapper;
    }

    /**
     * Return a {@link List} of {@link ShoppingCatalogCartDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShoppingCatalogCartDTO> findByCriteria(ShoppingCatalogCartCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShoppingCatalogCart> specification = createSpecification(criteria);
        return shoppingCatalogCartMapper.toDto(shoppingCatalogCartRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShoppingCatalogCartDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingCatalogCartDTO> findByCriteria(ShoppingCatalogCartCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShoppingCatalogCart> specification = createSpecification(criteria);
        return shoppingCatalogCartRepository.findAll(specification, page)
            .map(shoppingCatalogCartMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShoppingCatalogCartCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShoppingCatalogCart> specification = createSpecification(criteria);
        return shoppingCatalogCartRepository.count(specification);
    }

    /**
     * Function to convert {@link ShoppingCatalogCartCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ShoppingCatalogCart> createSpecification(ShoppingCatalogCartCriteria criteria) {
        Specification<ShoppingCatalogCart> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ShoppingCatalogCart_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), ShoppingCatalogCart_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), ShoppingCatalogCart_.lastUpdate));
            }
            if (criteria.getShoppingCatalogSheldId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCatalogSheldId(),
                    root -> root.join(ShoppingCatalogCart_.shoppingCatalogShelds, JoinType.LEFT).get(ShoppingCatalogSheld_.id)));
            }
        }
        return specification;
    }
}
