package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.SpentConfig;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.SpentConfigRepository;
import com.mgoulene.mhaweb.service.dto.SpentConfigCriteria;
import com.mgoulene.mhaweb.service.dto.SpentConfigDTO;
import com.mgoulene.mhaweb.service.mapper.SpentConfigMapper;

/**
 * Service for executing complex queries for {@link SpentConfig} entities in the database.
 * The main input is a {@link SpentConfigCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpentConfigDTO} or a {@link Page} of {@link SpentConfigDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpentConfigQueryService extends QueryService<SpentConfig> {

    private final Logger log = LoggerFactory.getLogger(SpentConfigQueryService.class);

    private final SpentConfigRepository spentConfigRepository;

    private final SpentConfigMapper spentConfigMapper;

    public SpentConfigQueryService(SpentConfigRepository spentConfigRepository, SpentConfigMapper spentConfigMapper) {
        this.spentConfigRepository = spentConfigRepository;
        this.spentConfigMapper = spentConfigMapper;
    }

    /**
     * Return a {@link List} of {@link SpentConfigDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpentConfigDTO> findByCriteria(SpentConfigCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SpentConfig> specification = createSpecification(criteria);
        return spentConfigMapper.toDto(spentConfigRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SpentConfigDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentConfigDTO> findByCriteria(SpentConfigCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SpentConfig> specification = createSpecification(criteria);
        return spentConfigRepository.findAll(specification, page)
            .map(spentConfigMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpentConfigCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SpentConfig> specification = createSpecification(criteria);
        return spentConfigRepository.count(specification);
    }

    /**
     * Function to convert {@link SpentConfigCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<SpentConfig> createSpecification(SpentConfigCriteria criteria) {
        Specification<SpentConfig> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), SpentConfig_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), SpentConfig_.label));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), SpentConfig_.amount));
            }
            if (criteria.getSpentLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpentLabel(), SpentConfig_.spentLabel));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), SpentConfig_.lastUpdate));
            }
            if (criteria.getWalletId() != null) {
                specification = specification.and(buildSpecification(criteria.getWalletId(),
                    root -> root.join(SpentConfig_.wallet, JoinType.LEFT).get(Wallet_.id)));
            }
        }
        return specification;
    }
}
