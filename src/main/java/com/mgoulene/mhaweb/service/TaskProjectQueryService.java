package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.TaskProjectRepository;
import com.mgoulene.mhaweb.service.dto.TaskProjectCriteria;
import com.mgoulene.mhaweb.service.dto.TaskProjectDTO;
import com.mgoulene.mhaweb.service.mapper.TaskProjectMapper;

/**
 * Service for executing complex queries for {@link TaskProject} entities in the database.
 * The main input is a {@link TaskProjectCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TaskProjectDTO} or a {@link Page} of {@link TaskProjectDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TaskProjectQueryService extends QueryService<TaskProject> {

    private final Logger log = LoggerFactory.getLogger(TaskProjectQueryService.class);

    private final TaskProjectRepository taskProjectRepository;

    private final TaskProjectMapper taskProjectMapper;

    public TaskProjectQueryService(TaskProjectRepository taskProjectRepository, TaskProjectMapper taskProjectMapper) {
        this.taskProjectRepository = taskProjectRepository;
        this.taskProjectMapper = taskProjectMapper;
    }

    /**
     * Return a {@link List} of {@link TaskProjectDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TaskProjectDTO> findByCriteria(TaskProjectCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TaskProject> specification = createSpecification(criteria);
        return taskProjectMapper.toDto(taskProjectRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TaskProjectDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TaskProjectDTO> findByCriteria(TaskProjectCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TaskProject> specification = createSpecification(criteria);
        return taskProjectRepository.findAll(specification, page)
            .map(taskProjectMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TaskProjectCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TaskProject> specification = createSpecification(criteria);
        return taskProjectRepository.count(specification);
    }

    /**
     * Function to convert {@link TaskProjectCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TaskProject> createSpecification(TaskProjectCriteria criteria) {
        Specification<TaskProject> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TaskProject_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), TaskProject_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), TaskProject_.lastUpdate));
            }
            if (criteria.getTaskId() != null) {
                specification = specification.and(buildSpecification(criteria.getTaskId(),
                    root -> root.join(TaskProject_.tasks, JoinType.LEFT).get(Task_.id)));
            }
            if (criteria.getOwnerId() != null) {
                specification = specification.and(buildSpecification(criteria.getOwnerId(),
                    root -> root.join(TaskProject_.owners, JoinType.LEFT).get(Profil_.id)));
            }
            if (criteria.getWorkspaceId() != null) {
                specification = specification.and(buildSpecification(criteria.getWorkspaceId(),
                    root -> root.join(TaskProject_.workspace, JoinType.LEFT).get(Workspace_.id)));
            }
        }
        return specification;
    }
}
