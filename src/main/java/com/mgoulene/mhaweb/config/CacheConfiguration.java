package com.mgoulene.mhaweb.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.mgoulene.mhaweb.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.mgoulene.mhaweb.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.mgoulene.mhaweb.domain.User.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Authority.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.User.class.getName() + ".authorities");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Workspace.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Workspace.class.getName() + ".owners");
            createCache(cm, com.mgoulene.mhaweb.domain.Workspace.class.getName() + ".taskProjects");
            createCache(cm, com.mgoulene.mhaweb.domain.TaskProject.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.TaskProject.class.getName() + ".owners");
            createCache(cm, com.mgoulene.mhaweb.domain.TaskProject.class.getName() + ".tasks");
            createCache(cm, com.mgoulene.mhaweb.domain.Task.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Task.class.getName() + ".owners");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".taskProjects");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".workspaces");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".tasks");
            createCache(cm, com.mgoulene.mhaweb.domain.ProfilData.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Wallet.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Spent.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.SpentSharing.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Wallet.class.getName() + ".owners");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".wallets");
            createCache(cm, com.mgoulene.mhaweb.domain.Workspace.class.getName() + ".wallets");
            createCache(cm, com.mgoulene.mhaweb.domain.SpentSharingConfig.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.SpentConfig.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Spent.class.getName() + ".spentSharings");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".spents");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCart.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.TodoList.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Todo.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.TodoList.class.getName() + ".todos");
            createCache(cm, com.mgoulene.mhaweb.domain.TodoList.class.getName() + ".owners");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".todoLists");
            createCache(cm, com.mgoulene.mhaweb.domain.Workspace.class.getName() + ".todoLists");
            createCache(cm, com.mgoulene.mhaweb.domain.TodoListTemplate.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.TodoTemplate.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".todoListTemplates");
            createCache(cm, com.mgoulene.mhaweb.domain.TodoListTemplate.class.getName() + ".todoTemplates");
            createCache(cm, com.mgoulene.mhaweb.domain.TodoListTemplate.class.getName() + ".owners");
            createCache(cm, com.mgoulene.mhaweb.domain.Workspace.class.getName() + ".todoListTemplates");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCart.class.getName() + ".shoppingItems");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCatalogCart.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCatalogCart.class.getName() + ".shoppingCatalogShelds");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingItem.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCatalogItem.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCatalogSheld.class.getName());
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCatalogSheld.class.getName() + ".shoppingItems");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCatalogSheld.class.getName() + ".shoppingCatalogItems");
            createCache(cm, com.mgoulene.mhaweb.domain.Workspace.class.getName() + ".shoppingCarts");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCatalogCart.class.getName() + ".shoppingCatalogItems");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCart.class.getName() + ".shoppingCatalogShelds");
            createCache(cm, com.mgoulene.mhaweb.domain.ShoppingCart.class.getName() + ".owners");
            createCache(cm, com.mgoulene.mhaweb.domain.Profil.class.getName() + ".shoppingCarts");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

}
