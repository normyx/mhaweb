package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.SpentSharingConfigService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.SpentSharingConfigDTO;
import com.mgoulene.mhaweb.service.dto.SpentSharingConfigCriteria;
import com.mgoulene.mhaweb.service.SpentSharingConfigQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.SpentSharingConfig}.
 */
@RestController
@RequestMapping("/api")
public class SpentSharingConfigResource {

    private final Logger log = LoggerFactory.getLogger(SpentSharingConfigResource.class);

    private static final String ENTITY_NAME = "spentSharingConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpentSharingConfigService spentSharingConfigService;

    private final SpentSharingConfigQueryService spentSharingConfigQueryService;

    public SpentSharingConfigResource(SpentSharingConfigService spentSharingConfigService, SpentSharingConfigQueryService spentSharingConfigQueryService) {
        this.spentSharingConfigService = spentSharingConfigService;
        this.spentSharingConfigQueryService = spentSharingConfigQueryService;
    }

    /**
     * {@code POST  /spent-sharing-configs} : Create a new spentSharingConfig.
     *
     * @param spentSharingConfigDTO the spentSharingConfigDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new spentSharingConfigDTO, or with status {@code 400 (Bad Request)} if the spentSharingConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/spent-sharing-configs")
    public ResponseEntity<SpentSharingConfigDTO> createSpentSharingConfig(@Valid @RequestBody SpentSharingConfigDTO spentSharingConfigDTO) throws URISyntaxException {
        log.debug("REST request to save SpentSharingConfig : {}", spentSharingConfigDTO);
        if (spentSharingConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new spentSharingConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpentSharingConfigDTO result = spentSharingConfigService.save(spentSharingConfigDTO);
        return ResponseEntity.created(new URI("/api/spent-sharing-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /spent-sharing-configs} : Updates an existing spentSharingConfig.
     *
     * @param spentSharingConfigDTO the spentSharingConfigDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spentSharingConfigDTO,
     * or with status {@code 400 (Bad Request)} if the spentSharingConfigDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the spentSharingConfigDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/spent-sharing-configs")
    public ResponseEntity<SpentSharingConfigDTO> updateSpentSharingConfig(@Valid @RequestBody SpentSharingConfigDTO spentSharingConfigDTO) throws URISyntaxException {
        log.debug("REST request to update SpentSharingConfig : {}", spentSharingConfigDTO);
        if (spentSharingConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SpentSharingConfigDTO result = spentSharingConfigService.save(spentSharingConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spentSharingConfigDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /spent-sharing-configs} : get all the spentSharingConfigs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of spentSharingConfigs in body.
     */
    @GetMapping("/spent-sharing-configs")
    public ResponseEntity<List<SpentSharingConfigDTO>> getAllSpentSharingConfigs(SpentSharingConfigCriteria criteria, Pageable pageable) {
        log.debug("REST request to get SpentSharingConfigs by criteria: {}", criteria);
        Page<SpentSharingConfigDTO> page = spentSharingConfigQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /spent-sharing-configs/count} : count all the spentSharingConfigs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/spent-sharing-configs/count")
    public ResponseEntity<Long> countSpentSharingConfigs(SpentSharingConfigCriteria criteria) {
        log.debug("REST request to count SpentSharingConfigs by criteria: {}", criteria);
        return ResponseEntity.ok().body(spentSharingConfigQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /spent-sharing-configs/:id} : get the "id" spentSharingConfig.
     *
     * @param id the id of the spentSharingConfigDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the spentSharingConfigDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/spent-sharing-configs/{id}")
    public ResponseEntity<SpentSharingConfigDTO> getSpentSharingConfig(@PathVariable Long id) {
        log.debug("REST request to get SpentSharingConfig : {}", id);
        Optional<SpentSharingConfigDTO> spentSharingConfigDTO = spentSharingConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(spentSharingConfigDTO);
    }

    /**
     * {@code DELETE  /spent-sharing-configs/:id} : delete the "id" spentSharingConfig.
     *
     * @param id the id of the spentSharingConfigDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/spent-sharing-configs/{id}")
    public ResponseEntity<Void> deleteSpentSharingConfig(@PathVariable Long id) {
        log.debug("REST request to delete SpentSharingConfig : {}", id);
        spentSharingConfigService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
