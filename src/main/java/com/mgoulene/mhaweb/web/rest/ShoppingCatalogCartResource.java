package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.ShoppingCatalogCartService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartDTO;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartCriteria;
import com.mgoulene.mhaweb.service.ShoppingCatalogCartQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.ShoppingCatalogCart}.
 */
@RestController
@RequestMapping("/api")
public class ShoppingCatalogCartResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogCartResource.class);

    private static final String ENTITY_NAME = "shoppingCatalogCart";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoppingCatalogCartService shoppingCatalogCartService;

    private final ShoppingCatalogCartQueryService shoppingCatalogCartQueryService;

    public ShoppingCatalogCartResource(ShoppingCatalogCartService shoppingCatalogCartService, ShoppingCatalogCartQueryService shoppingCatalogCartQueryService) {
        this.shoppingCatalogCartService = shoppingCatalogCartService;
        this.shoppingCatalogCartQueryService = shoppingCatalogCartQueryService;
    }

    /**
     * {@code POST  /shopping-catalog-carts} : Create a new shoppingCatalogCart.
     *
     * @param shoppingCatalogCartDTO the shoppingCatalogCartDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shoppingCatalogCartDTO, or with status {@code 400 (Bad Request)} if the shoppingCatalogCart has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shopping-catalog-carts")
    public ResponseEntity<ShoppingCatalogCartDTO> createShoppingCatalogCart(@Valid @RequestBody ShoppingCatalogCartDTO shoppingCatalogCartDTO) throws URISyntaxException {
        log.debug("REST request to save ShoppingCatalogCart : {}", shoppingCatalogCartDTO);
        if (shoppingCatalogCartDTO.getId() != null) {
            throw new BadRequestAlertException("A new shoppingCatalogCart cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShoppingCatalogCartDTO result = shoppingCatalogCartService.save(shoppingCatalogCartDTO);
        return ResponseEntity.created(new URI("/api/shopping-catalog-carts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shopping-catalog-carts} : Updates an existing shoppingCatalogCart.
     *
     * @param shoppingCatalogCartDTO the shoppingCatalogCartDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shoppingCatalogCartDTO,
     * or with status {@code 400 (Bad Request)} if the shoppingCatalogCartDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shoppingCatalogCartDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shopping-catalog-carts")
    public ResponseEntity<ShoppingCatalogCartDTO> updateShoppingCatalogCart(@Valid @RequestBody ShoppingCatalogCartDTO shoppingCatalogCartDTO) throws URISyntaxException {
        log.debug("REST request to update ShoppingCatalogCart : {}", shoppingCatalogCartDTO);
        if (shoppingCatalogCartDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShoppingCatalogCartDTO result = shoppingCatalogCartService.save(shoppingCatalogCartDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shoppingCatalogCartDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shopping-catalog-carts} : get all the shoppingCatalogCarts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shoppingCatalogCarts in body.
     */
    @GetMapping("/shopping-catalog-carts")
    public ResponseEntity<List<ShoppingCatalogCartDTO>> getAllShoppingCatalogCarts(ShoppingCatalogCartCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ShoppingCatalogCarts by criteria: {}", criteria);
        Page<ShoppingCatalogCartDTO> page = shoppingCatalogCartQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shopping-catalog-carts/count} : count all the shoppingCatalogCarts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/shopping-catalog-carts/count")
    public ResponseEntity<Long> countShoppingCatalogCarts(ShoppingCatalogCartCriteria criteria) {
        log.debug("REST request to count ShoppingCatalogCarts by criteria: {}", criteria);
        return ResponseEntity.ok().body(shoppingCatalogCartQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shopping-catalog-carts/:id} : get the "id" shoppingCatalogCart.
     *
     * @param id the id of the shoppingCatalogCartDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shoppingCatalogCartDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shopping-catalog-carts/{id}")
    public ResponseEntity<ShoppingCatalogCartDTO> getShoppingCatalogCart(@PathVariable Long id) {
        log.debug("REST request to get ShoppingCatalogCart : {}", id);
        Optional<ShoppingCatalogCartDTO> shoppingCatalogCartDTO = shoppingCatalogCartService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shoppingCatalogCartDTO);
    }

    /**
     * {@code DELETE  /shopping-catalog-carts/:id} : delete the "id" shoppingCatalogCart.
     *
     * @param id the id of the shoppingCatalogCartDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shopping-catalog-carts/{id}")
    public ResponseEntity<Void> deleteShoppingCatalogCart(@PathVariable Long id) {
        log.debug("REST request to delete ShoppingCatalogCart : {}", id);
        shoppingCatalogCartService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
