package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.SpentSharingService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.SpentSharingDTO;
import com.mgoulene.mhaweb.service.dto.SpentSharingCriteria;
import com.mgoulene.mhaweb.service.SpentSharingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.SpentSharing}.
 */
@RestController
@RequestMapping("/api")
public class SpentSharingResource {

    private final Logger log = LoggerFactory.getLogger(SpentSharingResource.class);

    private static final String ENTITY_NAME = "spentSharing";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpentSharingService spentSharingService;

    private final SpentSharingQueryService spentSharingQueryService;

    public SpentSharingResource(SpentSharingService spentSharingService, SpentSharingQueryService spentSharingQueryService) {
        this.spentSharingService = spentSharingService;
        this.spentSharingQueryService = spentSharingQueryService;
    }

    /**
     * {@code POST  /spent-sharings} : Create a new spentSharing.
     *
     * @param spentSharingDTO the spentSharingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new spentSharingDTO, or with status {@code 400 (Bad Request)} if the spentSharing has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/spent-sharings")
    public ResponseEntity<SpentSharingDTO> createSpentSharing(@Valid @RequestBody SpentSharingDTO spentSharingDTO) throws URISyntaxException {
        log.debug("REST request to save SpentSharing : {}", spentSharingDTO);
        if (spentSharingDTO.getId() != null) {
            throw new BadRequestAlertException("A new spentSharing cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpentSharingDTO result = spentSharingService.save(spentSharingDTO);
        return ResponseEntity.created(new URI("/api/spent-sharings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /spent-sharings} : Updates an existing spentSharing.
     *
     * @param spentSharingDTO the spentSharingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spentSharingDTO,
     * or with status {@code 400 (Bad Request)} if the spentSharingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the spentSharingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/spent-sharings")
    public ResponseEntity<SpentSharingDTO> updateSpentSharing(@Valid @RequestBody SpentSharingDTO spentSharingDTO) throws URISyntaxException {
        log.debug("REST request to update SpentSharing : {}", spentSharingDTO);
        if (spentSharingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SpentSharingDTO result = spentSharingService.save(spentSharingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spentSharingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /spent-sharings} : get all the spentSharings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of spentSharings in body.
     */
    @GetMapping("/spent-sharings")
    public ResponseEntity<List<SpentSharingDTO>> getAllSpentSharings(SpentSharingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get SpentSharings by criteria: {}", criteria);
        Page<SpentSharingDTO> page = spentSharingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /spent-sharings/count} : count all the spentSharings.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/spent-sharings/count")
    public ResponseEntity<Long> countSpentSharings(SpentSharingCriteria criteria) {
        log.debug("REST request to count SpentSharings by criteria: {}", criteria);
        return ResponseEntity.ok().body(spentSharingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /spent-sharings/:id} : get the "id" spentSharing.
     *
     * @param id the id of the spentSharingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the spentSharingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/spent-sharings/{id}")
    public ResponseEntity<SpentSharingDTO> getSpentSharing(@PathVariable Long id) {
        log.debug("REST request to get SpentSharing : {}", id);
        Optional<SpentSharingDTO> spentSharingDTO = spentSharingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(spentSharingDTO);
    }

    /**
     * {@code DELETE  /spent-sharings/:id} : delete the "id" spentSharing.
     *
     * @param id the id of the spentSharingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/spent-sharings/{id}")
    public ResponseEntity<Void> deleteSpentSharing(@PathVariable Long id) {
        log.debug("REST request to delete SpentSharing : {}", id);
        spentSharingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
