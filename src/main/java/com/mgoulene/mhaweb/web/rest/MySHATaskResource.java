package com.mgoulene.mhaweb.web.rest;

import java.util.List;

import com.mgoulene.mhaweb.service.MySHATaskService;
import com.mgoulene.mhaweb.service.TaskQueryService;
import com.mgoulene.mhaweb.service.TaskService;
import com.mgoulene.mhaweb.service.dto.TaskDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import io.github.jhipster.web.util.PaginationUtil;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.Task}.
 */
@RestController
@RequestMapping("/api")
public class MySHATaskResource {

    private final Logger log = LoggerFactory.getLogger(TaskResource.class);

    private static final String ENTITY_NAME = "task";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TaskService taskService;

    private final TaskQueryService taskQueryService;

    private final MySHATaskService mySHATaskService;

    public MySHATaskResource(TaskService taskService, TaskQueryService taskQueryService, MySHATaskService mySHATaskService) {
        this.taskService = taskService;
        this.taskQueryService = taskQueryService;
        this.mySHATaskService = mySHATaskService;
    }


    /**
     * {@code GET  /tasks} : get all the tasks.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tasks in body.
     */
    @GetMapping("/mysha-tasks-where-task-project/{taskProjectId}")
    public ResponseEntity<List<TaskDTO>> getAllTasks(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder, @PathVariable Long taskProjectId) {
        log.debug("REST request to get Tasks from takProjectId: {}", taskProjectId);
        Page<TaskDTO> page = mySHATaskService.findAllWhereTaskProjectIdWithEagerRelationships(pageable, taskProjectId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
