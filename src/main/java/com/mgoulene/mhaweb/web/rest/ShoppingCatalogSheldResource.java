package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.ShoppingCatalogSheldService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldDTO;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldCriteria;
import com.mgoulene.mhaweb.service.ShoppingCatalogSheldQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.ShoppingCatalogSheld}.
 */
@RestController
@RequestMapping("/api")
public class ShoppingCatalogSheldResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogSheldResource.class);

    private static final String ENTITY_NAME = "shoppingCatalogSheld";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoppingCatalogSheldService shoppingCatalogSheldService;

    private final ShoppingCatalogSheldQueryService shoppingCatalogSheldQueryService;

    public ShoppingCatalogSheldResource(ShoppingCatalogSheldService shoppingCatalogSheldService, ShoppingCatalogSheldQueryService shoppingCatalogSheldQueryService) {
        this.shoppingCatalogSheldService = shoppingCatalogSheldService;
        this.shoppingCatalogSheldQueryService = shoppingCatalogSheldQueryService;
    }

    /**
     * {@code POST  /shopping-catalog-shelds} : Create a new shoppingCatalogSheld.
     *
     * @param shoppingCatalogSheldDTO the shoppingCatalogSheldDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shoppingCatalogSheldDTO, or with status {@code 400 (Bad Request)} if the shoppingCatalogSheld has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shopping-catalog-shelds")
    public ResponseEntity<ShoppingCatalogSheldDTO> createShoppingCatalogSheld(@Valid @RequestBody ShoppingCatalogSheldDTO shoppingCatalogSheldDTO) throws URISyntaxException {
        log.debug("REST request to save ShoppingCatalogSheld : {}", shoppingCatalogSheldDTO);
        if (shoppingCatalogSheldDTO.getId() != null) {
            throw new BadRequestAlertException("A new shoppingCatalogSheld cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShoppingCatalogSheldDTO result = shoppingCatalogSheldService.save(shoppingCatalogSheldDTO);
        return ResponseEntity.created(new URI("/api/shopping-catalog-shelds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shopping-catalog-shelds} : Updates an existing shoppingCatalogSheld.
     *
     * @param shoppingCatalogSheldDTO the shoppingCatalogSheldDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shoppingCatalogSheldDTO,
     * or with status {@code 400 (Bad Request)} if the shoppingCatalogSheldDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shoppingCatalogSheldDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shopping-catalog-shelds")
    public ResponseEntity<ShoppingCatalogSheldDTO> updateShoppingCatalogSheld(@Valid @RequestBody ShoppingCatalogSheldDTO shoppingCatalogSheldDTO) throws URISyntaxException {
        log.debug("REST request to update ShoppingCatalogSheld : {}", shoppingCatalogSheldDTO);
        if (shoppingCatalogSheldDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShoppingCatalogSheldDTO result = shoppingCatalogSheldService.save(shoppingCatalogSheldDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shoppingCatalogSheldDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shopping-catalog-shelds} : get all the shoppingCatalogShelds.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shoppingCatalogShelds in body.
     */
    @GetMapping("/shopping-catalog-shelds")
    public ResponseEntity<List<ShoppingCatalogSheldDTO>> getAllShoppingCatalogShelds(ShoppingCatalogSheldCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ShoppingCatalogShelds by criteria: {}", criteria);
        Page<ShoppingCatalogSheldDTO> page = shoppingCatalogSheldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shopping-catalog-shelds/count} : count all the shoppingCatalogShelds.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/shopping-catalog-shelds/count")
    public ResponseEntity<Long> countShoppingCatalogShelds(ShoppingCatalogSheldCriteria criteria) {
        log.debug("REST request to count ShoppingCatalogShelds by criteria: {}", criteria);
        return ResponseEntity.ok().body(shoppingCatalogSheldQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shopping-catalog-shelds/:id} : get the "id" shoppingCatalogSheld.
     *
     * @param id the id of the shoppingCatalogSheldDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shoppingCatalogSheldDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shopping-catalog-shelds/{id}")
    public ResponseEntity<ShoppingCatalogSheldDTO> getShoppingCatalogSheld(@PathVariable Long id) {
        log.debug("REST request to get ShoppingCatalogSheld : {}", id);
        Optional<ShoppingCatalogSheldDTO> shoppingCatalogSheldDTO = shoppingCatalogSheldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shoppingCatalogSheldDTO);
    }

    /**
     * {@code DELETE  /shopping-catalog-shelds/:id} : delete the "id" shoppingCatalogSheld.
     *
     * @param id the id of the shoppingCatalogSheldDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shopping-catalog-shelds/{id}")
    public ResponseEntity<Void> deleteShoppingCatalogSheld(@PathVariable Long id) {
        log.debug("REST request to delete ShoppingCatalogSheld : {}", id);
        shoppingCatalogSheldService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
