package com.mgoulene.mhaweb.web.rest;

import java.util.List;
import java.util.Optional;

import com.mgoulene.mhaweb.domain.User;
import com.mgoulene.mhaweb.service.MySHATaskProjectService;
import com.mgoulene.mhaweb.service.TaskProjectQueryService;
import com.mgoulene.mhaweb.service.TaskProjectService;
import com.mgoulene.mhaweb.service.UserService;
import com.mgoulene.mhaweb.service.dto.TaskProjectDTO;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import io.github.jhipster.web.util.PaginationUtil;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.TaskProject}.
 */
@RestController
@RequestMapping("/api")
public class MySHATaskProjectResource {

    private final Logger log = LoggerFactory.getLogger(MySHATaskProjectResource.class);

    private static final String ENTITY_NAME = "taskProject";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TaskProjectService taskProjectService;

    private final MySHATaskProjectService mySHATaskProjectService;

    private final UserService userService;


    public MySHATaskProjectResource(TaskProjectService taskProjectService, UserService userService, MySHATaskProjectService mySHATaskProjectService) {
        this.taskProjectService = taskProjectService;
        this.userService = userService;
        this.mySHATaskProjectService = mySHATaskProjectService;
    }



    /**
     * {@code GET  /task-projects} : get all the taskProjects owned by the looged user.
     *
     * 
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of taskProjects in body.
     */
    @GetMapping("/mysha-task-projects-owned-by-logged-user")
    public ResponseEntity<List<TaskProjectDTO>> getAllTaskProjectsOwnedByLoggedUser(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get TaskProjects owned by user");
        Optional<User> userOptional = userService.getUserWithAuthorities();
        if (userOptional.isPresent()) {
            Page<TaskProjectDTO> page = mySHATaskProjectService.findAllWhereOwnerId(pageable, userOptional.get().getId());
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
            return ResponseEntity.ok().headers(headers).body(page.getContent());
        } 
        throw new BadRequestAlertException("Cannot find Task Projects without a logged user", ENTITY_NAME, "idnull");
    }


}
