package com.mgoulene.mhaweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Workspace.
 */
@Entity
@Table(name = "workspace")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Workspace implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @OneToMany(mappedBy = "workspace")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShoppingCart> shoppingCarts = new HashSet<>();

    @OneToMany(mappedBy = "workspace")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TaskProject> taskProjects = new HashSet<>();

    @OneToMany(mappedBy = "workspace")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TodoList> todoLists = new HashSet<>();

    @OneToMany(mappedBy = "workspace")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TodoListTemplate> todoListTemplates = new HashSet<>();

    @OneToMany(mappedBy = "workspace")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Wallet> wallets = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "workspace_owner",
               joinColumns = @JoinColumn(name = "workspace_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private Set<Profil> owners = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Workspace label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public Workspace lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<ShoppingCart> getShoppingCarts() {
        return shoppingCarts;
    }

    public Workspace shoppingCarts(Set<ShoppingCart> shoppingCarts) {
        this.shoppingCarts = shoppingCarts;
        return this;
    }

    public Workspace addShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCarts.add(shoppingCart);
        shoppingCart.setWorkspace(this);
        return this;
    }

    public Workspace removeShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCarts.remove(shoppingCart);
        shoppingCart.setWorkspace(null);
        return this;
    }

    public void setShoppingCarts(Set<ShoppingCart> shoppingCarts) {
        this.shoppingCarts = shoppingCarts;
    }

    public Set<TaskProject> getTaskProjects() {
        return taskProjects;
    }

    public Workspace taskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
        return this;
    }

    public Workspace addTaskProject(TaskProject taskProject) {
        this.taskProjects.add(taskProject);
        taskProject.setWorkspace(this);
        return this;
    }

    public Workspace removeTaskProject(TaskProject taskProject) {
        this.taskProjects.remove(taskProject);
        taskProject.setWorkspace(null);
        return this;
    }

    public void setTaskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
    }

    public Set<TodoList> getTodoLists() {
        return todoLists;
    }

    public Workspace todoLists(Set<TodoList> todoLists) {
        this.todoLists = todoLists;
        return this;
    }

    public Workspace addTodoList(TodoList todoList) {
        this.todoLists.add(todoList);
        todoList.setWorkspace(this);
        return this;
    }

    public Workspace removeTodoList(TodoList todoList) {
        this.todoLists.remove(todoList);
        todoList.setWorkspace(null);
        return this;
    }

    public void setTodoLists(Set<TodoList> todoLists) {
        this.todoLists = todoLists;
    }

    public Set<TodoListTemplate> getTodoListTemplates() {
        return todoListTemplates;
    }

    public Workspace todoListTemplates(Set<TodoListTemplate> todoListTemplates) {
        this.todoListTemplates = todoListTemplates;
        return this;
    }

    public Workspace addTodoListTemplate(TodoListTemplate todoListTemplate) {
        this.todoListTemplates.add(todoListTemplate);
        todoListTemplate.setWorkspace(this);
        return this;
    }

    public Workspace removeTodoListTemplate(TodoListTemplate todoListTemplate) {
        this.todoListTemplates.remove(todoListTemplate);
        todoListTemplate.setWorkspace(null);
        return this;
    }

    public void setTodoListTemplates(Set<TodoListTemplate> todoListTemplates) {
        this.todoListTemplates = todoListTemplates;
    }

    public Set<Wallet> getWallets() {
        return wallets;
    }

    public Workspace wallets(Set<Wallet> wallets) {
        this.wallets = wallets;
        return this;
    }

    public Workspace addWallet(Wallet wallet) {
        this.wallets.add(wallet);
        wallet.setWorkspace(this);
        return this;
    }

    public Workspace removeWallet(Wallet wallet) {
        this.wallets.remove(wallet);
        wallet.setWorkspace(null);
        return this;
    }

    public void setWallets(Set<Wallet> wallets) {
        this.wallets = wallets;
    }

    public Set<Profil> getOwners() {
        return owners;
    }

    public Workspace owners(Set<Profil> profils) {
        this.owners = profils;
        return this;
    }

    public Workspace addOwner(Profil profil) {
        this.owners.add(profil);
        profil.getWorkspaces().add(this);
        return this;
    }

    public Workspace removeOwner(Profil profil) {
        this.owners.remove(profil);
        profil.getWorkspaces().remove(this);
        return this;
    }

    public void setOwners(Set<Profil> profils) {
        this.owners = profils;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workspace)) {
            return false;
        }
        return id != null && id.equals(((Workspace) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Workspace{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
