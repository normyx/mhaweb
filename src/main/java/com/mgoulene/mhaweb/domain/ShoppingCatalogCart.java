package com.mgoulene.mhaweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A ShoppingCatalogCart.
 */
@Entity
@Table(name = "shopping_catalog_cart")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingCatalogCart implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @OneToMany(mappedBy = "shoppingCatalogCart")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShoppingCatalogSheld> shoppingCatalogShelds = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public ShoppingCatalogCart label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public ShoppingCatalogCart lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<ShoppingCatalogSheld> getShoppingCatalogShelds() {
        return shoppingCatalogShelds;
    }

    public ShoppingCatalogCart shoppingCatalogShelds(Set<ShoppingCatalogSheld> shoppingCatalogShelds) {
        this.shoppingCatalogShelds = shoppingCatalogShelds;
        return this;
    }

    public ShoppingCatalogCart addShoppingCatalogSheld(ShoppingCatalogSheld shoppingCatalogSheld) {
        this.shoppingCatalogShelds.add(shoppingCatalogSheld);
        shoppingCatalogSheld.setShoppingCatalogCart(this);
        return this;
    }

    public ShoppingCatalogCart removeShoppingCatalogSheld(ShoppingCatalogSheld shoppingCatalogSheld) {
        this.shoppingCatalogShelds.remove(shoppingCatalogSheld);
        shoppingCatalogSheld.setShoppingCatalogCart(null);
        return this;
    }

    public void setShoppingCatalogShelds(Set<ShoppingCatalogSheld> shoppingCatalogShelds) {
        this.shoppingCatalogShelds = shoppingCatalogShelds;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingCatalogCart)) {
            return false;
        }
        return id != null && id.equals(((ShoppingCatalogCart) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingCatalogCart{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
