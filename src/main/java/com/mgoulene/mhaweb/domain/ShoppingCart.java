package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A ShoppingCart.
 */
@Entity
@Table(name = "shopping_cart")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @OneToMany(mappedBy = "shoppingCart")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShoppingItem> shoppingItems = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("shoppingCarts")
    private ShoppingCatalogCart shoppingCatalogCart;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "shopping_cart_owner",
               joinColumns = @JoinColumn(name = "shopping_cart_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private Set<Profil> owners = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shoppingCarts")
    private Workspace workspace;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public ShoppingCart label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public ShoppingCart lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<ShoppingItem> getShoppingItems() {
        return shoppingItems;
    }

    public ShoppingCart shoppingItems(Set<ShoppingItem> shoppingItems) {
        this.shoppingItems = shoppingItems;
        return this;
    }

    public ShoppingCart addShoppingItem(ShoppingItem shoppingItem) {
        this.shoppingItems.add(shoppingItem);
        shoppingItem.setShoppingCart(this);
        return this;
    }

    public ShoppingCart removeShoppingItem(ShoppingItem shoppingItem) {
        this.shoppingItems.remove(shoppingItem);
        shoppingItem.setShoppingCart(null);
        return this;
    }

    public void setShoppingItems(Set<ShoppingItem> shoppingItems) {
        this.shoppingItems = shoppingItems;
    }

    public ShoppingCatalogCart getShoppingCatalogCart() {
        return shoppingCatalogCart;
    }

    public ShoppingCart shoppingCatalogCart(ShoppingCatalogCart shoppingCatalogCart) {
        this.shoppingCatalogCart = shoppingCatalogCart;
        return this;
    }

    public void setShoppingCatalogCart(ShoppingCatalogCart shoppingCatalogCart) {
        this.shoppingCatalogCart = shoppingCatalogCart;
    }

    public Set<Profil> getOwners() {
        return owners;
    }

    public ShoppingCart owners(Set<Profil> profils) {
        this.owners = profils;
        return this;
    }

    public ShoppingCart addOwner(Profil profil) {
        this.owners.add(profil);
        profil.getShoppingCarts().add(this);
        return this;
    }

    public ShoppingCart removeOwner(Profil profil) {
        this.owners.remove(profil);
        profil.getShoppingCarts().remove(this);
        return this;
    }

    public void setOwners(Set<Profil> profils) {
        this.owners = profils;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public ShoppingCart workspace(Workspace workspace) {
        this.workspace = workspace;
        return this;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingCart)) {
            return false;
        }
        return id != null && id.equals(((ShoppingCart) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
