package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A SpentSharingConfig.
 */
@Entity
@Table(name = "spent_sharing_config")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpentSharingConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Min(value = 0)
    @Column(name = "share", nullable = false)
    private Integer share;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("spentSharingConfigs")
    private SpentConfig spentConfig;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("spentSharingConfigs")
    private Profil profil;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getShare() {
        return share;
    }

    public SpentSharingConfig share(Integer share) {
        this.share = share;
        return this;
    }

    public void setShare(Integer share) {
        this.share = share;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public SpentSharingConfig lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public SpentConfig getSpentConfig() {
        return spentConfig;
    }

    public SpentSharingConfig spentConfig(SpentConfig spentConfig) {
        this.spentConfig = spentConfig;
        return this;
    }

    public void setSpentConfig(SpentConfig spentConfig) {
        this.spentConfig = spentConfig;
    }

    public Profil getProfil() {
        return profil;
    }

    public SpentSharingConfig profil(Profil profil) {
        this.profil = profil;
        return this;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpentSharingConfig)) {
            return false;
        }
        return id != null && id.equals(((SpentSharingConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SpentSharingConfig{" +
            "id=" + getId() +
            ", share=" + getShare() +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
