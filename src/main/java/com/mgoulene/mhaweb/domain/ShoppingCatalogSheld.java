package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A ShoppingCatalogSheld.
 */
@Entity
@Table(name = "shopping_catalog_sheld")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingCatalogSheld implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @NotNull
    @Column(name = "is_default", nullable = false)
    private Boolean isDefault;

    @NotNull
    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted;

    @OneToMany(mappedBy = "shoppingCatalogSheld")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShoppingCatalogItem> shoppingCatalogItems = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shoppingCatalogShelds")
    private ShoppingCatalogCart shoppingCatalogCart;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public ShoppingCatalogSheld label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public ShoppingCatalogSheld lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isIsDefault() {
        return isDefault;
    }

    public ShoppingCatalogSheld isDefault(Boolean isDefault) {
        this.isDefault = isDefault;
        return this;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public ShoppingCatalogSheld isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Set<ShoppingCatalogItem> getShoppingCatalogItems() {
        return shoppingCatalogItems;
    }

    public ShoppingCatalogSheld shoppingCatalogItems(Set<ShoppingCatalogItem> shoppingCatalogItems) {
        this.shoppingCatalogItems = shoppingCatalogItems;
        return this;
    }

    public ShoppingCatalogSheld addShoppingCatalogItem(ShoppingCatalogItem shoppingCatalogItem) {
        this.shoppingCatalogItems.add(shoppingCatalogItem);
        shoppingCatalogItem.setShoppingCatalogSheld(this);
        return this;
    }

    public ShoppingCatalogSheld removeShoppingCatalogItem(ShoppingCatalogItem shoppingCatalogItem) {
        this.shoppingCatalogItems.remove(shoppingCatalogItem);
        shoppingCatalogItem.setShoppingCatalogSheld(null);
        return this;
    }

    public void setShoppingCatalogItems(Set<ShoppingCatalogItem> shoppingCatalogItems) {
        this.shoppingCatalogItems = shoppingCatalogItems;
    }

    public ShoppingCatalogCart getShoppingCatalogCart() {
        return shoppingCatalogCart;
    }

    public ShoppingCatalogSheld shoppingCatalogCart(ShoppingCatalogCart shoppingCatalogCart) {
        this.shoppingCatalogCart = shoppingCatalogCart;
        return this;
    }

    public void setShoppingCatalogCart(ShoppingCatalogCart shoppingCatalogCart) {
        this.shoppingCatalogCart = shoppingCatalogCart;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingCatalogSheld)) {
            return false;
        }
        return id != null && id.equals(((ShoppingCatalogSheld) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingCatalogSheld{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", isDefault='" + isIsDefault() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            "}";
    }
}
