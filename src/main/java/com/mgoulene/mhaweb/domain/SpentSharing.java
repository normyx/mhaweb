package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A SpentSharing.
 */
@Entity
@Table(name = "spent_sharing")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpentSharing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "amount_share", nullable = false)
    private Float amountShare;

    @NotNull
    @Min(value = 0)
    @Column(name = "share", nullable = false)
    private Integer share;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @ManyToOne
    @JsonIgnoreProperties("spentSharings")
    private Profil sharingProfil;

    @ManyToOne
    @JsonIgnoreProperties("spentSharings")
    private Spent spent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmountShare() {
        return amountShare;
    }

    public SpentSharing amountShare(Float amountShare) {
        this.amountShare = amountShare;
        return this;
    }

    public void setAmountShare(Float amountShare) {
        this.amountShare = amountShare;
    }

    public Integer getShare() {
        return share;
    }

    public SpentSharing share(Integer share) {
        this.share = share;
        return this;
    }

    public void setShare(Integer share) {
        this.share = share;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public SpentSharing lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Profil getSharingProfil() {
        return sharingProfil;
    }

    public SpentSharing sharingProfil(Profil profil) {
        this.sharingProfil = profil;
        return this;
    }

    public void setSharingProfil(Profil profil) {
        this.sharingProfil = profil;
    }

    public Spent getSpent() {
        return spent;
    }

    public SpentSharing spent(Spent spent) {
        this.spent = spent;
        return this;
    }

    public void setSpent(Spent spent) {
        this.spent = spent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpentSharing)) {
            return false;
        }
        return id != null && id.equals(((SpentSharing) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SpentSharing{" +
            "id=" + getId() +
            ", amountShare=" + getAmountShare() +
            ", share=" + getShare() +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
