package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A TodoListTemplate.
 */
@Entity
@Table(name = "todo_list_template")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TodoListTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @OneToMany(mappedBy = "todoListTemplate")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TodoTemplate> todoTemplates = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "todo_list_template_owner",
               joinColumns = @JoinColumn(name = "todo_list_template_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private Set<Profil> owners = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("todoListTemplates")
    private Workspace workspace;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public TodoListTemplate label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public TodoListTemplate lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<TodoTemplate> getTodoTemplates() {
        return todoTemplates;
    }

    public TodoListTemplate todoTemplates(Set<TodoTemplate> todoTemplates) {
        this.todoTemplates = todoTemplates;
        return this;
    }

    public TodoListTemplate addTodoTemplate(TodoTemplate todoTemplate) {
        this.todoTemplates.add(todoTemplate);
        todoTemplate.setTodoListTemplate(this);
        return this;
    }

    public TodoListTemplate removeTodoTemplate(TodoTemplate todoTemplate) {
        this.todoTemplates.remove(todoTemplate);
        todoTemplate.setTodoListTemplate(null);
        return this;
    }

    public void setTodoTemplates(Set<TodoTemplate> todoTemplates) {
        this.todoTemplates = todoTemplates;
    }

    public Set<Profil> getOwners() {
        return owners;
    }

    public TodoListTemplate owners(Set<Profil> profils) {
        this.owners = profils;
        return this;
    }

    public TodoListTemplate addOwner(Profil profil) {
        this.owners.add(profil);
        profil.getTodoListTemplates().add(this);
        return this;
    }

    public TodoListTemplate removeOwner(Profil profil) {
        this.owners.remove(profil);
        profil.getTodoListTemplates().remove(this);
        return this;
    }

    public void setOwners(Set<Profil> profils) {
        this.owners = profils;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public TodoListTemplate workspace(Workspace workspace) {
        this.workspace = workspace;
        return this;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TodoListTemplate)) {
            return false;
        }
        return id != null && id.equals(((TodoListTemplate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TodoListTemplate{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
