package com.mgoulene.mhaweb.domain.enumeration;

/**
 * The Unit enumeration.
 */
public enum Unit {
    QUANTITY, G, KG, L, ML
}
