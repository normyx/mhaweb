package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

import com.mgoulene.mhaweb.domain.enumeration.Unit;

/**
 * A ShoppingItem.
 */
@Entity
@Table(name = "shopping_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2, max = 80)
    @Column(name = "label", length = 80, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @NotNull
    @Column(name = "checked", nullable = false)
    private Boolean checked;

    @DecimalMin(value = "0")
    @Column(name = "quantity")
    private Float quantity;

    @Enumerated(EnumType.STRING)
    @Column(name = "unit")
    private Unit unit;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shoppingItems")
    private ShoppingCatalogSheld shoppingCatalogSheld;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shoppingItems")
    private ShoppingCart shoppingCart;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public ShoppingItem label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public ShoppingItem lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isChecked() {
        return checked;
    }

    public ShoppingItem checked(Boolean checked) {
        this.checked = checked;
        return this;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Float getQuantity() {
        return quantity;
    }

    public ShoppingItem quantity(Float quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public ShoppingItem unit(Unit unit) {
        this.unit = unit;
        return this;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public ShoppingCatalogSheld getShoppingCatalogSheld() {
        return shoppingCatalogSheld;
    }

    public ShoppingItem shoppingCatalogSheld(ShoppingCatalogSheld shoppingCatalogSheld) {
        this.shoppingCatalogSheld = shoppingCatalogSheld;
        return this;
    }

    public void setShoppingCatalogSheld(ShoppingCatalogSheld shoppingCatalogSheld) {
        this.shoppingCatalogSheld = shoppingCatalogSheld;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public ShoppingItem shoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
        return this;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingItem)) {
            return false;
        }
        return id != null && id.equals(((ShoppingItem) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingItem{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", checked='" + isChecked() + "'" +
            ", quantity=" + getQuantity() +
            ", unit='" + getUnit() + "'" +
            "}";
    }
}
