package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Spent.
 */
@Entity
@Table(name = "spent")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Spent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2, max = 30)
    @Column(name = "label", length = 30, nullable = false)
    private String label;

    @Size(max = 200)
    @Column(name = "description", length = 200)
    private String description;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Float amount;

    @NotNull
    @Column(name = "spent_date", nullable = false)
    private LocalDate spentDate;

    @NotNull
    @Column(name = "confirmed", nullable = false)
    private Boolean confirmed;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @OneToMany(mappedBy = "spent")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpentSharing> spentSharings = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("spents")
    private Wallet wallet;

    @ManyToOne
    @JsonIgnoreProperties("spents")
    private Profil spender;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Spent label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public Spent description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getAmount() {
        return amount;
    }

    public Spent amount(Float amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public LocalDate getSpentDate() {
        return spentDate;
    }

    public Spent spentDate(LocalDate spentDate) {
        this.spentDate = spentDate;
        return this;
    }

    public void setSpentDate(LocalDate spentDate) {
        this.spentDate = spentDate;
    }

    public Boolean isConfirmed() {
        return confirmed;
    }

    public Spent confirmed(Boolean confirmed) {
        this.confirmed = confirmed;
        return this;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public Spent lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<SpentSharing> getSpentSharings() {
        return spentSharings;
    }

    public Spent spentSharings(Set<SpentSharing> spentSharings) {
        this.spentSharings = spentSharings;
        return this;
    }

    public Spent addSpentSharing(SpentSharing spentSharing) {
        this.spentSharings.add(spentSharing);
        spentSharing.setSpent(this);
        return this;
    }

    public Spent removeSpentSharing(SpentSharing spentSharing) {
        this.spentSharings.remove(spentSharing);
        spentSharing.setSpent(null);
        return this;
    }

    public void setSpentSharings(Set<SpentSharing> spentSharings) {
        this.spentSharings = spentSharings;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public Spent wallet(Wallet wallet) {
        this.wallet = wallet;
        return this;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public Profil getSpender() {
        return spender;
    }

    public Spent spender(Profil profil) {
        this.spender = profil;
        return this;
    }

    public void setSpender(Profil profil) {
        this.spender = profil;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Spent)) {
            return false;
        }
        return id != null && id.equals(((Spent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Spent{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", description='" + getDescription() + "'" +
            ", amount=" + getAmount() +
            ", spentDate='" + getSpentDate() + "'" +
            ", confirmed='" + isConfirmed() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
