package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A ShoppingCatalogItem.
 */
@Entity
@Table(name = "shopping_catalog_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingCatalogItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2, max = 80)
    @Column(name = "label", length = 80, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shoppingCatalogItems")
    private ShoppingCatalogSheld shoppingCatalogSheld;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public ShoppingCatalogItem label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public ShoppingCatalogItem lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public ShoppingCatalogSheld getShoppingCatalogSheld() {
        return shoppingCatalogSheld;
    }

    public ShoppingCatalogItem shoppingCatalogSheld(ShoppingCatalogSheld shoppingCatalogSheld) {
        this.shoppingCatalogSheld = shoppingCatalogSheld;
        return this;
    }

    public void setShoppingCatalogSheld(ShoppingCatalogSheld shoppingCatalogSheld) {
        this.shoppingCatalogSheld = shoppingCatalogSheld;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingCatalogItem)) {
            return false;
        }
        return id != null && id.equals(((ShoppingCatalogItem) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingCatalogItem{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
