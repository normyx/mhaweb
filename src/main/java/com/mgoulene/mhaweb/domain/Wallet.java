package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Wallet.
 */
@Entity
@Table(name = "wallet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Wallet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "wallet_owner",
               joinColumns = @JoinColumn(name = "wallet_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private Set<Profil> owners = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("wallets")
    private Workspace workspace;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Wallet label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public Wallet lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<Profil> getOwners() {
        return owners;
    }

    public Wallet owners(Set<Profil> profils) {
        this.owners = profils;
        return this;
    }

    public Wallet addOwner(Profil profil) {
        this.owners.add(profil);
        profil.getWallets().add(this);
        return this;
    }

    public Wallet removeOwner(Profil profil) {
        this.owners.remove(profil);
        profil.getWallets().remove(this);
        return this;
    }

    public void setOwners(Set<Profil> profils) {
        this.owners = profils;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public Wallet workspace(Workspace workspace) {
        this.workspace = workspace;
        return this;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Wallet)) {
            return false;
        }
        return id != null && id.equals(((Wallet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Wallet{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
