package com.mgoulene.mhaweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Profil.
 */
@Entity
@Table(name = "profil")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Profil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "display_name", nullable = false)
    private String displayName;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private LocalDate lastUpdate;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToOne
    @JoinColumn(unique = true)
    private ProfilData profilData;

    @OneToMany(mappedBy = "spender")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Spent> spents = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<ShoppingCart> shoppingCarts = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<TaskProject> taskProjects = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<TodoList> todoLists = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<TodoListTemplate> todoListTemplates = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Wallet> wallets = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Workspace> workspaces = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Profil displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public Profil lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public User getUser() {
        return user;
    }

    public Profil user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ProfilData getProfilData() {
        return profilData;
    }

    public Profil profilData(ProfilData profilData) {
        this.profilData = profilData;
        return this;
    }

    public void setProfilData(ProfilData profilData) {
        this.profilData = profilData;
    }

    public Set<Spent> getSpents() {
        return spents;
    }

    public Profil spents(Set<Spent> spents) {
        this.spents = spents;
        return this;
    }

    public Profil addSpent(Spent spent) {
        this.spents.add(spent);
        spent.setSpender(this);
        return this;
    }

    public Profil removeSpent(Spent spent) {
        this.spents.remove(spent);
        spent.setSpender(null);
        return this;
    }

    public void setSpents(Set<Spent> spents) {
        this.spents = spents;
    }

    public Set<ShoppingCart> getShoppingCarts() {
        return shoppingCarts;
    }

    public Profil shoppingCarts(Set<ShoppingCart> shoppingCarts) {
        this.shoppingCarts = shoppingCarts;
        return this;
    }

    public Profil addShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCarts.add(shoppingCart);
        shoppingCart.getOwners().add(this);
        return this;
    }

    public Profil removeShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCarts.remove(shoppingCart);
        shoppingCart.getOwners().remove(this);
        return this;
    }

    public void setShoppingCarts(Set<ShoppingCart> shoppingCarts) {
        this.shoppingCarts = shoppingCarts;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public Profil tasks(Set<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    public Profil addTask(Task task) {
        this.tasks.add(task);
        task.getOwners().add(this);
        return this;
    }

    public Profil removeTask(Task task) {
        this.tasks.remove(task);
        task.getOwners().remove(this);
        return this;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Set<TaskProject> getTaskProjects() {
        return taskProjects;
    }

    public Profil taskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
        return this;
    }

    public Profil addTaskProject(TaskProject taskProject) {
        this.taskProjects.add(taskProject);
        taskProject.getOwners().add(this);
        return this;
    }

    public Profil removeTaskProject(TaskProject taskProject) {
        this.taskProjects.remove(taskProject);
        taskProject.getOwners().remove(this);
        return this;
    }

    public void setTaskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
    }

    public Set<TodoList> getTodoLists() {
        return todoLists;
    }

    public Profil todoLists(Set<TodoList> todoLists) {
        this.todoLists = todoLists;
        return this;
    }

    public Profil addTodoList(TodoList todoList) {
        this.todoLists.add(todoList);
        todoList.getOwners().add(this);
        return this;
    }

    public Profil removeTodoList(TodoList todoList) {
        this.todoLists.remove(todoList);
        todoList.getOwners().remove(this);
        return this;
    }

    public void setTodoLists(Set<TodoList> todoLists) {
        this.todoLists = todoLists;
    }

    public Set<TodoListTemplate> getTodoListTemplates() {
        return todoListTemplates;
    }

    public Profil todoListTemplates(Set<TodoListTemplate> todoListTemplates) {
        this.todoListTemplates = todoListTemplates;
        return this;
    }

    public Profil addTodoListTemplate(TodoListTemplate todoListTemplate) {
        this.todoListTemplates.add(todoListTemplate);
        todoListTemplate.getOwners().add(this);
        return this;
    }

    public Profil removeTodoListTemplate(TodoListTemplate todoListTemplate) {
        this.todoListTemplates.remove(todoListTemplate);
        todoListTemplate.getOwners().remove(this);
        return this;
    }

    public void setTodoListTemplates(Set<TodoListTemplate> todoListTemplates) {
        this.todoListTemplates = todoListTemplates;
    }

    public Set<Wallet> getWallets() {
        return wallets;
    }

    public Profil wallets(Set<Wallet> wallets) {
        this.wallets = wallets;
        return this;
    }

    public Profil addWallet(Wallet wallet) {
        this.wallets.add(wallet);
        wallet.getOwners().add(this);
        return this;
    }

    public Profil removeWallet(Wallet wallet) {
        this.wallets.remove(wallet);
        wallet.getOwners().remove(this);
        return this;
    }

    public void setWallets(Set<Wallet> wallets) {
        this.wallets = wallets;
    }

    public Set<Workspace> getWorkspaces() {
        return workspaces;
    }

    public Profil workspaces(Set<Workspace> workspaces) {
        this.workspaces = workspaces;
        return this;
    }

    public Profil addWorkspace(Workspace workspace) {
        this.workspaces.add(workspace);
        workspace.getOwners().add(this);
        return this;
    }

    public Profil removeWorkspace(Workspace workspace) {
        this.workspaces.remove(workspace);
        workspace.getOwners().remove(this);
        return this;
    }

    public void setWorkspaces(Set<Workspace> workspaces) {
        this.workspaces = workspaces;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Profil)) {
            return false;
        }
        return id != null && id.equals(((Profil) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Profil{" +
            "id=" + getId() +
            ", displayName='" + getDisplayName() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
