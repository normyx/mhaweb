package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.Wallet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Wallet entity.
 */
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long>, JpaSpecificationExecutor<Wallet> {

    @Query(value = "select distinct wallet from Wallet wallet left join fetch wallet.owners",
        countQuery = "select count(distinct wallet) from Wallet wallet")
    Page<Wallet> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct wallet from Wallet wallet left join fetch wallet.owners")
    List<Wallet> findAllWithEagerRelationships();

    @Query("select wallet from Wallet wallet left join fetch wallet.owners where wallet.id =:id")
    Optional<Wallet> findOneWithEagerRelationships(@Param("id") Long id);

}
