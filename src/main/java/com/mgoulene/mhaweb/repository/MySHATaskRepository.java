package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.Task;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Task entity.
 */
@Repository
public interface MySHATaskRepository extends TaskRepository {

    @Query(value = "select distinct task from Task task left join fetch task.owners where task.taskProject.id=:taskProjectId order by task.done, task.dueDate",
        countQuery = "select count(distinct task) from Task task where task.taskProject.id=:taskProjectId")
    Page<Task> findAllWhereTaskProjectIdWithEagerRelationships(Pageable pageable, @Param("taskProjectId") Long taskProjectId);


}
