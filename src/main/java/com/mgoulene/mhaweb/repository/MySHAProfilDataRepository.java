package com.mgoulene.mhaweb.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the TaskProject entity.
 * 
 * @param <ProfilData>
 */
@Repository
public interface MySHAProfilDataRepository<ProfilData> extends ProfilDataRepository {

    @Query(value = "select distinct profilData from ProfilData profilData where profilData.id in (select pd.id from Workspace w join w.owners p join p.profilData pd where w.id in :workspaceIds) AND lastUpdate >= :refreshDate")
    List<ProfilData> findAllWhereWorkspace(@Param("workspaceIds") List<Long> workspaceIds, @Param("refreshDate") LocalDate refreshDate);

    @Query(value = "select distinct profilData from ProfilData profilData where profilData.id in (select pd.id from Workspace w join w.owners p join p.profilData pd where w.id in :workspaceIds)")
    List<ProfilData> findAllWhereWorkspace(@Param("workspaceIds") List<Long> workspaceIds);
}
