package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.ShoppingCart;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ShoppingCart entity.
 */
@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long> {

    @Query(value = "select distinct shoppingCart from ShoppingCart shoppingCart left join fetch shoppingCart.owners",
        countQuery = "select count(distinct shoppingCart) from ShoppingCart shoppingCart")
    Page<ShoppingCart> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct shoppingCart from ShoppingCart shoppingCart left join fetch shoppingCart.owners")
    List<ShoppingCart> findAllWithEagerRelationships();

    @Query("select shoppingCart from ShoppingCart shoppingCart left join fetch shoppingCart.owners where shoppingCart.id =:id")
    Optional<ShoppingCart> findOneWithEagerRelationships(@Param("id") Long id);

}
