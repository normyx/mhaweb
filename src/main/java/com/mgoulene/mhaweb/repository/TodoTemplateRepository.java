package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.TodoTemplate;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TodoTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TodoTemplateRepository extends JpaRepository<TodoTemplate, Long>, JpaSpecificationExecutor<TodoTemplate> {

}
