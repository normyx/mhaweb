package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.SpentConfig;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SpentConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpentConfigRepository extends JpaRepository<SpentConfig, Long>, JpaSpecificationExecutor<SpentConfig> {

}
