package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.TodoList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TodoList entity.
 */
@Repository
public interface TodoListRepository extends JpaRepository<TodoList, Long>, JpaSpecificationExecutor<TodoList> {

    @Query(value = "select distinct todoList from TodoList todoList left join fetch todoList.owners",
        countQuery = "select count(distinct todoList) from TodoList todoList")
    Page<TodoList> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct todoList from TodoList todoList left join fetch todoList.owners")
    List<TodoList> findAllWithEagerRelationships();

    @Query("select todoList from TodoList todoList left join fetch todoList.owners where todoList.id =:id")
    Optional<TodoList> findOneWithEagerRelationships(@Param("id") Long id);

}
