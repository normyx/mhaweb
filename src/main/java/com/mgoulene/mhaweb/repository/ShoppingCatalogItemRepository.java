package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.ShoppingCatalogItem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ShoppingCatalogItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShoppingCatalogItemRepository extends JpaRepository<ShoppingCatalogItem, Long>, JpaSpecificationExecutor<ShoppingCatalogItem> {

}
