package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.Workspace;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MySHAWorkspaceRepository extends WorkspaceRepository {

    @Query(value = "select distinct workspace from Workspace workspace join workspace.owners profils join profils.user user where user.id =:userId", 
        countQuery = "select count(distinct workspace) from Workspace workspace join workspace.owners profils join profils.user user where user.id =:userId")
    Page<Workspace> findAllWhereOwnerId(Pageable pageable, @Param("userId") Long userId);

}
