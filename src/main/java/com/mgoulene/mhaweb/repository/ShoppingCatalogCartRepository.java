package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.ShoppingCatalogCart;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ShoppingCatalogCart entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShoppingCatalogCartRepository extends JpaRepository<ShoppingCatalogCart, Long>, JpaSpecificationExecutor<ShoppingCatalogCart> {

}
