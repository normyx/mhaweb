package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.ProfilData;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProfilData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfilDataRepository extends JpaRepository<ProfilData, Long>, JpaSpecificationExecutor<ProfilData> {

}
