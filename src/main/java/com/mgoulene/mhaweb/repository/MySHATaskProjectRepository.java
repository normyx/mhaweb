package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.TaskProject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the TaskProject entity.
 */
@Repository
public interface MySHATaskProjectRepository extends TaskProjectRepository {

    @Query(value = "select distinct taskProject from TaskProject taskProject join taskProject.owners profils join profils.user user where user.id =:userId", countQuery = "select count(distinct taskProject) from TaskProject taskProject join taskProject.owners profils join profils.user user where user.id =:userId")
    Page<TaskProject> findAllWhereOwnerId(Pageable pageable, @Param("userId") Long userId);
    /*
     * @Query(value =
     * "select distinct taskProject from TaskProject taskProject join taskProject.workspace workspace where workspace.id =:workspaceId"
     * , countQuery =
     * "select count(distinct taskProject) from TaskProject taskProject join taskProject.workspace workspace where workspace.id =:workspaceId"
     * ) Page<TaskProject> findAllWhereWorkspace(Pageable
     * pageable, @Param("workspaceId") Long workspaceId);
     */
}
