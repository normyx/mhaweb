package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.TodoListTemplate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TodoListTemplate entity.
 */
@Repository
public interface TodoListTemplateRepository extends JpaRepository<TodoListTemplate, Long>, JpaSpecificationExecutor<TodoListTemplate> {

    @Query(value = "select distinct todoListTemplate from TodoListTemplate todoListTemplate left join fetch todoListTemplate.owners",
        countQuery = "select count(distinct todoListTemplate) from TodoListTemplate todoListTemplate")
    Page<TodoListTemplate> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct todoListTemplate from TodoListTemplate todoListTemplate left join fetch todoListTemplate.owners")
    List<TodoListTemplate> findAllWithEagerRelationships();

    @Query("select todoListTemplate from TodoListTemplate todoListTemplate left join fetch todoListTemplate.owners where todoListTemplate.id =:id")
    Optional<TodoListTemplate> findOneWithEagerRelationships(@Param("id") Long id);

}
