package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.TaskProject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TaskProject entity.
 */
@Repository
public interface TaskProjectRepository extends JpaRepository<TaskProject, Long>, JpaSpecificationExecutor<TaskProject> {

    @Query(value = "select distinct taskProject from TaskProject taskProject left join fetch taskProject.owners",
        countQuery = "select count(distinct taskProject) from TaskProject taskProject")
    Page<TaskProject> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct taskProject from TaskProject taskProject left join fetch taskProject.owners")
    List<TaskProject> findAllWithEagerRelationships();

    @Query("select taskProject from TaskProject taskProject left join fetch taskProject.owners where taskProject.id =:id")
    Optional<TaskProject> findOneWithEagerRelationships(@Param("id") Long id);

}
