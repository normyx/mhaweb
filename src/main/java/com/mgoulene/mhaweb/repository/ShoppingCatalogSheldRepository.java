package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.ShoppingCatalogSheld;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ShoppingCatalogSheld entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShoppingCatalogSheldRepository extends JpaRepository<ShoppingCatalogSheld, Long>, JpaSpecificationExecutor<ShoppingCatalogSheld> {

}
