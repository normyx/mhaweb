package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.SpentSharing;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SpentSharing entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpentSharingRepository extends JpaRepository<SpentSharing, Long>, JpaSpecificationExecutor<SpentSharing> {

}
