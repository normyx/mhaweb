package com.mgoulene.mhaweb.repository;

import com.mgoulene.mhaweb.domain.Spent;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Spent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpentRepository extends JpaRepository<Spent, Long>, JpaSpecificationExecutor<Spent> {

}
