import { Moment } from 'moment';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { ITodoList } from 'app/shared/model/todo-list.model';
import { ITodoListTemplate } from 'app/shared/model/todo-list-template.model';
import { IWallet } from 'app/shared/model/wallet.model';
import { IProfil } from 'app/shared/model/profil.model';

export interface IWorkspace {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  shoppingCarts?: IShoppingCart[];
  taskProjects?: ITaskProject[];
  todoLists?: ITodoList[];
  todoListTemplates?: ITodoListTemplate[];
  wallets?: IWallet[];
  owners?: IProfil[];
}

export class Workspace implements IWorkspace {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public shoppingCarts?: IShoppingCart[],
    public taskProjects?: ITaskProject[],
    public todoLists?: ITodoList[],
    public todoListTemplates?: ITodoListTemplate[],
    public wallets?: IWallet[],
    public owners?: IProfil[]
  ) {}
}
