import { Moment } from 'moment';
import { IProfil } from 'app/shared/model/profil.model';

export interface IWallet {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  owners?: IProfil[];
  workspaceLabel?: string;
  workspaceId?: number;
}

export class Wallet implements IWallet {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public owners?: IProfil[],
    public workspaceLabel?: string,
    public workspaceId?: number
  ) {}
}
