import { Moment } from 'moment';

export interface IProfilData {
  id?: number;
  photoContentType?: string;
  photo?: any;
  lastUpdate?: Moment;
}

export class ProfilData implements IProfilData {
  constructor(public id?: number, public photoContentType?: string, public photo?: any, public lastUpdate?: Moment) {}
}
