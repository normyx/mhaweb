import { Moment } from 'moment';
import { ITodoTemplate } from 'app/shared/model/todo-template.model';
import { IProfil } from 'app/shared/model/profil.model';

export interface ITodoListTemplate {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  todoTemplates?: ITodoTemplate[];
  owners?: IProfil[];
  workspaceLabel?: string;
  workspaceId?: number;
}

export class TodoListTemplate implements ITodoListTemplate {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public todoTemplates?: ITodoTemplate[],
    public owners?: IProfil[],
    public workspaceLabel?: string,
    public workspaceId?: number
  ) {}
}
