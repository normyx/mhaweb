import { Moment } from 'moment';

export interface IShoppingCatalogItem {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  shoppingCatalogSheldLabel?: string;
  shoppingCatalogSheldId?: number;
}

export class ShoppingCatalogItem implements IShoppingCatalogItem {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public shoppingCatalogSheldLabel?: string,
    public shoppingCatalogSheldId?: number
  ) {}
}
