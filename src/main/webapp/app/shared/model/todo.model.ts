import { Moment } from 'moment';

export interface ITodo {
  id?: number;
  label?: string;
  done?: boolean;
  lastUpdate?: Moment;
  todoListLabel?: string;
  todoListId?: number;
}

export class Todo implements ITodo {
  constructor(
    public id?: number,
    public label?: string,
    public done?: boolean,
    public lastUpdate?: Moment,
    public todoListLabel?: string,
    public todoListId?: number
  ) {
    this.done = this.done || false;
  }
}
