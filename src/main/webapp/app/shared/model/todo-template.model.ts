import { Moment } from 'moment';

export interface ITodoTemplate {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  todoListTemplateLabel?: string;
  todoListTemplateId?: number;
}

export class TodoTemplate implements ITodoTemplate {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public todoListTemplateLabel?: string,
    public todoListTemplateId?: number
  ) {}
}
