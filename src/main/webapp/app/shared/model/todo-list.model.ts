import { Moment } from 'moment';
import { ITodo } from 'app/shared/model/todo.model';
import { IProfil } from 'app/shared/model/profil.model';

export interface ITodoList {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  todos?: ITodo[];
  owners?: IProfil[];
  workspaceLabel?: string;
  workspaceId?: number;
}

export class TodoList implements ITodoList {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public todos?: ITodo[],
    public owners?: IProfil[],
    public workspaceLabel?: string,
    public workspaceId?: number
  ) {}
}
