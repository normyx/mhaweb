import { Moment } from 'moment';

export interface ISpentConfig {
  id?: number;
  label?: string;
  amount?: number;
  spentLabel?: string;
  lastUpdate?: Moment;
  walletLabel?: string;
  walletId?: number;
}

export class SpentConfig implements ISpentConfig {
  constructor(
    public id?: number,
    public label?: string,
    public amount?: number,
    public spentLabel?: string,
    public lastUpdate?: Moment,
    public walletLabel?: string,
    public walletId?: number
  ) {}
}
