export const enum Unit {
  QUANTITY,
  G,
  KG,
  L,
  ML
}
