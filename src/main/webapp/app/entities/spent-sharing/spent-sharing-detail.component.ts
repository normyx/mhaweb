import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

@Component({
  selector: 'mha-spent-sharing-detail',
  templateUrl: './spent-sharing-detail.component.html'
})
export class SpentSharingDetailComponent implements OnInit {
  spentSharing: ISpentSharing | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spentSharing }) => (this.spentSharing = spentSharing));
  }

  previousState(): void {
    window.history.back();
  }
}
