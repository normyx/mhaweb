import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISpentSharing, SpentSharing } from 'app/shared/model/spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';
import { SpentSharingComponent } from './spent-sharing.component';
import { SpentSharingDetailComponent } from './spent-sharing-detail.component';
import { SpentSharingUpdateComponent } from './spent-sharing-update.component';

@Injectable({ providedIn: 'root' })
export class SpentSharingResolve implements Resolve<ISpentSharing> {
  constructor(private service: SpentSharingService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISpentSharing> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((spentSharing: HttpResponse<SpentSharing>) => {
          if (spentSharing.body) {
            return of(spentSharing.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new SpentSharing());
  }
}

export const spentSharingRoute: Routes = [
  {
    path: '',
    component: SpentSharingComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentSharingDetailComponent,
    resolve: {
      spentSharing: SpentSharingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentSharingUpdateComponent,
    resolve: {
      spentSharing: SpentSharingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentSharingUpdateComponent,
    resolve: {
      spentSharing: SpentSharingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
