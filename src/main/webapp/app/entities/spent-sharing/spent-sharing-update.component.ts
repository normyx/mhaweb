import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISpentSharing, SpentSharing } from 'app/shared/model/spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';
import { ISpent } from 'app/shared/model/spent.model';
import { SpentService } from 'app/entities/spent/spent.service';

type SelectableEntity = IProfil | ISpent;

@Component({
  selector: 'mha-spent-sharing-update',
  templateUrl: './spent-sharing-update.component.html'
})
export class SpentSharingUpdateComponent implements OnInit {
  isSaving = false;
  profils: IProfil[] = [];
  spents: ISpent[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    amountShare: [null, [Validators.required]],
    share: [null, [Validators.required, Validators.min(0)]],
    lastUpdate: [null, [Validators.required]],
    sharingProfilId: [],
    spentId: []
  });

  constructor(
    protected spentSharingService: SpentSharingService,
    protected profilService: ProfilService,
    protected spentService: SpentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spentSharing }) => {
      this.updateForm(spentSharing);

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));

      this.spentService.query().subscribe((res: HttpResponse<ISpent[]>) => (this.spents = res.body || []));
    });
  }

  updateForm(spentSharing: ISpentSharing): void {
    this.editForm.patchValue({
      id: spentSharing.id,
      amountShare: spentSharing.amountShare,
      share: spentSharing.share,
      lastUpdate: spentSharing.lastUpdate,
      sharingProfilId: spentSharing.sharingProfilId,
      spentId: spentSharing.spentId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const spentSharing = this.createFromForm();
    if (spentSharing.id !== undefined) {
      this.subscribeToSaveResponse(this.spentSharingService.update(spentSharing));
    } else {
      this.subscribeToSaveResponse(this.spentSharingService.create(spentSharing));
    }
  }

  private createFromForm(): ISpentSharing {
    return {
      ...new SpentSharing(),
      id: this.editForm.get(['id'])!.value,
      amountShare: this.editForm.get(['amountShare'])!.value,
      share: this.editForm.get(['share'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      sharingProfilId: this.editForm.get(['sharingProfilId'])!.value,
      spentId: this.editForm.get(['spentId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpentSharing>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
