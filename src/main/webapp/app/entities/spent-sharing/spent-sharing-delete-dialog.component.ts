import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpentSharing } from 'app/shared/model/spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';

@Component({
  templateUrl: './spent-sharing-delete-dialog.component.html'
})
export class SpentSharingDeleteDialogComponent {
  spentSharing?: ISpentSharing;

  constructor(
    protected spentSharingService: SpentSharingService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.spentSharingService.delete(id).subscribe(() => {
      this.eventManager.broadcast('spentSharingListModification');
      this.activeModal.close();
    });
  }
}
