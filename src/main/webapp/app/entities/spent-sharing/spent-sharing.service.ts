import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

type EntityResponseType = HttpResponse<ISpentSharing>;
type EntityArrayResponseType = HttpResponse<ISpentSharing[]>;

@Injectable({ providedIn: 'root' })
export class SpentSharingService {
  public resourceUrl = SERVER_API_URL + 'api/spent-sharings';

  constructor(protected http: HttpClient) {}

  create(spentSharing: ISpentSharing): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentSharing);
    return this.http
      .post<ISpentSharing>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(spentSharing: ISpentSharing): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentSharing);
    return this.http
      .put<ISpentSharing>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISpentSharing>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISpentSharing[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(spentSharing: ISpentSharing): ISpentSharing {
    const copy: ISpentSharing = Object.assign({}, spentSharing, {
      lastUpdate: spentSharing.lastUpdate && spentSharing.lastUpdate.isValid() ? spentSharing.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((spentSharing: ISpentSharing) => {
        spentSharing.lastUpdate = spentSharing.lastUpdate ? moment(spentSharing.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
