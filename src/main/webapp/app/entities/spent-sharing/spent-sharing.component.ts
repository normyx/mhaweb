import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SpentSharingService } from './spent-sharing.service';
import { SpentSharingDeleteDialogComponent } from './spent-sharing-delete-dialog.component';

@Component({
  selector: 'mha-spent-sharing',
  templateUrl: './spent-sharing.component.html'
})
export class SpentSharingComponent implements OnInit, OnDestroy {
  spentSharings: ISpentSharing[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected spentSharingService: SpentSharingService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.spentSharings = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.spentSharingService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ISpentSharing[]>) => this.paginateSpentSharings(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.spentSharings = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSpentSharings();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISpentSharing): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSpentSharings(): void {
    this.eventSubscriber = this.eventManager.subscribe('spentSharingListModification', () => this.reset());
  }

  delete(spentSharing: ISpentSharing): void {
    const modalRef = this.modalService.open(SpentSharingDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.spentSharing = spentSharing;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateSpentSharings(data: ISpentSharing[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.spentSharings.push(data[i]);
      }
    }
  }
}
