import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISpent, Spent } from 'app/shared/model/spent.model';
import { SpentService } from './spent.service';
import { SpentComponent } from './spent.component';
import { SpentDetailComponent } from './spent-detail.component';
import { SpentUpdateComponent } from './spent-update.component';

@Injectable({ providedIn: 'root' })
export class SpentResolve implements Resolve<ISpent> {
  constructor(private service: SpentService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISpent> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((spent: HttpResponse<Spent>) => {
          if (spent.body) {
            return of(spent.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Spent());
  }
}

export const spentRoute: Routes = [
  {
    path: '',
    component: SpentComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentDetailComponent,
    resolve: {
      spent: SpentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentUpdateComponent,
    resolve: {
      spent: SpentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentUpdateComponent,
    resolve: {
      spent: SpentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
