import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISpent, Spent } from 'app/shared/model/spent.model';
import { SpentService } from './spent.service';
import { IWallet } from 'app/shared/model/wallet.model';
import { WalletService } from 'app/entities/wallet/wallet.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';

type SelectableEntity = IWallet | IProfil;

@Component({
  selector: 'mha-spent-update',
  templateUrl: './spent-update.component.html'
})
export class SpentUpdateComponent implements OnInit {
  isSaving = false;
  wallets: IWallet[] = [];
  profils: IProfil[] = [];
  spentDateDp: any;
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    description: [null, [Validators.maxLength(200)]],
    amount: [null, [Validators.required]],
    spentDate: [null, [Validators.required]],
    confirmed: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    walletId: [],
    spenderId: []
  });

  constructor(
    protected spentService: SpentService,
    protected walletService: WalletService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spent }) => {
      this.updateForm(spent);

      this.walletService.query().subscribe((res: HttpResponse<IWallet[]>) => (this.wallets = res.body || []));

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));
    });
  }

  updateForm(spent: ISpent): void {
    this.editForm.patchValue({
      id: spent.id,
      label: spent.label,
      description: spent.description,
      amount: spent.amount,
      spentDate: spent.spentDate,
      confirmed: spent.confirmed,
      lastUpdate: spent.lastUpdate,
      walletId: spent.walletId,
      spenderId: spent.spenderId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const spent = this.createFromForm();
    if (spent.id !== undefined) {
      this.subscribeToSaveResponse(this.spentService.update(spent));
    } else {
      this.subscribeToSaveResponse(this.spentService.create(spent));
    }
  }

  private createFromForm(): ISpent {
    return {
      ...new Spent(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      description: this.editForm.get(['description'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      spentDate: this.editForm.get(['spentDate'])!.value,
      confirmed: this.editForm.get(['confirmed'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      walletId: this.editForm.get(['walletId'])!.value,
      spenderId: this.editForm.get(['spenderId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpent>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
