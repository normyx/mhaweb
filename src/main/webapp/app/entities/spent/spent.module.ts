import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { SpentComponent } from './spent.component';
import { SpentDetailComponent } from './spent-detail.component';
import { SpentUpdateComponent } from './spent-update.component';
import { SpentDeleteDialogComponent } from './spent-delete-dialog.component';
import { spentRoute } from './spent.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(spentRoute)],
  declarations: [SpentComponent, SpentDetailComponent, SpentUpdateComponent, SpentDeleteDialogComponent],
  entryComponents: [SpentDeleteDialogComponent]
})
export class MhawebSpentModule {}
