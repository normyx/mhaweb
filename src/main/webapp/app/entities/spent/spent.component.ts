import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISpent } from 'app/shared/model/spent.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SpentService } from './spent.service';
import { SpentDeleteDialogComponent } from './spent-delete-dialog.component';

@Component({
  selector: 'mha-spent',
  templateUrl: './spent.component.html'
})
export class SpentComponent implements OnInit, OnDestroy {
  spents: ISpent[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected spentService: SpentService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.spents = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.spentService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ISpent[]>) => this.paginateSpents(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.spents = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSpents();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISpent): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSpents(): void {
    this.eventSubscriber = this.eventManager.subscribe('spentListModification', () => this.reset());
  }

  delete(spent: ISpent): void {
    const modalRef = this.modalService.open(SpentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.spent = spent;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateSpents(data: ISpent[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.spents.push(data[i]);
      }
    }
  }
}
