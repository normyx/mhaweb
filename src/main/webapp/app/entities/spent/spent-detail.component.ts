import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpent } from 'app/shared/model/spent.model';

@Component({
  selector: 'mha-spent-detail',
  templateUrl: './spent-detail.component.html'
})
export class SpentDetailComponent implements OnInit {
  spent: ISpent | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spent }) => (this.spent = spent));
  }

  previousState(): void {
    window.history.back();
  }
}
