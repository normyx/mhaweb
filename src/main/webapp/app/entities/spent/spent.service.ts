import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISpent } from 'app/shared/model/spent.model';

type EntityResponseType = HttpResponse<ISpent>;
type EntityArrayResponseType = HttpResponse<ISpent[]>;

@Injectable({ providedIn: 'root' })
export class SpentService {
  public resourceUrl = SERVER_API_URL + 'api/spents';

  constructor(protected http: HttpClient) {}

  create(spent: ISpent): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spent);
    return this.http
      .post<ISpent>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(spent: ISpent): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spent);
    return this.http
      .put<ISpent>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISpent>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISpent[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(spent: ISpent): ISpent {
    const copy: ISpent = Object.assign({}, spent, {
      spentDate: spent.spentDate && spent.spentDate.isValid() ? spent.spentDate.format(DATE_FORMAT) : undefined,
      lastUpdate: spent.lastUpdate && spent.lastUpdate.isValid() ? spent.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.spentDate = res.body.spentDate ? moment(res.body.spentDate) : undefined;
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((spent: ISpent) => {
        spent.spentDate = spent.spentDate ? moment(spent.spentDate) : undefined;
        spent.lastUpdate = spent.lastUpdate ? moment(spent.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
