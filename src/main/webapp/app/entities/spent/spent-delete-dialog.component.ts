import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpent } from 'app/shared/model/spent.model';
import { SpentService } from './spent.service';

@Component({
  templateUrl: './spent-delete-dialog.component.html'
})
export class SpentDeleteDialogComponent {
  spent?: ISpent;

  constructor(protected spentService: SpentService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.spentService.delete(id).subscribe(() => {
      this.eventManager.broadcast('spentListModification');
      this.activeModal.close();
    });
  }
}
