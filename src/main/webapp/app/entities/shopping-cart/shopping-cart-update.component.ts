import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShoppingCart, ShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ShoppingCartService } from './shopping-cart.service';
import { IShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from 'app/entities/shopping-catalog-cart/shopping-catalog-cart.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from 'app/entities/workspace/workspace.service';

type SelectableEntity = IShoppingCatalogCart | IProfil | IWorkspace;

@Component({
  selector: 'mha-shopping-cart-update',
  templateUrl: './shopping-cart-update.component.html'
})
export class ShoppingCartUpdateComponent implements OnInit {
  isSaving = false;
  shoppingcatalogcarts: IShoppingCatalogCart[] = [];
  profils: IProfil[] = [];
  workspaces: IWorkspace[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]],
    shoppingCatalogCartId: [],
    owners: [],
    workspaceId: [null, Validators.required]
  });

  constructor(
    protected shoppingCartService: ShoppingCartService,
    protected shoppingCatalogCartService: ShoppingCatalogCartService,
    protected profilService: ProfilService,
    protected workspaceService: WorkspaceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingCart }) => {
      this.updateForm(shoppingCart);

      this.shoppingCatalogCartService
        .query()
        .subscribe((res: HttpResponse<IShoppingCatalogCart[]>) => (this.shoppingcatalogcarts = res.body || []));

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));

      this.workspaceService.query().subscribe((res: HttpResponse<IWorkspace[]>) => (this.workspaces = res.body || []));
    });
  }

  updateForm(shoppingCart: IShoppingCart): void {
    this.editForm.patchValue({
      id: shoppingCart.id,
      label: shoppingCart.label,
      lastUpdate: shoppingCart.lastUpdate,
      shoppingCatalogCartId: shoppingCart.shoppingCatalogCartId,
      owners: shoppingCart.owners,
      workspaceId: shoppingCart.workspaceId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shoppingCart = this.createFromForm();
    if (shoppingCart.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingCartService.update(shoppingCart));
    } else {
      this.subscribeToSaveResponse(this.shoppingCartService.create(shoppingCart));
    }
  }

  private createFromForm(): IShoppingCart {
    return {
      ...new ShoppingCart(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      shoppingCatalogCartId: this.editForm.get(['shoppingCatalogCartId'])!.value,
      owners: this.editForm.get(['owners'])!.value,
      workspaceId: this.editForm.get(['workspaceId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingCart>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IProfil[], option: IProfil): IProfil {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
