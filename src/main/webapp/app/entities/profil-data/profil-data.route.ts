import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProfilData, ProfilData } from 'app/shared/model/profil-data.model';
import { ProfilDataService } from './profil-data.service';
import { ProfilDataComponent } from './profil-data.component';
import { ProfilDataDetailComponent } from './profil-data-detail.component';
import { ProfilDataUpdateComponent } from './profil-data-update.component';

@Injectable({ providedIn: 'root' })
export class ProfilDataResolve implements Resolve<IProfilData> {
  constructor(private service: ProfilDataService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProfilData> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((profilData: HttpResponse<ProfilData>) => {
          if (profilData.body) {
            return of(profilData.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProfilData());
  }
}

export const profilDataRoute: Routes = [
  {
    path: '',
    component: ProfilDataComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.profilData.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ProfilDataDetailComponent,
    resolve: {
      profilData: ProfilDataResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.profilData.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ProfilDataUpdateComponent,
    resolve: {
      profilData: ProfilDataResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.profilData.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ProfilDataUpdateComponent,
    resolve: {
      profilData: ProfilDataResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.profilData.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
