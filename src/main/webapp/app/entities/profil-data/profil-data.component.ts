import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProfilData } from 'app/shared/model/profil-data.model';
import { ProfilDataService } from './profil-data.service';
import { ProfilDataDeleteDialogComponent } from './profil-data-delete-dialog.component';

@Component({
  selector: 'mha-profil-data',
  templateUrl: './profil-data.component.html'
})
export class ProfilDataComponent implements OnInit, OnDestroy {
  profilData?: IProfilData[];
  eventSubscriber?: Subscription;

  constructor(
    protected profilDataService: ProfilDataService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.profilDataService.query().subscribe((res: HttpResponse<IProfilData[]>) => (this.profilData = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProfilData();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProfilData): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInProfilData(): void {
    this.eventSubscriber = this.eventManager.subscribe('profilDataListModification', () => this.loadAll());
  }

  delete(profilData: IProfilData): void {
    const modalRef = this.modalService.open(ProfilDataDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.profilData = profilData;
  }
}
