import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProfilData } from 'app/shared/model/profil-data.model';
import { ProfilDataService } from './profil-data.service';

@Component({
  templateUrl: './profil-data-delete-dialog.component.html'
})
export class ProfilDataDeleteDialogComponent {
  profilData?: IProfilData;

  constructor(
    protected profilDataService: ProfilDataService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.profilDataService.delete(id).subscribe(() => {
      this.eventManager.broadcast('profilDataListModification');
      this.activeModal.close();
    });
  }
}
