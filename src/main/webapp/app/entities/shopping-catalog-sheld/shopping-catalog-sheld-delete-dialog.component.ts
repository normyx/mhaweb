import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';
import { ShoppingCatalogSheldService } from './shopping-catalog-sheld.service';

@Component({
  templateUrl: './shopping-catalog-sheld-delete-dialog.component.html'
})
export class ShoppingCatalogSheldDeleteDialogComponent {
  shoppingCatalogSheld?: IShoppingCatalogSheld;

  constructor(
    protected shoppingCatalogSheldService: ShoppingCatalogSheldService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.shoppingCatalogSheldService.delete(id).subscribe(() => {
      this.eventManager.broadcast('shoppingCatalogSheldListModification');
      this.activeModal.close();
    });
  }
}
