import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';

@Component({
  selector: 'mha-shopping-catalog-sheld-detail',
  templateUrl: './shopping-catalog-sheld-detail.component.html'
})
export class ShoppingCatalogSheldDetailComponent implements OnInit {
  shoppingCatalogSheld: IShoppingCatalogSheld | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingCatalogSheld }) => (this.shoppingCatalogSheld = shoppingCatalogSheld));
  }

  previousState(): void {
    window.history.back();
  }
}
