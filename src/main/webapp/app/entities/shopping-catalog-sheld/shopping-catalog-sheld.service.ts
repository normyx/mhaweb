import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';

type EntityResponseType = HttpResponse<IShoppingCatalogSheld>;
type EntityArrayResponseType = HttpResponse<IShoppingCatalogSheld[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogSheldService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-catalog-shelds';

  constructor(protected http: HttpClient) {}

  create(shoppingCatalogSheld: IShoppingCatalogSheld): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingCatalogSheld);
    return this.http
      .post<IShoppingCatalogSheld>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shoppingCatalogSheld: IShoppingCatalogSheld): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingCatalogSheld);
    return this.http
      .put<IShoppingCatalogSheld>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShoppingCatalogSheld>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShoppingCatalogSheld[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shoppingCatalogSheld: IShoppingCatalogSheld): IShoppingCatalogSheld {
    const copy: IShoppingCatalogSheld = Object.assign({}, shoppingCatalogSheld, {
      lastUpdate:
        shoppingCatalogSheld.lastUpdate && shoppingCatalogSheld.lastUpdate.isValid()
          ? shoppingCatalogSheld.lastUpdate.format(DATE_FORMAT)
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shoppingCatalogSheld: IShoppingCatalogSheld) => {
        shoppingCatalogSheld.lastUpdate = shoppingCatalogSheld.lastUpdate ? moment(shoppingCatalogSheld.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
