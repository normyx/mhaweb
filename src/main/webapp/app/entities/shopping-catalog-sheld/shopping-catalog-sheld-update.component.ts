import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShoppingCatalogSheld, ShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';
import { ShoppingCatalogSheldService } from './shopping-catalog-sheld.service';
import { IShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from 'app/entities/shopping-catalog-cart/shopping-catalog-cart.service';

@Component({
  selector: 'mha-shopping-catalog-sheld-update',
  templateUrl: './shopping-catalog-sheld-update.component.html'
})
export class ShoppingCatalogSheldUpdateComponent implements OnInit {
  isSaving = false;
  shoppingcatalogcarts: IShoppingCatalogCart[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]],
    isDefault: [null, [Validators.required]],
    isDeleted: [null, [Validators.required]],
    shoppingCatalogCartId: [null, Validators.required]
  });

  constructor(
    protected shoppingCatalogSheldService: ShoppingCatalogSheldService,
    protected shoppingCatalogCartService: ShoppingCatalogCartService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingCatalogSheld }) => {
      this.updateForm(shoppingCatalogSheld);

      this.shoppingCatalogCartService
        .query()
        .subscribe((res: HttpResponse<IShoppingCatalogCart[]>) => (this.shoppingcatalogcarts = res.body || []));
    });
  }

  updateForm(shoppingCatalogSheld: IShoppingCatalogSheld): void {
    this.editForm.patchValue({
      id: shoppingCatalogSheld.id,
      label: shoppingCatalogSheld.label,
      lastUpdate: shoppingCatalogSheld.lastUpdate,
      isDefault: shoppingCatalogSheld.isDefault,
      isDeleted: shoppingCatalogSheld.isDeleted,
      shoppingCatalogCartId: shoppingCatalogSheld.shoppingCatalogCartId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shoppingCatalogSheld = this.createFromForm();
    if (shoppingCatalogSheld.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingCatalogSheldService.update(shoppingCatalogSheld));
    } else {
      this.subscribeToSaveResponse(this.shoppingCatalogSheldService.create(shoppingCatalogSheld));
    }
  }

  private createFromForm(): IShoppingCatalogSheld {
    return {
      ...new ShoppingCatalogSheld(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      isDefault: this.editForm.get(['isDefault'])!.value,
      isDeleted: this.editForm.get(['isDeleted'])!.value,
      shoppingCatalogCartId: this.editForm.get(['shoppingCatalogCartId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingCatalogSheld>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IShoppingCatalogCart): any {
    return item.id;
  }
}
