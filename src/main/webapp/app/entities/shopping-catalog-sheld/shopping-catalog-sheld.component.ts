import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ShoppingCatalogSheldService } from './shopping-catalog-sheld.service';
import { ShoppingCatalogSheldDeleteDialogComponent } from './shopping-catalog-sheld-delete-dialog.component';

@Component({
  selector: 'mha-shopping-catalog-sheld',
  templateUrl: './shopping-catalog-sheld.component.html'
})
export class ShoppingCatalogSheldComponent implements OnInit, OnDestroy {
  shoppingCatalogShelds: IShoppingCatalogSheld[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected shoppingCatalogSheldService: ShoppingCatalogSheldService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.shoppingCatalogShelds = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.shoppingCatalogSheldService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IShoppingCatalogSheld[]>) => this.paginateShoppingCatalogShelds(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.shoppingCatalogShelds = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShoppingCatalogShelds();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShoppingCatalogSheld): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShoppingCatalogShelds(): void {
    this.eventSubscriber = this.eventManager.subscribe('shoppingCatalogSheldListModification', () => this.reset());
  }

  delete(shoppingCatalogSheld: IShoppingCatalogSheld): void {
    const modalRef = this.modalService.open(ShoppingCatalogSheldDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shoppingCatalogSheld = shoppingCatalogSheld;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateShoppingCatalogShelds(data: IShoppingCatalogSheld[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.shoppingCatalogShelds.push(data[i]);
      }
    }
  }
}
