import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { ShoppingCatalogSheldComponent } from './shopping-catalog-sheld.component';
import { ShoppingCatalogSheldDetailComponent } from './shopping-catalog-sheld-detail.component';
import { ShoppingCatalogSheldUpdateComponent } from './shopping-catalog-sheld-update.component';
import { ShoppingCatalogSheldDeleteDialogComponent } from './shopping-catalog-sheld-delete-dialog.component';
import { shoppingCatalogSheldRoute } from './shopping-catalog-sheld.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(shoppingCatalogSheldRoute)],
  declarations: [
    ShoppingCatalogSheldComponent,
    ShoppingCatalogSheldDetailComponent,
    ShoppingCatalogSheldUpdateComponent,
    ShoppingCatalogSheldDeleteDialogComponent
  ],
  entryComponents: [ShoppingCatalogSheldDeleteDialogComponent]
})
export class MhawebShoppingCatalogSheldModule {}
