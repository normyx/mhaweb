import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ShoppingCatalogCartService } from './shopping-catalog-cart.service';
import { ShoppingCatalogCartDeleteDialogComponent } from './shopping-catalog-cart-delete-dialog.component';

@Component({
  selector: 'mha-shopping-catalog-cart',
  templateUrl: './shopping-catalog-cart.component.html'
})
export class ShoppingCatalogCartComponent implements OnInit, OnDestroy {
  shoppingCatalogCarts: IShoppingCatalogCart[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected shoppingCatalogCartService: ShoppingCatalogCartService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.shoppingCatalogCarts = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.shoppingCatalogCartService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IShoppingCatalogCart[]>) => this.paginateShoppingCatalogCarts(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.shoppingCatalogCarts = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShoppingCatalogCarts();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShoppingCatalogCart): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShoppingCatalogCarts(): void {
    this.eventSubscriber = this.eventManager.subscribe('shoppingCatalogCartListModification', () => this.reset());
  }

  delete(shoppingCatalogCart: IShoppingCatalogCart): void {
    const modalRef = this.modalService.open(ShoppingCatalogCartDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shoppingCatalogCart = shoppingCatalogCart;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateShoppingCatalogCarts(data: IShoppingCatalogCart[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.shoppingCatalogCarts.push(data[i]);
      }
    }
  }
}
