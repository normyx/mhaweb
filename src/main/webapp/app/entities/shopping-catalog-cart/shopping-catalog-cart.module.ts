import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { ShoppingCatalogCartComponent } from './shopping-catalog-cart.component';
import { ShoppingCatalogCartDetailComponent } from './shopping-catalog-cart-detail.component';
import { ShoppingCatalogCartUpdateComponent } from './shopping-catalog-cart-update.component';
import { ShoppingCatalogCartDeleteDialogComponent } from './shopping-catalog-cart-delete-dialog.component';
import { shoppingCatalogCartRoute } from './shopping-catalog-cart.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(shoppingCatalogCartRoute)],
  declarations: [
    ShoppingCatalogCartComponent,
    ShoppingCatalogCartDetailComponent,
    ShoppingCatalogCartUpdateComponent,
    ShoppingCatalogCartDeleteDialogComponent
  ],
  entryComponents: [ShoppingCatalogCartDeleteDialogComponent]
})
export class MhawebShoppingCatalogCartModule {}
