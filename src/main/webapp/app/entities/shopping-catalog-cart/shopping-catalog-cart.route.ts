import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IShoppingCatalogCart, ShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from './shopping-catalog-cart.service';
import { ShoppingCatalogCartComponent } from './shopping-catalog-cart.component';
import { ShoppingCatalogCartDetailComponent } from './shopping-catalog-cart-detail.component';
import { ShoppingCatalogCartUpdateComponent } from './shopping-catalog-cart-update.component';

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogCartResolve implements Resolve<IShoppingCatalogCart> {
  constructor(private service: ShoppingCatalogCartService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IShoppingCatalogCart> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((shoppingCatalogCart: HttpResponse<ShoppingCatalogCart>) => {
          if (shoppingCatalogCart.body) {
            return of(shoppingCatalogCart.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ShoppingCatalogCart());
  }
}

export const shoppingCatalogCartRoute: Routes = [
  {
    path: '',
    component: ShoppingCatalogCartComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingCatalogCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShoppingCatalogCartDetailComponent,
    resolve: {
      shoppingCatalogCart: ShoppingCatalogCartResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingCatalogCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShoppingCatalogCartUpdateComponent,
    resolve: {
      shoppingCatalogCart: ShoppingCatalogCartResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingCatalogCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShoppingCatalogCartUpdateComponent,
    resolve: {
      shoppingCatalogCart: ShoppingCatalogCartResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingCatalogCart.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
