import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';

type EntityResponseType = HttpResponse<IShoppingCatalogCart>;
type EntityArrayResponseType = HttpResponse<IShoppingCatalogCart[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogCartService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-catalog-carts';

  constructor(protected http: HttpClient) {}

  create(shoppingCatalogCart: IShoppingCatalogCart): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingCatalogCart);
    return this.http
      .post<IShoppingCatalogCart>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shoppingCatalogCart: IShoppingCatalogCart): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingCatalogCart);
    return this.http
      .put<IShoppingCatalogCart>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShoppingCatalogCart>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShoppingCatalogCart[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shoppingCatalogCart: IShoppingCatalogCart): IShoppingCatalogCart {
    const copy: IShoppingCatalogCart = Object.assign({}, shoppingCatalogCart, {
      lastUpdate:
        shoppingCatalogCart.lastUpdate && shoppingCatalogCart.lastUpdate.isValid()
          ? shoppingCatalogCart.lastUpdate.format(DATE_FORMAT)
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shoppingCatalogCart: IShoppingCatalogCart) => {
        shoppingCatalogCart.lastUpdate = shoppingCatalogCart.lastUpdate ? moment(shoppingCatalogCart.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
