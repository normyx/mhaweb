import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';

@Component({
  selector: 'mha-shopping-catalog-cart-detail',
  templateUrl: './shopping-catalog-cart-detail.component.html'
})
export class ShoppingCatalogCartDetailComponent implements OnInit {
  shoppingCatalogCart: IShoppingCatalogCart | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingCatalogCart }) => (this.shoppingCatalogCart = shoppingCatalogCart));
  }

  previousState(): void {
    window.history.back();
  }
}
