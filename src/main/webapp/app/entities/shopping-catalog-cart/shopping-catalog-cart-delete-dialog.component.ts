import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from './shopping-catalog-cart.service';

@Component({
  templateUrl: './shopping-catalog-cart-delete-dialog.component.html'
})
export class ShoppingCatalogCartDeleteDialogComponent {
  shoppingCatalogCart?: IShoppingCatalogCart;

  constructor(
    protected shoppingCatalogCartService: ShoppingCatalogCartService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.shoppingCatalogCartService.delete(id).subscribe(() => {
      this.eventManager.broadcast('shoppingCatalogCartListModification');
      this.activeModal.close();
    });
  }
}
