import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShoppingCatalogCart, ShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from './shopping-catalog-cart.service';

@Component({
  selector: 'mha-shopping-catalog-cart-update',
  templateUrl: './shopping-catalog-cart-update.component.html'
})
export class ShoppingCatalogCartUpdateComponent implements OnInit {
  isSaving = false;
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]]
  });

  constructor(
    protected shoppingCatalogCartService: ShoppingCatalogCartService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingCatalogCart }) => {
      this.updateForm(shoppingCatalogCart);
    });
  }

  updateForm(shoppingCatalogCart: IShoppingCatalogCart): void {
    this.editForm.patchValue({
      id: shoppingCatalogCart.id,
      label: shoppingCatalogCart.label,
      lastUpdate: shoppingCatalogCart.lastUpdate
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shoppingCatalogCart = this.createFromForm();
    if (shoppingCatalogCart.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingCatalogCartService.update(shoppingCatalogCart));
    } else {
      this.subscribeToSaveResponse(this.shoppingCatalogCartService.create(shoppingCatalogCart));
    }
  }

  private createFromForm(): IShoppingCatalogCart {
    return {
      ...new ShoppingCatalogCart(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingCatalogCart>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
