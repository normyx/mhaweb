import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITodoTemplate } from 'app/shared/model/todo-template.model';
import { TodoTemplateService } from './todo-template.service';

@Component({
  templateUrl: './todo-template-delete-dialog.component.html'
})
export class TodoTemplateDeleteDialogComponent {
  todoTemplate?: ITodoTemplate;

  constructor(
    protected todoTemplateService: TodoTemplateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.todoTemplateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('todoTemplateListModification');
      this.activeModal.close();
    });
  }
}
