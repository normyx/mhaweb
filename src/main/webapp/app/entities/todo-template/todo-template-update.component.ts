import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITodoTemplate, TodoTemplate } from 'app/shared/model/todo-template.model';
import { TodoTemplateService } from './todo-template.service';
import { ITodoListTemplate } from 'app/shared/model/todo-list-template.model';
import { TodoListTemplateService } from 'app/entities/todo-list-template/todo-list-template.service';

@Component({
  selector: 'mha-todo-template-update',
  templateUrl: './todo-template-update.component.html'
})
export class TodoTemplateUpdateComponent implements OnInit {
  isSaving = false;
  todolisttemplates: ITodoListTemplate[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]],
    todoListTemplateId: [null, Validators.required]
  });

  constructor(
    protected todoTemplateService: TodoTemplateService,
    protected todoListTemplateService: TodoListTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ todoTemplate }) => {
      this.updateForm(todoTemplate);

      this.todoListTemplateService.query().subscribe((res: HttpResponse<ITodoListTemplate[]>) => (this.todolisttemplates = res.body || []));
    });
  }

  updateForm(todoTemplate: ITodoTemplate): void {
    this.editForm.patchValue({
      id: todoTemplate.id,
      label: todoTemplate.label,
      lastUpdate: todoTemplate.lastUpdate,
      todoListTemplateId: todoTemplate.todoListTemplateId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const todoTemplate = this.createFromForm();
    if (todoTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.todoTemplateService.update(todoTemplate));
    } else {
      this.subscribeToSaveResponse(this.todoTemplateService.create(todoTemplate));
    }
  }

  private createFromForm(): ITodoTemplate {
    return {
      ...new TodoTemplate(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      todoListTemplateId: this.editForm.get(['todoListTemplateId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITodoTemplate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITodoListTemplate): any {
    return item.id;
  }
}
