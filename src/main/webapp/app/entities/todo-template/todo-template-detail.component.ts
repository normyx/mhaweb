import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITodoTemplate } from 'app/shared/model/todo-template.model';

@Component({
  selector: 'mha-todo-template-detail',
  templateUrl: './todo-template-detail.component.html'
})
export class TodoTemplateDetailComponent implements OnInit {
  todoTemplate: ITodoTemplate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ todoTemplate }) => (this.todoTemplate = todoTemplate));
  }

  previousState(): void {
    window.history.back();
  }
}
