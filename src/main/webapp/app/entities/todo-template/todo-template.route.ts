import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITodoTemplate, TodoTemplate } from 'app/shared/model/todo-template.model';
import { TodoTemplateService } from './todo-template.service';
import { TodoTemplateComponent } from './todo-template.component';
import { TodoTemplateDetailComponent } from './todo-template-detail.component';
import { TodoTemplateUpdateComponent } from './todo-template-update.component';

@Injectable({ providedIn: 'root' })
export class TodoTemplateResolve implements Resolve<ITodoTemplate> {
  constructor(private service: TodoTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITodoTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((todoTemplate: HttpResponse<TodoTemplate>) => {
          if (todoTemplate.body) {
            return of(todoTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TodoTemplate());
  }
}

export const todoTemplateRoute: Routes = [
  {
    path: '',
    component: TodoTemplateComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TodoTemplateDetailComponent,
    resolve: {
      todoTemplate: TodoTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TodoTemplateUpdateComponent,
    resolve: {
      todoTemplate: TodoTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TodoTemplateUpdateComponent,
    resolve: {
      todoTemplate: TodoTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
