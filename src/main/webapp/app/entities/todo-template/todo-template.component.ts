import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITodoTemplate } from 'app/shared/model/todo-template.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TodoTemplateService } from './todo-template.service';
import { TodoTemplateDeleteDialogComponent } from './todo-template-delete-dialog.component';

@Component({
  selector: 'mha-todo-template',
  templateUrl: './todo-template.component.html'
})
export class TodoTemplateComponent implements OnInit, OnDestroy {
  todoTemplates: ITodoTemplate[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected todoTemplateService: TodoTemplateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.todoTemplates = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.todoTemplateService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ITodoTemplate[]>) => this.paginateTodoTemplates(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.todoTemplates = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTodoTemplates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITodoTemplate): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTodoTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('todoTemplateListModification', () => this.reset());
  }

  delete(todoTemplate: ITodoTemplate): void {
    const modalRef = this.modalService.open(TodoTemplateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.todoTemplate = todoTemplate;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTodoTemplates(data: ITodoTemplate[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.todoTemplates.push(data[i]);
      }
    }
  }
}
