import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITodoTemplate } from 'app/shared/model/todo-template.model';

type EntityResponseType = HttpResponse<ITodoTemplate>;
type EntityArrayResponseType = HttpResponse<ITodoTemplate[]>;

@Injectable({ providedIn: 'root' })
export class TodoTemplateService {
  public resourceUrl = SERVER_API_URL + 'api/todo-templates';

  constructor(protected http: HttpClient) {}

  create(todoTemplate: ITodoTemplate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(todoTemplate);
    return this.http
      .post<ITodoTemplate>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(todoTemplate: ITodoTemplate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(todoTemplate);
    return this.http
      .put<ITodoTemplate>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITodoTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITodoTemplate[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(todoTemplate: ITodoTemplate): ITodoTemplate {
    const copy: ITodoTemplate = Object.assign({}, todoTemplate, {
      lastUpdate: todoTemplate.lastUpdate && todoTemplate.lastUpdate.isValid() ? todoTemplate.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((todoTemplate: ITodoTemplate) => {
        todoTemplate.lastUpdate = todoTemplate.lastUpdate ? moment(todoTemplate.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
