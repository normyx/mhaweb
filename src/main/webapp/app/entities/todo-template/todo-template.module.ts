import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { TodoTemplateComponent } from './todo-template.component';
import { TodoTemplateDetailComponent } from './todo-template-detail.component';
import { TodoTemplateUpdateComponent } from './todo-template-update.component';
import { TodoTemplateDeleteDialogComponent } from './todo-template-delete-dialog.component';
import { todoTemplateRoute } from './todo-template.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(todoTemplateRoute)],
  declarations: [TodoTemplateComponent, TodoTemplateDetailComponent, TodoTemplateUpdateComponent, TodoTemplateDeleteDialogComponent],
  entryComponents: [TodoTemplateDeleteDialogComponent]
})
export class MhawebTodoTemplateModule {}
