import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITodo, Todo } from 'app/shared/model/todo.model';
import { TodoService } from './todo.service';
import { ITodoList } from 'app/shared/model/todo-list.model';
import { TodoListService } from 'app/entities/todo-list/todo-list.service';

@Component({
  selector: 'mha-todo-update',
  templateUrl: './todo-update.component.html'
})
export class TodoUpdateComponent implements OnInit {
  isSaving = false;
  todolists: ITodoList[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    done: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    todoListId: [null, Validators.required]
  });

  constructor(
    protected todoService: TodoService,
    protected todoListService: TodoListService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ todo }) => {
      this.updateForm(todo);

      this.todoListService.query().subscribe((res: HttpResponse<ITodoList[]>) => (this.todolists = res.body || []));
    });
  }

  updateForm(todo: ITodo): void {
    this.editForm.patchValue({
      id: todo.id,
      label: todo.label,
      done: todo.done,
      lastUpdate: todo.lastUpdate,
      todoListId: todo.todoListId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const todo = this.createFromForm();
    if (todo.id !== undefined) {
      this.subscribeToSaveResponse(this.todoService.update(todo));
    } else {
      this.subscribeToSaveResponse(this.todoService.create(todo));
    }
  }

  private createFromForm(): ITodo {
    return {
      ...new Todo(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      done: this.editForm.get(['done'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      todoListId: this.editForm.get(['todoListId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITodo>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITodoList): any {
    return item.id;
  }
}
