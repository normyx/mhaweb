import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'profil',
        loadChildren: () => import('./profil/profil.module').then(m => m.MhawebProfilModule)
      },
      {
        path: 'workspace',
        loadChildren: () => import('./workspace/workspace.module').then(m => m.MhawebWorkspaceModule)
      },
      {
        path: 'task-project',
        loadChildren: () => import('./task-project/task-project.module').then(m => m.MhawebTaskProjectModule)
      },
      {
        path: 'task',
        loadChildren: () => import('./task/task.module').then(m => m.MhawebTaskModule)
      },
      {
        path: 'profil-data',
        loadChildren: () => import('./profil-data/profil-data.module').then(m => m.MhawebProfilDataModule)
      },
      {
        path: 'wallet',
        loadChildren: () => import('./wallet/wallet.module').then(m => m.MhawebWalletModule)
      },
      {
        path: 'spent',
        loadChildren: () => import('./spent/spent.module').then(m => m.MhawebSpentModule)
      },
      {
        path: 'spent-sharing',
        loadChildren: () => import('./spent-sharing/spent-sharing.module').then(m => m.MhawebSpentSharingModule)
      },
      {
        path: 'spent-sharing-config',
        loadChildren: './spent-sharing-config/spent-sharing-config.module#MhawebSpentSharingConfigModule'
      },
      {
        path: 'spent-config',
        loadChildren: () => import('./spent-config/spent-config.module').then(m => m.MhawebSpentConfigModule)
      },
      {
        path: 'shopping-cart',
        loadChildren: () => import('./shopping-cart/shopping-cart.module').then(m => m.MhawebShoppingCartModule)
      },
      {
        path: 'todo-list',
        loadChildren: () => import('./todo-list/todo-list.module').then(m => m.MhawebTodoListModule)
      },
      {
        path: 'todo',
        loadChildren: () => import('./todo/todo.module').then(m => m.MhawebTodoModule)
      },
      {
        path: 'todo-list-template',
        loadChildren: () => import('./todo-list-template/todo-list-template.module').then(m => m.MhawebTodoListTemplateModule)
      },
      {
        path: 'todo-template',
        loadChildren: () => import('./todo-template/todo-template.module').then(m => m.MhawebTodoTemplateModule)
      },
      {
        path: 'shopping-catalog-cart',
        loadChildren: () => import('./shopping-catalog-cart/shopping-catalog-cart.module').then(m => m.MhawebShoppingCatalogCartModule)
      },
      {
        path: 'shopping-item',
        loadChildren: () => import('./shopping-item/shopping-item.module').then(m => m.MhawebShoppingItemModule)
      },
      {
        path: 'shopping-catalog-item',
        loadChildren: () => import('./shopping-catalog-item/shopping-catalog-item.module').then(m => m.MhawebShoppingCatalogItemModule)
      },
      {
        path: 'shopping-catalog-sheld',
        loadChildren: () => import('./shopping-catalog-sheld/shopping-catalog-sheld.module').then(m => m.MhawebShoppingCatalogSheldModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class MhawebEntityModule {}
