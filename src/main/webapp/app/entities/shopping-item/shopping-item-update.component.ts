import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShoppingItem, ShoppingItem } from 'app/shared/model/shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';
import { IShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';
import { ShoppingCatalogSheldService } from 'app/entities/shopping-catalog-sheld/shopping-catalog-sheld.service';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ShoppingCartService } from 'app/entities/shopping-cart/shopping-cart.service';

type SelectableEntity = IShoppingCatalogSheld | IShoppingCart;

@Component({
  selector: 'mha-shopping-item-update',
  templateUrl: './shopping-item-update.component.html'
})
export class ShoppingItemUpdateComponent implements OnInit {
  isSaving = false;
  shoppingcatalogshelds: IShoppingCatalogSheld[] = [];
  shoppingcarts: IShoppingCart[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(80)]],
    lastUpdate: [null, [Validators.required]],
    checked: [null, [Validators.required]],
    quantity: [null, [Validators.min(0)]],
    unit: [],
    shoppingCatalogSheldId: [null, Validators.required],
    shoppingCartId: [null, Validators.required]
  });

  constructor(
    protected shoppingItemService: ShoppingItemService,
    protected shoppingCatalogSheldService: ShoppingCatalogSheldService,
    protected shoppingCartService: ShoppingCartService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingItem }) => {
      this.updateForm(shoppingItem);

      this.shoppingCatalogSheldService
        .query()
        .subscribe((res: HttpResponse<IShoppingCatalogSheld[]>) => (this.shoppingcatalogshelds = res.body || []));

      this.shoppingCartService.query().subscribe((res: HttpResponse<IShoppingCart[]>) => (this.shoppingcarts = res.body || []));
    });
  }

  updateForm(shoppingItem: IShoppingItem): void {
    this.editForm.patchValue({
      id: shoppingItem.id,
      label: shoppingItem.label,
      lastUpdate: shoppingItem.lastUpdate,
      checked: shoppingItem.checked,
      quantity: shoppingItem.quantity,
      unit: shoppingItem.unit,
      shoppingCatalogSheldId: shoppingItem.shoppingCatalogSheldId,
      shoppingCartId: shoppingItem.shoppingCartId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shoppingItem = this.createFromForm();
    if (shoppingItem.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingItemService.update(shoppingItem));
    } else {
      this.subscribeToSaveResponse(this.shoppingItemService.create(shoppingItem));
    }
  }

  private createFromForm(): IShoppingItem {
    return {
      ...new ShoppingItem(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      checked: this.editForm.get(['checked'])!.value,
      quantity: this.editForm.get(['quantity'])!.value,
      unit: this.editForm.get(['unit'])!.value,
      shoppingCatalogSheldId: this.editForm.get(['shoppingCatalogSheldId'])!.value,
      shoppingCartId: this.editForm.get(['shoppingCartId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingItem>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
