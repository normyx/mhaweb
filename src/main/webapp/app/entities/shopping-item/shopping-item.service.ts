import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShoppingItem } from 'app/shared/model/shopping-item.model';

type EntityResponseType = HttpResponse<IShoppingItem>;
type EntityArrayResponseType = HttpResponse<IShoppingItem[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingItemService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-items';

  constructor(protected http: HttpClient) {}

  create(shoppingItem: IShoppingItem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingItem);
    return this.http
      .post<IShoppingItem>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shoppingItem: IShoppingItem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingItem);
    return this.http
      .put<IShoppingItem>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShoppingItem>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShoppingItem[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shoppingItem: IShoppingItem): IShoppingItem {
    const copy: IShoppingItem = Object.assign({}, shoppingItem, {
      lastUpdate: shoppingItem.lastUpdate && shoppingItem.lastUpdate.isValid() ? shoppingItem.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shoppingItem: IShoppingItem) => {
        shoppingItem.lastUpdate = shoppingItem.lastUpdate ? moment(shoppingItem.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
