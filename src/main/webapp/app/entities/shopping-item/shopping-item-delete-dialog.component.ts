import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShoppingItem } from 'app/shared/model/shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';

@Component({
  templateUrl: './shopping-item-delete-dialog.component.html'
})
export class ShoppingItemDeleteDialogComponent {
  shoppingItem?: IShoppingItem;

  constructor(
    protected shoppingItemService: ShoppingItemService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.shoppingItemService.delete(id).subscribe(() => {
      this.eventManager.broadcast('shoppingItemListModification');
      this.activeModal.close();
    });
  }
}
