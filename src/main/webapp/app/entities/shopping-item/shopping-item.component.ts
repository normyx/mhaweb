import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShoppingItem } from 'app/shared/model/shopping-item.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ShoppingItemService } from './shopping-item.service';
import { ShoppingItemDeleteDialogComponent } from './shopping-item-delete-dialog.component';

@Component({
  selector: 'mha-shopping-item',
  templateUrl: './shopping-item.component.html'
})
export class ShoppingItemComponent implements OnInit, OnDestroy {
  shoppingItems: IShoppingItem[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected shoppingItemService: ShoppingItemService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.shoppingItems = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.shoppingItemService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IShoppingItem[]>) => this.paginateShoppingItems(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.shoppingItems = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShoppingItems();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShoppingItem): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShoppingItems(): void {
    this.eventSubscriber = this.eventManager.subscribe('shoppingItemListModification', () => this.reset());
  }

  delete(shoppingItem: IShoppingItem): void {
    const modalRef = this.modalService.open(ShoppingItemDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shoppingItem = shoppingItem;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateShoppingItems(data: IShoppingItem[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.shoppingItems.push(data[i]);
      }
    }
  }
}
