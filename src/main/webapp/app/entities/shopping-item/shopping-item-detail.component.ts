import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShoppingItem } from 'app/shared/model/shopping-item.model';

@Component({
  selector: 'mha-shopping-item-detail',
  templateUrl: './shopping-item-detail.component.html'
})
export class ShoppingItemDetailComponent implements OnInit {
  shoppingItem: IShoppingItem | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingItem }) => (this.shoppingItem = shoppingItem));
  }

  previousState(): void {
    window.history.back();
  }
}
