import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IShoppingItem, ShoppingItem } from 'app/shared/model/shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';
import { ShoppingItemComponent } from './shopping-item.component';
import { ShoppingItemDetailComponent } from './shopping-item-detail.component';
import { ShoppingItemUpdateComponent } from './shopping-item-update.component';

@Injectable({ providedIn: 'root' })
export class ShoppingItemResolve implements Resolve<IShoppingItem> {
  constructor(private service: ShoppingItemService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IShoppingItem> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((shoppingItem: HttpResponse<ShoppingItem>) => {
          if (shoppingItem.body) {
            return of(shoppingItem.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ShoppingItem());
  }
}

export const shoppingItemRoute: Routes = [
  {
    path: '',
    component: ShoppingItemComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShoppingItemDetailComponent,
    resolve: {
      shoppingItem: ShoppingItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShoppingItemUpdateComponent,
    resolve: {
      shoppingItem: ShoppingItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShoppingItemUpdateComponent,
    resolve: {
      shoppingItem: ShoppingItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
