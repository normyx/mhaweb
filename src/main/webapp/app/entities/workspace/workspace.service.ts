import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWorkspace } from 'app/shared/model/workspace.model';

type EntityResponseType = HttpResponse<IWorkspace>;
type EntityArrayResponseType = HttpResponse<IWorkspace[]>;

@Injectable({ providedIn: 'root' })
export class WorkspaceService {
  public resourceUrl = SERVER_API_URL + 'api/workspaces';

  constructor(protected http: HttpClient) {}

  create(workspace: IWorkspace): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workspace);
    return this.http
      .post<IWorkspace>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(workspace: IWorkspace): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workspace);
    return this.http
      .put<IWorkspace>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWorkspace>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWorkspace[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(workspace: IWorkspace): IWorkspace {
    const copy: IWorkspace = Object.assign({}, workspace, {
      lastUpdate: workspace.lastUpdate && workspace.lastUpdate.isValid() ? workspace.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((workspace: IWorkspace) => {
        workspace.lastUpdate = workspace.lastUpdate ? moment(workspace.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
