import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { WorkspaceComponent } from './workspace.component';
import { WorkspaceDetailComponent } from './workspace-detail.component';
import { WorkspaceUpdateComponent } from './workspace-update.component';
import { WorkspaceDeleteDialogComponent } from './workspace-delete-dialog.component';
import { workspaceRoute } from './workspace.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(workspaceRoute)],
  declarations: [WorkspaceComponent, WorkspaceDetailComponent, WorkspaceUpdateComponent, WorkspaceDeleteDialogComponent],
  entryComponents: [WorkspaceDeleteDialogComponent]
})
export class MhawebWorkspaceModule {}
