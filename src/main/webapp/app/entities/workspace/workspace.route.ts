import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IWorkspace, Workspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from './workspace.service';
import { WorkspaceComponent } from './workspace.component';
import { WorkspaceDetailComponent } from './workspace-detail.component';
import { WorkspaceUpdateComponent } from './workspace-update.component';

@Injectable({ providedIn: 'root' })
export class WorkspaceResolve implements Resolve<IWorkspace> {
  constructor(private service: WorkspaceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWorkspace> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((workspace: HttpResponse<Workspace>) => {
          if (workspace.body) {
            return of(workspace.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Workspace());
  }
}

export const workspaceRoute: Routes = [
  {
    path: '',
    component: WorkspaceComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.workspace.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WorkspaceDetailComponent,
    resolve: {
      workspace: WorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.workspace.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WorkspaceUpdateComponent,
    resolve: {
      workspace: WorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.workspace.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WorkspaceUpdateComponent,
    resolve: {
      workspace: WorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.workspace.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
