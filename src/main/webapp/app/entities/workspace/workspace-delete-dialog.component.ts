import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from './workspace.service';

@Component({
  templateUrl: './workspace-delete-dialog.component.html'
})
export class WorkspaceDeleteDialogComponent {
  workspace?: IWorkspace;

  constructor(protected workspaceService: WorkspaceService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.workspaceService.delete(id).subscribe(() => {
      this.eventManager.broadcast('workspaceListModification');
      this.activeModal.close();
    });
  }
}
