import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IWorkspace, Workspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from './workspace.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';

@Component({
  selector: 'mha-workspace-update',
  templateUrl: './workspace-update.component.html'
})
export class WorkspaceUpdateComponent implements OnInit {
  isSaving = false;
  profils: IProfil[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]],
    owners: []
  });

  constructor(
    protected workspaceService: WorkspaceService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ workspace }) => {
      this.updateForm(workspace);

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));
    });
  }

  updateForm(workspace: IWorkspace): void {
    this.editForm.patchValue({
      id: workspace.id,
      label: workspace.label,
      lastUpdate: workspace.lastUpdate,
      owners: workspace.owners
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const workspace = this.createFromForm();
    if (workspace.id !== undefined) {
      this.subscribeToSaveResponse(this.workspaceService.update(workspace));
    } else {
      this.subscribeToSaveResponse(this.workspaceService.create(workspace));
    }
  }

  private createFromForm(): IWorkspace {
    return {
      ...new Workspace(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      owners: this.editForm.get(['owners'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkspace>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProfil): any {
    return item.id;
  }

  getSelected(selectedVals: IProfil[], option: IProfil): IProfil {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
