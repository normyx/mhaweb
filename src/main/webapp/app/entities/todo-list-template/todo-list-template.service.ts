import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITodoListTemplate } from 'app/shared/model/todo-list-template.model';

type EntityResponseType = HttpResponse<ITodoListTemplate>;
type EntityArrayResponseType = HttpResponse<ITodoListTemplate[]>;

@Injectable({ providedIn: 'root' })
export class TodoListTemplateService {
  public resourceUrl = SERVER_API_URL + 'api/todo-list-templates';

  constructor(protected http: HttpClient) {}

  create(todoListTemplate: ITodoListTemplate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(todoListTemplate);
    return this.http
      .post<ITodoListTemplate>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(todoListTemplate: ITodoListTemplate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(todoListTemplate);
    return this.http
      .put<ITodoListTemplate>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITodoListTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITodoListTemplate[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(todoListTemplate: ITodoListTemplate): ITodoListTemplate {
    const copy: ITodoListTemplate = Object.assign({}, todoListTemplate, {
      lastUpdate:
        todoListTemplate.lastUpdate && todoListTemplate.lastUpdate.isValid() ? todoListTemplate.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((todoListTemplate: ITodoListTemplate) => {
        todoListTemplate.lastUpdate = todoListTemplate.lastUpdate ? moment(todoListTemplate.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
