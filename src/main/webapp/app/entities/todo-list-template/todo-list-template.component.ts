import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITodoListTemplate } from 'app/shared/model/todo-list-template.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TodoListTemplateService } from './todo-list-template.service';
import { TodoListTemplateDeleteDialogComponent } from './todo-list-template-delete-dialog.component';

@Component({
  selector: 'mha-todo-list-template',
  templateUrl: './todo-list-template.component.html'
})
export class TodoListTemplateComponent implements OnInit, OnDestroy {
  todoListTemplates: ITodoListTemplate[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected todoListTemplateService: TodoListTemplateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.todoListTemplates = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.todoListTemplateService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ITodoListTemplate[]>) => this.paginateTodoListTemplates(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.todoListTemplates = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTodoListTemplates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITodoListTemplate): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTodoListTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('todoListTemplateListModification', () => this.reset());
  }

  delete(todoListTemplate: ITodoListTemplate): void {
    const modalRef = this.modalService.open(TodoListTemplateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.todoListTemplate = todoListTemplate;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTodoListTemplates(data: ITodoListTemplate[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.todoListTemplates.push(data[i]);
      }
    }
  }
}
