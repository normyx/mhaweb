import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITodoListTemplate } from 'app/shared/model/todo-list-template.model';
import { TodoListTemplateService } from './todo-list-template.service';

@Component({
  templateUrl: './todo-list-template-delete-dialog.component.html'
})
export class TodoListTemplateDeleteDialogComponent {
  todoListTemplate?: ITodoListTemplate;

  constructor(
    protected todoListTemplateService: TodoListTemplateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.todoListTemplateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('todoListTemplateListModification');
      this.activeModal.close();
    });
  }
}
