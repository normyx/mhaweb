import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITodoListTemplate } from 'app/shared/model/todo-list-template.model';

@Component({
  selector: 'mha-todo-list-template-detail',
  templateUrl: './todo-list-template-detail.component.html'
})
export class TodoListTemplateDetailComponent implements OnInit {
  todoListTemplate: ITodoListTemplate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ todoListTemplate }) => (this.todoListTemplate = todoListTemplate));
  }

  previousState(): void {
    window.history.back();
  }
}
