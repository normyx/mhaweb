import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITodoListTemplate, TodoListTemplate } from 'app/shared/model/todo-list-template.model';
import { TodoListTemplateService } from './todo-list-template.service';
import { TodoListTemplateComponent } from './todo-list-template.component';
import { TodoListTemplateDetailComponent } from './todo-list-template-detail.component';
import { TodoListTemplateUpdateComponent } from './todo-list-template-update.component';

@Injectable({ providedIn: 'root' })
export class TodoListTemplateResolve implements Resolve<ITodoListTemplate> {
  constructor(private service: TodoListTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITodoListTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((todoListTemplate: HttpResponse<TodoListTemplate>) => {
          if (todoListTemplate.body) {
            return of(todoListTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TodoListTemplate());
  }
}

export const todoListTemplateRoute: Routes = [
  {
    path: '',
    component: TodoListTemplateComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoListTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TodoListTemplateDetailComponent,
    resolve: {
      todoListTemplate: TodoListTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoListTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TodoListTemplateUpdateComponent,
    resolve: {
      todoListTemplate: TodoListTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoListTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TodoListTemplateUpdateComponent,
    resolve: {
      todoListTemplate: TodoListTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoListTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
