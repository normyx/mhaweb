import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { TodoListTemplateComponent } from './todo-list-template.component';
import { TodoListTemplateDetailComponent } from './todo-list-template-detail.component';
import { TodoListTemplateUpdateComponent } from './todo-list-template-update.component';
import { TodoListTemplateDeleteDialogComponent } from './todo-list-template-delete-dialog.component';
import { todoListTemplateRoute } from './todo-list-template.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(todoListTemplateRoute)],
  declarations: [
    TodoListTemplateComponent,
    TodoListTemplateDetailComponent,
    TodoListTemplateUpdateComponent,
    TodoListTemplateDeleteDialogComponent
  ],
  entryComponents: [TodoListTemplateDeleteDialogComponent]
})
export class MhawebTodoListTemplateModule {}
