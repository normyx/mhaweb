import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITodoListTemplate, TodoListTemplate } from 'app/shared/model/todo-list-template.model';
import { TodoListTemplateService } from './todo-list-template.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from 'app/entities/workspace/workspace.service';

type SelectableEntity = IProfil | IWorkspace;

@Component({
  selector: 'mha-todo-list-template-update',
  templateUrl: './todo-list-template-update.component.html'
})
export class TodoListTemplateUpdateComponent implements OnInit {
  isSaving = false;
  profils: IProfil[] = [];
  workspaces: IWorkspace[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]],
    owners: [],
    workspaceId: [null, Validators.required]
  });

  constructor(
    protected todoListTemplateService: TodoListTemplateService,
    protected profilService: ProfilService,
    protected workspaceService: WorkspaceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ todoListTemplate }) => {
      this.updateForm(todoListTemplate);

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));

      this.workspaceService.query().subscribe((res: HttpResponse<IWorkspace[]>) => (this.workspaces = res.body || []));
    });
  }

  updateForm(todoListTemplate: ITodoListTemplate): void {
    this.editForm.patchValue({
      id: todoListTemplate.id,
      label: todoListTemplate.label,
      lastUpdate: todoListTemplate.lastUpdate,
      owners: todoListTemplate.owners,
      workspaceId: todoListTemplate.workspaceId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const todoListTemplate = this.createFromForm();
    if (todoListTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.todoListTemplateService.update(todoListTemplate));
    } else {
      this.subscribeToSaveResponse(this.todoListTemplateService.create(todoListTemplate));
    }
  }

  private createFromForm(): ITodoListTemplate {
    return {
      ...new TodoListTemplate(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      owners: this.editForm.get(['owners'])!.value,
      workspaceId: this.editForm.get(['workspaceId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITodoListTemplate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IProfil[], option: IProfil): IProfil {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
