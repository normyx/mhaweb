import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITodoList, TodoList } from 'app/shared/model/todo-list.model';
import { TodoListService } from './todo-list.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from 'app/entities/workspace/workspace.service';

type SelectableEntity = IProfil | IWorkspace;

@Component({
  selector: 'mha-todo-list-update',
  templateUrl: './todo-list-update.component.html'
})
export class TodoListUpdateComponent implements OnInit {
  isSaving = false;
  profils: IProfil[] = [];
  workspaces: IWorkspace[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]],
    owners: [],
    workspaceId: [null, Validators.required]
  });

  constructor(
    protected todoListService: TodoListService,
    protected profilService: ProfilService,
    protected workspaceService: WorkspaceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ todoList }) => {
      this.updateForm(todoList);

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));

      this.workspaceService.query().subscribe((res: HttpResponse<IWorkspace[]>) => (this.workspaces = res.body || []));
    });
  }

  updateForm(todoList: ITodoList): void {
    this.editForm.patchValue({
      id: todoList.id,
      label: todoList.label,
      lastUpdate: todoList.lastUpdate,
      owners: todoList.owners,
      workspaceId: todoList.workspaceId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const todoList = this.createFromForm();
    if (todoList.id !== undefined) {
      this.subscribeToSaveResponse(this.todoListService.update(todoList));
    } else {
      this.subscribeToSaveResponse(this.todoListService.create(todoList));
    }
  }

  private createFromForm(): ITodoList {
    return {
      ...new TodoList(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      owners: this.editForm.get(['owners'])!.value,
      workspaceId: this.editForm.get(['workspaceId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITodoList>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IProfil[], option: IProfil): IProfil {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
