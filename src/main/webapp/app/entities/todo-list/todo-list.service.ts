import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITodoList } from 'app/shared/model/todo-list.model';

type EntityResponseType = HttpResponse<ITodoList>;
type EntityArrayResponseType = HttpResponse<ITodoList[]>;

@Injectable({ providedIn: 'root' })
export class TodoListService {
  public resourceUrl = SERVER_API_URL + 'api/todo-lists';

  constructor(protected http: HttpClient) {}

  create(todoList: ITodoList): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(todoList);
    return this.http
      .post<ITodoList>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(todoList: ITodoList): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(todoList);
    return this.http
      .put<ITodoList>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITodoList>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITodoList[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(todoList: ITodoList): ITodoList {
    const copy: ITodoList = Object.assign({}, todoList, {
      lastUpdate: todoList.lastUpdate && todoList.lastUpdate.isValid() ? todoList.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((todoList: ITodoList) => {
        todoList.lastUpdate = todoList.lastUpdate ? moment(todoList.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
