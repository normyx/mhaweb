import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { TodoListComponent } from './todo-list.component';
import { TodoListDetailComponent } from './todo-list-detail.component';
import { TodoListUpdateComponent } from './todo-list-update.component';
import { TodoListDeleteDialogComponent } from './todo-list-delete-dialog.component';
import { todoListRoute } from './todo-list.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(todoListRoute)],
  declarations: [TodoListComponent, TodoListDetailComponent, TodoListUpdateComponent, TodoListDeleteDialogComponent],
  entryComponents: [TodoListDeleteDialogComponent]
})
export class MhawebTodoListModule {}
