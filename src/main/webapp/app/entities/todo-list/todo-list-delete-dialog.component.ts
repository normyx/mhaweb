import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITodoList } from 'app/shared/model/todo-list.model';
import { TodoListService } from './todo-list.service';

@Component({
  templateUrl: './todo-list-delete-dialog.component.html'
})
export class TodoListDeleteDialogComponent {
  todoList?: ITodoList;

  constructor(protected todoListService: TodoListService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.todoListService.delete(id).subscribe(() => {
      this.eventManager.broadcast('todoListListModification');
      this.activeModal.close();
    });
  }
}
