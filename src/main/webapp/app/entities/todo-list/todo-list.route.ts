import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITodoList, TodoList } from 'app/shared/model/todo-list.model';
import { TodoListService } from './todo-list.service';
import { TodoListComponent } from './todo-list.component';
import { TodoListDetailComponent } from './todo-list-detail.component';
import { TodoListUpdateComponent } from './todo-list-update.component';

@Injectable({ providedIn: 'root' })
export class TodoListResolve implements Resolve<ITodoList> {
  constructor(private service: TodoListService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITodoList> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((todoList: HttpResponse<TodoList>) => {
          if (todoList.body) {
            return of(todoList.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TodoList());
  }
}

export const todoListRoute: Routes = [
  {
    path: '',
    component: TodoListComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoList.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TodoListDetailComponent,
    resolve: {
      todoList: TodoListResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoList.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TodoListUpdateComponent,
    resolve: {
      todoList: TodoListResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoList.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TodoListUpdateComponent,
    resolve: {
      todoList: TodoListResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.todoList.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
