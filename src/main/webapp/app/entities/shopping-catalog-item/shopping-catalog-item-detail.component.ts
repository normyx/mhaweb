import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';

@Component({
  selector: 'mha-shopping-catalog-item-detail',
  templateUrl: './shopping-catalog-item-detail.component.html'
})
export class ShoppingCatalogItemDetailComponent implements OnInit {
  shoppingCatalogItem: IShoppingCatalogItem | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingCatalogItem }) => (this.shoppingCatalogItem = shoppingCatalogItem));
  }

  previousState(): void {
    window.history.back();
  }
}
