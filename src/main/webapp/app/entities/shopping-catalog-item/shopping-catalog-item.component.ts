import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ShoppingCatalogItemService } from './shopping-catalog-item.service';
import { ShoppingCatalogItemDeleteDialogComponent } from './shopping-catalog-item-delete-dialog.component';

@Component({
  selector: 'mha-shopping-catalog-item',
  templateUrl: './shopping-catalog-item.component.html'
})
export class ShoppingCatalogItemComponent implements OnInit, OnDestroy {
  shoppingCatalogItems: IShoppingCatalogItem[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected shoppingCatalogItemService: ShoppingCatalogItemService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.shoppingCatalogItems = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.shoppingCatalogItemService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IShoppingCatalogItem[]>) => this.paginateShoppingCatalogItems(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.shoppingCatalogItems = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShoppingCatalogItems();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShoppingCatalogItem): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShoppingCatalogItems(): void {
    this.eventSubscriber = this.eventManager.subscribe('shoppingCatalogItemListModification', () => this.reset());
  }

  delete(shoppingCatalogItem: IShoppingCatalogItem): void {
    const modalRef = this.modalService.open(ShoppingCatalogItemDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shoppingCatalogItem = shoppingCatalogItem;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateShoppingCatalogItems(data: IShoppingCatalogItem[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.shoppingCatalogItems.push(data[i]);
      }
    }
  }
}
