import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { ShoppingCatalogItemComponent } from './shopping-catalog-item.component';
import { ShoppingCatalogItemDetailComponent } from './shopping-catalog-item-detail.component';
import { ShoppingCatalogItemUpdateComponent } from './shopping-catalog-item-update.component';
import { ShoppingCatalogItemDeleteDialogComponent } from './shopping-catalog-item-delete-dialog.component';
import { shoppingCatalogItemRoute } from './shopping-catalog-item.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(shoppingCatalogItemRoute)],
  declarations: [
    ShoppingCatalogItemComponent,
    ShoppingCatalogItemDetailComponent,
    ShoppingCatalogItemUpdateComponent,
    ShoppingCatalogItemDeleteDialogComponent
  ],
  entryComponents: [ShoppingCatalogItemDeleteDialogComponent]
})
export class MhawebShoppingCatalogItemModule {}
