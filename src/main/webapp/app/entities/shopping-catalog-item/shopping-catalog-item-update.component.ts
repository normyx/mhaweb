import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShoppingCatalogItem, ShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';
import { ShoppingCatalogItemService } from './shopping-catalog-item.service';
import { IShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';
import { ShoppingCatalogSheldService } from 'app/entities/shopping-catalog-sheld/shopping-catalog-sheld.service';

@Component({
  selector: 'mha-shopping-catalog-item-update',
  templateUrl: './shopping-catalog-item-update.component.html'
})
export class ShoppingCatalogItemUpdateComponent implements OnInit {
  isSaving = false;
  shoppingcatalogshelds: IShoppingCatalogSheld[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(80)]],
    lastUpdate: [null, [Validators.required]],
    shoppingCatalogSheldId: [null, Validators.required]
  });

  constructor(
    protected shoppingCatalogItemService: ShoppingCatalogItemService,
    protected shoppingCatalogSheldService: ShoppingCatalogSheldService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingCatalogItem }) => {
      this.updateForm(shoppingCatalogItem);

      this.shoppingCatalogSheldService
        .query()
        .subscribe((res: HttpResponse<IShoppingCatalogSheld[]>) => (this.shoppingcatalogshelds = res.body || []));
    });
  }

  updateForm(shoppingCatalogItem: IShoppingCatalogItem): void {
    this.editForm.patchValue({
      id: shoppingCatalogItem.id,
      label: shoppingCatalogItem.label,
      lastUpdate: shoppingCatalogItem.lastUpdate,
      shoppingCatalogSheldId: shoppingCatalogItem.shoppingCatalogSheldId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shoppingCatalogItem = this.createFromForm();
    if (shoppingCatalogItem.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingCatalogItemService.update(shoppingCatalogItem));
    } else {
      this.subscribeToSaveResponse(this.shoppingCatalogItemService.create(shoppingCatalogItem));
    }
  }

  private createFromForm(): IShoppingCatalogItem {
    return {
      ...new ShoppingCatalogItem(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      shoppingCatalogSheldId: this.editForm.get(['shoppingCatalogSheldId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingCatalogItem>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IShoppingCatalogSheld): any {
    return item.id;
  }
}
