import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';
import { ShoppingCatalogItemService } from './shopping-catalog-item.service';

@Component({
  templateUrl: './shopping-catalog-item-delete-dialog.component.html'
})
export class ShoppingCatalogItemDeleteDialogComponent {
  shoppingCatalogItem?: IShoppingCatalogItem;

  constructor(
    protected shoppingCatalogItemService: ShoppingCatalogItemService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.shoppingCatalogItemService.delete(id).subscribe(() => {
      this.eventManager.broadcast('shoppingCatalogItemListModification');
      this.activeModal.close();
    });
  }
}
