import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';

type EntityResponseType = HttpResponse<IShoppingCatalogItem>;
type EntityArrayResponseType = HttpResponse<IShoppingCatalogItem[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogItemService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-catalog-items';

  constructor(protected http: HttpClient) {}

  create(shoppingCatalogItem: IShoppingCatalogItem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingCatalogItem);
    return this.http
      .post<IShoppingCatalogItem>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shoppingCatalogItem: IShoppingCatalogItem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingCatalogItem);
    return this.http
      .put<IShoppingCatalogItem>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShoppingCatalogItem>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShoppingCatalogItem[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shoppingCatalogItem: IShoppingCatalogItem): IShoppingCatalogItem {
    const copy: IShoppingCatalogItem = Object.assign({}, shoppingCatalogItem, {
      lastUpdate:
        shoppingCatalogItem.lastUpdate && shoppingCatalogItem.lastUpdate.isValid()
          ? shoppingCatalogItem.lastUpdate.format(DATE_FORMAT)
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shoppingCatalogItem: IShoppingCatalogItem) => {
        shoppingCatalogItem.lastUpdate = shoppingCatalogItem.lastUpdate ? moment(shoppingCatalogItem.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
