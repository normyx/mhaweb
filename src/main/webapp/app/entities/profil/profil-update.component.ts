import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IProfil, Profil } from 'app/shared/model/profil.model';
import { ProfilService } from './profil.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IProfilData } from 'app/shared/model/profil-data.model';
import { ProfilDataService } from 'app/entities/profil-data/profil-data.service';

type SelectableEntity = IUser | IProfilData;

@Component({
  selector: 'mha-profil-update',
  templateUrl: './profil-update.component.html'
})
export class ProfilUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  profildata: IProfilData[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    displayName: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    userId: [],
    profilDataId: []
  });

  constructor(
    protected profilService: ProfilService,
    protected userService: UserService,
    protected profilDataService: ProfilDataService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ profil }) => {
      this.updateForm(profil);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.profilDataService
        .query({ 'profilId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IProfilData[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IProfilData[]) => {
          if (!profil.profilDataId) {
            this.profildata = resBody;
          } else {
            this.profilDataService
              .find(profil.profilDataId)
              .pipe(
                map((subRes: HttpResponse<IProfilData>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IProfilData[]) => (this.profildata = concatRes));
          }
        });
    });
  }

  updateForm(profil: IProfil): void {
    this.editForm.patchValue({
      id: profil.id,
      displayName: profil.displayName,
      lastUpdate: profil.lastUpdate,
      userId: profil.userId,
      profilDataId: profil.profilDataId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const profil = this.createFromForm();
    if (profil.id !== undefined) {
      this.subscribeToSaveResponse(this.profilService.update(profil));
    } else {
      this.subscribeToSaveResponse(this.profilService.create(profil));
    }
  }

  private createFromForm(): IProfil {
    return {
      ...new Profil(),
      id: this.editForm.get(['id'])!.value,
      displayName: this.editForm.get(['displayName'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      profilDataId: this.editForm.get(['profilDataId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfil>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
