import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITask, Task } from 'app/shared/model/task.model';
import { TaskService } from './task.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from 'app/entities/task-project/task-project.service';

type SelectableEntity = IProfil | ITaskProject;

@Component({
  selector: 'mha-task-update',
  templateUrl: './task-update.component.html'
})
export class TaskUpdateComponent implements OnInit {
  isSaving = false;
  profils: IProfil[] = [];
  taskprojects: ITaskProject[] = [];
  dueDateDp: any;
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    description: [null, [Validators.maxLength(4000)]],
    done: [null, [Validators.required]],
    dueDate: [],
    lastUpdate: [null, [Validators.required]],
    owners: [],
    taskProjectId: []
  });

  constructor(
    protected taskService: TaskService,
    protected profilService: ProfilService,
    protected taskProjectService: TaskProjectService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ task }) => {
      this.updateForm(task);

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));

      this.taskProjectService.query().subscribe((res: HttpResponse<ITaskProject[]>) => (this.taskprojects = res.body || []));
    });
  }

  updateForm(task: ITask): void {
    this.editForm.patchValue({
      id: task.id,
      label: task.label,
      description: task.description,
      done: task.done,
      dueDate: task.dueDate,
      lastUpdate: task.lastUpdate,
      owners: task.owners,
      taskProjectId: task.taskProjectId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const task = this.createFromForm();
    if (task.id !== undefined) {
      this.subscribeToSaveResponse(this.taskService.update(task));
    } else {
      this.subscribeToSaveResponse(this.taskService.create(task));
    }
  }

  private createFromForm(): ITask {
    return {
      ...new Task(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      description: this.editForm.get(['description'])!.value,
      done: this.editForm.get(['done'])!.value,
      dueDate: this.editForm.get(['dueDate'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      owners: this.editForm.get(['owners'])!.value,
      taskProjectId: this.editForm.get(['taskProjectId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITask>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IProfil[], option: IProfil): IProfil {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
