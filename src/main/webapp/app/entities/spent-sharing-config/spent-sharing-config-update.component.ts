import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISpentSharingConfig, SpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';
import { ISpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from 'app/entities/spent-config/spent-config.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';

type SelectableEntity = ISpentConfig | IProfil;

@Component({
  selector: 'mha-spent-sharing-config-update',
  templateUrl: './spent-sharing-config-update.component.html'
})
export class SpentSharingConfigUpdateComponent implements OnInit {
  isSaving = false;
  spentconfigs: ISpentConfig[] = [];
  profils: IProfil[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    share: [null, [Validators.required, Validators.min(0)]],
    lastUpdate: [null, [Validators.required]],
    spentConfigId: [null, Validators.required],
    profilId: [null, Validators.required]
  });

  constructor(
    protected spentSharingConfigService: SpentSharingConfigService,
    protected spentConfigService: SpentConfigService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spentSharingConfig }) => {
      this.updateForm(spentSharingConfig);

      this.spentConfigService.query().subscribe((res: HttpResponse<ISpentConfig[]>) => (this.spentconfigs = res.body || []));

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));
    });
  }

  updateForm(spentSharingConfig: ISpentSharingConfig): void {
    this.editForm.patchValue({
      id: spentSharingConfig.id,
      share: spentSharingConfig.share,
      lastUpdate: spentSharingConfig.lastUpdate,
      spentConfigId: spentSharingConfig.spentConfigId,
      profilId: spentSharingConfig.profilId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const spentSharingConfig = this.createFromForm();
    if (spentSharingConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.spentSharingConfigService.update(spentSharingConfig));
    } else {
      this.subscribeToSaveResponse(this.spentSharingConfigService.create(spentSharingConfig));
    }
  }

  private createFromForm(): ISpentSharingConfig {
    return {
      ...new SpentSharingConfig(),
      id: this.editForm.get(['id'])!.value,
      share: this.editForm.get(['share'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      spentConfigId: this.editForm.get(['spentConfigId'])!.value,
      profilId: this.editForm.get(['profilId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpentSharingConfig>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
