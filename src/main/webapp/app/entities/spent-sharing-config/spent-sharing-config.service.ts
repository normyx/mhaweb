import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

type EntityResponseType = HttpResponse<ISpentSharingConfig>;
type EntityArrayResponseType = HttpResponse<ISpentSharingConfig[]>;

@Injectable({ providedIn: 'root' })
export class SpentSharingConfigService {
  public resourceUrl = SERVER_API_URL + 'api/spent-sharing-configs';

  constructor(protected http: HttpClient) {}

  create(spentSharingConfig: ISpentSharingConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentSharingConfig);
    return this.http
      .post<ISpentSharingConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(spentSharingConfig: ISpentSharingConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentSharingConfig);
    return this.http
      .put<ISpentSharingConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISpentSharingConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISpentSharingConfig[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(spentSharingConfig: ISpentSharingConfig): ISpentSharingConfig {
    const copy: ISpentSharingConfig = Object.assign({}, spentSharingConfig, {
      lastUpdate:
        spentSharingConfig.lastUpdate && spentSharingConfig.lastUpdate.isValid()
          ? spentSharingConfig.lastUpdate.format(DATE_FORMAT)
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((spentSharingConfig: ISpentSharingConfig) => {
        spentSharingConfig.lastUpdate = spentSharingConfig.lastUpdate ? moment(spentSharingConfig.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
