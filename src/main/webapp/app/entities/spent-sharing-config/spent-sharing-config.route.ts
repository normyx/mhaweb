import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISpentSharingConfig, SpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';
import { SpentSharingConfigComponent } from './spent-sharing-config.component';
import { SpentSharingConfigDetailComponent } from './spent-sharing-config-detail.component';
import { SpentSharingConfigUpdateComponent } from './spent-sharing-config-update.component';

@Injectable({ providedIn: 'root' })
export class SpentSharingConfigResolve implements Resolve<ISpentSharingConfig> {
  constructor(private service: SpentSharingConfigService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISpentSharingConfig> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((spentSharingConfig: HttpResponse<SpentSharingConfig>) => {
          if (spentSharingConfig.body) {
            return of(spentSharingConfig.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new SpentSharingConfig());
  }
}

export const spentSharingConfigRoute: Routes = [
  {
    path: '',
    component: SpentSharingConfigComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentSharingConfigDetailComponent,
    resolve: {
      spentSharingConfig: SpentSharingConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentSharingConfigUpdateComponent,
    resolve: {
      spentSharingConfig: SpentSharingConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentSharingConfigUpdateComponent,
    resolve: {
      spentSharingConfig: SpentSharingConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
