import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

@Component({
  selector: 'mha-spent-sharing-config-detail',
  templateUrl: './spent-sharing-config-detail.component.html'
})
export class SpentSharingConfigDetailComponent implements OnInit {
  spentSharingConfig: ISpentSharingConfig | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spentSharingConfig }) => (this.spentSharingConfig = spentSharingConfig));
  }

  previousState(): void {
    window.history.back();
  }
}
