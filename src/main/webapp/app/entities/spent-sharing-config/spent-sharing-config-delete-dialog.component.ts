import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';

@Component({
  templateUrl: './spent-sharing-config-delete-dialog.component.html'
})
export class SpentSharingConfigDeleteDialogComponent {
  spentSharingConfig?: ISpentSharingConfig;

  constructor(
    protected spentSharingConfigService: SpentSharingConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.spentSharingConfigService.delete(id).subscribe(() => {
      this.eventManager.broadcast('spentSharingConfigListModification');
      this.activeModal.close();
    });
  }
}
