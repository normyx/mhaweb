import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { SpentSharingConfigComponent } from './spent-sharing-config.component';
import { SpentSharingConfigDetailComponent } from './spent-sharing-config-detail.component';
import { SpentSharingConfigUpdateComponent } from './spent-sharing-config-update.component';
import { SpentSharingConfigDeleteDialogComponent } from './spent-sharing-config-delete-dialog.component';
import { spentSharingConfigRoute } from './spent-sharing-config.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(spentSharingConfigRoute)],
  declarations: [
    SpentSharingConfigComponent,
    SpentSharingConfigDetailComponent,
    SpentSharingConfigUpdateComponent,
    SpentSharingConfigDeleteDialogComponent
  ],
  entryComponents: [SpentSharingConfigDeleteDialogComponent]
})
export class MhawebSpentSharingConfigModule {}
