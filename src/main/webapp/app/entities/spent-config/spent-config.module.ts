import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { SpentConfigComponent } from './spent-config.component';
import { SpentConfigDetailComponent } from './spent-config-detail.component';
import { SpentConfigUpdateComponent } from './spent-config-update.component';
import { SpentConfigDeleteDialogComponent } from './spent-config-delete-dialog.component';
import { spentConfigRoute } from './spent-config.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(spentConfigRoute)],
  declarations: [SpentConfigComponent, SpentConfigDetailComponent, SpentConfigUpdateComponent, SpentConfigDeleteDialogComponent],
  entryComponents: [SpentConfigDeleteDialogComponent]
})
export class MhawebSpentConfigModule {}
