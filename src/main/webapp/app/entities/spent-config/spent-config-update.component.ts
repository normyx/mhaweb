import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISpentConfig, SpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from './spent-config.service';
import { IWallet } from 'app/shared/model/wallet.model';
import { WalletService } from 'app/entities/wallet/wallet.service';

@Component({
  selector: 'mha-spent-config-update',
  templateUrl: './spent-config-update.component.html'
})
export class SpentConfigUpdateComponent implements OnInit {
  isSaving = false;
  wallets: IWallet[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    amount: [null, [Validators.min(0)]],
    spentLabel: [null, [Validators.minLength(2), Validators.maxLength(20)]],
    lastUpdate: [null, [Validators.required]],
    walletId: [null, Validators.required]
  });

  constructor(
    protected spentConfigService: SpentConfigService,
    protected walletService: WalletService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spentConfig }) => {
      this.updateForm(spentConfig);

      this.walletService.query().subscribe((res: HttpResponse<IWallet[]>) => (this.wallets = res.body || []));
    });
  }

  updateForm(spentConfig: ISpentConfig): void {
    this.editForm.patchValue({
      id: spentConfig.id,
      label: spentConfig.label,
      amount: spentConfig.amount,
      spentLabel: spentConfig.spentLabel,
      lastUpdate: spentConfig.lastUpdate,
      walletId: spentConfig.walletId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const spentConfig = this.createFromForm();
    if (spentConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.spentConfigService.update(spentConfig));
    } else {
      this.subscribeToSaveResponse(this.spentConfigService.create(spentConfig));
    }
  }

  private createFromForm(): ISpentConfig {
    return {
      ...new SpentConfig(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      spentLabel: this.editForm.get(['spentLabel'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      walletId: this.editForm.get(['walletId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpentConfig>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IWallet): any {
    return item.id;
  }
}
