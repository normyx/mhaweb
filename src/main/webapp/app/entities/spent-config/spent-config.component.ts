import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISpentConfig } from 'app/shared/model/spent-config.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SpentConfigService } from './spent-config.service';
import { SpentConfigDeleteDialogComponent } from './spent-config-delete-dialog.component';

@Component({
  selector: 'mha-spent-config',
  templateUrl: './spent-config.component.html'
})
export class SpentConfigComponent implements OnInit, OnDestroy {
  spentConfigs: ISpentConfig[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected spentConfigService: SpentConfigService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.spentConfigs = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.spentConfigService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ISpentConfig[]>) => this.paginateSpentConfigs(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.spentConfigs = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSpentConfigs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISpentConfig): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSpentConfigs(): void {
    this.eventSubscriber = this.eventManager.subscribe('spentConfigListModification', () => this.reset());
  }

  delete(spentConfig: ISpentConfig): void {
    const modalRef = this.modalService.open(SpentConfigDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.spentConfig = spentConfig;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateSpentConfigs(data: ISpentConfig[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.spentConfigs.push(data[i]);
      }
    }
  }
}
