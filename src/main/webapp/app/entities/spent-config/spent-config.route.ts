import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISpentConfig, SpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from './spent-config.service';
import { SpentConfigComponent } from './spent-config.component';
import { SpentConfigDetailComponent } from './spent-config-detail.component';
import { SpentConfigUpdateComponent } from './spent-config-update.component';

@Injectable({ providedIn: 'root' })
export class SpentConfigResolve implements Resolve<ISpentConfig> {
  constructor(private service: SpentConfigService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISpentConfig> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((spentConfig: HttpResponse<SpentConfig>) => {
          if (spentConfig.body) {
            return of(spentConfig.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new SpentConfig());
  }
}

export const spentConfigRoute: Routes = [
  {
    path: '',
    component: SpentConfigComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentConfigDetailComponent,
    resolve: {
      spentConfig: SpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentConfigUpdateComponent,
    resolve: {
      spentConfig: SpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentConfigUpdateComponent,
    resolve: {
      spentConfig: SpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
