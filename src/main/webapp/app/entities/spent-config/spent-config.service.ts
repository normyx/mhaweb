import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISpentConfig } from 'app/shared/model/spent-config.model';

type EntityResponseType = HttpResponse<ISpentConfig>;
type EntityArrayResponseType = HttpResponse<ISpentConfig[]>;

@Injectable({ providedIn: 'root' })
export class SpentConfigService {
  public resourceUrl = SERVER_API_URL + 'api/spent-configs';

  constructor(protected http: HttpClient) {}

  create(spentConfig: ISpentConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentConfig);
    return this.http
      .post<ISpentConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(spentConfig: ISpentConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentConfig);
    return this.http
      .put<ISpentConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISpentConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISpentConfig[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(spentConfig: ISpentConfig): ISpentConfig {
    const copy: ISpentConfig = Object.assign({}, spentConfig, {
      lastUpdate: spentConfig.lastUpdate && spentConfig.lastUpdate.isValid() ? spentConfig.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((spentConfig: ISpentConfig) => {
        spentConfig.lastUpdate = spentConfig.lastUpdate ? moment(spentConfig.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
