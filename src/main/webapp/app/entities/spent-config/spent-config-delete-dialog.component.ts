import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from './spent-config.service';

@Component({
  templateUrl: './spent-config-delete-dialog.component.html'
})
export class SpentConfigDeleteDialogComponent {
  spentConfig?: ISpentConfig;

  constructor(
    protected spentConfigService: SpentConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.spentConfigService.delete(id).subscribe(() => {
      this.eventManager.broadcast('spentConfigListModification');
      this.activeModal.close();
    });
  }
}
