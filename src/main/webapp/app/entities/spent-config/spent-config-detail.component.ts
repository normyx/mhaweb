import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpentConfig } from 'app/shared/model/spent-config.model';

@Component({
  selector: 'mha-spent-config-detail',
  templateUrl: './spent-config-detail.component.html'
})
export class SpentConfigDetailComponent implements OnInit {
  spentConfig: ISpentConfig | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spentConfig }) => (this.spentConfig = spentConfig));
  }

  previousState(): void {
    window.history.back();
  }
}
