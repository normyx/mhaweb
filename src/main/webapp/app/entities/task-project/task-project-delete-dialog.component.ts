import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from './task-project.service';

@Component({
  templateUrl: './task-project-delete-dialog.component.html'
})
export class TaskProjectDeleteDialogComponent {
  taskProject?: ITaskProject;

  constructor(
    protected taskProjectService: TaskProjectService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.taskProjectService.delete(id).subscribe(() => {
      this.eventManager.broadcast('taskProjectListModification');
      this.activeModal.close();
    });
  }
}
