import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITaskProject } from 'app/shared/model/task-project.model';

@Component({
  selector: 'mha-task-project-detail',
  templateUrl: './task-project-detail.component.html'
})
export class TaskProjectDetailComponent implements OnInit {
  taskProject: ITaskProject | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ taskProject }) => (this.taskProject = taskProject));
  }

  previousState(): void {
    window.history.back();
  }
}
