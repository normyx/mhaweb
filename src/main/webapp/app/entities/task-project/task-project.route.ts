import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITaskProject, TaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from './task-project.service';
import { TaskProjectComponent } from './task-project.component';
import { TaskProjectDetailComponent } from './task-project-detail.component';
import { TaskProjectUpdateComponent } from './task-project-update.component';

@Injectable({ providedIn: 'root' })
export class TaskProjectResolve implements Resolve<ITaskProject> {
  constructor(private service: TaskProjectService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITaskProject> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((taskProject: HttpResponse<TaskProject>) => {
          if (taskProject.body) {
            return of(taskProject.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TaskProject());
  }
}

export const taskProjectRoute: Routes = [
  {
    path: '',
    component: TaskProjectComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TaskProjectDetailComponent,
    resolve: {
      taskProject: TaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TaskProjectUpdateComponent,
    resolve: {
      taskProject: TaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TaskProjectUpdateComponent,
    resolve: {
      taskProject: TaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mhawebApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
