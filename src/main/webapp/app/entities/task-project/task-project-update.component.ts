import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITaskProject, TaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from './task-project.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from 'app/entities/workspace/workspace.service';

type SelectableEntity = IProfil | IWorkspace;

@Component({
  selector: 'mha-task-project-update',
  templateUrl: './task-project-update.component.html'
})
export class TaskProjectUpdateComponent implements OnInit {
  isSaving = false;
  profils: IProfil[] = [];
  workspaces: IWorkspace[] = [];
  lastUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    lastUpdate: [null, [Validators.required]],
    owners: [],
    workspaceId: []
  });

  constructor(
    protected taskProjectService: TaskProjectService,
    protected profilService: ProfilService,
    protected workspaceService: WorkspaceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ taskProject }) => {
      this.updateForm(taskProject);

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));

      this.workspaceService.query().subscribe((res: HttpResponse<IWorkspace[]>) => (this.workspaces = res.body || []));
    });
  }

  updateForm(taskProject: ITaskProject): void {
    this.editForm.patchValue({
      id: taskProject.id,
      label: taskProject.label,
      lastUpdate: taskProject.lastUpdate,
      owners: taskProject.owners,
      workspaceId: taskProject.workspaceId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const taskProject = this.createFromForm();
    if (taskProject.id !== undefined) {
      this.subscribeToSaveResponse(this.taskProjectService.update(taskProject));
    } else {
      this.subscribeToSaveResponse(this.taskProjectService.create(taskProject));
    }
  }

  private createFromForm(): ITaskProject {
    return {
      ...new TaskProject(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      lastUpdate: this.editForm.get(['lastUpdate'])!.value,
      owners: this.editForm.get(['owners'])!.value,
      workspaceId: this.editForm.get(['workspaceId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITaskProject>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IProfil[], option: IProfil): IProfil {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
