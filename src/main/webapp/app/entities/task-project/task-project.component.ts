import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITaskProject } from 'app/shared/model/task-project.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TaskProjectService } from './task-project.service';
import { TaskProjectDeleteDialogComponent } from './task-project-delete-dialog.component';

@Component({
  selector: 'mha-task-project',
  templateUrl: './task-project.component.html'
})
export class TaskProjectComponent implements OnInit, OnDestroy {
  taskProjects: ITaskProject[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected taskProjectService: TaskProjectService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.taskProjects = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.taskProjectService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ITaskProject[]>) => this.paginateTaskProjects(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.taskProjects = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTaskProjects();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITaskProject): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTaskProjects(): void {
    this.eventSubscriber = this.eventManager.subscribe('taskProjectListModification', () => this.reset());
  }

  delete(taskProject: ITaskProject): void {
    const modalRef = this.modalService.open(TaskProjectDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.taskProject = taskProject;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTaskProjects(data: ITaskProject[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.taskProjects.push(data[i]);
      }
    }
  }
}
