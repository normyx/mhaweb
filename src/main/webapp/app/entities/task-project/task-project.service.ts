import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITaskProject } from 'app/shared/model/task-project.model';

type EntityResponseType = HttpResponse<ITaskProject>;
type EntityArrayResponseType = HttpResponse<ITaskProject[]>;

@Injectable({ providedIn: 'root' })
export class TaskProjectService {
  public resourceUrl = SERVER_API_URL + 'api/task-projects';

  constructor(protected http: HttpClient) {}

  create(taskProject: ITaskProject): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(taskProject);
    return this.http
      .post<ITaskProject>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(taskProject: ITaskProject): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(taskProject);
    return this.http
      .put<ITaskProject>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITaskProject>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITaskProject[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(taskProject: ITaskProject): ITaskProject {
    const copy: ITaskProject = Object.assign({}, taskProject, {
      lastUpdate: taskProject.lastUpdate && taskProject.lastUpdate.isValid() ? taskProject.lastUpdate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate ? moment(res.body.lastUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((taskProject: ITaskProject) => {
        taskProject.lastUpdate = taskProject.lastUpdate ? moment(taskProject.lastUpdate) : undefined;
      });
    }
    return res;
  }
}
