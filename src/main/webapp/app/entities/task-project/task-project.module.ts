import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MhawebSharedModule } from 'app/shared/shared.module';
import { TaskProjectComponent } from './task-project.component';
import { TaskProjectDetailComponent } from './task-project-detail.component';
import { TaskProjectUpdateComponent } from './task-project-update.component';
import { TaskProjectDeleteDialogComponent } from './task-project-delete-dialog.component';
import { taskProjectRoute } from './task-project.route';

@NgModule({
  imports: [MhawebSharedModule, RouterModule.forChild(taskProjectRoute)],
  declarations: [TaskProjectComponent, TaskProjectDetailComponent, TaskProjectUpdateComponent, TaskProjectDeleteDialogComponent],
  entryComponents: [TaskProjectDeleteDialogComponent]
})
export class MhawebTaskProjectModule {}
