import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TaskProjectDetailComponent } from 'app/entities/task-project/task-project-detail.component';
import { TaskProject } from 'app/shared/model/task-project.model';

describe('Component Tests', () => {
  describe('TaskProject Management Detail Component', () => {
    let comp: TaskProjectDetailComponent;
    let fixture: ComponentFixture<TaskProjectDetailComponent>;
    const route = ({ data: of({ taskProject: new TaskProject(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TaskProjectDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TaskProjectDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TaskProjectDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load taskProject on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.taskProject).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
