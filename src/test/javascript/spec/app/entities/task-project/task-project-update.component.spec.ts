import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TaskProjectUpdateComponent } from 'app/entities/task-project/task-project-update.component';
import { TaskProjectService } from 'app/entities/task-project/task-project.service';
import { TaskProject } from 'app/shared/model/task-project.model';

describe('Component Tests', () => {
  describe('TaskProject Management Update Component', () => {
    let comp: TaskProjectUpdateComponent;
    let fixture: ComponentFixture<TaskProjectUpdateComponent>;
    let service: TaskProjectService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TaskProjectUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TaskProjectUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TaskProjectUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TaskProjectService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TaskProject(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TaskProject();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
