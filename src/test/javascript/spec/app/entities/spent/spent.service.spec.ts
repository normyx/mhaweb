import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SpentService } from 'app/entities/spent/spent.service';
import { ISpent, Spent } from 'app/shared/model/spent.model';

describe('Service Tests', () => {
  describe('Spent Service', () => {
    let injector: TestBed;
    let service: SpentService;
    let httpMock: HttpTestingController;
    let elemDefault: ISpent;
    let expectedResult: ISpent | ISpent[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(SpentService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Spent(0, 'AAAAAAA', 'AAAAAAA', 0, currentDate, false, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            spentDate: currentDate.format(DATE_FORMAT),
            lastUpdate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Spent', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            spentDate: currentDate.format(DATE_FORMAT),
            lastUpdate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            spentDate: currentDate,
            lastUpdate: currentDate
          },
          returnedFromService
        );

        service.create(new Spent()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Spent', () => {
        const returnedFromService = Object.assign(
          {
            label: 'BBBBBB',
            description: 'BBBBBB',
            amount: 1,
            spentDate: currentDate.format(DATE_FORMAT),
            confirmed: true,
            lastUpdate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            spentDate: currentDate,
            lastUpdate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Spent', () => {
        const returnedFromService = Object.assign(
          {
            label: 'BBBBBB',
            description: 'BBBBBB',
            amount: 1,
            spentDate: currentDate.format(DATE_FORMAT),
            confirmed: true,
            lastUpdate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            spentDate: currentDate,
            lastUpdate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Spent', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
