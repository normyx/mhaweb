import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { SpentUpdateComponent } from 'app/entities/spent/spent-update.component';
import { SpentService } from 'app/entities/spent/spent.service';
import { Spent } from 'app/shared/model/spent.model';

describe('Component Tests', () => {
  describe('Spent Management Update Component', () => {
    let comp: SpentUpdateComponent;
    let fixture: ComponentFixture<SpentUpdateComponent>;
    let service: SpentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [SpentUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SpentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Spent(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Spent();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
