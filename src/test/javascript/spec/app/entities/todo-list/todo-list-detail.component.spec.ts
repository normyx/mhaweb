import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TodoListDetailComponent } from 'app/entities/todo-list/todo-list-detail.component';
import { TodoList } from 'app/shared/model/todo-list.model';

describe('Component Tests', () => {
  describe('TodoList Management Detail Component', () => {
    let comp: TodoListDetailComponent;
    let fixture: ComponentFixture<TodoListDetailComponent>;
    const route = ({ data: of({ todoList: new TodoList(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TodoListDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TodoListDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TodoListDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load todoList on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.todoList).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
