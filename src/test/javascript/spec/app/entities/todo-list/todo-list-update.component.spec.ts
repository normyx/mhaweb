import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TodoListUpdateComponent } from 'app/entities/todo-list/todo-list-update.component';
import { TodoListService } from 'app/entities/todo-list/todo-list.service';
import { TodoList } from 'app/shared/model/todo-list.model';

describe('Component Tests', () => {
  describe('TodoList Management Update Component', () => {
    let comp: TodoListUpdateComponent;
    let fixture: ComponentFixture<TodoListUpdateComponent>;
    let service: TodoListService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TodoListUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TodoListUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TodoListUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TodoListService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TodoList(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TodoList();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
