import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { SpentSharingUpdateComponent } from 'app/entities/spent-sharing/spent-sharing-update.component';
import { SpentSharingService } from 'app/entities/spent-sharing/spent-sharing.service';
import { SpentSharing } from 'app/shared/model/spent-sharing.model';

describe('Component Tests', () => {
  describe('SpentSharing Management Update Component', () => {
    let comp: SpentSharingUpdateComponent;
    let fixture: ComponentFixture<SpentSharingUpdateComponent>;
    let service: SpentSharingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [SpentSharingUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SpentSharingUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentSharingUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentSharingService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentSharing(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentSharing();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
