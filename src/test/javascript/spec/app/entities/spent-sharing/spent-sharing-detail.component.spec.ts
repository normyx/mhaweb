import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { SpentSharingDetailComponent } from 'app/entities/spent-sharing/spent-sharing-detail.component';
import { SpentSharing } from 'app/shared/model/spent-sharing.model';

describe('Component Tests', () => {
  describe('SpentSharing Management Detail Component', () => {
    let comp: SpentSharingDetailComponent;
    let fixture: ComponentFixture<SpentSharingDetailComponent>;
    const route = ({ data: of({ spentSharing: new SpentSharing(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [SpentSharingDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SpentSharingDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentSharingDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load spentSharing on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.spentSharing).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
