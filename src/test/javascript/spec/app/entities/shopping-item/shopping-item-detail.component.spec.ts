import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ShoppingItemDetailComponent } from 'app/entities/shopping-item/shopping-item-detail.component';
import { ShoppingItem } from 'app/shared/model/shopping-item.model';

describe('Component Tests', () => {
  describe('ShoppingItem Management Detail Component', () => {
    let comp: ShoppingItemDetailComponent;
    let fixture: ComponentFixture<ShoppingItemDetailComponent>;
    const route = ({ data: of({ shoppingItem: new ShoppingItem(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingItemDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShoppingItemDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingItemDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load shoppingItem on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shoppingItem).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
