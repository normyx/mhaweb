import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ShoppingCatalogSheldDetailComponent } from 'app/entities/shopping-catalog-sheld/shopping-catalog-sheld-detail.component';
import { ShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';

describe('Component Tests', () => {
  describe('ShoppingCatalogSheld Management Detail Component', () => {
    let comp: ShoppingCatalogSheldDetailComponent;
    let fixture: ComponentFixture<ShoppingCatalogSheldDetailComponent>;
    const route = ({ data: of({ shoppingCatalogSheld: new ShoppingCatalogSheld(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingCatalogSheldDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShoppingCatalogSheldDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingCatalogSheldDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load shoppingCatalogSheld on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shoppingCatalogSheld).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
