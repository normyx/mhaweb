import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ShoppingCatalogSheldUpdateComponent } from 'app/entities/shopping-catalog-sheld/shopping-catalog-sheld-update.component';
import { ShoppingCatalogSheldService } from 'app/entities/shopping-catalog-sheld/shopping-catalog-sheld.service';
import { ShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';

describe('Component Tests', () => {
  describe('ShoppingCatalogSheld Management Update Component', () => {
    let comp: ShoppingCatalogSheldUpdateComponent;
    let fixture: ComponentFixture<ShoppingCatalogSheldUpdateComponent>;
    let service: ShoppingCatalogSheldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingCatalogSheldUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShoppingCatalogSheldUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShoppingCatalogSheldUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingCatalogSheldService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCatalogSheld(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCatalogSheld();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
