import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ShoppingCatalogSheldService } from 'app/entities/shopping-catalog-sheld/shopping-catalog-sheld.service';
import { IShoppingCatalogSheld, ShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';

describe('Service Tests', () => {
  describe('ShoppingCatalogSheld Service', () => {
    let injector: TestBed;
    let service: ShoppingCatalogSheldService;
    let httpMock: HttpTestingController;
    let elemDefault: IShoppingCatalogSheld;
    let expectedResult: IShoppingCatalogSheld | IShoppingCatalogSheld[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ShoppingCatalogSheldService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new ShoppingCatalogSheld(0, 'AAAAAAA', currentDate, false, false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            lastUpdate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ShoppingCatalogSheld', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            lastUpdate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastUpdate: currentDate
          },
          returnedFromService
        );

        service.create(new ShoppingCatalogSheld()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ShoppingCatalogSheld', () => {
        const returnedFromService = Object.assign(
          {
            label: 'BBBBBB',
            lastUpdate: currentDate.format(DATE_FORMAT),
            isDefault: true,
            isDeleted: true
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastUpdate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ShoppingCatalogSheld', () => {
        const returnedFromService = Object.assign(
          {
            label: 'BBBBBB',
            lastUpdate: currentDate.format(DATE_FORMAT),
            isDefault: true,
            isDeleted: true
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastUpdate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ShoppingCatalogSheld', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
