import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MhawebTestModule } from '../../../test.module';
import { ProfilDataComponent } from 'app/entities/profil-data/profil-data.component';
import { ProfilDataService } from 'app/entities/profil-data/profil-data.service';
import { ProfilData } from 'app/shared/model/profil-data.model';

describe('Component Tests', () => {
  describe('ProfilData Management Component', () => {
    let comp: ProfilDataComponent;
    let fixture: ComponentFixture<ProfilDataComponent>;
    let service: ProfilDataService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ProfilDataComponent]
      })
        .overrideTemplate(ProfilDataComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProfilDataComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProfilDataService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProfilData(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.profilData && comp.profilData[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
