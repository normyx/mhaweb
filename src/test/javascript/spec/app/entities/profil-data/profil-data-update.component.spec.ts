import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ProfilDataUpdateComponent } from 'app/entities/profil-data/profil-data-update.component';
import { ProfilDataService } from 'app/entities/profil-data/profil-data.service';
import { ProfilData } from 'app/shared/model/profil-data.model';

describe('Component Tests', () => {
  describe('ProfilData Management Update Component', () => {
    let comp: ProfilDataUpdateComponent;
    let fixture: ComponentFixture<ProfilDataUpdateComponent>;
    let service: ProfilDataService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ProfilDataUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ProfilDataUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProfilDataUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProfilDataService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProfilData(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProfilData();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
