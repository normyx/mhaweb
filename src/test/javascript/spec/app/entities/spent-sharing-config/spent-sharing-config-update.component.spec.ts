import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { SpentSharingConfigUpdateComponent } from 'app/entities/spent-sharing-config/spent-sharing-config-update.component';
import { SpentSharingConfigService } from 'app/entities/spent-sharing-config/spent-sharing-config.service';
import { SpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

describe('Component Tests', () => {
  describe('SpentSharingConfig Management Update Component', () => {
    let comp: SpentSharingConfigUpdateComponent;
    let fixture: ComponentFixture<SpentSharingConfigUpdateComponent>;
    let service: SpentSharingConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [SpentSharingConfigUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SpentSharingConfigUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentSharingConfigUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentSharingConfigService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentSharingConfig(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentSharingConfig();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
