import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TodoTemplateDetailComponent } from 'app/entities/todo-template/todo-template-detail.component';
import { TodoTemplate } from 'app/shared/model/todo-template.model';

describe('Component Tests', () => {
  describe('TodoTemplate Management Detail Component', () => {
    let comp: TodoTemplateDetailComponent;
    let fixture: ComponentFixture<TodoTemplateDetailComponent>;
    const route = ({ data: of({ todoTemplate: new TodoTemplate(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TodoTemplateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TodoTemplateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TodoTemplateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load todoTemplate on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.todoTemplate).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
