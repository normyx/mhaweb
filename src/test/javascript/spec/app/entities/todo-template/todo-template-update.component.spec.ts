import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TodoTemplateUpdateComponent } from 'app/entities/todo-template/todo-template-update.component';
import { TodoTemplateService } from 'app/entities/todo-template/todo-template.service';
import { TodoTemplate } from 'app/shared/model/todo-template.model';

describe('Component Tests', () => {
  describe('TodoTemplate Management Update Component', () => {
    let comp: TodoTemplateUpdateComponent;
    let fixture: ComponentFixture<TodoTemplateUpdateComponent>;
    let service: TodoTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TodoTemplateUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TodoTemplateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TodoTemplateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TodoTemplateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TodoTemplate(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TodoTemplate();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
