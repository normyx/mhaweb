import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TodoListTemplateUpdateComponent } from 'app/entities/todo-list-template/todo-list-template-update.component';
import { TodoListTemplateService } from 'app/entities/todo-list-template/todo-list-template.service';
import { TodoListTemplate } from 'app/shared/model/todo-list-template.model';

describe('Component Tests', () => {
  describe('TodoListTemplate Management Update Component', () => {
    let comp: TodoListTemplateUpdateComponent;
    let fixture: ComponentFixture<TodoListTemplateUpdateComponent>;
    let service: TodoListTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TodoListTemplateUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TodoListTemplateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TodoListTemplateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TodoListTemplateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TodoListTemplate(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TodoListTemplate();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
