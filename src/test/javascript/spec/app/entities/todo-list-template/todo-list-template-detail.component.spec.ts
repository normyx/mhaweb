import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { TodoListTemplateDetailComponent } from 'app/entities/todo-list-template/todo-list-template-detail.component';
import { TodoListTemplate } from 'app/shared/model/todo-list-template.model';

describe('Component Tests', () => {
  describe('TodoListTemplate Management Detail Component', () => {
    let comp: TodoListTemplateDetailComponent;
    let fixture: ComponentFixture<TodoListTemplateDetailComponent>;
    const route = ({ data: of({ todoListTemplate: new TodoListTemplate(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [TodoListTemplateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TodoListTemplateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TodoListTemplateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load todoListTemplate on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.todoListTemplate).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
