import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { SpentConfigUpdateComponent } from 'app/entities/spent-config/spent-config-update.component';
import { SpentConfigService } from 'app/entities/spent-config/spent-config.service';
import { SpentConfig } from 'app/shared/model/spent-config.model';

describe('Component Tests', () => {
  describe('SpentConfig Management Update Component', () => {
    let comp: SpentConfigUpdateComponent;
    let fixture: ComponentFixture<SpentConfigUpdateComponent>;
    let service: SpentConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [SpentConfigUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SpentConfigUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentConfigUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentConfigService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentConfig(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentConfig();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
