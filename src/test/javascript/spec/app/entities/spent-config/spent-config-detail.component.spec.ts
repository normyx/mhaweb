import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { SpentConfigDetailComponent } from 'app/entities/spent-config/spent-config-detail.component';
import { SpentConfig } from 'app/shared/model/spent-config.model';

describe('Component Tests', () => {
  describe('SpentConfig Management Detail Component', () => {
    let comp: SpentConfigDetailComponent;
    let fixture: ComponentFixture<SpentConfigDetailComponent>;
    const route = ({ data: of({ spentConfig: new SpentConfig(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [SpentConfigDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SpentConfigDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentConfigDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load spentConfig on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.spentConfig).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
