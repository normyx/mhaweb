import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ShoppingCatalogItemDetailComponent } from 'app/entities/shopping-catalog-item/shopping-catalog-item-detail.component';
import { ShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';

describe('Component Tests', () => {
  describe('ShoppingCatalogItem Management Detail Component', () => {
    let comp: ShoppingCatalogItemDetailComponent;
    let fixture: ComponentFixture<ShoppingCatalogItemDetailComponent>;
    const route = ({ data: of({ shoppingCatalogItem: new ShoppingCatalogItem(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingCatalogItemDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShoppingCatalogItemDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingCatalogItemDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load shoppingCatalogItem on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shoppingCatalogItem).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
