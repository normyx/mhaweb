import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ShoppingCatalogItemUpdateComponent } from 'app/entities/shopping-catalog-item/shopping-catalog-item-update.component';
import { ShoppingCatalogItemService } from 'app/entities/shopping-catalog-item/shopping-catalog-item.service';
import { ShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';

describe('Component Tests', () => {
  describe('ShoppingCatalogItem Management Update Component', () => {
    let comp: ShoppingCatalogItemUpdateComponent;
    let fixture: ComponentFixture<ShoppingCatalogItemUpdateComponent>;
    let service: ShoppingCatalogItemService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingCatalogItemUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShoppingCatalogItemUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShoppingCatalogItemUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingCatalogItemService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCatalogItem(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCatalogItem();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
