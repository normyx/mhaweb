import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ShoppingCatalogCartDetailComponent } from 'app/entities/shopping-catalog-cart/shopping-catalog-cart-detail.component';
import { ShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';

describe('Component Tests', () => {
  describe('ShoppingCatalogCart Management Detail Component', () => {
    let comp: ShoppingCatalogCartDetailComponent;
    let fixture: ComponentFixture<ShoppingCatalogCartDetailComponent>;
    const route = ({ data: of({ shoppingCatalogCart: new ShoppingCatalogCart(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingCatalogCartDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShoppingCatalogCartDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingCatalogCartDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load shoppingCatalogCart on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shoppingCatalogCart).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
