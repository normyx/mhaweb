import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MhawebTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { ShoppingCatalogCartDeleteDialogComponent } from 'app/entities/shopping-catalog-cart/shopping-catalog-cart-delete-dialog.component';
import { ShoppingCatalogCartService } from 'app/entities/shopping-catalog-cart/shopping-catalog-cart.service';

describe('Component Tests', () => {
  describe('ShoppingCatalogCart Management Delete Component', () => {
    let comp: ShoppingCatalogCartDeleteDialogComponent;
    let fixture: ComponentFixture<ShoppingCatalogCartDeleteDialogComponent>;
    let service: ShoppingCatalogCartService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingCatalogCartDeleteDialogComponent]
      })
        .overrideTemplate(ShoppingCatalogCartDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingCatalogCartDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingCatalogCartService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
