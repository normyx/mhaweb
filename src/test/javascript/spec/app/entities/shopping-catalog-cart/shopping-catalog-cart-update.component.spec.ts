import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MhawebTestModule } from '../../../test.module';
import { ShoppingCatalogCartUpdateComponent } from 'app/entities/shopping-catalog-cart/shopping-catalog-cart-update.component';
import { ShoppingCatalogCartService } from 'app/entities/shopping-catalog-cart/shopping-catalog-cart.service';
import { ShoppingCatalogCart } from 'app/shared/model/shopping-catalog-cart.model';

describe('Component Tests', () => {
  describe('ShoppingCatalogCart Management Update Component', () => {
    let comp: ShoppingCatalogCartUpdateComponent;
    let fixture: ComponentFixture<ShoppingCatalogCartUpdateComponent>;
    let service: ShoppingCatalogCartService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MhawebTestModule],
        declarations: [ShoppingCatalogCartUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShoppingCatalogCartUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShoppingCatalogCartUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingCatalogCartService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCatalogCart(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCatalogCart();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
