import { element, by, ElementFinder } from 'protractor';

export class SpentConfigComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('mha-spent-config div table .btn-danger'));
  title = element.all(by.css('mha-spent-config div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentConfigUpdatePage {
  pageTitle = element(by.id('mha-spent-config-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  labelInput = element(by.id('field_label'));
  amountInput = element(by.id('field_amount'));
  spentLabelInput = element(by.id('field_spentLabel'));
  lastUpdateInput = element(by.id('field_lastUpdate'));

  walletSelect = element(by.id('field_wallet'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLabelInput(label: string): Promise<void> {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput(): Promise<string> {
    return await this.labelInput.getAttribute('value');
  }

  async setAmountInput(amount: string): Promise<void> {
    await this.amountInput.sendKeys(amount);
  }

  async getAmountInput(): Promise<string> {
    return await this.amountInput.getAttribute('value');
  }

  async setSpentLabelInput(spentLabel: string): Promise<void> {
    await this.spentLabelInput.sendKeys(spentLabel);
  }

  async getSpentLabelInput(): Promise<string> {
    return await this.spentLabelInput.getAttribute('value');
  }

  async setLastUpdateInput(lastUpdate: string): Promise<void> {
    await this.lastUpdateInput.sendKeys(lastUpdate);
  }

  async getLastUpdateInput(): Promise<string> {
    return await this.lastUpdateInput.getAttribute('value');
  }

  async walletSelectLastOption(): Promise<void> {
    await this.walletSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async walletSelectOption(option: string): Promise<void> {
    await this.walletSelect.sendKeys(option);
  }

  getWalletSelect(): ElementFinder {
    return this.walletSelect;
  }

  async getWalletSelectedOption(): Promise<string> {
    return await this.walletSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentConfigDeleteDialog {
  private dialogTitle = element(by.id('mha-delete-spentConfig-heading'));
  private confirmButton = element(by.id('mha-confirm-delete-spentConfig'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
