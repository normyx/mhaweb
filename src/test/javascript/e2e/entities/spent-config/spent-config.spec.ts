import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  SpentConfigComponentsPage,
  /* SpentConfigDeleteDialog, */
  SpentConfigUpdatePage
} from './spent-config.page-object';

const expect = chai.expect;

describe('SpentConfig e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let spentConfigComponentsPage: SpentConfigComponentsPage;
  let spentConfigUpdatePage: SpentConfigUpdatePage;
  /* let spentConfigDeleteDialog: SpentConfigDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load SpentConfigs', async () => {
    await navBarPage.goToEntity('spent-config');
    spentConfigComponentsPage = new SpentConfigComponentsPage();
    await browser.wait(ec.visibilityOf(spentConfigComponentsPage.title), 5000);
    expect(await spentConfigComponentsPage.getTitle()).to.eq('mhawebApp.spentConfig.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(spentConfigComponentsPage.entities), ec.visibilityOf(spentConfigComponentsPage.noResult)),
      1000
    );
  });

  it('should load create SpentConfig page', async () => {
    await spentConfigComponentsPage.clickOnCreateButton();
    spentConfigUpdatePage = new SpentConfigUpdatePage();
    expect(await spentConfigUpdatePage.getPageTitle()).to.eq('mhawebApp.spentConfig.home.createOrEditLabel');
    await spentConfigUpdatePage.cancel();
  });

  /* it('should create and save SpentConfigs', async () => {
        const nbButtonsBeforeCreate = await spentConfigComponentsPage.countDeleteButtons();

        await spentConfigComponentsPage.clickOnCreateButton();

        await promise.all([
            spentConfigUpdatePage.setLabelInput('label'),
            spentConfigUpdatePage.setAmountInput('5'),
            spentConfigUpdatePage.setSpentLabelInput('spentLabel'),
            spentConfigUpdatePage.setLastUpdateInput('2000-12-31'),
            spentConfigUpdatePage.walletSelectLastOption(),
        ]);

        expect(await spentConfigUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        expect(await spentConfigUpdatePage.getAmountInput()).to.eq('5', 'Expected amount value to be equals to 5');
        expect(await spentConfigUpdatePage.getSpentLabelInput()).to.eq('spentLabel', 'Expected SpentLabel value to be equals to spentLabel');
        expect(await spentConfigUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

        await spentConfigUpdatePage.save();
        expect(await spentConfigUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await spentConfigComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last SpentConfig', async () => {
        const nbButtonsBeforeDelete = await spentConfigComponentsPage.countDeleteButtons();
        await spentConfigComponentsPage.clickOnLastDeleteButton();

        spentConfigDeleteDialog = new SpentConfigDeleteDialog();
        expect(await spentConfigDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.spentConfig.delete.question');
        await spentConfigDeleteDialog.clickOnConfirmButton();

        expect(await spentConfigComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
