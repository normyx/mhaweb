import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ShoppingCatalogItemComponentsPage,
  /* ShoppingCatalogItemDeleteDialog, */
  ShoppingCatalogItemUpdatePage
} from './shopping-catalog-item.page-object';

const expect = chai.expect;

describe('ShoppingCatalogItem e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shoppingCatalogItemComponentsPage: ShoppingCatalogItemComponentsPage;
  let shoppingCatalogItemUpdatePage: ShoppingCatalogItemUpdatePage;
  /* let shoppingCatalogItemDeleteDialog: ShoppingCatalogItemDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ShoppingCatalogItems', async () => {
    await navBarPage.goToEntity('shopping-catalog-item');
    shoppingCatalogItemComponentsPage = new ShoppingCatalogItemComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogItemComponentsPage.title), 5000);
    expect(await shoppingCatalogItemComponentsPage.getTitle()).to.eq('mhawebApp.shoppingCatalogItem.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(shoppingCatalogItemComponentsPage.entities), ec.visibilityOf(shoppingCatalogItemComponentsPage.noResult)),
      1000
    );
  });

  it('should load create ShoppingCatalogItem page', async () => {
    await shoppingCatalogItemComponentsPage.clickOnCreateButton();
    shoppingCatalogItemUpdatePage = new ShoppingCatalogItemUpdatePage();
    expect(await shoppingCatalogItemUpdatePage.getPageTitle()).to.eq('mhawebApp.shoppingCatalogItem.home.createOrEditLabel');
    await shoppingCatalogItemUpdatePage.cancel();
  });

  /* it('should create and save ShoppingCatalogItems', async () => {
        const nbButtonsBeforeCreate = await shoppingCatalogItemComponentsPage.countDeleteButtons();

        await shoppingCatalogItemComponentsPage.clickOnCreateButton();

        await promise.all([
            shoppingCatalogItemUpdatePage.setLabelInput('label'),
            shoppingCatalogItemUpdatePage.setLastUpdateInput('2000-12-31'),
            shoppingCatalogItemUpdatePage.shoppingCatalogSheldSelectLastOption(),
        ]);

        expect(await shoppingCatalogItemUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        expect(await shoppingCatalogItemUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

        await shoppingCatalogItemUpdatePage.save();
        expect(await shoppingCatalogItemUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await shoppingCatalogItemComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last ShoppingCatalogItem', async () => {
        const nbButtonsBeforeDelete = await shoppingCatalogItemComponentsPage.countDeleteButtons();
        await shoppingCatalogItemComponentsPage.clickOnLastDeleteButton();

        shoppingCatalogItemDeleteDialog = new ShoppingCatalogItemDeleteDialog();
        expect(await shoppingCatalogItemDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.shoppingCatalogItem.delete.question');
        await shoppingCatalogItemDeleteDialog.clickOnConfirmButton();

        expect(await shoppingCatalogItemComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
