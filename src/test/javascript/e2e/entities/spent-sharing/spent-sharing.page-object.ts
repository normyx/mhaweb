import { element, by, ElementFinder } from 'protractor';

export class SpentSharingComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('mha-spent-sharing div table .btn-danger'));
  title = element.all(by.css('mha-spent-sharing div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentSharingUpdatePage {
  pageTitle = element(by.id('mha-spent-sharing-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  amountShareInput = element(by.id('field_amountShare'));
  shareInput = element(by.id('field_share'));
  lastUpdateInput = element(by.id('field_lastUpdate'));

  sharingProfilSelect = element(by.id('field_sharingProfil'));
  spentSelect = element(by.id('field_spent'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setAmountShareInput(amountShare: string): Promise<void> {
    await this.amountShareInput.sendKeys(amountShare);
  }

  async getAmountShareInput(): Promise<string> {
    return await this.amountShareInput.getAttribute('value');
  }

  async setShareInput(share: string): Promise<void> {
    await this.shareInput.sendKeys(share);
  }

  async getShareInput(): Promise<string> {
    return await this.shareInput.getAttribute('value');
  }

  async setLastUpdateInput(lastUpdate: string): Promise<void> {
    await this.lastUpdateInput.sendKeys(lastUpdate);
  }

  async getLastUpdateInput(): Promise<string> {
    return await this.lastUpdateInput.getAttribute('value');
  }

  async sharingProfilSelectLastOption(): Promise<void> {
    await this.sharingProfilSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async sharingProfilSelectOption(option: string): Promise<void> {
    await this.sharingProfilSelect.sendKeys(option);
  }

  getSharingProfilSelect(): ElementFinder {
    return this.sharingProfilSelect;
  }

  async getSharingProfilSelectedOption(): Promise<string> {
    return await this.sharingProfilSelect.element(by.css('option:checked')).getText();
  }

  async spentSelectLastOption(): Promise<void> {
    await this.spentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async spentSelectOption(option: string): Promise<void> {
    await this.spentSelect.sendKeys(option);
  }

  getSpentSelect(): ElementFinder {
    return this.spentSelect;
  }

  async getSpentSelectedOption(): Promise<string> {
    return await this.spentSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentSharingDeleteDialog {
  private dialogTitle = element(by.id('mha-delete-spentSharing-heading'));
  private confirmButton = element(by.id('mha-confirm-delete-spentSharing'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
