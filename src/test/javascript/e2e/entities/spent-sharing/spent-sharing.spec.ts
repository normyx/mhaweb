import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SpentSharingComponentsPage, SpentSharingDeleteDialog, SpentSharingUpdatePage } from './spent-sharing.page-object';

const expect = chai.expect;

describe('SpentSharing e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let spentSharingComponentsPage: SpentSharingComponentsPage;
  let spentSharingUpdatePage: SpentSharingUpdatePage;
  let spentSharingDeleteDialog: SpentSharingDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load SpentSharings', async () => {
    await navBarPage.goToEntity('spent-sharing');
    spentSharingComponentsPage = new SpentSharingComponentsPage();
    await browser.wait(ec.visibilityOf(spentSharingComponentsPage.title), 5000);
    expect(await spentSharingComponentsPage.getTitle()).to.eq('mhawebApp.spentSharing.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(spentSharingComponentsPage.entities), ec.visibilityOf(spentSharingComponentsPage.noResult)),
      1000
    );
  });

  it('should load create SpentSharing page', async () => {
    await spentSharingComponentsPage.clickOnCreateButton();
    spentSharingUpdatePage = new SpentSharingUpdatePage();
    expect(await spentSharingUpdatePage.getPageTitle()).to.eq('mhawebApp.spentSharing.home.createOrEditLabel');
    await spentSharingUpdatePage.cancel();
  });

  it('should create and save SpentSharings', async () => {
    const nbButtonsBeforeCreate = await spentSharingComponentsPage.countDeleteButtons();

    await spentSharingComponentsPage.clickOnCreateButton();

    await promise.all([
      spentSharingUpdatePage.setAmountShareInput('5'),
      spentSharingUpdatePage.setShareInput('5'),
      spentSharingUpdatePage.setLastUpdateInput('2000-12-31'),
      spentSharingUpdatePage.sharingProfilSelectLastOption(),
      spentSharingUpdatePage.spentSelectLastOption()
    ]);

    expect(await spentSharingUpdatePage.getAmountShareInput()).to.eq('5', 'Expected amountShare value to be equals to 5');
    expect(await spentSharingUpdatePage.getShareInput()).to.eq('5', 'Expected share value to be equals to 5');
    expect(await spentSharingUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

    await spentSharingUpdatePage.save();
    expect(await spentSharingUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await spentSharingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last SpentSharing', async () => {
    const nbButtonsBeforeDelete = await spentSharingComponentsPage.countDeleteButtons();
    await spentSharingComponentsPage.clickOnLastDeleteButton();

    spentSharingDeleteDialog = new SpentSharingDeleteDialog();
    expect(await spentSharingDeleteDialog.getDialogTitle()).to.eq('mhawebApp.spentSharing.delete.question');
    await spentSharingDeleteDialog.clickOnConfirmButton();

    expect(await spentSharingComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
