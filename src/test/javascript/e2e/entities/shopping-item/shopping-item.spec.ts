import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ShoppingItemComponentsPage,
  /* ShoppingItemDeleteDialog, */
  ShoppingItemUpdatePage
} from './shopping-item.page-object';

const expect = chai.expect;

describe('ShoppingItem e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shoppingItemComponentsPage: ShoppingItemComponentsPage;
  let shoppingItemUpdatePage: ShoppingItemUpdatePage;
  /* let shoppingItemDeleteDialog: ShoppingItemDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ShoppingItems', async () => {
    await navBarPage.goToEntity('shopping-item');
    shoppingItemComponentsPage = new ShoppingItemComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingItemComponentsPage.title), 5000);
    expect(await shoppingItemComponentsPage.getTitle()).to.eq('mhawebApp.shoppingItem.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(shoppingItemComponentsPage.entities), ec.visibilityOf(shoppingItemComponentsPage.noResult)),
      1000
    );
  });

  it('should load create ShoppingItem page', async () => {
    await shoppingItemComponentsPage.clickOnCreateButton();
    shoppingItemUpdatePage = new ShoppingItemUpdatePage();
    expect(await shoppingItemUpdatePage.getPageTitle()).to.eq('mhawebApp.shoppingItem.home.createOrEditLabel');
    await shoppingItemUpdatePage.cancel();
  });

  /* it('should create and save ShoppingItems', async () => {
        const nbButtonsBeforeCreate = await shoppingItemComponentsPage.countDeleteButtons();

        await shoppingItemComponentsPage.clickOnCreateButton();

        await promise.all([
            shoppingItemUpdatePage.setLabelInput('label'),
            shoppingItemUpdatePage.setLastUpdateInput('2000-12-31'),
            shoppingItemUpdatePage.setQuantityInput('5'),
            shoppingItemUpdatePage.unitSelectLastOption(),
            shoppingItemUpdatePage.shoppingCatalogSheldSelectLastOption(),
            shoppingItemUpdatePage.shoppingCartSelectLastOption(),
        ]);

        expect(await shoppingItemUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        expect(await shoppingItemUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');
        const selectedChecked = shoppingItemUpdatePage.getCheckedInput();
        if (await selectedChecked.isSelected()) {
            await shoppingItemUpdatePage.getCheckedInput().click();
            expect(await shoppingItemUpdatePage.getCheckedInput().isSelected(), 'Expected checked not to be selected').to.be.false;
        } else {
            await shoppingItemUpdatePage.getCheckedInput().click();
            expect(await shoppingItemUpdatePage.getCheckedInput().isSelected(), 'Expected checked to be selected').to.be.true;
        }
        expect(await shoppingItemUpdatePage.getQuantityInput()).to.eq('5', 'Expected quantity value to be equals to 5');

        await shoppingItemUpdatePage.save();
        expect(await shoppingItemUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await shoppingItemComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last ShoppingItem', async () => {
        const nbButtonsBeforeDelete = await shoppingItemComponentsPage.countDeleteButtons();
        await shoppingItemComponentsPage.clickOnLastDeleteButton();

        shoppingItemDeleteDialog = new ShoppingItemDeleteDialog();
        expect(await shoppingItemDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.shoppingItem.delete.question');
        await shoppingItemDeleteDialog.clickOnConfirmButton();

        expect(await shoppingItemComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
