import { element, by, ElementFinder } from 'protractor';

export class ShoppingItemComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('mha-shopping-item div table .btn-danger'));
  title = element.all(by.css('mha-shopping-item div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ShoppingItemUpdatePage {
  pageTitle = element(by.id('mha-shopping-item-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  labelInput = element(by.id('field_label'));
  lastUpdateInput = element(by.id('field_lastUpdate'));
  checkedInput = element(by.id('field_checked'));
  quantityInput = element(by.id('field_quantity'));
  unitSelect = element(by.id('field_unit'));

  shoppingCatalogSheldSelect = element(by.id('field_shoppingCatalogSheld'));
  shoppingCartSelect = element(by.id('field_shoppingCart'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLabelInput(label: string): Promise<void> {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput(): Promise<string> {
    return await this.labelInput.getAttribute('value');
  }

  async setLastUpdateInput(lastUpdate: string): Promise<void> {
    await this.lastUpdateInput.sendKeys(lastUpdate);
  }

  async getLastUpdateInput(): Promise<string> {
    return await this.lastUpdateInput.getAttribute('value');
  }

  getCheckedInput(): ElementFinder {
    return this.checkedInput;
  }

  async setQuantityInput(quantity: string): Promise<void> {
    await this.quantityInput.sendKeys(quantity);
  }

  async getQuantityInput(): Promise<string> {
    return await this.quantityInput.getAttribute('value');
  }

  async setUnitSelect(unit: string): Promise<void> {
    await this.unitSelect.sendKeys(unit);
  }

  async getUnitSelect(): Promise<string> {
    return await this.unitSelect.element(by.css('option:checked')).getText();
  }

  async unitSelectLastOption(): Promise<void> {
    await this.unitSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async shoppingCatalogSheldSelectLastOption(): Promise<void> {
    await this.shoppingCatalogSheldSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async shoppingCatalogSheldSelectOption(option: string): Promise<void> {
    await this.shoppingCatalogSheldSelect.sendKeys(option);
  }

  getShoppingCatalogSheldSelect(): ElementFinder {
    return this.shoppingCatalogSheldSelect;
  }

  async getShoppingCatalogSheldSelectedOption(): Promise<string> {
    return await this.shoppingCatalogSheldSelect.element(by.css('option:checked')).getText();
  }

  async shoppingCartSelectLastOption(): Promise<void> {
    await this.shoppingCartSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async shoppingCartSelectOption(option: string): Promise<void> {
    await this.shoppingCartSelect.sendKeys(option);
  }

  getShoppingCartSelect(): ElementFinder {
    return this.shoppingCartSelect;
  }

  async getShoppingCartSelectedOption(): Promise<string> {
    return await this.shoppingCartSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ShoppingItemDeleteDialog {
  private dialogTitle = element(by.id('mha-delete-shoppingItem-heading'));
  private confirmButton = element(by.id('mha-confirm-delete-shoppingItem'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
