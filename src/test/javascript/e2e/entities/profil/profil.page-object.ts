import { element, by, ElementFinder } from 'protractor';

export class ProfilComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('mha-profil div table .btn-danger'));
  title = element.all(by.css('mha-profil div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ProfilUpdatePage {
  pageTitle = element(by.id('mha-profil-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  displayNameInput = element(by.id('field_displayName'));
  lastUpdateInput = element(by.id('field_lastUpdate'));

  userSelect = element(by.id('field_user'));
  profilDataSelect = element(by.id('field_profilData'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDisplayNameInput(displayName: string): Promise<void> {
    await this.displayNameInput.sendKeys(displayName);
  }

  async getDisplayNameInput(): Promise<string> {
    return await this.displayNameInput.getAttribute('value');
  }

  async setLastUpdateInput(lastUpdate: string): Promise<void> {
    await this.lastUpdateInput.sendKeys(lastUpdate);
  }

  async getLastUpdateInput(): Promise<string> {
    return await this.lastUpdateInput.getAttribute('value');
  }

  async userSelectLastOption(): Promise<void> {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option: string): Promise<void> {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption(): Promise<string> {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async profilDataSelectLastOption(): Promise<void> {
    await this.profilDataSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async profilDataSelectOption(option: string): Promise<void> {
    await this.profilDataSelect.sendKeys(option);
  }

  getProfilDataSelect(): ElementFinder {
    return this.profilDataSelect;
  }

  async getProfilDataSelectedOption(): Promise<string> {
    return await this.profilDataSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ProfilDeleteDialog {
  private dialogTitle = element(by.id('mha-delete-profil-heading'));
  private confirmButton = element(by.id('mha-confirm-delete-profil'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
