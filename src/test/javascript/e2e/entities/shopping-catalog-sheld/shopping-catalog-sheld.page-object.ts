import { element, by, ElementFinder } from 'protractor';

export class ShoppingCatalogSheldComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('mha-shopping-catalog-sheld div table .btn-danger'));
  title = element.all(by.css('mha-shopping-catalog-sheld div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ShoppingCatalogSheldUpdatePage {
  pageTitle = element(by.id('mha-shopping-catalog-sheld-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  labelInput = element(by.id('field_label'));
  lastUpdateInput = element(by.id('field_lastUpdate'));
  isDefaultInput = element(by.id('field_isDefault'));
  isDeletedInput = element(by.id('field_isDeleted'));

  shoppingCatalogCartSelect = element(by.id('field_shoppingCatalogCart'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLabelInput(label: string): Promise<void> {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput(): Promise<string> {
    return await this.labelInput.getAttribute('value');
  }

  async setLastUpdateInput(lastUpdate: string): Promise<void> {
    await this.lastUpdateInput.sendKeys(lastUpdate);
  }

  async getLastUpdateInput(): Promise<string> {
    return await this.lastUpdateInput.getAttribute('value');
  }

  getIsDefaultInput(): ElementFinder {
    return this.isDefaultInput;
  }

  getIsDeletedInput(): ElementFinder {
    return this.isDeletedInput;
  }

  async shoppingCatalogCartSelectLastOption(): Promise<void> {
    await this.shoppingCatalogCartSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async shoppingCatalogCartSelectOption(option: string): Promise<void> {
    await this.shoppingCatalogCartSelect.sendKeys(option);
  }

  getShoppingCatalogCartSelect(): ElementFinder {
    return this.shoppingCatalogCartSelect;
  }

  async getShoppingCatalogCartSelectedOption(): Promise<string> {
    return await this.shoppingCatalogCartSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ShoppingCatalogSheldDeleteDialog {
  private dialogTitle = element(by.id('mha-delete-shoppingCatalogSheld-heading'));
  private confirmButton = element(by.id('mha-confirm-delete-shoppingCatalogSheld'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
