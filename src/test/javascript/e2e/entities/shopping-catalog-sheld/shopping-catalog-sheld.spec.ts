import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ShoppingCatalogSheldComponentsPage,
  /* ShoppingCatalogSheldDeleteDialog, */
  ShoppingCatalogSheldUpdatePage
} from './shopping-catalog-sheld.page-object';

const expect = chai.expect;

describe('ShoppingCatalogSheld e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shoppingCatalogSheldComponentsPage: ShoppingCatalogSheldComponentsPage;
  let shoppingCatalogSheldUpdatePage: ShoppingCatalogSheldUpdatePage;
  /* let shoppingCatalogSheldDeleteDialog: ShoppingCatalogSheldDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ShoppingCatalogShelds', async () => {
    await navBarPage.goToEntity('shopping-catalog-sheld');
    shoppingCatalogSheldComponentsPage = new ShoppingCatalogSheldComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogSheldComponentsPage.title), 5000);
    expect(await shoppingCatalogSheldComponentsPage.getTitle()).to.eq('mhawebApp.shoppingCatalogSheld.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(shoppingCatalogSheldComponentsPage.entities), ec.visibilityOf(shoppingCatalogSheldComponentsPage.noResult)),
      1000
    );
  });

  it('should load create ShoppingCatalogSheld page', async () => {
    await shoppingCatalogSheldComponentsPage.clickOnCreateButton();
    shoppingCatalogSheldUpdatePage = new ShoppingCatalogSheldUpdatePage();
    expect(await shoppingCatalogSheldUpdatePage.getPageTitle()).to.eq('mhawebApp.shoppingCatalogSheld.home.createOrEditLabel');
    await shoppingCatalogSheldUpdatePage.cancel();
  });

  /* it('should create and save ShoppingCatalogShelds', async () => {
        const nbButtonsBeforeCreate = await shoppingCatalogSheldComponentsPage.countDeleteButtons();

        await shoppingCatalogSheldComponentsPage.clickOnCreateButton();

        await promise.all([
            shoppingCatalogSheldUpdatePage.setLabelInput('label'),
            shoppingCatalogSheldUpdatePage.setLastUpdateInput('2000-12-31'),
            shoppingCatalogSheldUpdatePage.shoppingCatalogCartSelectLastOption(),
        ]);

        expect(await shoppingCatalogSheldUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        expect(await shoppingCatalogSheldUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');
        const selectedIsDefault = shoppingCatalogSheldUpdatePage.getIsDefaultInput();
        if (await selectedIsDefault.isSelected()) {
            await shoppingCatalogSheldUpdatePage.getIsDefaultInput().click();
            expect(await shoppingCatalogSheldUpdatePage.getIsDefaultInput().isSelected(), 'Expected isDefault not to be selected').to.be.false;
        } else {
            await shoppingCatalogSheldUpdatePage.getIsDefaultInput().click();
            expect(await shoppingCatalogSheldUpdatePage.getIsDefaultInput().isSelected(), 'Expected isDefault to be selected').to.be.true;
        }
        const selectedIsDeleted = shoppingCatalogSheldUpdatePage.getIsDeletedInput();
        if (await selectedIsDeleted.isSelected()) {
            await shoppingCatalogSheldUpdatePage.getIsDeletedInput().click();
            expect(await shoppingCatalogSheldUpdatePage.getIsDeletedInput().isSelected(), 'Expected isDeleted not to be selected').to.be.false;
        } else {
            await shoppingCatalogSheldUpdatePage.getIsDeletedInput().click();
            expect(await shoppingCatalogSheldUpdatePage.getIsDeletedInput().isSelected(), 'Expected isDeleted to be selected').to.be.true;
        }

        await shoppingCatalogSheldUpdatePage.save();
        expect(await shoppingCatalogSheldUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await shoppingCatalogSheldComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last ShoppingCatalogSheld', async () => {
        const nbButtonsBeforeDelete = await shoppingCatalogSheldComponentsPage.countDeleteButtons();
        await shoppingCatalogSheldComponentsPage.clickOnLastDeleteButton();

        shoppingCatalogSheldDeleteDialog = new ShoppingCatalogSheldDeleteDialog();
        expect(await shoppingCatalogSheldDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.shoppingCatalogSheld.delete.question');
        await shoppingCatalogSheldDeleteDialog.clickOnConfirmButton();

        expect(await shoppingCatalogSheldComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
