import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { WorkspaceComponentsPage, WorkspaceDeleteDialog, WorkspaceUpdatePage } from './workspace.page-object';

const expect = chai.expect;

describe('Workspace e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let workspaceComponentsPage: WorkspaceComponentsPage;
  let workspaceUpdatePage: WorkspaceUpdatePage;
  let workspaceDeleteDialog: WorkspaceDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Workspaces', async () => {
    await navBarPage.goToEntity('workspace');
    workspaceComponentsPage = new WorkspaceComponentsPage();
    await browser.wait(ec.visibilityOf(workspaceComponentsPage.title), 5000);
    expect(await workspaceComponentsPage.getTitle()).to.eq('mhawebApp.workspace.home.title');
    await browser.wait(ec.or(ec.visibilityOf(workspaceComponentsPage.entities), ec.visibilityOf(workspaceComponentsPage.noResult)), 1000);
  });

  it('should load create Workspace page', async () => {
    await workspaceComponentsPage.clickOnCreateButton();
    workspaceUpdatePage = new WorkspaceUpdatePage();
    expect(await workspaceUpdatePage.getPageTitle()).to.eq('mhawebApp.workspace.home.createOrEditLabel');
    await workspaceUpdatePage.cancel();
  });

  it('should create and save Workspaces', async () => {
    const nbButtonsBeforeCreate = await workspaceComponentsPage.countDeleteButtons();

    await workspaceComponentsPage.clickOnCreateButton();

    await promise.all([
      workspaceUpdatePage.setLabelInput('label'),
      workspaceUpdatePage.setLastUpdateInput('2000-12-31')
      // workspaceUpdatePage.ownerSelectLastOption(),
    ]);

    expect(await workspaceUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    expect(await workspaceUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

    await workspaceUpdatePage.save();
    expect(await workspaceUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await workspaceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Workspace', async () => {
    const nbButtonsBeforeDelete = await workspaceComponentsPage.countDeleteButtons();
    await workspaceComponentsPage.clickOnLastDeleteButton();

    workspaceDeleteDialog = new WorkspaceDeleteDialog();
    expect(await workspaceDeleteDialog.getDialogTitle()).to.eq('mhawebApp.workspace.delete.question');
    await workspaceDeleteDialog.clickOnConfirmButton();

    expect(await workspaceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
