import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  TodoListTemplateComponentsPage,
  /* TodoListTemplateDeleteDialog, */
  TodoListTemplateUpdatePage
} from './todo-list-template.page-object';

const expect = chai.expect;

describe('TodoListTemplate e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let todoListTemplateComponentsPage: TodoListTemplateComponentsPage;
  let todoListTemplateUpdatePage: TodoListTemplateUpdatePage;
  /* let todoListTemplateDeleteDialog: TodoListTemplateDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TodoListTemplates', async () => {
    await navBarPage.goToEntity('todo-list-template');
    todoListTemplateComponentsPage = new TodoListTemplateComponentsPage();
    await browser.wait(ec.visibilityOf(todoListTemplateComponentsPage.title), 5000);
    expect(await todoListTemplateComponentsPage.getTitle()).to.eq('mhawebApp.todoListTemplate.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(todoListTemplateComponentsPage.entities), ec.visibilityOf(todoListTemplateComponentsPage.noResult)),
      1000
    );
  });

  it('should load create TodoListTemplate page', async () => {
    await todoListTemplateComponentsPage.clickOnCreateButton();
    todoListTemplateUpdatePage = new TodoListTemplateUpdatePage();
    expect(await todoListTemplateUpdatePage.getPageTitle()).to.eq('mhawebApp.todoListTemplate.home.createOrEditLabel');
    await todoListTemplateUpdatePage.cancel();
  });

  /* it('should create and save TodoListTemplates', async () => {
        const nbButtonsBeforeCreate = await todoListTemplateComponentsPage.countDeleteButtons();

        await todoListTemplateComponentsPage.clickOnCreateButton();

        await promise.all([
            todoListTemplateUpdatePage.setLabelInput('label'),
            todoListTemplateUpdatePage.setLastUpdateInput('2000-12-31'),
            // todoListTemplateUpdatePage.ownerSelectLastOption(),
            todoListTemplateUpdatePage.workspaceSelectLastOption(),
        ]);

        expect(await todoListTemplateUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        expect(await todoListTemplateUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

        await todoListTemplateUpdatePage.save();
        expect(await todoListTemplateUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await todoListTemplateComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last TodoListTemplate', async () => {
        const nbButtonsBeforeDelete = await todoListTemplateComponentsPage.countDeleteButtons();
        await todoListTemplateComponentsPage.clickOnLastDeleteButton();

        todoListTemplateDeleteDialog = new TodoListTemplateDeleteDialog();
        expect(await todoListTemplateDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.todoListTemplate.delete.question');
        await todoListTemplateDeleteDialog.clickOnConfirmButton();

        expect(await todoListTemplateComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
