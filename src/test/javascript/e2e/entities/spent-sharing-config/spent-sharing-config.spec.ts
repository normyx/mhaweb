import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  SpentSharingConfigComponentsPage,
  /* SpentSharingConfigDeleteDialog, */
  SpentSharingConfigUpdatePage
} from './spent-sharing-config.page-object';

const expect = chai.expect;

describe('SpentSharingConfig e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let spentSharingConfigComponentsPage: SpentSharingConfigComponentsPage;
  let spentSharingConfigUpdatePage: SpentSharingConfigUpdatePage;
  /* let spentSharingConfigDeleteDialog: SpentSharingConfigDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load SpentSharingConfigs', async () => {
    await navBarPage.goToEntity('spent-sharing-config');
    spentSharingConfigComponentsPage = new SpentSharingConfigComponentsPage();
    await browser.wait(ec.visibilityOf(spentSharingConfigComponentsPage.title), 5000);
    expect(await spentSharingConfigComponentsPage.getTitle()).to.eq('mhawebApp.spentSharingConfig.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(spentSharingConfigComponentsPage.entities), ec.visibilityOf(spentSharingConfigComponentsPage.noResult)),
      1000
    );
  });

  it('should load create SpentSharingConfig page', async () => {
    await spentSharingConfigComponentsPage.clickOnCreateButton();
    spentSharingConfigUpdatePage = new SpentSharingConfigUpdatePage();
    expect(await spentSharingConfigUpdatePage.getPageTitle()).to.eq('mhawebApp.spentSharingConfig.home.createOrEditLabel');
    await spentSharingConfigUpdatePage.cancel();
  });

  /* it('should create and save SpentSharingConfigs', async () => {
        const nbButtonsBeforeCreate = await spentSharingConfigComponentsPage.countDeleteButtons();

        await spentSharingConfigComponentsPage.clickOnCreateButton();

        await promise.all([
            spentSharingConfigUpdatePage.setShareInput('5'),
            spentSharingConfigUpdatePage.setLastUpdateInput('2000-12-31'),
            spentSharingConfigUpdatePage.spentConfigSelectLastOption(),
            spentSharingConfigUpdatePage.profilSelectLastOption(),
        ]);

        expect(await spentSharingConfigUpdatePage.getShareInput()).to.eq('5', 'Expected share value to be equals to 5');
        expect(await spentSharingConfigUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

        await spentSharingConfigUpdatePage.save();
        expect(await spentSharingConfigUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await spentSharingConfigComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last SpentSharingConfig', async () => {
        const nbButtonsBeforeDelete = await spentSharingConfigComponentsPage.countDeleteButtons();
        await spentSharingConfigComponentsPage.clickOnLastDeleteButton();

        spentSharingConfigDeleteDialog = new SpentSharingConfigDeleteDialog();
        expect(await spentSharingConfigDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.spentSharingConfig.delete.question');
        await spentSharingConfigDeleteDialog.clickOnConfirmButton();

        expect(await spentSharingConfigComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
