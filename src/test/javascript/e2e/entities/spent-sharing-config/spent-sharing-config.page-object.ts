import { element, by, ElementFinder } from 'protractor';

export class SpentSharingConfigComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('mha-spent-sharing-config div table .btn-danger'));
  title = element.all(by.css('mha-spent-sharing-config div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentSharingConfigUpdatePage {
  pageTitle = element(by.id('mha-spent-sharing-config-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  shareInput = element(by.id('field_share'));
  lastUpdateInput = element(by.id('field_lastUpdate'));

  spentConfigSelect = element(by.id('field_spentConfig'));
  profilSelect = element(by.id('field_profil'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setShareInput(share: string): Promise<void> {
    await this.shareInput.sendKeys(share);
  }

  async getShareInput(): Promise<string> {
    return await this.shareInput.getAttribute('value');
  }

  async setLastUpdateInput(lastUpdate: string): Promise<void> {
    await this.lastUpdateInput.sendKeys(lastUpdate);
  }

  async getLastUpdateInput(): Promise<string> {
    return await this.lastUpdateInput.getAttribute('value');
  }

  async spentConfigSelectLastOption(): Promise<void> {
    await this.spentConfigSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async spentConfigSelectOption(option: string): Promise<void> {
    await this.spentConfigSelect.sendKeys(option);
  }

  getSpentConfigSelect(): ElementFinder {
    return this.spentConfigSelect;
  }

  async getSpentConfigSelectedOption(): Promise<string> {
    return await this.spentConfigSelect.element(by.css('option:checked')).getText();
  }

  async profilSelectLastOption(): Promise<void> {
    await this.profilSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async profilSelectOption(option: string): Promise<void> {
    await this.profilSelect.sendKeys(option);
  }

  getProfilSelect(): ElementFinder {
    return this.profilSelect;
  }

  async getProfilSelectedOption(): Promise<string> {
    return await this.profilSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentSharingConfigDeleteDialog {
  private dialogTitle = element(by.id('mha-delete-spentSharingConfig-heading'));
  private confirmButton = element(by.id('mha-confirm-delete-spentSharingConfig'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
