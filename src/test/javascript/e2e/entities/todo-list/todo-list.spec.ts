import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  TodoListComponentsPage,
  /* TodoListDeleteDialog, */
  TodoListUpdatePage
} from './todo-list.page-object';

const expect = chai.expect;

describe('TodoList e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let todoListComponentsPage: TodoListComponentsPage;
  let todoListUpdatePage: TodoListUpdatePage;
  /* let todoListDeleteDialog: TodoListDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TodoLists', async () => {
    await navBarPage.goToEntity('todo-list');
    todoListComponentsPage = new TodoListComponentsPage();
    await browser.wait(ec.visibilityOf(todoListComponentsPage.title), 5000);
    expect(await todoListComponentsPage.getTitle()).to.eq('mhawebApp.todoList.home.title');
    await browser.wait(ec.or(ec.visibilityOf(todoListComponentsPage.entities), ec.visibilityOf(todoListComponentsPage.noResult)), 1000);
  });

  it('should load create TodoList page', async () => {
    await todoListComponentsPage.clickOnCreateButton();
    todoListUpdatePage = new TodoListUpdatePage();
    expect(await todoListUpdatePage.getPageTitle()).to.eq('mhawebApp.todoList.home.createOrEditLabel');
    await todoListUpdatePage.cancel();
  });

  /* it('should create and save TodoLists', async () => {
        const nbButtonsBeforeCreate = await todoListComponentsPage.countDeleteButtons();

        await todoListComponentsPage.clickOnCreateButton();

        await promise.all([
            todoListUpdatePage.setLabelInput('label'),
            todoListUpdatePage.setLastUpdateInput('2000-12-31'),
            // todoListUpdatePage.ownerSelectLastOption(),
            todoListUpdatePage.workspaceSelectLastOption(),
        ]);

        expect(await todoListUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        expect(await todoListUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

        await todoListUpdatePage.save();
        expect(await todoListUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await todoListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last TodoList', async () => {
        const nbButtonsBeforeDelete = await todoListComponentsPage.countDeleteButtons();
        await todoListComponentsPage.clickOnLastDeleteButton();

        todoListDeleteDialog = new TodoListDeleteDialog();
        expect(await todoListDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.todoList.delete.question');
        await todoListDeleteDialog.clickOnConfirmButton();

        expect(await todoListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
