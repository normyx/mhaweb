import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TaskProjectComponentsPage, TaskProjectDeleteDialog, TaskProjectUpdatePage } from './task-project.page-object';

const expect = chai.expect;

describe('TaskProject e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let taskProjectComponentsPage: TaskProjectComponentsPage;
  let taskProjectUpdatePage: TaskProjectUpdatePage;
  let taskProjectDeleteDialog: TaskProjectDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TaskProjects', async () => {
    await navBarPage.goToEntity('task-project');
    taskProjectComponentsPage = new TaskProjectComponentsPage();
    await browser.wait(ec.visibilityOf(taskProjectComponentsPage.title), 5000);
    expect(await taskProjectComponentsPage.getTitle()).to.eq('mhawebApp.taskProject.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(taskProjectComponentsPage.entities), ec.visibilityOf(taskProjectComponentsPage.noResult)),
      1000
    );
  });

  it('should load create TaskProject page', async () => {
    await taskProjectComponentsPage.clickOnCreateButton();
    taskProjectUpdatePage = new TaskProjectUpdatePage();
    expect(await taskProjectUpdatePage.getPageTitle()).to.eq('mhawebApp.taskProject.home.createOrEditLabel');
    await taskProjectUpdatePage.cancel();
  });

  it('should create and save TaskProjects', async () => {
    const nbButtonsBeforeCreate = await taskProjectComponentsPage.countDeleteButtons();

    await taskProjectComponentsPage.clickOnCreateButton();

    await promise.all([
      taskProjectUpdatePage.setLabelInput('label'),
      taskProjectUpdatePage.setLastUpdateInput('2000-12-31'),
      // taskProjectUpdatePage.ownerSelectLastOption(),
      taskProjectUpdatePage.workspaceSelectLastOption()
    ]);

    expect(await taskProjectUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    expect(await taskProjectUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

    await taskProjectUpdatePage.save();
    expect(await taskProjectUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await taskProjectComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last TaskProject', async () => {
    const nbButtonsBeforeDelete = await taskProjectComponentsPage.countDeleteButtons();
    await taskProjectComponentsPage.clickOnLastDeleteButton();

    taskProjectDeleteDialog = new TaskProjectDeleteDialog();
    expect(await taskProjectDeleteDialog.getDialogTitle()).to.eq('mhawebApp.taskProject.delete.question');
    await taskProjectDeleteDialog.clickOnConfirmButton();

    expect(await taskProjectComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
