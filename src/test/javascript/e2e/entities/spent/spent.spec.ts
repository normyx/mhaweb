import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SpentComponentsPage, SpentDeleteDialog, SpentUpdatePage } from './spent.page-object';

const expect = chai.expect;

describe('Spent e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let spentComponentsPage: SpentComponentsPage;
  let spentUpdatePage: SpentUpdatePage;
  let spentDeleteDialog: SpentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Spents', async () => {
    await navBarPage.goToEntity('spent');
    spentComponentsPage = new SpentComponentsPage();
    await browser.wait(ec.visibilityOf(spentComponentsPage.title), 5000);
    expect(await spentComponentsPage.getTitle()).to.eq('mhawebApp.spent.home.title');
    await browser.wait(ec.or(ec.visibilityOf(spentComponentsPage.entities), ec.visibilityOf(spentComponentsPage.noResult)), 1000);
  });

  it('should load create Spent page', async () => {
    await spentComponentsPage.clickOnCreateButton();
    spentUpdatePage = new SpentUpdatePage();
    expect(await spentUpdatePage.getPageTitle()).to.eq('mhawebApp.spent.home.createOrEditLabel');
    await spentUpdatePage.cancel();
  });

  it('should create and save Spents', async () => {
    const nbButtonsBeforeCreate = await spentComponentsPage.countDeleteButtons();

    await spentComponentsPage.clickOnCreateButton();

    await promise.all([
      spentUpdatePage.setLabelInput('label'),
      spentUpdatePage.setDescriptionInput('description'),
      spentUpdatePage.setAmountInput('5'),
      spentUpdatePage.setSpentDateInput('2000-12-31'),
      spentUpdatePage.setLastUpdateInput('2000-12-31'),
      spentUpdatePage.walletSelectLastOption(),
      spentUpdatePage.spenderSelectLastOption()
    ]);

    expect(await spentUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    expect(await spentUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');
    expect(await spentUpdatePage.getAmountInput()).to.eq('5', 'Expected amount value to be equals to 5');
    expect(await spentUpdatePage.getSpentDateInput()).to.eq('2000-12-31', 'Expected spentDate value to be equals to 2000-12-31');
    const selectedConfirmed = spentUpdatePage.getConfirmedInput();
    if (await selectedConfirmed.isSelected()) {
      await spentUpdatePage.getConfirmedInput().click();
      expect(await spentUpdatePage.getConfirmedInput().isSelected(), 'Expected confirmed not to be selected').to.be.false;
    } else {
      await spentUpdatePage.getConfirmedInput().click();
      expect(await spentUpdatePage.getConfirmedInput().isSelected(), 'Expected confirmed to be selected').to.be.true;
    }
    expect(await spentUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

    await spentUpdatePage.save();
    expect(await spentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await spentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Spent', async () => {
    const nbButtonsBeforeDelete = await spentComponentsPage.countDeleteButtons();
    await spentComponentsPage.clickOnLastDeleteButton();

    spentDeleteDialog = new SpentDeleteDialog();
    expect(await spentDeleteDialog.getDialogTitle()).to.eq('mhawebApp.spent.delete.question');
    await spentDeleteDialog.clickOnConfirmButton();

    expect(await spentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
