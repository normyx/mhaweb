import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ShoppingCatalogCartComponentsPage,
  ShoppingCatalogCartDeleteDialog,
  ShoppingCatalogCartUpdatePage
} from './shopping-catalog-cart.page-object';

const expect = chai.expect;

describe('ShoppingCatalogCart e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shoppingCatalogCartComponentsPage: ShoppingCatalogCartComponentsPage;
  let shoppingCatalogCartUpdatePage: ShoppingCatalogCartUpdatePage;
  let shoppingCatalogCartDeleteDialog: ShoppingCatalogCartDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ShoppingCatalogCarts', async () => {
    await navBarPage.goToEntity('shopping-catalog-cart');
    shoppingCatalogCartComponentsPage = new ShoppingCatalogCartComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogCartComponentsPage.title), 5000);
    expect(await shoppingCatalogCartComponentsPage.getTitle()).to.eq('mhawebApp.shoppingCatalogCart.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(shoppingCatalogCartComponentsPage.entities), ec.visibilityOf(shoppingCatalogCartComponentsPage.noResult)),
      1000
    );
  });

  it('should load create ShoppingCatalogCart page', async () => {
    await shoppingCatalogCartComponentsPage.clickOnCreateButton();
    shoppingCatalogCartUpdatePage = new ShoppingCatalogCartUpdatePage();
    expect(await shoppingCatalogCartUpdatePage.getPageTitle()).to.eq('mhawebApp.shoppingCatalogCart.home.createOrEditLabel');
    await shoppingCatalogCartUpdatePage.cancel();
  });

  it('should create and save ShoppingCatalogCarts', async () => {
    const nbButtonsBeforeCreate = await shoppingCatalogCartComponentsPage.countDeleteButtons();

    await shoppingCatalogCartComponentsPage.clickOnCreateButton();

    await promise.all([
      shoppingCatalogCartUpdatePage.setLabelInput('label'),
      shoppingCatalogCartUpdatePage.setLastUpdateInput('2000-12-31')
    ]);

    expect(await shoppingCatalogCartUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    expect(await shoppingCatalogCartUpdatePage.getLastUpdateInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdate value to be equals to 2000-12-31'
    );

    await shoppingCatalogCartUpdatePage.save();
    expect(await shoppingCatalogCartUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await shoppingCatalogCartComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last ShoppingCatalogCart', async () => {
    const nbButtonsBeforeDelete = await shoppingCatalogCartComponentsPage.countDeleteButtons();
    await shoppingCatalogCartComponentsPage.clickOnLastDeleteButton();

    shoppingCatalogCartDeleteDialog = new ShoppingCatalogCartDeleteDialog();
    expect(await shoppingCatalogCartDeleteDialog.getDialogTitle()).to.eq('mhawebApp.shoppingCatalogCart.delete.question');
    await shoppingCatalogCartDeleteDialog.clickOnConfirmButton();

    expect(await shoppingCatalogCartComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
