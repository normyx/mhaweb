import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  TodoTemplateComponentsPage,
  /* TodoTemplateDeleteDialog, */
  TodoTemplateUpdatePage
} from './todo-template.page-object';

const expect = chai.expect;

describe('TodoTemplate e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let todoTemplateComponentsPage: TodoTemplateComponentsPage;
  let todoTemplateUpdatePage: TodoTemplateUpdatePage;
  /* let todoTemplateDeleteDialog: TodoTemplateDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TodoTemplates', async () => {
    await navBarPage.goToEntity('todo-template');
    todoTemplateComponentsPage = new TodoTemplateComponentsPage();
    await browser.wait(ec.visibilityOf(todoTemplateComponentsPage.title), 5000);
    expect(await todoTemplateComponentsPage.getTitle()).to.eq('mhawebApp.todoTemplate.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(todoTemplateComponentsPage.entities), ec.visibilityOf(todoTemplateComponentsPage.noResult)),
      1000
    );
  });

  it('should load create TodoTemplate page', async () => {
    await todoTemplateComponentsPage.clickOnCreateButton();
    todoTemplateUpdatePage = new TodoTemplateUpdatePage();
    expect(await todoTemplateUpdatePage.getPageTitle()).to.eq('mhawebApp.todoTemplate.home.createOrEditLabel');
    await todoTemplateUpdatePage.cancel();
  });

  /* it('should create and save TodoTemplates', async () => {
        const nbButtonsBeforeCreate = await todoTemplateComponentsPage.countDeleteButtons();

        await todoTemplateComponentsPage.clickOnCreateButton();

        await promise.all([
            todoTemplateUpdatePage.setLabelInput('label'),
            todoTemplateUpdatePage.setLastUpdateInput('2000-12-31'),
            todoTemplateUpdatePage.todoListTemplateSelectLastOption(),
        ]);

        expect(await todoTemplateUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        expect(await todoTemplateUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

        await todoTemplateUpdatePage.save();
        expect(await todoTemplateUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await todoTemplateComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last TodoTemplate', async () => {
        const nbButtonsBeforeDelete = await todoTemplateComponentsPage.countDeleteButtons();
        await todoTemplateComponentsPage.clickOnLastDeleteButton();

        todoTemplateDeleteDialog = new TodoTemplateDeleteDialog();
        expect(await todoTemplateDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.todoTemplate.delete.question');
        await todoTemplateDeleteDialog.clickOnConfirmButton();

        expect(await todoTemplateComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
