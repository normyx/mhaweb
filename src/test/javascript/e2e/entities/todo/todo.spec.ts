import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  TodoComponentsPage,
  /* TodoDeleteDialog, */
  TodoUpdatePage
} from './todo.page-object';

const expect = chai.expect;

describe('Todo e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let todoComponentsPage: TodoComponentsPage;
  let todoUpdatePage: TodoUpdatePage;
  /* let todoDeleteDialog: TodoDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Todos', async () => {
    await navBarPage.goToEntity('todo');
    todoComponentsPage = new TodoComponentsPage();
    await browser.wait(ec.visibilityOf(todoComponentsPage.title), 5000);
    expect(await todoComponentsPage.getTitle()).to.eq('mhawebApp.todo.home.title');
    await browser.wait(ec.or(ec.visibilityOf(todoComponentsPage.entities), ec.visibilityOf(todoComponentsPage.noResult)), 1000);
  });

  it('should load create Todo page', async () => {
    await todoComponentsPage.clickOnCreateButton();
    todoUpdatePage = new TodoUpdatePage();
    expect(await todoUpdatePage.getPageTitle()).to.eq('mhawebApp.todo.home.createOrEditLabel');
    await todoUpdatePage.cancel();
  });

  /* it('should create and save Todos', async () => {
        const nbButtonsBeforeCreate = await todoComponentsPage.countDeleteButtons();

        await todoComponentsPage.clickOnCreateButton();

        await promise.all([
            todoUpdatePage.setLabelInput('label'),
            todoUpdatePage.setLastUpdateInput('2000-12-31'),
            todoUpdatePage.todoListSelectLastOption(),
        ]);

        expect(await todoUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
        const selectedDone = todoUpdatePage.getDoneInput();
        if (await selectedDone.isSelected()) {
            await todoUpdatePage.getDoneInput().click();
            expect(await todoUpdatePage.getDoneInput().isSelected(), 'Expected done not to be selected').to.be.false;
        } else {
            await todoUpdatePage.getDoneInput().click();
            expect(await todoUpdatePage.getDoneInput().isSelected(), 'Expected done to be selected').to.be.true;
        }
        expect(await todoUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

        await todoUpdatePage.save();
        expect(await todoUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await todoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Todo', async () => {
        const nbButtonsBeforeDelete = await todoComponentsPage.countDeleteButtons();
        await todoComponentsPage.clickOnLastDeleteButton();

        todoDeleteDialog = new TodoDeleteDialog();
        expect(await todoDeleteDialog.getDialogTitle())
            .to.eq('mhawebApp.todo.delete.question');
        await todoDeleteDialog.clickOnConfirmButton();

        expect(await todoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
