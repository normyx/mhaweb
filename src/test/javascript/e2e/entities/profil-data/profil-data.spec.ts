import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ProfilDataComponentsPage, ProfilDataDeleteDialog, ProfilDataUpdatePage } from './profil-data.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('ProfilData e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let profilDataComponentsPage: ProfilDataComponentsPage;
  let profilDataUpdatePage: ProfilDataUpdatePage;
  let profilDataDeleteDialog: ProfilDataDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ProfilData', async () => {
    await navBarPage.goToEntity('profil-data');
    profilDataComponentsPage = new ProfilDataComponentsPage();
    await browser.wait(ec.visibilityOf(profilDataComponentsPage.title), 5000);
    expect(await profilDataComponentsPage.getTitle()).to.eq('mhawebApp.profilData.home.title');
    await browser.wait(ec.or(ec.visibilityOf(profilDataComponentsPage.entities), ec.visibilityOf(profilDataComponentsPage.noResult)), 1000);
  });

  it('should load create ProfilData page', async () => {
    await profilDataComponentsPage.clickOnCreateButton();
    profilDataUpdatePage = new ProfilDataUpdatePage();
    expect(await profilDataUpdatePage.getPageTitle()).to.eq('mhawebApp.profilData.home.createOrEditLabel');
    await profilDataUpdatePage.cancel();
  });

  it('should create and save ProfilData', async () => {
    const nbButtonsBeforeCreate = await profilDataComponentsPage.countDeleteButtons();

    await profilDataComponentsPage.clickOnCreateButton();

    await promise.all([profilDataUpdatePage.setPhotoInput(absolutePath), profilDataUpdatePage.setLastUpdateInput('2000-12-31')]);

    expect(await profilDataUpdatePage.getPhotoInput()).to.endsWith(
      fileNameToUpload,
      'Expected Photo value to be end with ' + fileNameToUpload
    );
    expect(await profilDataUpdatePage.getLastUpdateInput()).to.eq('2000-12-31', 'Expected lastUpdate value to be equals to 2000-12-31');

    await profilDataUpdatePage.save();
    expect(await profilDataUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await profilDataComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last ProfilData', async () => {
    const nbButtonsBeforeDelete = await profilDataComponentsPage.countDeleteButtons();
    await profilDataComponentsPage.clickOnLastDeleteButton();

    profilDataDeleteDialog = new ProfilDataDeleteDialog();
    expect(await profilDataDeleteDialog.getDialogTitle()).to.eq('mhawebApp.profilData.delete.question');
    await profilDataDeleteDialog.clickOnConfirmButton();

    expect(await profilDataComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
