package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ShoppingCatalogCartDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingCatalogCartDTO.class);
        ShoppingCatalogCartDTO shoppingCatalogCartDTO1 = new ShoppingCatalogCartDTO();
        shoppingCatalogCartDTO1.setId(1L);
        ShoppingCatalogCartDTO shoppingCatalogCartDTO2 = new ShoppingCatalogCartDTO();
        assertThat(shoppingCatalogCartDTO1).isNotEqualTo(shoppingCatalogCartDTO2);
        shoppingCatalogCartDTO2.setId(shoppingCatalogCartDTO1.getId());
        assertThat(shoppingCatalogCartDTO1).isEqualTo(shoppingCatalogCartDTO2);
        shoppingCatalogCartDTO2.setId(2L);
        assertThat(shoppingCatalogCartDTO1).isNotEqualTo(shoppingCatalogCartDTO2);
        shoppingCatalogCartDTO1.setId(null);
        assertThat(shoppingCatalogCartDTO1).isNotEqualTo(shoppingCatalogCartDTO2);
    }
}
