package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentSharingDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharingDTO.class);
        SpentSharingDTO spentSharingDTO1 = new SpentSharingDTO();
        spentSharingDTO1.setId(1L);
        SpentSharingDTO spentSharingDTO2 = new SpentSharingDTO();
        assertThat(spentSharingDTO1).isNotEqualTo(spentSharingDTO2);
        spentSharingDTO2.setId(spentSharingDTO1.getId());
        assertThat(spentSharingDTO1).isEqualTo(spentSharingDTO2);
        spentSharingDTO2.setId(2L);
        assertThat(spentSharingDTO1).isNotEqualTo(spentSharingDTO2);
        spentSharingDTO1.setId(null);
        assertThat(spentSharingDTO1).isNotEqualTo(spentSharingDTO2);
    }
}
