package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class TaskProjectDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TaskProjectDTO.class);
        TaskProjectDTO taskProjectDTO1 = new TaskProjectDTO();
        taskProjectDTO1.setId(1L);
        TaskProjectDTO taskProjectDTO2 = new TaskProjectDTO();
        assertThat(taskProjectDTO1).isNotEqualTo(taskProjectDTO2);
        taskProjectDTO2.setId(taskProjectDTO1.getId());
        assertThat(taskProjectDTO1).isEqualTo(taskProjectDTO2);
        taskProjectDTO2.setId(2L);
        assertThat(taskProjectDTO1).isNotEqualTo(taskProjectDTO2);
        taskProjectDTO1.setId(null);
        assertThat(taskProjectDTO1).isNotEqualTo(taskProjectDTO2);
    }
}
