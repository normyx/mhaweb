package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentConfigDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentConfigDTO.class);
        SpentConfigDTO spentConfigDTO1 = new SpentConfigDTO();
        spentConfigDTO1.setId(1L);
        SpentConfigDTO spentConfigDTO2 = new SpentConfigDTO();
        assertThat(spentConfigDTO1).isNotEqualTo(spentConfigDTO2);
        spentConfigDTO2.setId(spentConfigDTO1.getId());
        assertThat(spentConfigDTO1).isEqualTo(spentConfigDTO2);
        spentConfigDTO2.setId(2L);
        assertThat(spentConfigDTO1).isNotEqualTo(spentConfigDTO2);
        spentConfigDTO1.setId(null);
        assertThat(spentConfigDTO1).isNotEqualTo(spentConfigDTO2);
    }
}
