package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SpentMapperTest {

    private SpentMapper spentMapper;

    @BeforeEach
    public void setUp() {
        spentMapper = new SpentMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(spentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(spentMapper.fromId(null)).isNull();
    }
}
