package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TodoListMapperTest {

    private TodoListMapper todoListMapper;

    @BeforeEach
    public void setUp() {
        todoListMapper = new TodoListMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(todoListMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(todoListMapper.fromId(null)).isNull();
    }
}
