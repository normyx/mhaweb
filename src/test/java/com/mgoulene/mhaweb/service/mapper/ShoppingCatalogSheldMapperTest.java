package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ShoppingCatalogSheldMapperTest {

    private ShoppingCatalogSheldMapper shoppingCatalogSheldMapper;

    @BeforeEach
    public void setUp() {
        shoppingCatalogSheldMapper = new ShoppingCatalogSheldMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(shoppingCatalogSheldMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(shoppingCatalogSheldMapper.fromId(null)).isNull();
    }
}
