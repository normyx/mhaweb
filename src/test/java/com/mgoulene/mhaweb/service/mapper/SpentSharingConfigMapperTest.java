package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SpentSharingConfigMapperTest {

    private SpentSharingConfigMapper spentSharingConfigMapper;

    @BeforeEach
    public void setUp() {
        spentSharingConfigMapper = new SpentSharingConfigMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(spentSharingConfigMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(spentSharingConfigMapper.fromId(null)).isNull();
    }
}
