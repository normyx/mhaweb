package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class WorkspaceMapperTest {

    private WorkspaceMapper workspaceMapper;

    @BeforeEach
    public void setUp() {
        workspaceMapper = new WorkspaceMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(workspaceMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(workspaceMapper.fromId(null)).isNull();
    }
}
