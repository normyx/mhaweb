package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ProfilDataMapperTest {

    private ProfilDataMapper profilDataMapper;

    @BeforeEach
    public void setUp() {
        profilDataMapper = new ProfilDataMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(profilDataMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(profilDataMapper.fromId(null)).isNull();
    }
}
