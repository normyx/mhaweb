package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TodoTemplateMapperTest {

    private TodoTemplateMapper todoTemplateMapper;

    @BeforeEach
    public void setUp() {
        todoTemplateMapper = new TodoTemplateMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(todoTemplateMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(todoTemplateMapper.fromId(null)).isNull();
    }
}
