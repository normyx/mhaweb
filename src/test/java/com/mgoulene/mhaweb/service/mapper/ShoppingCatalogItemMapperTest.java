package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ShoppingCatalogItemMapperTest {

    private ShoppingCatalogItemMapper shoppingCatalogItemMapper;

    @BeforeEach
    public void setUp() {
        shoppingCatalogItemMapper = new ShoppingCatalogItemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(shoppingCatalogItemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(shoppingCatalogItemMapper.fromId(null)).isNull();
    }
}
