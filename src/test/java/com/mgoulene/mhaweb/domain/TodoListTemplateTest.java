package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class TodoListTemplateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TodoListTemplate.class);
        TodoListTemplate todoListTemplate1 = new TodoListTemplate();
        todoListTemplate1.setId(1L);
        TodoListTemplate todoListTemplate2 = new TodoListTemplate();
        todoListTemplate2.setId(todoListTemplate1.getId());
        assertThat(todoListTemplate1).isEqualTo(todoListTemplate2);
        todoListTemplate2.setId(2L);
        assertThat(todoListTemplate1).isNotEqualTo(todoListTemplate2);
        todoListTemplate1.setId(null);
        assertThat(todoListTemplate1).isNotEqualTo(todoListTemplate2);
    }
}
