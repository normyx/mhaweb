package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Spent.class);
        Spent spent1 = new Spent();
        spent1.setId(1L);
        Spent spent2 = new Spent();
        spent2.setId(spent1.getId());
        assertThat(spent1).isEqualTo(spent2);
        spent2.setId(2L);
        assertThat(spent1).isNotEqualTo(spent2);
        spent1.setId(null);
        assertThat(spent1).isNotEqualTo(spent2);
    }
}
