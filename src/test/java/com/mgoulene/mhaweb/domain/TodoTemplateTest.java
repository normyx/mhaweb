package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class TodoTemplateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TodoTemplate.class);
        TodoTemplate todoTemplate1 = new TodoTemplate();
        todoTemplate1.setId(1L);
        TodoTemplate todoTemplate2 = new TodoTemplate();
        todoTemplate2.setId(todoTemplate1.getId());
        assertThat(todoTemplate1).isEqualTo(todoTemplate2);
        todoTemplate2.setId(2L);
        assertThat(todoTemplate1).isNotEqualTo(todoTemplate2);
        todoTemplate1.setId(null);
        assertThat(todoTemplate1).isNotEqualTo(todoTemplate2);
    }
}
