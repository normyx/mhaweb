package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentSharingTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharing.class);
        SpentSharing spentSharing1 = new SpentSharing();
        spentSharing1.setId(1L);
        SpentSharing spentSharing2 = new SpentSharing();
        spentSharing2.setId(spentSharing1.getId());
        assertThat(spentSharing1).isEqualTo(spentSharing2);
        spentSharing2.setId(2L);
        assertThat(spentSharing1).isNotEqualTo(spentSharing2);
        spentSharing1.setId(null);
        assertThat(spentSharing1).isNotEqualTo(spentSharing2);
    }
}
