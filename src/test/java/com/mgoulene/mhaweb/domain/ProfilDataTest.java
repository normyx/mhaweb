package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ProfilDataTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProfilData.class);
        ProfilData profilData1 = new ProfilData();
        profilData1.setId(1L);
        ProfilData profilData2 = new ProfilData();
        profilData2.setId(profilData1.getId());
        assertThat(profilData1).isEqualTo(profilData2);
        profilData2.setId(2L);
        assertThat(profilData1).isNotEqualTo(profilData2);
        profilData1.setId(null);
        assertThat(profilData1).isNotEqualTo(profilData2);
    }
}
