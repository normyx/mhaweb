package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ShoppingCatalogSheldTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingCatalogSheld.class);
        ShoppingCatalogSheld shoppingCatalogSheld1 = new ShoppingCatalogSheld();
        shoppingCatalogSheld1.setId(1L);
        ShoppingCatalogSheld shoppingCatalogSheld2 = new ShoppingCatalogSheld();
        shoppingCatalogSheld2.setId(shoppingCatalogSheld1.getId());
        assertThat(shoppingCatalogSheld1).isEqualTo(shoppingCatalogSheld2);
        shoppingCatalogSheld2.setId(2L);
        assertThat(shoppingCatalogSheld1).isNotEqualTo(shoppingCatalogSheld2);
        shoppingCatalogSheld1.setId(null);
        assertThat(shoppingCatalogSheld1).isNotEqualTo(shoppingCatalogSheld2);
    }
}
