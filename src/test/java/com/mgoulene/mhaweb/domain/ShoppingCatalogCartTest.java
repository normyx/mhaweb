package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ShoppingCatalogCartTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingCatalogCart.class);
        ShoppingCatalogCart shoppingCatalogCart1 = new ShoppingCatalogCart();
        shoppingCatalogCart1.setId(1L);
        ShoppingCatalogCart shoppingCatalogCart2 = new ShoppingCatalogCart();
        shoppingCatalogCart2.setId(shoppingCatalogCart1.getId());
        assertThat(shoppingCatalogCart1).isEqualTo(shoppingCatalogCart2);
        shoppingCatalogCart2.setId(2L);
        assertThat(shoppingCatalogCart1).isNotEqualTo(shoppingCatalogCart2);
        shoppingCatalogCart1.setId(null);
        assertThat(shoppingCatalogCart1).isNotEqualTo(shoppingCatalogCart2);
    }
}
