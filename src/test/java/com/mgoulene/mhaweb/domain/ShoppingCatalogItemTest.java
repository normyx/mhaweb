package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ShoppingCatalogItemTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingCatalogItem.class);
        ShoppingCatalogItem shoppingCatalogItem1 = new ShoppingCatalogItem();
        shoppingCatalogItem1.setId(1L);
        ShoppingCatalogItem shoppingCatalogItem2 = new ShoppingCatalogItem();
        shoppingCatalogItem2.setId(shoppingCatalogItem1.getId());
        assertThat(shoppingCatalogItem1).isEqualTo(shoppingCatalogItem2);
        shoppingCatalogItem2.setId(2L);
        assertThat(shoppingCatalogItem1).isNotEqualTo(shoppingCatalogItem2);
        shoppingCatalogItem1.setId(null);
        assertThat(shoppingCatalogItem1).isNotEqualTo(shoppingCatalogItem2);
    }
}
