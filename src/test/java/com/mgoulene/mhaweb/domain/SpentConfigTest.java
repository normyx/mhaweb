package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentConfigTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentConfig.class);
        SpentConfig spentConfig1 = new SpentConfig();
        spentConfig1.setId(1L);
        SpentConfig spentConfig2 = new SpentConfig();
        spentConfig2.setId(spentConfig1.getId());
        assertThat(spentConfig1).isEqualTo(spentConfig2);
        spentConfig2.setId(2L);
        assertThat(spentConfig1).isNotEqualTo(spentConfig2);
        spentConfig1.setId(null);
        assertThat(spentConfig1).isNotEqualTo(spentConfig2);
    }
}
