package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class TaskProjectTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TaskProject.class);
        TaskProject taskProject1 = new TaskProject();
        taskProject1.setId(1L);
        TaskProject taskProject2 = new TaskProject();
        taskProject2.setId(taskProject1.getId());
        assertThat(taskProject1).isEqualTo(taskProject2);
        taskProject2.setId(2L);
        assertThat(taskProject1).isNotEqualTo(taskProject2);
        taskProject1.setId(null);
        assertThat(taskProject1).isNotEqualTo(taskProject2);
    }
}
