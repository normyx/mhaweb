package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.Wallet;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.domain.Workspace;
import com.mgoulene.mhaweb.repository.WalletRepository;
import com.mgoulene.mhaweb.service.WalletService;
import com.mgoulene.mhaweb.service.dto.WalletDTO;
import com.mgoulene.mhaweb.service.mapper.WalletMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.WalletCriteria;
import com.mgoulene.mhaweb.service.WalletQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WalletResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class WalletResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private WalletRepository walletRepository;

    @Mock
    private WalletRepository walletRepositoryMock;

    @Autowired
    private WalletMapper walletMapper;

    @Mock
    private WalletService walletServiceMock;

    @Autowired
    private WalletService walletService;

    @Autowired
    private WalletQueryService walletQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restWalletMockMvc;

    private Wallet wallet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WalletResource walletResource = new WalletResource(walletService, walletQueryService);
        this.restWalletMockMvc = MockMvcBuilders.standaloneSetup(walletResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Wallet createEntity(EntityManager em) {
        Wallet wallet = new Wallet()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        // Add required entity
        Workspace workspace;
        if (TestUtil.findAll(em, Workspace.class).isEmpty()) {
            workspace = WorkspaceResourceIT.createEntity(em);
            em.persist(workspace);
            em.flush();
        } else {
            workspace = TestUtil.findAll(em, Workspace.class).get(0);
        }
        wallet.setWorkspace(workspace);
        return wallet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Wallet createUpdatedEntity(EntityManager em) {
        Wallet wallet = new Wallet()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        // Add required entity
        Workspace workspace;
        if (TestUtil.findAll(em, Workspace.class).isEmpty()) {
            workspace = WorkspaceResourceIT.createUpdatedEntity(em);
            em.persist(workspace);
            em.flush();
        } else {
            workspace = TestUtil.findAll(em, Workspace.class).get(0);
        }
        wallet.setWorkspace(workspace);
        return wallet;
    }

    @BeforeEach
    public void initTest() {
        wallet = createEntity(em);
    }

    @Test
    @Transactional
    public void createWallet() throws Exception {
        int databaseSizeBeforeCreate = walletRepository.findAll().size();

        // Create the Wallet
        WalletDTO walletDTO = walletMapper.toDto(wallet);
        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isCreated());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeCreate + 1);
        Wallet testWallet = walletList.get(walletList.size() - 1);
        assertThat(testWallet.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testWallet.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createWalletWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = walletRepository.findAll().size();

        // Create the Wallet with an existing ID
        wallet.setId(1L);
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = walletRepository.findAll().size();
        // set the field null
        wallet.setLabel(null);

        // Create the Wallet, which fails.
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = walletRepository.findAll().size();
        // set the field null
        wallet.setLastUpdate(null);

        // Create the Wallet, which fails.
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        restWalletMockMvc.perform(post("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllWallets() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList
        restWalletMockMvc.perform(get("/api/wallets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wallet.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllWalletsWithEagerRelationshipsIsEnabled() throws Exception {
        WalletResource walletResource = new WalletResource(walletServiceMock, walletQueryService);
        when(walletServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restWalletMockMvc = MockMvcBuilders.standaloneSetup(walletResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restWalletMockMvc.perform(get("/api/wallets?eagerload=true"))
        .andExpect(status().isOk());

        verify(walletServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllWalletsWithEagerRelationshipsIsNotEnabled() throws Exception {
        WalletResource walletResource = new WalletResource(walletServiceMock, walletQueryService);
            when(walletServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restWalletMockMvc = MockMvcBuilders.standaloneSetup(walletResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restWalletMockMvc.perform(get("/api/wallets?eagerload=true"))
        .andExpect(status().isOk());

            verify(walletServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getWallet() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get the wallet
        restWalletMockMvc.perform(get("/api/wallets/{id}", wallet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(wallet.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getWalletsByIdFiltering() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        Long id = wallet.getId();

        defaultWalletShouldBeFound("id.equals=" + id);
        defaultWalletShouldNotBeFound("id.notEquals=" + id);

        defaultWalletShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultWalletShouldNotBeFound("id.greaterThan=" + id);

        defaultWalletShouldBeFound("id.lessThanOrEqual=" + id);
        defaultWalletShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllWalletsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where label equals to DEFAULT_LABEL
        defaultWalletShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the walletList where label equals to UPDATED_LABEL
        defaultWalletShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWalletsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where label not equals to DEFAULT_LABEL
        defaultWalletShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the walletList where label not equals to UPDATED_LABEL
        defaultWalletShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWalletsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultWalletShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the walletList where label equals to UPDATED_LABEL
        defaultWalletShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWalletsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where label is not null
        defaultWalletShouldBeFound("label.specified=true");

        // Get all the walletList where label is null
        defaultWalletShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllWalletsByLabelContainsSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where label contains DEFAULT_LABEL
        defaultWalletShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the walletList where label contains UPDATED_LABEL
        defaultWalletShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWalletsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where label does not contain DEFAULT_LABEL
        defaultWalletShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the walletList where label does not contain UPDATED_LABEL
        defaultWalletShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultWalletShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the walletList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultWalletShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultWalletShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the walletList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultWalletShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultWalletShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the walletList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultWalletShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate is not null
        defaultWalletShouldBeFound("lastUpdate.specified=true");

        // Get all the walletList where lastUpdate is null
        defaultWalletShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultWalletShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the walletList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultWalletShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultWalletShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the walletList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultWalletShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultWalletShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the walletList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultWalletShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWalletsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        // Get all the walletList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultWalletShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the walletList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultWalletShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllWalletsByOwnerIsEqualToSomething() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);
        Profil owner = ProfilResourceIT.createEntity(em);
        em.persist(owner);
        em.flush();
        wallet.addOwner(owner);
        walletRepository.saveAndFlush(wallet);
        Long ownerId = owner.getId();

        // Get all the walletList where owner equals to ownerId
        defaultWalletShouldBeFound("ownerId.equals=" + ownerId);

        // Get all the walletList where owner equals to ownerId + 1
        defaultWalletShouldNotBeFound("ownerId.equals=" + (ownerId + 1));
    }


    @Test
    @Transactional
    public void getAllWalletsByWorkspaceIsEqualToSomething() throws Exception {
        // Get already existing entity
        Workspace workspace = wallet.getWorkspace();
        walletRepository.saveAndFlush(wallet);
        Long workspaceId = workspace.getId();

        // Get all the walletList where workspace equals to workspaceId
        defaultWalletShouldBeFound("workspaceId.equals=" + workspaceId);

        // Get all the walletList where workspace equals to workspaceId + 1
        defaultWalletShouldNotBeFound("workspaceId.equals=" + (workspaceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultWalletShouldBeFound(String filter) throws Exception {
        restWalletMockMvc.perform(get("/api/wallets?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wallet.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restWalletMockMvc.perform(get("/api/wallets/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultWalletShouldNotBeFound(String filter) throws Exception {
        restWalletMockMvc.perform(get("/api/wallets?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restWalletMockMvc.perform(get("/api/wallets/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingWallet() throws Exception {
        // Get the wallet
        restWalletMockMvc.perform(get("/api/wallets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWallet() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        int databaseSizeBeforeUpdate = walletRepository.findAll().size();

        // Update the wallet
        Wallet updatedWallet = walletRepository.findById(wallet.getId()).get();
        // Disconnect from session so that the updates on updatedWallet are not directly saved in db
        em.detach(updatedWallet);
        updatedWallet
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        WalletDTO walletDTO = walletMapper.toDto(updatedWallet);

        restWalletMockMvc.perform(put("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isOk());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeUpdate);
        Wallet testWallet = walletList.get(walletList.size() - 1);
        assertThat(testWallet.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testWallet.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingWallet() throws Exception {
        int databaseSizeBeforeUpdate = walletRepository.findAll().size();

        // Create the Wallet
        WalletDTO walletDTO = walletMapper.toDto(wallet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletMockMvc.perform(put("/api/wallets")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(walletDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Wallet in the database
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWallet() throws Exception {
        // Initialize the database
        walletRepository.saveAndFlush(wallet);

        int databaseSizeBeforeDelete = walletRepository.findAll().size();

        // Delete the wallet
        restWalletMockMvc.perform(delete("/api/wallets/{id}", wallet.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Wallet> walletList = walletRepository.findAll();
        assertThat(walletList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
