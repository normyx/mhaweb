package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.domain.Task;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.domain.Workspace;
import com.mgoulene.mhaweb.repository.TaskProjectRepository;
import com.mgoulene.mhaweb.service.TaskProjectService;
import com.mgoulene.mhaweb.service.dto.TaskProjectDTO;
import com.mgoulene.mhaweb.service.mapper.TaskProjectMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.TaskProjectCriteria;
import com.mgoulene.mhaweb.service.TaskProjectQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TaskProjectResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class TaskProjectResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private TaskProjectRepository taskProjectRepository;

    @Mock
    private TaskProjectRepository taskProjectRepositoryMock;

    @Autowired
    private TaskProjectMapper taskProjectMapper;

    @Mock
    private TaskProjectService taskProjectServiceMock;

    @Autowired
    private TaskProjectService taskProjectService;

    @Autowired
    private TaskProjectQueryService taskProjectQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTaskProjectMockMvc;

    private TaskProject taskProject;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TaskProjectResource taskProjectResource = new TaskProjectResource(taskProjectService, taskProjectQueryService);
        this.restTaskProjectMockMvc = MockMvcBuilders.standaloneSetup(taskProjectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TaskProject createEntity(EntityManager em) {
        TaskProject taskProject = new TaskProject()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return taskProject;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TaskProject createUpdatedEntity(EntityManager em) {
        TaskProject taskProject = new TaskProject()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return taskProject;
    }

    @BeforeEach
    public void initTest() {
        taskProject = createEntity(em);
    }

    @Test
    @Transactional
    public void createTaskProject() throws Exception {
        int databaseSizeBeforeCreate = taskProjectRepository.findAll().size();

        // Create the TaskProject
        TaskProjectDTO taskProjectDTO = taskProjectMapper.toDto(taskProject);
        restTaskProjectMockMvc.perform(post("/api/task-projects")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(taskProjectDTO)))
            .andExpect(status().isCreated());

        // Validate the TaskProject in the database
        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeCreate + 1);
        TaskProject testTaskProject = taskProjectList.get(taskProjectList.size() - 1);
        assertThat(testTaskProject.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testTaskProject.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createTaskProjectWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = taskProjectRepository.findAll().size();

        // Create the TaskProject with an existing ID
        taskProject.setId(1L);
        TaskProjectDTO taskProjectDTO = taskProjectMapper.toDto(taskProject);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTaskProjectMockMvc.perform(post("/api/task-projects")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(taskProjectDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TaskProject in the database
        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = taskProjectRepository.findAll().size();
        // set the field null
        taskProject.setLabel(null);

        // Create the TaskProject, which fails.
        TaskProjectDTO taskProjectDTO = taskProjectMapper.toDto(taskProject);

        restTaskProjectMockMvc.perform(post("/api/task-projects")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(taskProjectDTO)))
            .andExpect(status().isBadRequest());

        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = taskProjectRepository.findAll().size();
        // set the field null
        taskProject.setLastUpdate(null);

        // Create the TaskProject, which fails.
        TaskProjectDTO taskProjectDTO = taskProjectMapper.toDto(taskProject);

        restTaskProjectMockMvc.perform(post("/api/task-projects")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(taskProjectDTO)))
            .andExpect(status().isBadRequest());

        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTaskProjects() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList
        restTaskProjectMockMvc.perform(get("/api/task-projects?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(taskProject.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllTaskProjectsWithEagerRelationshipsIsEnabled() throws Exception {
        TaskProjectResource taskProjectResource = new TaskProjectResource(taskProjectServiceMock, taskProjectQueryService);
        when(taskProjectServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restTaskProjectMockMvc = MockMvcBuilders.standaloneSetup(taskProjectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restTaskProjectMockMvc.perform(get("/api/task-projects?eagerload=true"))
        .andExpect(status().isOk());

        verify(taskProjectServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllTaskProjectsWithEagerRelationshipsIsNotEnabled() throws Exception {
        TaskProjectResource taskProjectResource = new TaskProjectResource(taskProjectServiceMock, taskProjectQueryService);
            when(taskProjectServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restTaskProjectMockMvc = MockMvcBuilders.standaloneSetup(taskProjectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restTaskProjectMockMvc.perform(get("/api/task-projects?eagerload=true"))
        .andExpect(status().isOk());

            verify(taskProjectServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getTaskProject() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get the taskProject
        restTaskProjectMockMvc.perform(get("/api/task-projects/{id}", taskProject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(taskProject.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getTaskProjectsByIdFiltering() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        Long id = taskProject.getId();

        defaultTaskProjectShouldBeFound("id.equals=" + id);
        defaultTaskProjectShouldNotBeFound("id.notEquals=" + id);

        defaultTaskProjectShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTaskProjectShouldNotBeFound("id.greaterThan=" + id);

        defaultTaskProjectShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTaskProjectShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTaskProjectsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where label equals to DEFAULT_LABEL
        defaultTaskProjectShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the taskProjectList where label equals to UPDATED_LABEL
        defaultTaskProjectShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where label not equals to DEFAULT_LABEL
        defaultTaskProjectShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the taskProjectList where label not equals to UPDATED_LABEL
        defaultTaskProjectShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultTaskProjectShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the taskProjectList where label equals to UPDATED_LABEL
        defaultTaskProjectShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where label is not null
        defaultTaskProjectShouldBeFound("label.specified=true");

        // Get all the taskProjectList where label is null
        defaultTaskProjectShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllTaskProjectsByLabelContainsSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where label contains DEFAULT_LABEL
        defaultTaskProjectShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the taskProjectList where label contains UPDATED_LABEL
        defaultTaskProjectShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where label does not contain DEFAULT_LABEL
        defaultTaskProjectShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the taskProjectList where label does not contain UPDATED_LABEL
        defaultTaskProjectShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultTaskProjectShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the taskProjectList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTaskProjectShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultTaskProjectShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the taskProjectList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultTaskProjectShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultTaskProjectShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the taskProjectList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTaskProjectShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate is not null
        defaultTaskProjectShouldBeFound("lastUpdate.specified=true");

        // Get all the taskProjectList where lastUpdate is null
        defaultTaskProjectShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultTaskProjectShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the taskProjectList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultTaskProjectShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultTaskProjectShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the taskProjectList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultTaskProjectShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultTaskProjectShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the taskProjectList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultTaskProjectShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTaskProjectsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultTaskProjectShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the taskProjectList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultTaskProjectShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllTaskProjectsByTaskIsEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);
        Task task = TaskResourceIT.createEntity(em);
        em.persist(task);
        em.flush();
        taskProject.addTask(task);
        taskProjectRepository.saveAndFlush(taskProject);
        Long taskId = task.getId();

        // Get all the taskProjectList where task equals to taskId
        defaultTaskProjectShouldBeFound("taskId.equals=" + taskId);

        // Get all the taskProjectList where task equals to taskId + 1
        defaultTaskProjectShouldNotBeFound("taskId.equals=" + (taskId + 1));
    }


    @Test
    @Transactional
    public void getAllTaskProjectsByOwnerIsEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);
        Profil owner = ProfilResourceIT.createEntity(em);
        em.persist(owner);
        em.flush();
        taskProject.addOwner(owner);
        taskProjectRepository.saveAndFlush(taskProject);
        Long ownerId = owner.getId();

        // Get all the taskProjectList where owner equals to ownerId
        defaultTaskProjectShouldBeFound("ownerId.equals=" + ownerId);

        // Get all the taskProjectList where owner equals to ownerId + 1
        defaultTaskProjectShouldNotBeFound("ownerId.equals=" + (ownerId + 1));
    }


    @Test
    @Transactional
    public void getAllTaskProjectsByWorkspaceIsEqualToSomething() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);
        Workspace workspace = WorkspaceResourceIT.createEntity(em);
        em.persist(workspace);
        em.flush();
        taskProject.setWorkspace(workspace);
        taskProjectRepository.saveAndFlush(taskProject);
        Long workspaceId = workspace.getId();

        // Get all the taskProjectList where workspace equals to workspaceId
        defaultTaskProjectShouldBeFound("workspaceId.equals=" + workspaceId);

        // Get all the taskProjectList where workspace equals to workspaceId + 1
        defaultTaskProjectShouldNotBeFound("workspaceId.equals=" + (workspaceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTaskProjectShouldBeFound(String filter) throws Exception {
        restTaskProjectMockMvc.perform(get("/api/task-projects?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(taskProject.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restTaskProjectMockMvc.perform(get("/api/task-projects/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTaskProjectShouldNotBeFound(String filter) throws Exception {
        restTaskProjectMockMvc.perform(get("/api/task-projects?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTaskProjectMockMvc.perform(get("/api/task-projects/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTaskProject() throws Exception {
        // Get the taskProject
        restTaskProjectMockMvc.perform(get("/api/task-projects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTaskProject() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        int databaseSizeBeforeUpdate = taskProjectRepository.findAll().size();

        // Update the taskProject
        TaskProject updatedTaskProject = taskProjectRepository.findById(taskProject.getId()).get();
        // Disconnect from session so that the updates on updatedTaskProject are not directly saved in db
        em.detach(updatedTaskProject);
        updatedTaskProject
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        TaskProjectDTO taskProjectDTO = taskProjectMapper.toDto(updatedTaskProject);

        restTaskProjectMockMvc.perform(put("/api/task-projects")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(taskProjectDTO)))
            .andExpect(status().isOk());

        // Validate the TaskProject in the database
        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeUpdate);
        TaskProject testTaskProject = taskProjectList.get(taskProjectList.size() - 1);
        assertThat(testTaskProject.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testTaskProject.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingTaskProject() throws Exception {
        int databaseSizeBeforeUpdate = taskProjectRepository.findAll().size();

        // Create the TaskProject
        TaskProjectDTO taskProjectDTO = taskProjectMapper.toDto(taskProject);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTaskProjectMockMvc.perform(put("/api/task-projects")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(taskProjectDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TaskProject in the database
        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTaskProject() throws Exception {
        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        int databaseSizeBeforeDelete = taskProjectRepository.findAll().size();

        // Delete the taskProject
        restTaskProjectMockMvc.perform(delete("/api/task-projects/{id}", taskProject.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
