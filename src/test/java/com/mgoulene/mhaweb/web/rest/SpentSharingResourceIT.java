package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.SpentSharing;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.domain.Spent;
import com.mgoulene.mhaweb.repository.SpentSharingRepository;
import com.mgoulene.mhaweb.service.SpentSharingService;
import com.mgoulene.mhaweb.service.dto.SpentSharingDTO;
import com.mgoulene.mhaweb.service.mapper.SpentSharingMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.SpentSharingCriteria;
import com.mgoulene.mhaweb.service.SpentSharingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SpentSharingResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class SpentSharingResourceIT {

    private static final Float DEFAULT_AMOUNT_SHARE = 1F;
    private static final Float UPDATED_AMOUNT_SHARE = 2F;
    private static final Float SMALLER_AMOUNT_SHARE = 1F - 1F;

    private static final Integer DEFAULT_SHARE = 0;
    private static final Integer UPDATED_SHARE = 1;
    private static final Integer SMALLER_SHARE = 0 - 1;

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private SpentSharingRepository spentSharingRepository;

    @Autowired
    private SpentSharingMapper spentSharingMapper;

    @Autowired
    private SpentSharingService spentSharingService;

    @Autowired
    private SpentSharingQueryService spentSharingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentSharingMockMvc;

    private SpentSharing spentSharing;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentSharingResource spentSharingResource = new SpentSharingResource(spentSharingService, spentSharingQueryService);
        this.restSpentSharingMockMvc = MockMvcBuilders.standaloneSetup(spentSharingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharing createEntity(EntityManager em) {
        SpentSharing spentSharing = new SpentSharing()
            .amountShare(DEFAULT_AMOUNT_SHARE)
            .share(DEFAULT_SHARE)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return spentSharing;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharing createUpdatedEntity(EntityManager em) {
        SpentSharing spentSharing = new SpentSharing()
            .amountShare(UPDATED_AMOUNT_SHARE)
            .share(UPDATED_SHARE)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return spentSharing;
    }

    @BeforeEach
    public void initTest() {
        spentSharing = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpentSharing() throws Exception {
        int databaseSizeBeforeCreate = spentSharingRepository.findAll().size();

        // Create the SpentSharing
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);
        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isCreated());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeCreate + 1);
        SpentSharing testSpentSharing = spentSharingList.get(spentSharingList.size() - 1);
        assertThat(testSpentSharing.getAmountShare()).isEqualTo(DEFAULT_AMOUNT_SHARE);
        assertThat(testSpentSharing.getShare()).isEqualTo(DEFAULT_SHARE);
        assertThat(testSpentSharing.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createSpentSharingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentSharingRepository.findAll().size();

        // Create the SpentSharing with an existing ID
        spentSharing.setId(1L);
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAmountShareIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingRepository.findAll().size();
        // set the field null
        spentSharing.setAmountShare(null);

        // Create the SpentSharing, which fails.
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkShareIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingRepository.findAll().size();
        // set the field null
        spentSharing.setShare(null);

        // Create the SpentSharing, which fails.
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingRepository.findAll().size();
        // set the field null
        spentSharing.setLastUpdate(null);

        // Create the SpentSharing, which fails.
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpentSharings() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList
        restSpentSharingMockMvc.perform(get("/api/spent-sharings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharing.getId().intValue())))
            .andExpect(jsonPath("$.[*].amountShare").value(hasItem(DEFAULT_AMOUNT_SHARE.doubleValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getSpentSharing() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get the spentSharing
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/{id}", spentSharing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(spentSharing.getId().intValue()))
            .andExpect(jsonPath("$.amountShare").value(DEFAULT_AMOUNT_SHARE.doubleValue()))
            .andExpect(jsonPath("$.share").value(DEFAULT_SHARE))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getSpentSharingsByIdFiltering() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        Long id = spentSharing.getId();

        defaultSpentSharingShouldBeFound("id.equals=" + id);
        defaultSpentSharingShouldNotBeFound("id.notEquals=" + id);

        defaultSpentSharingShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSpentSharingShouldNotBeFound("id.greaterThan=" + id);

        defaultSpentSharingShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSpentSharingShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare equals to DEFAULT_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.equals=" + DEFAULT_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare equals to UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.equals=" + UPDATED_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare not equals to DEFAULT_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.notEquals=" + DEFAULT_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare not equals to UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.notEquals=" + UPDATED_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare in DEFAULT_AMOUNT_SHARE or UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.in=" + DEFAULT_AMOUNT_SHARE + "," + UPDATED_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare equals to UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.in=" + UPDATED_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare is not null
        defaultSpentSharingShouldBeFound("amountShare.specified=true");

        // Get all the spentSharingList where amountShare is null
        defaultSpentSharingShouldNotBeFound("amountShare.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare is greater than or equal to DEFAULT_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.greaterThanOrEqual=" + DEFAULT_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare is greater than or equal to UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.greaterThanOrEqual=" + UPDATED_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare is less than or equal to DEFAULT_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.lessThanOrEqual=" + DEFAULT_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare is less than or equal to SMALLER_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.lessThanOrEqual=" + SMALLER_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsLessThanSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare is less than DEFAULT_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.lessThan=" + DEFAULT_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare is less than UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.lessThan=" + UPDATED_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare is greater than DEFAULT_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.greaterThan=" + DEFAULT_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare is greater than SMALLER_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.greaterThan=" + SMALLER_AMOUNT_SHARE);
    }


    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share equals to DEFAULT_SHARE
        defaultSpentSharingShouldBeFound("share.equals=" + DEFAULT_SHARE);

        // Get all the spentSharingList where share equals to UPDATED_SHARE
        defaultSpentSharingShouldNotBeFound("share.equals=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share not equals to DEFAULT_SHARE
        defaultSpentSharingShouldNotBeFound("share.notEquals=" + DEFAULT_SHARE);

        // Get all the spentSharingList where share not equals to UPDATED_SHARE
        defaultSpentSharingShouldBeFound("share.notEquals=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share in DEFAULT_SHARE or UPDATED_SHARE
        defaultSpentSharingShouldBeFound("share.in=" + DEFAULT_SHARE + "," + UPDATED_SHARE);

        // Get all the spentSharingList where share equals to UPDATED_SHARE
        defaultSpentSharingShouldNotBeFound("share.in=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share is not null
        defaultSpentSharingShouldBeFound("share.specified=true");

        // Get all the spentSharingList where share is null
        defaultSpentSharingShouldNotBeFound("share.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share is greater than or equal to DEFAULT_SHARE
        defaultSpentSharingShouldBeFound("share.greaterThanOrEqual=" + DEFAULT_SHARE);

        // Get all the spentSharingList where share is greater than or equal to UPDATED_SHARE
        defaultSpentSharingShouldNotBeFound("share.greaterThanOrEqual=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share is less than or equal to DEFAULT_SHARE
        defaultSpentSharingShouldBeFound("share.lessThanOrEqual=" + DEFAULT_SHARE);

        // Get all the spentSharingList where share is less than or equal to SMALLER_SHARE
        defaultSpentSharingShouldNotBeFound("share.lessThanOrEqual=" + SMALLER_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsLessThanSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share is less than DEFAULT_SHARE
        defaultSpentSharingShouldNotBeFound("share.lessThan=" + DEFAULT_SHARE);

        // Get all the spentSharingList where share is less than UPDATED_SHARE
        defaultSpentSharingShouldBeFound("share.lessThan=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share is greater than DEFAULT_SHARE
        defaultSpentSharingShouldNotBeFound("share.greaterThan=" + DEFAULT_SHARE);

        // Get all the spentSharingList where share is greater than SMALLER_SHARE
        defaultSpentSharingShouldBeFound("share.greaterThan=" + SMALLER_SHARE);
    }


    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultSpentSharingShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentSharingShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultSpentSharingShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultSpentSharingShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultSpentSharingShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the spentSharingList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentSharingShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate is not null
        defaultSpentSharingShouldBeFound("lastUpdate.specified=true");

        // Get all the spentSharingList where lastUpdate is null
        defaultSpentSharingShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultSpentSharingShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultSpentSharingShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultSpentSharingShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultSpentSharingShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultSpentSharingShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultSpentSharingShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultSpentSharingShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultSpentSharingShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllSpentSharingsBySharingProfilIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);
        Profil sharingProfil = ProfilResourceIT.createEntity(em);
        em.persist(sharingProfil);
        em.flush();
        spentSharing.setSharingProfil(sharingProfil);
        spentSharingRepository.saveAndFlush(spentSharing);
        Long sharingProfilId = sharingProfil.getId();

        // Get all the spentSharingList where sharingProfil equals to sharingProfilId
        defaultSpentSharingShouldBeFound("sharingProfilId.equals=" + sharingProfilId);

        // Get all the spentSharingList where sharingProfil equals to sharingProfilId + 1
        defaultSpentSharingShouldNotBeFound("sharingProfilId.equals=" + (sharingProfilId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentSharingsBySpentIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);
        Spent spent = SpentResourceIT.createEntity(em);
        em.persist(spent);
        em.flush();
        spentSharing.setSpent(spent);
        spentSharingRepository.saveAndFlush(spentSharing);
        Long spentId = spent.getId();

        // Get all the spentSharingList where spent equals to spentId
        defaultSpentSharingShouldBeFound("spentId.equals=" + spentId);

        // Get all the spentSharingList where spent equals to spentId + 1
        defaultSpentSharingShouldNotBeFound("spentId.equals=" + (spentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentSharingShouldBeFound(String filter) throws Exception {
        restSpentSharingMockMvc.perform(get("/api/spent-sharings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharing.getId().intValue())))
            .andExpect(jsonPath("$.[*].amountShare").value(hasItem(DEFAULT_AMOUNT_SHARE.doubleValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentSharingShouldNotBeFound(String filter) throws Exception {
        restSpentSharingMockMvc.perform(get("/api/spent-sharings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpentSharing() throws Exception {
        // Get the spentSharing
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpentSharing() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        int databaseSizeBeforeUpdate = spentSharingRepository.findAll().size();

        // Update the spentSharing
        SpentSharing updatedSpentSharing = spentSharingRepository.findById(spentSharing.getId()).get();
        // Disconnect from session so that the updates on updatedSpentSharing are not directly saved in db
        em.detach(updatedSpentSharing);
        updatedSpentSharing
            .amountShare(UPDATED_AMOUNT_SHARE)
            .share(UPDATED_SHARE)
            .lastUpdate(UPDATED_LAST_UPDATE);
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(updatedSpentSharing);

        restSpentSharingMockMvc.perform(put("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isOk());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeUpdate);
        SpentSharing testSpentSharing = spentSharingList.get(spentSharingList.size() - 1);
        assertThat(testSpentSharing.getAmountShare()).isEqualTo(UPDATED_AMOUNT_SHARE);
        assertThat(testSpentSharing.getShare()).isEqualTo(UPDATED_SHARE);
        assertThat(testSpentSharing.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSpentSharing() throws Exception {
        int databaseSizeBeforeUpdate = spentSharingRepository.findAll().size();

        // Create the SpentSharing
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentSharingMockMvc.perform(put("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpentSharing() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        int databaseSizeBeforeDelete = spentSharingRepository.findAll().size();

        // Delete the spentSharing
        restSpentSharingMockMvc.perform(delete("/api/spent-sharings/{id}", spentSharing.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
