package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.TodoTemplate;
import com.mgoulene.mhaweb.domain.TodoListTemplate;
import com.mgoulene.mhaweb.repository.TodoTemplateRepository;
import com.mgoulene.mhaweb.service.TodoTemplateService;
import com.mgoulene.mhaweb.service.dto.TodoTemplateDTO;
import com.mgoulene.mhaweb.service.mapper.TodoTemplateMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.TodoTemplateCriteria;
import com.mgoulene.mhaweb.service.TodoTemplateQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TodoTemplateResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class TodoTemplateResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private TodoTemplateRepository todoTemplateRepository;

    @Autowired
    private TodoTemplateMapper todoTemplateMapper;

    @Autowired
    private TodoTemplateService todoTemplateService;

    @Autowired
    private TodoTemplateQueryService todoTemplateQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTodoTemplateMockMvc;

    private TodoTemplate todoTemplate;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TodoTemplateResource todoTemplateResource = new TodoTemplateResource(todoTemplateService, todoTemplateQueryService);
        this.restTodoTemplateMockMvc = MockMvcBuilders.standaloneSetup(todoTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TodoTemplate createEntity(EntityManager em) {
        TodoTemplate todoTemplate = new TodoTemplate()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        // Add required entity
        TodoListTemplate todoListTemplate;
        if (TestUtil.findAll(em, TodoListTemplate.class).isEmpty()) {
            todoListTemplate = TodoListTemplateResourceIT.createEntity(em);
            em.persist(todoListTemplate);
            em.flush();
        } else {
            todoListTemplate = TestUtil.findAll(em, TodoListTemplate.class).get(0);
        }
        todoTemplate.setTodoListTemplate(todoListTemplate);
        return todoTemplate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TodoTemplate createUpdatedEntity(EntityManager em) {
        TodoTemplate todoTemplate = new TodoTemplate()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        // Add required entity
        TodoListTemplate todoListTemplate;
        if (TestUtil.findAll(em, TodoListTemplate.class).isEmpty()) {
            todoListTemplate = TodoListTemplateResourceIT.createUpdatedEntity(em);
            em.persist(todoListTemplate);
            em.flush();
        } else {
            todoListTemplate = TestUtil.findAll(em, TodoListTemplate.class).get(0);
        }
        todoTemplate.setTodoListTemplate(todoListTemplate);
        return todoTemplate;
    }

    @BeforeEach
    public void initTest() {
        todoTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createTodoTemplate() throws Exception {
        int databaseSizeBeforeCreate = todoTemplateRepository.findAll().size();

        // Create the TodoTemplate
        TodoTemplateDTO todoTemplateDTO = todoTemplateMapper.toDto(todoTemplate);
        restTodoTemplateMockMvc.perform(post("/api/todo-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoTemplateDTO)))
            .andExpect(status().isCreated());

        // Validate the TodoTemplate in the database
        List<TodoTemplate> todoTemplateList = todoTemplateRepository.findAll();
        assertThat(todoTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        TodoTemplate testTodoTemplate = todoTemplateList.get(todoTemplateList.size() - 1);
        assertThat(testTodoTemplate.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testTodoTemplate.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createTodoTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = todoTemplateRepository.findAll().size();

        // Create the TodoTemplate with an existing ID
        todoTemplate.setId(1L);
        TodoTemplateDTO todoTemplateDTO = todoTemplateMapper.toDto(todoTemplate);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTodoTemplateMockMvc.perform(post("/api/todo-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoTemplateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TodoTemplate in the database
        List<TodoTemplate> todoTemplateList = todoTemplateRepository.findAll();
        assertThat(todoTemplateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = todoTemplateRepository.findAll().size();
        // set the field null
        todoTemplate.setLabel(null);

        // Create the TodoTemplate, which fails.
        TodoTemplateDTO todoTemplateDTO = todoTemplateMapper.toDto(todoTemplate);

        restTodoTemplateMockMvc.perform(post("/api/todo-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<TodoTemplate> todoTemplateList = todoTemplateRepository.findAll();
        assertThat(todoTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = todoTemplateRepository.findAll().size();
        // set the field null
        todoTemplate.setLastUpdate(null);

        // Create the TodoTemplate, which fails.
        TodoTemplateDTO todoTemplateDTO = todoTemplateMapper.toDto(todoTemplate);

        restTodoTemplateMockMvc.perform(post("/api/todo-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<TodoTemplate> todoTemplateList = todoTemplateRepository.findAll();
        assertThat(todoTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTodoTemplates() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList
        restTodoTemplateMockMvc.perform(get("/api/todo-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(todoTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getTodoTemplate() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get the todoTemplate
        restTodoTemplateMockMvc.perform(get("/api/todo-templates/{id}", todoTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(todoTemplate.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getTodoTemplatesByIdFiltering() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        Long id = todoTemplate.getId();

        defaultTodoTemplateShouldBeFound("id.equals=" + id);
        defaultTodoTemplateShouldNotBeFound("id.notEquals=" + id);

        defaultTodoTemplateShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTodoTemplateShouldNotBeFound("id.greaterThan=" + id);

        defaultTodoTemplateShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTodoTemplateShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTodoTemplatesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where label equals to DEFAULT_LABEL
        defaultTodoTemplateShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the todoTemplateList where label equals to UPDATED_LABEL
        defaultTodoTemplateShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where label not equals to DEFAULT_LABEL
        defaultTodoTemplateShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the todoTemplateList where label not equals to UPDATED_LABEL
        defaultTodoTemplateShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultTodoTemplateShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the todoTemplateList where label equals to UPDATED_LABEL
        defaultTodoTemplateShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where label is not null
        defaultTodoTemplateShouldBeFound("label.specified=true");

        // Get all the todoTemplateList where label is null
        defaultTodoTemplateShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllTodoTemplatesByLabelContainsSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where label contains DEFAULT_LABEL
        defaultTodoTemplateShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the todoTemplateList where label contains UPDATED_LABEL
        defaultTodoTemplateShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where label does not contain DEFAULT_LABEL
        defaultTodoTemplateShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the todoTemplateList where label does not contain UPDATED_LABEL
        defaultTodoTemplateShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultTodoTemplateShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the todoTemplateList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTodoTemplateShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultTodoTemplateShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the todoTemplateList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultTodoTemplateShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultTodoTemplateShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the todoTemplateList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTodoTemplateShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate is not null
        defaultTodoTemplateShouldBeFound("lastUpdate.specified=true");

        // Get all the todoTemplateList where lastUpdate is null
        defaultTodoTemplateShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultTodoTemplateShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the todoTemplateList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultTodoTemplateShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultTodoTemplateShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the todoTemplateList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultTodoTemplateShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultTodoTemplateShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the todoTemplateList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultTodoTemplateShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoTemplatesByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        // Get all the todoTemplateList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultTodoTemplateShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the todoTemplateList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultTodoTemplateShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllTodoTemplatesByTodoListTemplateIsEqualToSomething() throws Exception {
        // Get already existing entity
        TodoListTemplate todoListTemplate = todoTemplate.getTodoListTemplate();
        todoTemplateRepository.saveAndFlush(todoTemplate);
        Long todoListTemplateId = todoListTemplate.getId();

        // Get all the todoTemplateList where todoListTemplate equals to todoListTemplateId
        defaultTodoTemplateShouldBeFound("todoListTemplateId.equals=" + todoListTemplateId);

        // Get all the todoTemplateList where todoListTemplate equals to todoListTemplateId + 1
        defaultTodoTemplateShouldNotBeFound("todoListTemplateId.equals=" + (todoListTemplateId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTodoTemplateShouldBeFound(String filter) throws Exception {
        restTodoTemplateMockMvc.perform(get("/api/todo-templates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(todoTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restTodoTemplateMockMvc.perform(get("/api/todo-templates/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTodoTemplateShouldNotBeFound(String filter) throws Exception {
        restTodoTemplateMockMvc.perform(get("/api/todo-templates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTodoTemplateMockMvc.perform(get("/api/todo-templates/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTodoTemplate() throws Exception {
        // Get the todoTemplate
        restTodoTemplateMockMvc.perform(get("/api/todo-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTodoTemplate() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        int databaseSizeBeforeUpdate = todoTemplateRepository.findAll().size();

        // Update the todoTemplate
        TodoTemplate updatedTodoTemplate = todoTemplateRepository.findById(todoTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedTodoTemplate are not directly saved in db
        em.detach(updatedTodoTemplate);
        updatedTodoTemplate
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        TodoTemplateDTO todoTemplateDTO = todoTemplateMapper.toDto(updatedTodoTemplate);

        restTodoTemplateMockMvc.perform(put("/api/todo-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoTemplateDTO)))
            .andExpect(status().isOk());

        // Validate the TodoTemplate in the database
        List<TodoTemplate> todoTemplateList = todoTemplateRepository.findAll();
        assertThat(todoTemplateList).hasSize(databaseSizeBeforeUpdate);
        TodoTemplate testTodoTemplate = todoTemplateList.get(todoTemplateList.size() - 1);
        assertThat(testTodoTemplate.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testTodoTemplate.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingTodoTemplate() throws Exception {
        int databaseSizeBeforeUpdate = todoTemplateRepository.findAll().size();

        // Create the TodoTemplate
        TodoTemplateDTO todoTemplateDTO = todoTemplateMapper.toDto(todoTemplate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTodoTemplateMockMvc.perform(put("/api/todo-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoTemplateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TodoTemplate in the database
        List<TodoTemplate> todoTemplateList = todoTemplateRepository.findAll();
        assertThat(todoTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTodoTemplate() throws Exception {
        // Initialize the database
        todoTemplateRepository.saveAndFlush(todoTemplate);

        int databaseSizeBeforeDelete = todoTemplateRepository.findAll().size();

        // Delete the todoTemplate
        restTodoTemplateMockMvc.perform(delete("/api/todo-templates/{id}", todoTemplate.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TodoTemplate> todoTemplateList = todoTemplateRepository.findAll();
        assertThat(todoTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
