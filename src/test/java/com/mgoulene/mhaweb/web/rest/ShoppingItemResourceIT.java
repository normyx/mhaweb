package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.ShoppingItem;
import com.mgoulene.mhaweb.domain.ShoppingCatalogSheld;
import com.mgoulene.mhaweb.domain.ShoppingCart;
import com.mgoulene.mhaweb.repository.ShoppingItemRepository;
import com.mgoulene.mhaweb.service.ShoppingItemService;
import com.mgoulene.mhaweb.service.dto.ShoppingItemDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingItemMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.ShoppingItemCriteria;
import com.mgoulene.mhaweb.service.ShoppingItemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mhaweb.domain.enumeration.Unit;
/**
 * Integration tests for the {@link ShoppingItemResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class ShoppingItemResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_CHECKED = false;
    private static final Boolean UPDATED_CHECKED = true;

    private static final Float DEFAULT_QUANTITY = 0F;
    private static final Float UPDATED_QUANTITY = 1F;
    private static final Float SMALLER_QUANTITY = 0F - 1F;

    private static final Unit DEFAULT_UNIT = Unit.QUANTITY;
    private static final Unit UPDATED_UNIT = Unit.G;

    @Autowired
    private ShoppingItemRepository shoppingItemRepository;

    @Autowired
    private ShoppingItemMapper shoppingItemMapper;

    @Autowired
    private ShoppingItemService shoppingItemService;

    @Autowired
    private ShoppingItemQueryService shoppingItemQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoppingItemMockMvc;

    private ShoppingItem shoppingItem;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoppingItemResource shoppingItemResource = new ShoppingItemResource(shoppingItemService, shoppingItemQueryService);
        this.restShoppingItemMockMvc = MockMvcBuilders.standaloneSetup(shoppingItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingItem createEntity(EntityManager em) {
        ShoppingItem shoppingItem = new ShoppingItem()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE)
            .checked(DEFAULT_CHECKED)
            .quantity(DEFAULT_QUANTITY)
            .unit(DEFAULT_UNIT);
        // Add required entity
        ShoppingCatalogSheld shoppingCatalogSheld;
        if (TestUtil.findAll(em, ShoppingCatalogSheld.class).isEmpty()) {
            shoppingCatalogSheld = ShoppingCatalogSheldResourceIT.createEntity(em);
            em.persist(shoppingCatalogSheld);
            em.flush();
        } else {
            shoppingCatalogSheld = TestUtil.findAll(em, ShoppingCatalogSheld.class).get(0);
        }
        shoppingItem.setShoppingCatalogSheld(shoppingCatalogSheld);
        // Add required entity
        ShoppingCart shoppingCart;
        if (TestUtil.findAll(em, ShoppingCart.class).isEmpty()) {
            shoppingCart = ShoppingCartResourceIT.createEntity(em);
            em.persist(shoppingCart);
            em.flush();
        } else {
            shoppingCart = TestUtil.findAll(em, ShoppingCart.class).get(0);
        }
        shoppingItem.setShoppingCart(shoppingCart);
        return shoppingItem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingItem createUpdatedEntity(EntityManager em) {
        ShoppingItem shoppingItem = new ShoppingItem()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .checked(UPDATED_CHECKED)
            .quantity(UPDATED_QUANTITY)
            .unit(UPDATED_UNIT);
        // Add required entity
        ShoppingCatalogSheld shoppingCatalogSheld;
        if (TestUtil.findAll(em, ShoppingCatalogSheld.class).isEmpty()) {
            shoppingCatalogSheld = ShoppingCatalogSheldResourceIT.createUpdatedEntity(em);
            em.persist(shoppingCatalogSheld);
            em.flush();
        } else {
            shoppingCatalogSheld = TestUtil.findAll(em, ShoppingCatalogSheld.class).get(0);
        }
        shoppingItem.setShoppingCatalogSheld(shoppingCatalogSheld);
        // Add required entity
        ShoppingCart shoppingCart;
        if (TestUtil.findAll(em, ShoppingCart.class).isEmpty()) {
            shoppingCart = ShoppingCartResourceIT.createUpdatedEntity(em);
            em.persist(shoppingCart);
            em.flush();
        } else {
            shoppingCart = TestUtil.findAll(em, ShoppingCart.class).get(0);
        }
        shoppingItem.setShoppingCart(shoppingCart);
        return shoppingItem;
    }

    @BeforeEach
    public void initTest() {
        shoppingItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createShoppingItem() throws Exception {
        int databaseSizeBeforeCreate = shoppingItemRepository.findAll().size();

        // Create the ShoppingItem
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);
        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isCreated());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeCreate + 1);
        ShoppingItem testShoppingItem = shoppingItemList.get(shoppingItemList.size() - 1);
        assertThat(testShoppingItem.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testShoppingItem.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testShoppingItem.isChecked()).isEqualTo(DEFAULT_CHECKED);
        assertThat(testShoppingItem.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testShoppingItem.getUnit()).isEqualTo(DEFAULT_UNIT);
    }

    @Test
    @Transactional
    public void createShoppingItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoppingItemRepository.findAll().size();

        // Create the ShoppingItem with an existing ID
        shoppingItem.setId(1L);
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingItemRepository.findAll().size();
        // set the field null
        shoppingItem.setLabel(null);

        // Create the ShoppingItem, which fails.
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingItemRepository.findAll().size();
        // set the field null
        shoppingItem.setLastUpdate(null);

        // Create the ShoppingItem, which fails.
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCheckedIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingItemRepository.findAll().size();
        // set the field null
        shoppingItem.setChecked(null);

        // Create the ShoppingItem, which fails.
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShoppingItems() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList
        restShoppingItemMockMvc.perform(get("/api/shopping-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].checked").value(hasItem(DEFAULT_CHECKED.booleanValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.doubleValue())))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT.toString())));
    }
    
    @Test
    @Transactional
    public void getShoppingItem() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get the shoppingItem
        restShoppingItemMockMvc.perform(get("/api/shopping-items/{id}", shoppingItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shoppingItem.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()))
            .andExpect(jsonPath("$.checked").value(DEFAULT_CHECKED.booleanValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY.doubleValue()))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT.toString()));
    }


    @Test
    @Transactional
    public void getShoppingItemsByIdFiltering() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        Long id = shoppingItem.getId();

        defaultShoppingItemShouldBeFound("id.equals=" + id);
        defaultShoppingItemShouldNotBeFound("id.notEquals=" + id);

        defaultShoppingItemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultShoppingItemShouldNotBeFound("id.greaterThan=" + id);

        defaultShoppingItemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultShoppingItemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllShoppingItemsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label equals to DEFAULT_LABEL
        defaultShoppingItemShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the shoppingItemList where label equals to UPDATED_LABEL
        defaultShoppingItemShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label not equals to DEFAULT_LABEL
        defaultShoppingItemShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the shoppingItemList where label not equals to UPDATED_LABEL
        defaultShoppingItemShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultShoppingItemShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the shoppingItemList where label equals to UPDATED_LABEL
        defaultShoppingItemShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label is not null
        defaultShoppingItemShouldBeFound("label.specified=true");

        // Get all the shoppingItemList where label is null
        defaultShoppingItemShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllShoppingItemsByLabelContainsSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label contains DEFAULT_LABEL
        defaultShoppingItemShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the shoppingItemList where label contains UPDATED_LABEL
        defaultShoppingItemShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label does not contain DEFAULT_LABEL
        defaultShoppingItemShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the shoppingItemList where label does not contain UPDATED_LABEL
        defaultShoppingItemShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultShoppingItemShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingItemList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingItemShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultShoppingItemShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingItemList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultShoppingItemShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultShoppingItemShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the shoppingItemList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingItemShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate is not null
        defaultShoppingItemShouldBeFound("lastUpdate.specified=true");

        // Get all the shoppingItemList where lastUpdate is null
        defaultShoppingItemShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingItemShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingItemList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultShoppingItemShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingItemShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingItemList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultShoppingItemShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultShoppingItemShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingItemList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultShoppingItemShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultShoppingItemShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingItemList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultShoppingItemShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllShoppingItemsByCheckedIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where checked equals to DEFAULT_CHECKED
        defaultShoppingItemShouldBeFound("checked.equals=" + DEFAULT_CHECKED);

        // Get all the shoppingItemList where checked equals to UPDATED_CHECKED
        defaultShoppingItemShouldNotBeFound("checked.equals=" + UPDATED_CHECKED);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByCheckedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where checked not equals to DEFAULT_CHECKED
        defaultShoppingItemShouldNotBeFound("checked.notEquals=" + DEFAULT_CHECKED);

        // Get all the shoppingItemList where checked not equals to UPDATED_CHECKED
        defaultShoppingItemShouldBeFound("checked.notEquals=" + UPDATED_CHECKED);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByCheckedIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where checked in DEFAULT_CHECKED or UPDATED_CHECKED
        defaultShoppingItemShouldBeFound("checked.in=" + DEFAULT_CHECKED + "," + UPDATED_CHECKED);

        // Get all the shoppingItemList where checked equals to UPDATED_CHECKED
        defaultShoppingItemShouldNotBeFound("checked.in=" + UPDATED_CHECKED);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByCheckedIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where checked is not null
        defaultShoppingItemShouldBeFound("checked.specified=true");

        // Get all the shoppingItemList where checked is null
        defaultShoppingItemShouldNotBeFound("checked.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity equals to DEFAULT_QUANTITY
        defaultShoppingItemShouldBeFound("quantity.equals=" + DEFAULT_QUANTITY);

        // Get all the shoppingItemList where quantity equals to UPDATED_QUANTITY
        defaultShoppingItemShouldNotBeFound("quantity.equals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity not equals to DEFAULT_QUANTITY
        defaultShoppingItemShouldNotBeFound("quantity.notEquals=" + DEFAULT_QUANTITY);

        // Get all the shoppingItemList where quantity not equals to UPDATED_QUANTITY
        defaultShoppingItemShouldBeFound("quantity.notEquals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity in DEFAULT_QUANTITY or UPDATED_QUANTITY
        defaultShoppingItemShouldBeFound("quantity.in=" + DEFAULT_QUANTITY + "," + UPDATED_QUANTITY);

        // Get all the shoppingItemList where quantity equals to UPDATED_QUANTITY
        defaultShoppingItemShouldNotBeFound("quantity.in=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity is not null
        defaultShoppingItemShouldBeFound("quantity.specified=true");

        // Get all the shoppingItemList where quantity is null
        defaultShoppingItemShouldNotBeFound("quantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity is greater than or equal to DEFAULT_QUANTITY
        defaultShoppingItemShouldBeFound("quantity.greaterThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the shoppingItemList where quantity is greater than or equal to UPDATED_QUANTITY
        defaultShoppingItemShouldNotBeFound("quantity.greaterThanOrEqual=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity is less than or equal to DEFAULT_QUANTITY
        defaultShoppingItemShouldBeFound("quantity.lessThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the shoppingItemList where quantity is less than or equal to SMALLER_QUANTITY
        defaultShoppingItemShouldNotBeFound("quantity.lessThanOrEqual=" + SMALLER_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsLessThanSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity is less than DEFAULT_QUANTITY
        defaultShoppingItemShouldNotBeFound("quantity.lessThan=" + DEFAULT_QUANTITY);

        // Get all the shoppingItemList where quantity is less than UPDATED_QUANTITY
        defaultShoppingItemShouldBeFound("quantity.lessThan=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByQuantityIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where quantity is greater than DEFAULT_QUANTITY
        defaultShoppingItemShouldNotBeFound("quantity.greaterThan=" + DEFAULT_QUANTITY);

        // Get all the shoppingItemList where quantity is greater than SMALLER_QUANTITY
        defaultShoppingItemShouldBeFound("quantity.greaterThan=" + SMALLER_QUANTITY);
    }


    @Test
    @Transactional
    public void getAllShoppingItemsByUnitIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where unit equals to DEFAULT_UNIT
        defaultShoppingItemShouldBeFound("unit.equals=" + DEFAULT_UNIT);

        // Get all the shoppingItemList where unit equals to UPDATED_UNIT
        defaultShoppingItemShouldNotBeFound("unit.equals=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByUnitIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where unit not equals to DEFAULT_UNIT
        defaultShoppingItemShouldNotBeFound("unit.notEquals=" + DEFAULT_UNIT);

        // Get all the shoppingItemList where unit not equals to UPDATED_UNIT
        defaultShoppingItemShouldBeFound("unit.notEquals=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByUnitIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where unit in DEFAULT_UNIT or UPDATED_UNIT
        defaultShoppingItemShouldBeFound("unit.in=" + DEFAULT_UNIT + "," + UPDATED_UNIT);

        // Get all the shoppingItemList where unit equals to UPDATED_UNIT
        defaultShoppingItemShouldNotBeFound("unit.in=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByUnitIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where unit is not null
        defaultShoppingItemShouldBeFound("unit.specified=true");

        // Get all the shoppingItemList where unit is null
        defaultShoppingItemShouldNotBeFound("unit.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByShoppingCatalogSheldIsEqualToSomething() throws Exception {
        // Get already existing entity
        ShoppingCatalogSheld shoppingCatalogSheld = shoppingItem.getShoppingCatalogSheld();
        shoppingItemRepository.saveAndFlush(shoppingItem);
        Long shoppingCatalogSheldId = shoppingCatalogSheld.getId();

        // Get all the shoppingItemList where shoppingCatalogSheld equals to shoppingCatalogSheldId
        defaultShoppingItemShouldBeFound("shoppingCatalogSheldId.equals=" + shoppingCatalogSheldId);

        // Get all the shoppingItemList where shoppingCatalogSheld equals to shoppingCatalogSheldId + 1
        defaultShoppingItemShouldNotBeFound("shoppingCatalogSheldId.equals=" + (shoppingCatalogSheldId + 1));
    }


    @Test
    @Transactional
    public void getAllShoppingItemsByShoppingCartIsEqualToSomething() throws Exception {
        // Get already existing entity
        ShoppingCart shoppingCart = shoppingItem.getShoppingCart();
        shoppingItemRepository.saveAndFlush(shoppingItem);
        Long shoppingCartId = shoppingCart.getId();

        // Get all the shoppingItemList where shoppingCart equals to shoppingCartId
        defaultShoppingItemShouldBeFound("shoppingCartId.equals=" + shoppingCartId);

        // Get all the shoppingItemList where shoppingCart equals to shoppingCartId + 1
        defaultShoppingItemShouldNotBeFound("shoppingCartId.equals=" + (shoppingCartId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShoppingItemShouldBeFound(String filter) throws Exception {
        restShoppingItemMockMvc.perform(get("/api/shopping-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].checked").value(hasItem(DEFAULT_CHECKED.booleanValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.doubleValue())))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT.toString())));

        // Check, that the count call also returns 1
        restShoppingItemMockMvc.perform(get("/api/shopping-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShoppingItemShouldNotBeFound(String filter) throws Exception {
        restShoppingItemMockMvc.perform(get("/api/shopping-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShoppingItemMockMvc.perform(get("/api/shopping-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShoppingItem() throws Exception {
        // Get the shoppingItem
        restShoppingItemMockMvc.perform(get("/api/shopping-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShoppingItem() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        int databaseSizeBeforeUpdate = shoppingItemRepository.findAll().size();

        // Update the shoppingItem
        ShoppingItem updatedShoppingItem = shoppingItemRepository.findById(shoppingItem.getId()).get();
        // Disconnect from session so that the updates on updatedShoppingItem are not directly saved in db
        em.detach(updatedShoppingItem);
        updatedShoppingItem
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .checked(UPDATED_CHECKED)
            .quantity(UPDATED_QUANTITY)
            .unit(UPDATED_UNIT);
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(updatedShoppingItem);

        restShoppingItemMockMvc.perform(put("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isOk());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeUpdate);
        ShoppingItem testShoppingItem = shoppingItemList.get(shoppingItemList.size() - 1);
        assertThat(testShoppingItem.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testShoppingItem.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testShoppingItem.isChecked()).isEqualTo(UPDATED_CHECKED);
        assertThat(testShoppingItem.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testShoppingItem.getUnit()).isEqualTo(UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void updateNonExistingShoppingItem() throws Exception {
        int databaseSizeBeforeUpdate = shoppingItemRepository.findAll().size();

        // Create the ShoppingItem
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoppingItemMockMvc.perform(put("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShoppingItem() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        int databaseSizeBeforeDelete = shoppingItemRepository.findAll().size();

        // Delete the shoppingItem
        restShoppingItemMockMvc.perform(delete("/api/shopping-items/{id}", shoppingItem.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
