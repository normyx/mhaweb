package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.ShoppingCatalogItem;
import com.mgoulene.mhaweb.domain.ShoppingCatalogSheld;
import com.mgoulene.mhaweb.repository.ShoppingCatalogItemRepository;
import com.mgoulene.mhaweb.service.ShoppingCatalogItemService;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogItemMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemCriteria;
import com.mgoulene.mhaweb.service.ShoppingCatalogItemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShoppingCatalogItemResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class ShoppingCatalogItemResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private ShoppingCatalogItemRepository shoppingCatalogItemRepository;

    @Autowired
    private ShoppingCatalogItemMapper shoppingCatalogItemMapper;

    @Autowired
    private ShoppingCatalogItemService shoppingCatalogItemService;

    @Autowired
    private ShoppingCatalogItemQueryService shoppingCatalogItemQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoppingCatalogItemMockMvc;

    private ShoppingCatalogItem shoppingCatalogItem;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoppingCatalogItemResource shoppingCatalogItemResource = new ShoppingCatalogItemResource(shoppingCatalogItemService, shoppingCatalogItemQueryService);
        this.restShoppingCatalogItemMockMvc = MockMvcBuilders.standaloneSetup(shoppingCatalogItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCatalogItem createEntity(EntityManager em) {
        ShoppingCatalogItem shoppingCatalogItem = new ShoppingCatalogItem()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        // Add required entity
        ShoppingCatalogSheld shoppingCatalogSheld;
        if (TestUtil.findAll(em, ShoppingCatalogSheld.class).isEmpty()) {
            shoppingCatalogSheld = ShoppingCatalogSheldResourceIT.createEntity(em);
            em.persist(shoppingCatalogSheld);
            em.flush();
        } else {
            shoppingCatalogSheld = TestUtil.findAll(em, ShoppingCatalogSheld.class).get(0);
        }
        shoppingCatalogItem.setShoppingCatalogSheld(shoppingCatalogSheld);
        return shoppingCatalogItem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCatalogItem createUpdatedEntity(EntityManager em) {
        ShoppingCatalogItem shoppingCatalogItem = new ShoppingCatalogItem()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        // Add required entity
        ShoppingCatalogSheld shoppingCatalogSheld;
        if (TestUtil.findAll(em, ShoppingCatalogSheld.class).isEmpty()) {
            shoppingCatalogSheld = ShoppingCatalogSheldResourceIT.createUpdatedEntity(em);
            em.persist(shoppingCatalogSheld);
            em.flush();
        } else {
            shoppingCatalogSheld = TestUtil.findAll(em, ShoppingCatalogSheld.class).get(0);
        }
        shoppingCatalogItem.setShoppingCatalogSheld(shoppingCatalogSheld);
        return shoppingCatalogItem;
    }

    @BeforeEach
    public void initTest() {
        shoppingCatalogItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createShoppingCatalogItem() throws Exception {
        int databaseSizeBeforeCreate = shoppingCatalogItemRepository.findAll().size();

        // Create the ShoppingCatalogItem
        ShoppingCatalogItemDTO shoppingCatalogItemDTO = shoppingCatalogItemMapper.toDto(shoppingCatalogItem);
        restShoppingCatalogItemMockMvc.perform(post("/api/shopping-catalog-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogItemDTO)))
            .andExpect(status().isCreated());

        // Validate the ShoppingCatalogItem in the database
        List<ShoppingCatalogItem> shoppingCatalogItemList = shoppingCatalogItemRepository.findAll();
        assertThat(shoppingCatalogItemList).hasSize(databaseSizeBeforeCreate + 1);
        ShoppingCatalogItem testShoppingCatalogItem = shoppingCatalogItemList.get(shoppingCatalogItemList.size() - 1);
        assertThat(testShoppingCatalogItem.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testShoppingCatalogItem.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createShoppingCatalogItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoppingCatalogItemRepository.findAll().size();

        // Create the ShoppingCatalogItem with an existing ID
        shoppingCatalogItem.setId(1L);
        ShoppingCatalogItemDTO shoppingCatalogItemDTO = shoppingCatalogItemMapper.toDto(shoppingCatalogItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoppingCatalogItemMockMvc.perform(post("/api/shopping-catalog-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCatalogItem in the database
        List<ShoppingCatalogItem> shoppingCatalogItemList = shoppingCatalogItemRepository.findAll();
        assertThat(shoppingCatalogItemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogItemRepository.findAll().size();
        // set the field null
        shoppingCatalogItem.setLabel(null);

        // Create the ShoppingCatalogItem, which fails.
        ShoppingCatalogItemDTO shoppingCatalogItemDTO = shoppingCatalogItemMapper.toDto(shoppingCatalogItem);

        restShoppingCatalogItemMockMvc.perform(post("/api/shopping-catalog-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogItemDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogItem> shoppingCatalogItemList = shoppingCatalogItemRepository.findAll();
        assertThat(shoppingCatalogItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogItemRepository.findAll().size();
        // set the field null
        shoppingCatalogItem.setLastUpdate(null);

        // Create the ShoppingCatalogItem, which fails.
        ShoppingCatalogItemDTO shoppingCatalogItemDTO = shoppingCatalogItemMapper.toDto(shoppingCatalogItem);

        restShoppingCatalogItemMockMvc.perform(post("/api/shopping-catalog-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogItemDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogItem> shoppingCatalogItemList = shoppingCatalogItemRepository.findAll();
        assertThat(shoppingCatalogItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItems() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList
        restShoppingCatalogItemMockMvc.perform(get("/api/shopping-catalog-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCatalogItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getShoppingCatalogItem() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get the shoppingCatalogItem
        restShoppingCatalogItemMockMvc.perform(get("/api/shopping-catalog-items/{id}", shoppingCatalogItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shoppingCatalogItem.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getShoppingCatalogItemsByIdFiltering() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        Long id = shoppingCatalogItem.getId();

        defaultShoppingCatalogItemShouldBeFound("id.equals=" + id);
        defaultShoppingCatalogItemShouldNotBeFound("id.notEquals=" + id);

        defaultShoppingCatalogItemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultShoppingCatalogItemShouldNotBeFound("id.greaterThan=" + id);

        defaultShoppingCatalogItemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultShoppingCatalogItemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where label equals to DEFAULT_LABEL
        defaultShoppingCatalogItemShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogItemList where label equals to UPDATED_LABEL
        defaultShoppingCatalogItemShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where label not equals to DEFAULT_LABEL
        defaultShoppingCatalogItemShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogItemList where label not equals to UPDATED_LABEL
        defaultShoppingCatalogItemShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultShoppingCatalogItemShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the shoppingCatalogItemList where label equals to UPDATED_LABEL
        defaultShoppingCatalogItemShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where label is not null
        defaultShoppingCatalogItemShouldBeFound("label.specified=true");

        // Get all the shoppingCatalogItemList where label is null
        defaultShoppingCatalogItemShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLabelContainsSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where label contains DEFAULT_LABEL
        defaultShoppingCatalogItemShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogItemList where label contains UPDATED_LABEL
        defaultShoppingCatalogItemShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where label does not contain DEFAULT_LABEL
        defaultShoppingCatalogItemShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogItemList where label does not contain UPDATED_LABEL
        defaultShoppingCatalogItemShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogItemList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogItemList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the shoppingCatalogItemList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate is not null
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.specified=true");

        // Get all the shoppingCatalogItemList where lastUpdate is null
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogItemList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogItemList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogItemList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        // Get all the shoppingCatalogItemList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultShoppingCatalogItemShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogItemList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultShoppingCatalogItemShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogItemsByShoppingCatalogSheldIsEqualToSomething() throws Exception {
        // Get already existing entity
        ShoppingCatalogSheld shoppingCatalogSheld = shoppingCatalogItem.getShoppingCatalogSheld();
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);
        Long shoppingCatalogSheldId = shoppingCatalogSheld.getId();

        // Get all the shoppingCatalogItemList where shoppingCatalogSheld equals to shoppingCatalogSheldId
        defaultShoppingCatalogItemShouldBeFound("shoppingCatalogSheldId.equals=" + shoppingCatalogSheldId);

        // Get all the shoppingCatalogItemList where shoppingCatalogSheld equals to shoppingCatalogSheldId + 1
        defaultShoppingCatalogItemShouldNotBeFound("shoppingCatalogSheldId.equals=" + (shoppingCatalogSheldId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShoppingCatalogItemShouldBeFound(String filter) throws Exception {
        restShoppingCatalogItemMockMvc.perform(get("/api/shopping-catalog-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCatalogItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restShoppingCatalogItemMockMvc.perform(get("/api/shopping-catalog-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShoppingCatalogItemShouldNotBeFound(String filter) throws Exception {
        restShoppingCatalogItemMockMvc.perform(get("/api/shopping-catalog-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShoppingCatalogItemMockMvc.perform(get("/api/shopping-catalog-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShoppingCatalogItem() throws Exception {
        // Get the shoppingCatalogItem
        restShoppingCatalogItemMockMvc.perform(get("/api/shopping-catalog-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShoppingCatalogItem() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        int databaseSizeBeforeUpdate = shoppingCatalogItemRepository.findAll().size();

        // Update the shoppingCatalogItem
        ShoppingCatalogItem updatedShoppingCatalogItem = shoppingCatalogItemRepository.findById(shoppingCatalogItem.getId()).get();
        // Disconnect from session so that the updates on updatedShoppingCatalogItem are not directly saved in db
        em.detach(updatedShoppingCatalogItem);
        updatedShoppingCatalogItem
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        ShoppingCatalogItemDTO shoppingCatalogItemDTO = shoppingCatalogItemMapper.toDto(updatedShoppingCatalogItem);

        restShoppingCatalogItemMockMvc.perform(put("/api/shopping-catalog-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogItemDTO)))
            .andExpect(status().isOk());

        // Validate the ShoppingCatalogItem in the database
        List<ShoppingCatalogItem> shoppingCatalogItemList = shoppingCatalogItemRepository.findAll();
        assertThat(shoppingCatalogItemList).hasSize(databaseSizeBeforeUpdate);
        ShoppingCatalogItem testShoppingCatalogItem = shoppingCatalogItemList.get(shoppingCatalogItemList.size() - 1);
        assertThat(testShoppingCatalogItem.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testShoppingCatalogItem.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingShoppingCatalogItem() throws Exception {
        int databaseSizeBeforeUpdate = shoppingCatalogItemRepository.findAll().size();

        // Create the ShoppingCatalogItem
        ShoppingCatalogItemDTO shoppingCatalogItemDTO = shoppingCatalogItemMapper.toDto(shoppingCatalogItem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoppingCatalogItemMockMvc.perform(put("/api/shopping-catalog-items")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCatalogItem in the database
        List<ShoppingCatalogItem> shoppingCatalogItemList = shoppingCatalogItemRepository.findAll();
        assertThat(shoppingCatalogItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShoppingCatalogItem() throws Exception {
        // Initialize the database
        shoppingCatalogItemRepository.saveAndFlush(shoppingCatalogItem);

        int databaseSizeBeforeDelete = shoppingCatalogItemRepository.findAll().size();

        // Delete the shoppingCatalogItem
        restShoppingCatalogItemMockMvc.perform(delete("/api/shopping-catalog-items/{id}", shoppingCatalogItem.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShoppingCatalogItem> shoppingCatalogItemList = shoppingCatalogItemRepository.findAll();
        assertThat(shoppingCatalogItemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
