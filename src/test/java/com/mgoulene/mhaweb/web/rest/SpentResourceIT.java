package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.Spent;
import com.mgoulene.mhaweb.domain.SpentSharing;
import com.mgoulene.mhaweb.domain.Wallet;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.repository.SpentRepository;
import com.mgoulene.mhaweb.service.SpentService;
import com.mgoulene.mhaweb.service.dto.SpentDTO;
import com.mgoulene.mhaweb.service.mapper.SpentMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.SpentCriteria;
import com.mgoulene.mhaweb.service.SpentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SpentResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class SpentResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Float DEFAULT_AMOUNT = 1F;
    private static final Float UPDATED_AMOUNT = 2F;
    private static final Float SMALLER_AMOUNT = 1F - 1F;

    private static final LocalDate DEFAULT_SPENT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SPENT_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_SPENT_DATE = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_CONFIRMED = false;
    private static final Boolean UPDATED_CONFIRMED = true;

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private SpentRepository spentRepository;

    @Autowired
    private SpentMapper spentMapper;

    @Autowired
    private SpentService spentService;

    @Autowired
    private SpentQueryService spentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentMockMvc;

    private Spent spent;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentResource spentResource = new SpentResource(spentService, spentQueryService);
        this.restSpentMockMvc = MockMvcBuilders.standaloneSetup(spentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Spent createEntity(EntityManager em) {
        Spent spent = new Spent()
            .label(DEFAULT_LABEL)
            .description(DEFAULT_DESCRIPTION)
            .amount(DEFAULT_AMOUNT)
            .spentDate(DEFAULT_SPENT_DATE)
            .confirmed(DEFAULT_CONFIRMED)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return spent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Spent createUpdatedEntity(EntityManager em) {
        Spent spent = new Spent()
            .label(UPDATED_LABEL)
            .description(UPDATED_DESCRIPTION)
            .amount(UPDATED_AMOUNT)
            .spentDate(UPDATED_SPENT_DATE)
            .confirmed(UPDATED_CONFIRMED)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return spent;
    }

    @BeforeEach
    public void initTest() {
        spent = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpent() throws Exception {
        int databaseSizeBeforeCreate = spentRepository.findAll().size();

        // Create the Spent
        SpentDTO spentDTO = spentMapper.toDto(spent);
        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isCreated());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeCreate + 1);
        Spent testSpent = spentList.get(spentList.size() - 1);
        assertThat(testSpent.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testSpent.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSpent.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testSpent.getSpentDate()).isEqualTo(DEFAULT_SPENT_DATE);
        assertThat(testSpent.isConfirmed()).isEqualTo(DEFAULT_CONFIRMED);
        assertThat(testSpent.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createSpentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentRepository.findAll().size();

        // Create the Spent with an existing ID
        spent.setId(1L);
        SpentDTO spentDTO = spentMapper.toDto(spent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setLabel(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setAmount(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSpentDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setSpentDate(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConfirmedIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setConfirmed(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setLastUpdate(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpents() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList
        restSpentMockMvc.perform(get("/api/spents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spent.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spentDate").value(hasItem(DEFAULT_SPENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].confirmed").value(hasItem(DEFAULT_CONFIRMED.booleanValue())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getSpent() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get the spent
        restSpentMockMvc.perform(get("/api/spents/{id}", spent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(spent.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.spentDate").value(DEFAULT_SPENT_DATE.toString()))
            .andExpect(jsonPath("$.confirmed").value(DEFAULT_CONFIRMED.booleanValue()))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getSpentsByIdFiltering() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        Long id = spent.getId();

        defaultSpentShouldBeFound("id.equals=" + id);
        defaultSpentShouldNotBeFound("id.notEquals=" + id);

        defaultSpentShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSpentShouldNotBeFound("id.greaterThan=" + id);

        defaultSpentShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSpentShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllSpentsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label equals to DEFAULT_LABEL
        defaultSpentShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the spentList where label equals to UPDATED_LABEL
        defaultSpentShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label not equals to DEFAULT_LABEL
        defaultSpentShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the spentList where label not equals to UPDATED_LABEL
        defaultSpentShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultSpentShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the spentList where label equals to UPDATED_LABEL
        defaultSpentShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label is not null
        defaultSpentShouldBeFound("label.specified=true");

        // Get all the spentList where label is null
        defaultSpentShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllSpentsByLabelContainsSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label contains DEFAULT_LABEL
        defaultSpentShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the spentList where label contains UPDATED_LABEL
        defaultSpentShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label does not contain DEFAULT_LABEL
        defaultSpentShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the spentList where label does not contain UPDATED_LABEL
        defaultSpentShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllSpentsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where description equals to DEFAULT_DESCRIPTION
        defaultSpentShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the spentList where description equals to UPDATED_DESCRIPTION
        defaultSpentShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSpentsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where description not equals to DEFAULT_DESCRIPTION
        defaultSpentShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the spentList where description not equals to UPDATED_DESCRIPTION
        defaultSpentShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSpentsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultSpentShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the spentList where description equals to UPDATED_DESCRIPTION
        defaultSpentShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSpentsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where description is not null
        defaultSpentShouldBeFound("description.specified=true");

        // Get all the spentList where description is null
        defaultSpentShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllSpentsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where description contains DEFAULT_DESCRIPTION
        defaultSpentShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the spentList where description contains UPDATED_DESCRIPTION
        defaultSpentShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSpentsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where description does not contain DEFAULT_DESCRIPTION
        defaultSpentShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the spentList where description does not contain UPDATED_DESCRIPTION
        defaultSpentShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllSpentsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount equals to DEFAULT_AMOUNT
        defaultSpentShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the spentList where amount equals to UPDATED_AMOUNT
        defaultSpentShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount not equals to DEFAULT_AMOUNT
        defaultSpentShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the spentList where amount not equals to UPDATED_AMOUNT
        defaultSpentShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultSpentShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the spentList where amount equals to UPDATED_AMOUNT
        defaultSpentShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount is not null
        defaultSpentShouldBeFound("amount.specified=true");

        // Get all the spentList where amount is null
        defaultSpentShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultSpentShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the spentList where amount is greater than or equal to UPDATED_AMOUNT
        defaultSpentShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount is less than or equal to DEFAULT_AMOUNT
        defaultSpentShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the spentList where amount is less than or equal to SMALLER_AMOUNT
        defaultSpentShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount is less than DEFAULT_AMOUNT
        defaultSpentShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the spentList where amount is less than UPDATED_AMOUNT
        defaultSpentShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount is greater than DEFAULT_AMOUNT
        defaultSpentShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the spentList where amount is greater than SMALLER_AMOUNT
        defaultSpentShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }


    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate equals to DEFAULT_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.equals=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate equals to UPDATED_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.equals=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate not equals to DEFAULT_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.notEquals=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate not equals to UPDATED_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.notEquals=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate in DEFAULT_SPENT_DATE or UPDATED_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.in=" + DEFAULT_SPENT_DATE + "," + UPDATED_SPENT_DATE);

        // Get all the spentList where spentDate equals to UPDATED_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.in=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate is not null
        defaultSpentShouldBeFound("spentDate.specified=true");

        // Get all the spentList where spentDate is null
        defaultSpentShouldNotBeFound("spentDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate is greater than or equal to DEFAULT_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.greaterThanOrEqual=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate is greater than or equal to UPDATED_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.greaterThanOrEqual=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate is less than or equal to DEFAULT_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.lessThanOrEqual=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate is less than or equal to SMALLER_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.lessThanOrEqual=" + SMALLER_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsLessThanSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate is less than DEFAULT_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.lessThan=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate is less than UPDATED_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.lessThan=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate is greater than DEFAULT_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.greaterThan=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate is greater than SMALLER_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.greaterThan=" + SMALLER_SPENT_DATE);
    }


    @Test
    @Transactional
    public void getAllSpentsByConfirmedIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where confirmed equals to DEFAULT_CONFIRMED
        defaultSpentShouldBeFound("confirmed.equals=" + DEFAULT_CONFIRMED);

        // Get all the spentList where confirmed equals to UPDATED_CONFIRMED
        defaultSpentShouldNotBeFound("confirmed.equals=" + UPDATED_CONFIRMED);
    }

    @Test
    @Transactional
    public void getAllSpentsByConfirmedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where confirmed not equals to DEFAULT_CONFIRMED
        defaultSpentShouldNotBeFound("confirmed.notEquals=" + DEFAULT_CONFIRMED);

        // Get all the spentList where confirmed not equals to UPDATED_CONFIRMED
        defaultSpentShouldBeFound("confirmed.notEquals=" + UPDATED_CONFIRMED);
    }

    @Test
    @Transactional
    public void getAllSpentsByConfirmedIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where confirmed in DEFAULT_CONFIRMED or UPDATED_CONFIRMED
        defaultSpentShouldBeFound("confirmed.in=" + DEFAULT_CONFIRMED + "," + UPDATED_CONFIRMED);

        // Get all the spentList where confirmed equals to UPDATED_CONFIRMED
        defaultSpentShouldNotBeFound("confirmed.in=" + UPDATED_CONFIRMED);
    }

    @Test
    @Transactional
    public void getAllSpentsByConfirmedIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where confirmed is not null
        defaultSpentShouldBeFound("confirmed.specified=true");

        // Get all the spentList where confirmed is null
        defaultSpentShouldNotBeFound("confirmed.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultSpentShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultSpentShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultSpentShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultSpentShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the spentList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate is not null
        defaultSpentShouldBeFound("lastUpdate.specified=true");

        // Get all the spentList where lastUpdate is null
        defaultSpentShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultSpentShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultSpentShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultSpentShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultSpentShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultSpentShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultSpentShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultSpentShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultSpentShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllSpentsBySpentSharingIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);
        SpentSharing spentSharing = SpentSharingResourceIT.createEntity(em);
        em.persist(spentSharing);
        em.flush();
        spent.addSpentSharing(spentSharing);
        spentRepository.saveAndFlush(spent);
        Long spentSharingId = spentSharing.getId();

        // Get all the spentList where spentSharing equals to spentSharingId
        defaultSpentShouldBeFound("spentSharingId.equals=" + spentSharingId);

        // Get all the spentList where spentSharing equals to spentSharingId + 1
        defaultSpentShouldNotBeFound("spentSharingId.equals=" + (spentSharingId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentsByWalletIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);
        Wallet wallet = WalletResourceIT.createEntity(em);
        em.persist(wallet);
        em.flush();
        spent.setWallet(wallet);
        spentRepository.saveAndFlush(spent);
        Long walletId = wallet.getId();

        // Get all the spentList where wallet equals to walletId
        defaultSpentShouldBeFound("walletId.equals=" + walletId);

        // Get all the spentList where wallet equals to walletId + 1
        defaultSpentShouldNotBeFound("walletId.equals=" + (walletId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentsBySpenderIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);
        Profil spender = ProfilResourceIT.createEntity(em);
        em.persist(spender);
        em.flush();
        spent.setSpender(spender);
        spentRepository.saveAndFlush(spent);
        Long spenderId = spender.getId();

        // Get all the spentList where spender equals to spenderId
        defaultSpentShouldBeFound("spenderId.equals=" + spenderId);

        // Get all the spentList where spender equals to spenderId + 1
        defaultSpentShouldNotBeFound("spenderId.equals=" + (spenderId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentShouldBeFound(String filter) throws Exception {
        restSpentMockMvc.perform(get("/api/spents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spent.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spentDate").value(hasItem(DEFAULT_SPENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].confirmed").value(hasItem(DEFAULT_CONFIRMED.booleanValue())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restSpentMockMvc.perform(get("/api/spents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentShouldNotBeFound(String filter) throws Exception {
        restSpentMockMvc.perform(get("/api/spents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentMockMvc.perform(get("/api/spents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpent() throws Exception {
        // Get the spent
        restSpentMockMvc.perform(get("/api/spents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpent() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        int databaseSizeBeforeUpdate = spentRepository.findAll().size();

        // Update the spent
        Spent updatedSpent = spentRepository.findById(spent.getId()).get();
        // Disconnect from session so that the updates on updatedSpent are not directly saved in db
        em.detach(updatedSpent);
        updatedSpent
            .label(UPDATED_LABEL)
            .description(UPDATED_DESCRIPTION)
            .amount(UPDATED_AMOUNT)
            .spentDate(UPDATED_SPENT_DATE)
            .confirmed(UPDATED_CONFIRMED)
            .lastUpdate(UPDATED_LAST_UPDATE);
        SpentDTO spentDTO = spentMapper.toDto(updatedSpent);

        restSpentMockMvc.perform(put("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isOk());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeUpdate);
        Spent testSpent = spentList.get(spentList.size() - 1);
        assertThat(testSpent.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testSpent.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSpent.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testSpent.getSpentDate()).isEqualTo(UPDATED_SPENT_DATE);
        assertThat(testSpent.isConfirmed()).isEqualTo(UPDATED_CONFIRMED);
        assertThat(testSpent.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSpent() throws Exception {
        int databaseSizeBeforeUpdate = spentRepository.findAll().size();

        // Create the Spent
        SpentDTO spentDTO = spentMapper.toDto(spent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentMockMvc.perform(put("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpent() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        int databaseSizeBeforeDelete = spentRepository.findAll().size();

        // Delete the spent
        restSpentMockMvc.perform(delete("/api/spents/{id}", spent.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
