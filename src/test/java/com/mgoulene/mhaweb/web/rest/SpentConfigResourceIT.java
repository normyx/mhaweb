package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.SpentConfig;
import com.mgoulene.mhaweb.domain.Wallet;
import com.mgoulene.mhaweb.repository.SpentConfigRepository;
import com.mgoulene.mhaweb.service.SpentConfigService;
import com.mgoulene.mhaweb.service.dto.SpentConfigDTO;
import com.mgoulene.mhaweb.service.mapper.SpentConfigMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.SpentConfigCriteria;
import com.mgoulene.mhaweb.service.SpentConfigQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SpentConfigResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class SpentConfigResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final Float DEFAULT_AMOUNT = 0F;
    private static final Float UPDATED_AMOUNT = 1F;
    private static final Float SMALLER_AMOUNT = 0F - 1F;

    private static final String DEFAULT_SPENT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_SPENT_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private SpentConfigRepository spentConfigRepository;

    @Autowired
    private SpentConfigMapper spentConfigMapper;

    @Autowired
    private SpentConfigService spentConfigService;

    @Autowired
    private SpentConfigQueryService spentConfigQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentConfigMockMvc;

    private SpentConfig spentConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentConfigResource spentConfigResource = new SpentConfigResource(spentConfigService, spentConfigQueryService);
        this.restSpentConfigMockMvc = MockMvcBuilders.standaloneSetup(spentConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentConfig createEntity(EntityManager em) {
        SpentConfig spentConfig = new SpentConfig()
            .label(DEFAULT_LABEL)
            .amount(DEFAULT_AMOUNT)
            .spentLabel(DEFAULT_SPENT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        // Add required entity
        Wallet wallet;
        if (TestUtil.findAll(em, Wallet.class).isEmpty()) {
            wallet = WalletResourceIT.createEntity(em);
            em.persist(wallet);
            em.flush();
        } else {
            wallet = TestUtil.findAll(em, Wallet.class).get(0);
        }
        spentConfig.setWallet(wallet);
        return spentConfig;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentConfig createUpdatedEntity(EntityManager em) {
        SpentConfig spentConfig = new SpentConfig()
            .label(UPDATED_LABEL)
            .amount(UPDATED_AMOUNT)
            .spentLabel(UPDATED_SPENT_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        // Add required entity
        Wallet wallet;
        if (TestUtil.findAll(em, Wallet.class).isEmpty()) {
            wallet = WalletResourceIT.createUpdatedEntity(em);
            em.persist(wallet);
            em.flush();
        } else {
            wallet = TestUtil.findAll(em, Wallet.class).get(0);
        }
        spentConfig.setWallet(wallet);
        return spentConfig;
    }

    @BeforeEach
    public void initTest() {
        spentConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpentConfig() throws Exception {
        int databaseSizeBeforeCreate = spentConfigRepository.findAll().size();

        // Create the SpentConfig
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);
        restSpentConfigMockMvc.perform(post("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeCreate + 1);
        SpentConfig testSpentConfig = spentConfigList.get(spentConfigList.size() - 1);
        assertThat(testSpentConfig.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testSpentConfig.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testSpentConfig.getSpentLabel()).isEqualTo(DEFAULT_SPENT_LABEL);
        assertThat(testSpentConfig.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createSpentConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentConfigRepository.findAll().size();

        // Create the SpentConfig with an existing ID
        spentConfig.setId(1L);
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentConfigMockMvc.perform(post("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentConfigRepository.findAll().size();
        // set the field null
        spentConfig.setLabel(null);

        // Create the SpentConfig, which fails.
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);

        restSpentConfigMockMvc.perform(post("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isBadRequest());

        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentConfigRepository.findAll().size();
        // set the field null
        spentConfig.setLastUpdate(null);

        // Create the SpentConfig, which fails.
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);

        restSpentConfigMockMvc.perform(post("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isBadRequest());

        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpentConfigs() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList
        restSpentConfigMockMvc.perform(get("/api/spent-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spentLabel").value(hasItem(DEFAULT_SPENT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getSpentConfig() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get the spentConfig
        restSpentConfigMockMvc.perform(get("/api/spent-configs/{id}", spentConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(spentConfig.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.spentLabel").value(DEFAULT_SPENT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getSpentConfigsByIdFiltering() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        Long id = spentConfig.getId();

        defaultSpentConfigShouldBeFound("id.equals=" + id);
        defaultSpentConfigShouldNotBeFound("id.notEquals=" + id);

        defaultSpentConfigShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSpentConfigShouldNotBeFound("id.greaterThan=" + id);

        defaultSpentConfigShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSpentConfigShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllSpentConfigsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label equals to DEFAULT_LABEL
        defaultSpentConfigShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the spentConfigList where label equals to UPDATED_LABEL
        defaultSpentConfigShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label not equals to DEFAULT_LABEL
        defaultSpentConfigShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the spentConfigList where label not equals to UPDATED_LABEL
        defaultSpentConfigShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultSpentConfigShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the spentConfigList where label equals to UPDATED_LABEL
        defaultSpentConfigShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label is not null
        defaultSpentConfigShouldBeFound("label.specified=true");

        // Get all the spentConfigList where label is null
        defaultSpentConfigShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllSpentConfigsByLabelContainsSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label contains DEFAULT_LABEL
        defaultSpentConfigShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the spentConfigList where label contains UPDATED_LABEL
        defaultSpentConfigShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label does not contain DEFAULT_LABEL
        defaultSpentConfigShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the spentConfigList where label does not contain UPDATED_LABEL
        defaultSpentConfigShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount equals to DEFAULT_AMOUNT
        defaultSpentConfigShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the spentConfigList where amount equals to UPDATED_AMOUNT
        defaultSpentConfigShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount not equals to DEFAULT_AMOUNT
        defaultSpentConfigShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the spentConfigList where amount not equals to UPDATED_AMOUNT
        defaultSpentConfigShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultSpentConfigShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the spentConfigList where amount equals to UPDATED_AMOUNT
        defaultSpentConfigShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount is not null
        defaultSpentConfigShouldBeFound("amount.specified=true");

        // Get all the spentConfigList where amount is null
        defaultSpentConfigShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultSpentConfigShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the spentConfigList where amount is greater than or equal to UPDATED_AMOUNT
        defaultSpentConfigShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount is less than or equal to DEFAULT_AMOUNT
        defaultSpentConfigShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the spentConfigList where amount is less than or equal to SMALLER_AMOUNT
        defaultSpentConfigShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount is less than DEFAULT_AMOUNT
        defaultSpentConfigShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the spentConfigList where amount is less than UPDATED_AMOUNT
        defaultSpentConfigShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where amount is greater than DEFAULT_AMOUNT
        defaultSpentConfigShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the spentConfigList where amount is greater than SMALLER_AMOUNT
        defaultSpentConfigShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }


    @Test
    @Transactional
    public void getAllSpentConfigsBySpentLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where spentLabel equals to DEFAULT_SPENT_LABEL
        defaultSpentConfigShouldBeFound("spentLabel.equals=" + DEFAULT_SPENT_LABEL);

        // Get all the spentConfigList where spentLabel equals to UPDATED_SPENT_LABEL
        defaultSpentConfigShouldNotBeFound("spentLabel.equals=" + UPDATED_SPENT_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsBySpentLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where spentLabel not equals to DEFAULT_SPENT_LABEL
        defaultSpentConfigShouldNotBeFound("spentLabel.notEquals=" + DEFAULT_SPENT_LABEL);

        // Get all the spentConfigList where spentLabel not equals to UPDATED_SPENT_LABEL
        defaultSpentConfigShouldBeFound("spentLabel.notEquals=" + UPDATED_SPENT_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsBySpentLabelIsInShouldWork() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where spentLabel in DEFAULT_SPENT_LABEL or UPDATED_SPENT_LABEL
        defaultSpentConfigShouldBeFound("spentLabel.in=" + DEFAULT_SPENT_LABEL + "," + UPDATED_SPENT_LABEL);

        // Get all the spentConfigList where spentLabel equals to UPDATED_SPENT_LABEL
        defaultSpentConfigShouldNotBeFound("spentLabel.in=" + UPDATED_SPENT_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsBySpentLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where spentLabel is not null
        defaultSpentConfigShouldBeFound("spentLabel.specified=true");

        // Get all the spentConfigList where spentLabel is null
        defaultSpentConfigShouldNotBeFound("spentLabel.specified=false");
    }
                @Test
    @Transactional
    public void getAllSpentConfigsBySpentLabelContainsSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where spentLabel contains DEFAULT_SPENT_LABEL
        defaultSpentConfigShouldBeFound("spentLabel.contains=" + DEFAULT_SPENT_LABEL);

        // Get all the spentConfigList where spentLabel contains UPDATED_SPENT_LABEL
        defaultSpentConfigShouldNotBeFound("spentLabel.contains=" + UPDATED_SPENT_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsBySpentLabelNotContainsSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where spentLabel does not contain DEFAULT_SPENT_LABEL
        defaultSpentConfigShouldNotBeFound("spentLabel.doesNotContain=" + DEFAULT_SPENT_LABEL);

        // Get all the spentConfigList where spentLabel does not contain UPDATED_SPENT_LABEL
        defaultSpentConfigShouldBeFound("spentLabel.doesNotContain=" + UPDATED_SPENT_LABEL);
    }


    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultSpentConfigShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentConfigList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentConfigShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultSpentConfigShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentConfigList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultSpentConfigShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultSpentConfigShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the spentConfigList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentConfigShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate is not null
        defaultSpentConfigShouldBeFound("lastUpdate.specified=true");

        // Get all the spentConfigList where lastUpdate is null
        defaultSpentConfigShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultSpentConfigShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentConfigList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultSpentConfigShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultSpentConfigShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentConfigList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultSpentConfigShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultSpentConfigShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentConfigList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultSpentConfigShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultSpentConfigShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentConfigList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultSpentConfigShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllSpentConfigsByWalletIsEqualToSomething() throws Exception {
        // Get already existing entity
        Wallet wallet = spentConfig.getWallet();
        spentConfigRepository.saveAndFlush(spentConfig);
        Long walletId = wallet.getId();

        // Get all the spentConfigList where wallet equals to walletId
        defaultSpentConfigShouldBeFound("walletId.equals=" + walletId);

        // Get all the spentConfigList where wallet equals to walletId + 1
        defaultSpentConfigShouldNotBeFound("walletId.equals=" + (walletId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentConfigShouldBeFound(String filter) throws Exception {
        restSpentConfigMockMvc.perform(get("/api/spent-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spentLabel").value(hasItem(DEFAULT_SPENT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restSpentConfigMockMvc.perform(get("/api/spent-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentConfigShouldNotBeFound(String filter) throws Exception {
        restSpentConfigMockMvc.perform(get("/api/spent-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentConfigMockMvc.perform(get("/api/spent-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpentConfig() throws Exception {
        // Get the spentConfig
        restSpentConfigMockMvc.perform(get("/api/spent-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpentConfig() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        int databaseSizeBeforeUpdate = spentConfigRepository.findAll().size();

        // Update the spentConfig
        SpentConfig updatedSpentConfig = spentConfigRepository.findById(spentConfig.getId()).get();
        // Disconnect from session so that the updates on updatedSpentConfig are not directly saved in db
        em.detach(updatedSpentConfig);
        updatedSpentConfig
            .label(UPDATED_LABEL)
            .amount(UPDATED_AMOUNT)
            .spentLabel(UPDATED_SPENT_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(updatedSpentConfig);

        restSpentConfigMockMvc.perform(put("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isOk());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeUpdate);
        SpentConfig testSpentConfig = spentConfigList.get(spentConfigList.size() - 1);
        assertThat(testSpentConfig.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testSpentConfig.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testSpentConfig.getSpentLabel()).isEqualTo(UPDATED_SPENT_LABEL);
        assertThat(testSpentConfig.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSpentConfig() throws Exception {
        int databaseSizeBeforeUpdate = spentConfigRepository.findAll().size();

        // Create the SpentConfig
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentConfigMockMvc.perform(put("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpentConfig() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        int databaseSizeBeforeDelete = spentConfigRepository.findAll().size();

        // Delete the spentConfig
        restSpentConfigMockMvc.perform(delete("/api/spent-configs/{id}", spentConfig.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
