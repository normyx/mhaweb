package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.Workspace;
import com.mgoulene.mhaweb.domain.ShoppingCart;
import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.domain.TodoList;
import com.mgoulene.mhaweb.domain.TodoListTemplate;
import com.mgoulene.mhaweb.domain.Wallet;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.repository.WorkspaceRepository;
import com.mgoulene.mhaweb.service.WorkspaceService;
import com.mgoulene.mhaweb.service.dto.WorkspaceDTO;
import com.mgoulene.mhaweb.service.mapper.WorkspaceMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.WorkspaceCriteria;
import com.mgoulene.mhaweb.service.WorkspaceQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WorkspaceResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class WorkspaceResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private WorkspaceRepository workspaceRepository;

    @Mock
    private WorkspaceRepository workspaceRepositoryMock;

    @Autowired
    private WorkspaceMapper workspaceMapper;

    @Mock
    private WorkspaceService workspaceServiceMock;

    @Autowired
    private WorkspaceService workspaceService;

    @Autowired
    private WorkspaceQueryService workspaceQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restWorkspaceMockMvc;

    private Workspace workspace;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WorkspaceResource workspaceResource = new WorkspaceResource(workspaceService, workspaceQueryService);
        this.restWorkspaceMockMvc = MockMvcBuilders.standaloneSetup(workspaceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workspace createEntity(EntityManager em) {
        Workspace workspace = new Workspace()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return workspace;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workspace createUpdatedEntity(EntityManager em) {
        Workspace workspace = new Workspace()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return workspace;
    }

    @BeforeEach
    public void initTest() {
        workspace = createEntity(em);
    }

    @Test
    @Transactional
    public void createWorkspace() throws Exception {
        int databaseSizeBeforeCreate = workspaceRepository.findAll().size();

        // Create the Workspace
        WorkspaceDTO workspaceDTO = workspaceMapper.toDto(workspace);
        restWorkspaceMockMvc.perform(post("/api/workspaces")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workspaceDTO)))
            .andExpect(status().isCreated());

        // Validate the Workspace in the database
        List<Workspace> workspaceList = workspaceRepository.findAll();
        assertThat(workspaceList).hasSize(databaseSizeBeforeCreate + 1);
        Workspace testWorkspace = workspaceList.get(workspaceList.size() - 1);
        assertThat(testWorkspace.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testWorkspace.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createWorkspaceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = workspaceRepository.findAll().size();

        // Create the Workspace with an existing ID
        workspace.setId(1L);
        WorkspaceDTO workspaceDTO = workspaceMapper.toDto(workspace);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWorkspaceMockMvc.perform(post("/api/workspaces")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workspaceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Workspace in the database
        List<Workspace> workspaceList = workspaceRepository.findAll();
        assertThat(workspaceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = workspaceRepository.findAll().size();
        // set the field null
        workspace.setLabel(null);

        // Create the Workspace, which fails.
        WorkspaceDTO workspaceDTO = workspaceMapper.toDto(workspace);

        restWorkspaceMockMvc.perform(post("/api/workspaces")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workspaceDTO)))
            .andExpect(status().isBadRequest());

        List<Workspace> workspaceList = workspaceRepository.findAll();
        assertThat(workspaceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = workspaceRepository.findAll().size();
        // set the field null
        workspace.setLastUpdate(null);

        // Create the Workspace, which fails.
        WorkspaceDTO workspaceDTO = workspaceMapper.toDto(workspace);

        restWorkspaceMockMvc.perform(post("/api/workspaces")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workspaceDTO)))
            .andExpect(status().isBadRequest());

        List<Workspace> workspaceList = workspaceRepository.findAll();
        assertThat(workspaceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllWorkspaces() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList
        restWorkspaceMockMvc.perform(get("/api/workspaces?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(workspace.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllWorkspacesWithEagerRelationshipsIsEnabled() throws Exception {
        WorkspaceResource workspaceResource = new WorkspaceResource(workspaceServiceMock, workspaceQueryService);
        when(workspaceServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restWorkspaceMockMvc = MockMvcBuilders.standaloneSetup(workspaceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restWorkspaceMockMvc.perform(get("/api/workspaces?eagerload=true"))
        .andExpect(status().isOk());

        verify(workspaceServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllWorkspacesWithEagerRelationshipsIsNotEnabled() throws Exception {
        WorkspaceResource workspaceResource = new WorkspaceResource(workspaceServiceMock, workspaceQueryService);
            when(workspaceServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restWorkspaceMockMvc = MockMvcBuilders.standaloneSetup(workspaceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restWorkspaceMockMvc.perform(get("/api/workspaces?eagerload=true"))
        .andExpect(status().isOk());

            verify(workspaceServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getWorkspace() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get the workspace
        restWorkspaceMockMvc.perform(get("/api/workspaces/{id}", workspace.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(workspace.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getWorkspacesByIdFiltering() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        Long id = workspace.getId();

        defaultWorkspaceShouldBeFound("id.equals=" + id);
        defaultWorkspaceShouldNotBeFound("id.notEquals=" + id);

        defaultWorkspaceShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultWorkspaceShouldNotBeFound("id.greaterThan=" + id);

        defaultWorkspaceShouldBeFound("id.lessThanOrEqual=" + id);
        defaultWorkspaceShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllWorkspacesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where label equals to DEFAULT_LABEL
        defaultWorkspaceShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the workspaceList where label equals to UPDATED_LABEL
        defaultWorkspaceShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where label not equals to DEFAULT_LABEL
        defaultWorkspaceShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the workspaceList where label not equals to UPDATED_LABEL
        defaultWorkspaceShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultWorkspaceShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the workspaceList where label equals to UPDATED_LABEL
        defaultWorkspaceShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where label is not null
        defaultWorkspaceShouldBeFound("label.specified=true");

        // Get all the workspaceList where label is null
        defaultWorkspaceShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllWorkspacesByLabelContainsSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where label contains DEFAULT_LABEL
        defaultWorkspaceShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the workspaceList where label contains UPDATED_LABEL
        defaultWorkspaceShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where label does not contain DEFAULT_LABEL
        defaultWorkspaceShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the workspaceList where label does not contain UPDATED_LABEL
        defaultWorkspaceShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultWorkspaceShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the workspaceList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultWorkspaceShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultWorkspaceShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the workspaceList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultWorkspaceShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultWorkspaceShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the workspaceList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultWorkspaceShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate is not null
        defaultWorkspaceShouldBeFound("lastUpdate.specified=true");

        // Get all the workspaceList where lastUpdate is null
        defaultWorkspaceShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultWorkspaceShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the workspaceList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultWorkspaceShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultWorkspaceShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the workspaceList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultWorkspaceShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultWorkspaceShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the workspaceList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultWorkspaceShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllWorkspacesByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        // Get all the workspaceList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultWorkspaceShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the workspaceList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultWorkspaceShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllWorkspacesByShoppingCartIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);
        ShoppingCart shoppingCart = ShoppingCartResourceIT.createEntity(em);
        em.persist(shoppingCart);
        em.flush();
        workspace.addShoppingCart(shoppingCart);
        workspaceRepository.saveAndFlush(workspace);
        Long shoppingCartId = shoppingCart.getId();

        // Get all the workspaceList where shoppingCart equals to shoppingCartId
        defaultWorkspaceShouldBeFound("shoppingCartId.equals=" + shoppingCartId);

        // Get all the workspaceList where shoppingCart equals to shoppingCartId + 1
        defaultWorkspaceShouldNotBeFound("shoppingCartId.equals=" + (shoppingCartId + 1));
    }


    @Test
    @Transactional
    public void getAllWorkspacesByTaskProjectIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);
        TaskProject taskProject = TaskProjectResourceIT.createEntity(em);
        em.persist(taskProject);
        em.flush();
        workspace.addTaskProject(taskProject);
        workspaceRepository.saveAndFlush(workspace);
        Long taskProjectId = taskProject.getId();

        // Get all the workspaceList where taskProject equals to taskProjectId
        defaultWorkspaceShouldBeFound("taskProjectId.equals=" + taskProjectId);

        // Get all the workspaceList where taskProject equals to taskProjectId + 1
        defaultWorkspaceShouldNotBeFound("taskProjectId.equals=" + (taskProjectId + 1));
    }


    @Test
    @Transactional
    public void getAllWorkspacesByTodoListIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);
        TodoList todoList = TodoListResourceIT.createEntity(em);
        em.persist(todoList);
        em.flush();
        workspace.addTodoList(todoList);
        workspaceRepository.saveAndFlush(workspace);
        Long todoListId = todoList.getId();

        // Get all the workspaceList where todoList equals to todoListId
        defaultWorkspaceShouldBeFound("todoListId.equals=" + todoListId);

        // Get all the workspaceList where todoList equals to todoListId + 1
        defaultWorkspaceShouldNotBeFound("todoListId.equals=" + (todoListId + 1));
    }


    @Test
    @Transactional
    public void getAllWorkspacesByTodoListTemplateIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);
        TodoListTemplate todoListTemplate = TodoListTemplateResourceIT.createEntity(em);
        em.persist(todoListTemplate);
        em.flush();
        workspace.addTodoListTemplate(todoListTemplate);
        workspaceRepository.saveAndFlush(workspace);
        Long todoListTemplateId = todoListTemplate.getId();

        // Get all the workspaceList where todoListTemplate equals to todoListTemplateId
        defaultWorkspaceShouldBeFound("todoListTemplateId.equals=" + todoListTemplateId);

        // Get all the workspaceList where todoListTemplate equals to todoListTemplateId + 1
        defaultWorkspaceShouldNotBeFound("todoListTemplateId.equals=" + (todoListTemplateId + 1));
    }


    @Test
    @Transactional
    public void getAllWorkspacesByWalletIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);
        Wallet wallet = WalletResourceIT.createEntity(em);
        em.persist(wallet);
        em.flush();
        workspace.addWallet(wallet);
        workspaceRepository.saveAndFlush(workspace);
        Long walletId = wallet.getId();

        // Get all the workspaceList where wallet equals to walletId
        defaultWorkspaceShouldBeFound("walletId.equals=" + walletId);

        // Get all the workspaceList where wallet equals to walletId + 1
        defaultWorkspaceShouldNotBeFound("walletId.equals=" + (walletId + 1));
    }


    @Test
    @Transactional
    public void getAllWorkspacesByOwnerIsEqualToSomething() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);
        Profil owner = ProfilResourceIT.createEntity(em);
        em.persist(owner);
        em.flush();
        workspace.addOwner(owner);
        workspaceRepository.saveAndFlush(workspace);
        Long ownerId = owner.getId();

        // Get all the workspaceList where owner equals to ownerId
        defaultWorkspaceShouldBeFound("ownerId.equals=" + ownerId);

        // Get all the workspaceList where owner equals to ownerId + 1
        defaultWorkspaceShouldNotBeFound("ownerId.equals=" + (ownerId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultWorkspaceShouldBeFound(String filter) throws Exception {
        restWorkspaceMockMvc.perform(get("/api/workspaces?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(workspace.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restWorkspaceMockMvc.perform(get("/api/workspaces/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultWorkspaceShouldNotBeFound(String filter) throws Exception {
        restWorkspaceMockMvc.perform(get("/api/workspaces?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restWorkspaceMockMvc.perform(get("/api/workspaces/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingWorkspace() throws Exception {
        // Get the workspace
        restWorkspaceMockMvc.perform(get("/api/workspaces/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWorkspace() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        int databaseSizeBeforeUpdate = workspaceRepository.findAll().size();

        // Update the workspace
        Workspace updatedWorkspace = workspaceRepository.findById(workspace.getId()).get();
        // Disconnect from session so that the updates on updatedWorkspace are not directly saved in db
        em.detach(updatedWorkspace);
        updatedWorkspace
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        WorkspaceDTO workspaceDTO = workspaceMapper.toDto(updatedWorkspace);

        restWorkspaceMockMvc.perform(put("/api/workspaces")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workspaceDTO)))
            .andExpect(status().isOk());

        // Validate the Workspace in the database
        List<Workspace> workspaceList = workspaceRepository.findAll();
        assertThat(workspaceList).hasSize(databaseSizeBeforeUpdate);
        Workspace testWorkspace = workspaceList.get(workspaceList.size() - 1);
        assertThat(testWorkspace.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testWorkspace.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingWorkspace() throws Exception {
        int databaseSizeBeforeUpdate = workspaceRepository.findAll().size();

        // Create the Workspace
        WorkspaceDTO workspaceDTO = workspaceMapper.toDto(workspace);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWorkspaceMockMvc.perform(put("/api/workspaces")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workspaceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Workspace in the database
        List<Workspace> workspaceList = workspaceRepository.findAll();
        assertThat(workspaceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWorkspace() throws Exception {
        // Initialize the database
        workspaceRepository.saveAndFlush(workspace);

        int databaseSizeBeforeDelete = workspaceRepository.findAll().size();

        // Delete the workspace
        restWorkspaceMockMvc.perform(delete("/api/workspaces/{id}", workspace.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Workspace> workspaceList = workspaceRepository.findAll();
        assertThat(workspaceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
