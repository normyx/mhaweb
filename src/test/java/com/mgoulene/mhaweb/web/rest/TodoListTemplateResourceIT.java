package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.TodoListTemplate;
import com.mgoulene.mhaweb.domain.TodoTemplate;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.domain.Workspace;
import com.mgoulene.mhaweb.repository.TodoListTemplateRepository;
import com.mgoulene.mhaweb.service.TodoListTemplateService;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateDTO;
import com.mgoulene.mhaweb.service.mapper.TodoListTemplateMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateCriteria;
import com.mgoulene.mhaweb.service.TodoListTemplateQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TodoListTemplateResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class TodoListTemplateResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private TodoListTemplateRepository todoListTemplateRepository;

    @Mock
    private TodoListTemplateRepository todoListTemplateRepositoryMock;

    @Autowired
    private TodoListTemplateMapper todoListTemplateMapper;

    @Mock
    private TodoListTemplateService todoListTemplateServiceMock;

    @Autowired
    private TodoListTemplateService todoListTemplateService;

    @Autowired
    private TodoListTemplateQueryService todoListTemplateQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTodoListTemplateMockMvc;

    private TodoListTemplate todoListTemplate;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TodoListTemplateResource todoListTemplateResource = new TodoListTemplateResource(todoListTemplateService, todoListTemplateQueryService);
        this.restTodoListTemplateMockMvc = MockMvcBuilders.standaloneSetup(todoListTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TodoListTemplate createEntity(EntityManager em) {
        TodoListTemplate todoListTemplate = new TodoListTemplate()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        // Add required entity
        Workspace workspace;
        if (TestUtil.findAll(em, Workspace.class).isEmpty()) {
            workspace = WorkspaceResourceIT.createEntity(em);
            em.persist(workspace);
            em.flush();
        } else {
            workspace = TestUtil.findAll(em, Workspace.class).get(0);
        }
        todoListTemplate.setWorkspace(workspace);
        return todoListTemplate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TodoListTemplate createUpdatedEntity(EntityManager em) {
        TodoListTemplate todoListTemplate = new TodoListTemplate()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        // Add required entity
        Workspace workspace;
        if (TestUtil.findAll(em, Workspace.class).isEmpty()) {
            workspace = WorkspaceResourceIT.createUpdatedEntity(em);
            em.persist(workspace);
            em.flush();
        } else {
            workspace = TestUtil.findAll(em, Workspace.class).get(0);
        }
        todoListTemplate.setWorkspace(workspace);
        return todoListTemplate;
    }

    @BeforeEach
    public void initTest() {
        todoListTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createTodoListTemplate() throws Exception {
        int databaseSizeBeforeCreate = todoListTemplateRepository.findAll().size();

        // Create the TodoListTemplate
        TodoListTemplateDTO todoListTemplateDTO = todoListTemplateMapper.toDto(todoListTemplate);
        restTodoListTemplateMockMvc.perform(post("/api/todo-list-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListTemplateDTO)))
            .andExpect(status().isCreated());

        // Validate the TodoListTemplate in the database
        List<TodoListTemplate> todoListTemplateList = todoListTemplateRepository.findAll();
        assertThat(todoListTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        TodoListTemplate testTodoListTemplate = todoListTemplateList.get(todoListTemplateList.size() - 1);
        assertThat(testTodoListTemplate.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testTodoListTemplate.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createTodoListTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = todoListTemplateRepository.findAll().size();

        // Create the TodoListTemplate with an existing ID
        todoListTemplate.setId(1L);
        TodoListTemplateDTO todoListTemplateDTO = todoListTemplateMapper.toDto(todoListTemplate);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTodoListTemplateMockMvc.perform(post("/api/todo-list-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListTemplateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TodoListTemplate in the database
        List<TodoListTemplate> todoListTemplateList = todoListTemplateRepository.findAll();
        assertThat(todoListTemplateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = todoListTemplateRepository.findAll().size();
        // set the field null
        todoListTemplate.setLabel(null);

        // Create the TodoListTemplate, which fails.
        TodoListTemplateDTO todoListTemplateDTO = todoListTemplateMapper.toDto(todoListTemplate);

        restTodoListTemplateMockMvc.perform(post("/api/todo-list-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<TodoListTemplate> todoListTemplateList = todoListTemplateRepository.findAll();
        assertThat(todoListTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = todoListTemplateRepository.findAll().size();
        // set the field null
        todoListTemplate.setLastUpdate(null);

        // Create the TodoListTemplate, which fails.
        TodoListTemplateDTO todoListTemplateDTO = todoListTemplateMapper.toDto(todoListTemplate);

        restTodoListTemplateMockMvc.perform(post("/api/todo-list-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<TodoListTemplate> todoListTemplateList = todoListTemplateRepository.findAll();
        assertThat(todoListTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplates() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList
        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(todoListTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllTodoListTemplatesWithEagerRelationshipsIsEnabled() throws Exception {
        TodoListTemplateResource todoListTemplateResource = new TodoListTemplateResource(todoListTemplateServiceMock, todoListTemplateQueryService);
        when(todoListTemplateServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restTodoListTemplateMockMvc = MockMvcBuilders.standaloneSetup(todoListTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates?eagerload=true"))
        .andExpect(status().isOk());

        verify(todoListTemplateServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllTodoListTemplatesWithEagerRelationshipsIsNotEnabled() throws Exception {
        TodoListTemplateResource todoListTemplateResource = new TodoListTemplateResource(todoListTemplateServiceMock, todoListTemplateQueryService);
            when(todoListTemplateServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restTodoListTemplateMockMvc = MockMvcBuilders.standaloneSetup(todoListTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates?eagerload=true"))
        .andExpect(status().isOk());

            verify(todoListTemplateServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getTodoListTemplate() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get the todoListTemplate
        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates/{id}", todoListTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(todoListTemplate.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getTodoListTemplatesByIdFiltering() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        Long id = todoListTemplate.getId();

        defaultTodoListTemplateShouldBeFound("id.equals=" + id);
        defaultTodoListTemplateShouldNotBeFound("id.notEquals=" + id);

        defaultTodoListTemplateShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTodoListTemplateShouldNotBeFound("id.greaterThan=" + id);

        defaultTodoListTemplateShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTodoListTemplateShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTodoListTemplatesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where label equals to DEFAULT_LABEL
        defaultTodoListTemplateShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the todoListTemplateList where label equals to UPDATED_LABEL
        defaultTodoListTemplateShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where label not equals to DEFAULT_LABEL
        defaultTodoListTemplateShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the todoListTemplateList where label not equals to UPDATED_LABEL
        defaultTodoListTemplateShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultTodoListTemplateShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the todoListTemplateList where label equals to UPDATED_LABEL
        defaultTodoListTemplateShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where label is not null
        defaultTodoListTemplateShouldBeFound("label.specified=true");

        // Get all the todoListTemplateList where label is null
        defaultTodoListTemplateShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllTodoListTemplatesByLabelContainsSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where label contains DEFAULT_LABEL
        defaultTodoListTemplateShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the todoListTemplateList where label contains UPDATED_LABEL
        defaultTodoListTemplateShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where label does not contain DEFAULT_LABEL
        defaultTodoListTemplateShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the todoListTemplateList where label does not contain UPDATED_LABEL
        defaultTodoListTemplateShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultTodoListTemplateShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListTemplateList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListTemplateList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultTodoListTemplateShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultTodoListTemplateShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the todoListTemplateList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate is not null
        defaultTodoListTemplateShouldBeFound("lastUpdate.specified=true");

        // Get all the todoListTemplateList where lastUpdate is null
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultTodoListTemplateShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListTemplateList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultTodoListTemplateShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListTemplateList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListTemplateList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultTodoListTemplateShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListTemplatesByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        // Get all the todoListTemplateList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultTodoListTemplateShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListTemplateList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultTodoListTemplateShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllTodoListTemplatesByTodoTemplateIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);
        TodoTemplate todoTemplate = TodoTemplateResourceIT.createEntity(em);
        em.persist(todoTemplate);
        em.flush();
        todoListTemplate.addTodoTemplate(todoTemplate);
        todoListTemplateRepository.saveAndFlush(todoListTemplate);
        Long todoTemplateId = todoTemplate.getId();

        // Get all the todoListTemplateList where todoTemplate equals to todoTemplateId
        defaultTodoListTemplateShouldBeFound("todoTemplateId.equals=" + todoTemplateId);

        // Get all the todoListTemplateList where todoTemplate equals to todoTemplateId + 1
        defaultTodoListTemplateShouldNotBeFound("todoTemplateId.equals=" + (todoTemplateId + 1));
    }


    @Test
    @Transactional
    public void getAllTodoListTemplatesByOwnerIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);
        Profil owner = ProfilResourceIT.createEntity(em);
        em.persist(owner);
        em.flush();
        todoListTemplate.addOwner(owner);
        todoListTemplateRepository.saveAndFlush(todoListTemplate);
        Long ownerId = owner.getId();

        // Get all the todoListTemplateList where owner equals to ownerId
        defaultTodoListTemplateShouldBeFound("ownerId.equals=" + ownerId);

        // Get all the todoListTemplateList where owner equals to ownerId + 1
        defaultTodoListTemplateShouldNotBeFound("ownerId.equals=" + (ownerId + 1));
    }


    @Test
    @Transactional
    public void getAllTodoListTemplatesByWorkspaceIsEqualToSomething() throws Exception {
        // Get already existing entity
        Workspace workspace = todoListTemplate.getWorkspace();
        todoListTemplateRepository.saveAndFlush(todoListTemplate);
        Long workspaceId = workspace.getId();

        // Get all the todoListTemplateList where workspace equals to workspaceId
        defaultTodoListTemplateShouldBeFound("workspaceId.equals=" + workspaceId);

        // Get all the todoListTemplateList where workspace equals to workspaceId + 1
        defaultTodoListTemplateShouldNotBeFound("workspaceId.equals=" + (workspaceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTodoListTemplateShouldBeFound(String filter) throws Exception {
        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(todoListTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTodoListTemplateShouldNotBeFound(String filter) throws Exception {
        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTodoListTemplate() throws Exception {
        // Get the todoListTemplate
        restTodoListTemplateMockMvc.perform(get("/api/todo-list-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTodoListTemplate() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        int databaseSizeBeforeUpdate = todoListTemplateRepository.findAll().size();

        // Update the todoListTemplate
        TodoListTemplate updatedTodoListTemplate = todoListTemplateRepository.findById(todoListTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedTodoListTemplate are not directly saved in db
        em.detach(updatedTodoListTemplate);
        updatedTodoListTemplate
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        TodoListTemplateDTO todoListTemplateDTO = todoListTemplateMapper.toDto(updatedTodoListTemplate);

        restTodoListTemplateMockMvc.perform(put("/api/todo-list-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListTemplateDTO)))
            .andExpect(status().isOk());

        // Validate the TodoListTemplate in the database
        List<TodoListTemplate> todoListTemplateList = todoListTemplateRepository.findAll();
        assertThat(todoListTemplateList).hasSize(databaseSizeBeforeUpdate);
        TodoListTemplate testTodoListTemplate = todoListTemplateList.get(todoListTemplateList.size() - 1);
        assertThat(testTodoListTemplate.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testTodoListTemplate.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingTodoListTemplate() throws Exception {
        int databaseSizeBeforeUpdate = todoListTemplateRepository.findAll().size();

        // Create the TodoListTemplate
        TodoListTemplateDTO todoListTemplateDTO = todoListTemplateMapper.toDto(todoListTemplate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTodoListTemplateMockMvc.perform(put("/api/todo-list-templates")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListTemplateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TodoListTemplate in the database
        List<TodoListTemplate> todoListTemplateList = todoListTemplateRepository.findAll();
        assertThat(todoListTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTodoListTemplate() throws Exception {
        // Initialize the database
        todoListTemplateRepository.saveAndFlush(todoListTemplate);

        int databaseSizeBeforeDelete = todoListTemplateRepository.findAll().size();

        // Delete the todoListTemplate
        restTodoListTemplateMockMvc.perform(delete("/api/todo-list-templates/{id}", todoListTemplate.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TodoListTemplate> todoListTemplateList = todoListTemplateRepository.findAll();
        assertThat(todoListTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
