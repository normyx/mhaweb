package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.ShoppingCatalogSheld;
import com.mgoulene.mhaweb.domain.ShoppingCatalogItem;
import com.mgoulene.mhaweb.domain.ShoppingCatalogCart;
import com.mgoulene.mhaweb.repository.ShoppingCatalogSheldRepository;
import com.mgoulene.mhaweb.service.ShoppingCatalogSheldService;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogSheldMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldCriteria;
import com.mgoulene.mhaweb.service.ShoppingCatalogSheldQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShoppingCatalogSheldResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class ShoppingCatalogSheldResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_IS_DEFAULT = false;
    private static final Boolean UPDATED_IS_DEFAULT = true;

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    @Autowired
    private ShoppingCatalogSheldRepository shoppingCatalogSheldRepository;

    @Autowired
    private ShoppingCatalogSheldMapper shoppingCatalogSheldMapper;

    @Autowired
    private ShoppingCatalogSheldService shoppingCatalogSheldService;

    @Autowired
    private ShoppingCatalogSheldQueryService shoppingCatalogSheldQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoppingCatalogSheldMockMvc;

    private ShoppingCatalogSheld shoppingCatalogSheld;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoppingCatalogSheldResource shoppingCatalogSheldResource = new ShoppingCatalogSheldResource(shoppingCatalogSheldService, shoppingCatalogSheldQueryService);
        this.restShoppingCatalogSheldMockMvc = MockMvcBuilders.standaloneSetup(shoppingCatalogSheldResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCatalogSheld createEntity(EntityManager em) {
        ShoppingCatalogSheld shoppingCatalogSheld = new ShoppingCatalogSheld()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE)
            .isDefault(DEFAULT_IS_DEFAULT)
            .isDeleted(DEFAULT_IS_DELETED);
        // Add required entity
        ShoppingCatalogCart shoppingCatalogCart;
        if (TestUtil.findAll(em, ShoppingCatalogCart.class).isEmpty()) {
            shoppingCatalogCart = ShoppingCatalogCartResourceIT.createEntity(em);
            em.persist(shoppingCatalogCart);
            em.flush();
        } else {
            shoppingCatalogCart = TestUtil.findAll(em, ShoppingCatalogCart.class).get(0);
        }
        shoppingCatalogSheld.setShoppingCatalogCart(shoppingCatalogCart);
        return shoppingCatalogSheld;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCatalogSheld createUpdatedEntity(EntityManager em) {
        ShoppingCatalogSheld shoppingCatalogSheld = new ShoppingCatalogSheld()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .isDefault(UPDATED_IS_DEFAULT)
            .isDeleted(UPDATED_IS_DELETED);
        // Add required entity
        ShoppingCatalogCart shoppingCatalogCart;
        if (TestUtil.findAll(em, ShoppingCatalogCart.class).isEmpty()) {
            shoppingCatalogCart = ShoppingCatalogCartResourceIT.createUpdatedEntity(em);
            em.persist(shoppingCatalogCart);
            em.flush();
        } else {
            shoppingCatalogCart = TestUtil.findAll(em, ShoppingCatalogCart.class).get(0);
        }
        shoppingCatalogSheld.setShoppingCatalogCart(shoppingCatalogCart);
        return shoppingCatalogSheld;
    }

    @BeforeEach
    public void initTest() {
        shoppingCatalogSheld = createEntity(em);
    }

    @Test
    @Transactional
    public void createShoppingCatalogSheld() throws Exception {
        int databaseSizeBeforeCreate = shoppingCatalogSheldRepository.findAll().size();

        // Create the ShoppingCatalogSheld
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);
        restShoppingCatalogSheldMockMvc.perform(post("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isCreated());

        // Validate the ShoppingCatalogSheld in the database
        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeCreate + 1);
        ShoppingCatalogSheld testShoppingCatalogSheld = shoppingCatalogSheldList.get(shoppingCatalogSheldList.size() - 1);
        assertThat(testShoppingCatalogSheld.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testShoppingCatalogSheld.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testShoppingCatalogSheld.isIsDefault()).isEqualTo(DEFAULT_IS_DEFAULT);
        assertThat(testShoppingCatalogSheld.isIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createShoppingCatalogSheldWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoppingCatalogSheldRepository.findAll().size();

        // Create the ShoppingCatalogSheld with an existing ID
        shoppingCatalogSheld.setId(1L);
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoppingCatalogSheldMockMvc.perform(post("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCatalogSheld in the database
        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogSheldRepository.findAll().size();
        // set the field null
        shoppingCatalogSheld.setLabel(null);

        // Create the ShoppingCatalogSheld, which fails.
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);

        restShoppingCatalogSheldMockMvc.perform(post("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogSheldRepository.findAll().size();
        // set the field null
        shoppingCatalogSheld.setLastUpdate(null);

        // Create the ShoppingCatalogSheld, which fails.
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);

        restShoppingCatalogSheldMockMvc.perform(post("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsDefaultIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogSheldRepository.findAll().size();
        // set the field null
        shoppingCatalogSheld.setIsDefault(null);

        // Create the ShoppingCatalogSheld, which fails.
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);

        restShoppingCatalogSheldMockMvc.perform(post("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsDeletedIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogSheldRepository.findAll().size();
        // set the field null
        shoppingCatalogSheld.setIsDeleted(null);

        // Create the ShoppingCatalogSheld, which fails.
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);

        restShoppingCatalogSheldMockMvc.perform(post("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogShelds() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList
        restShoppingCatalogSheldMockMvc.perform(get("/api/shopping-catalog-shelds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCatalogSheld.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].isDefault").value(hasItem(DEFAULT_IS_DEFAULT.booleanValue())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getShoppingCatalogSheld() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get the shoppingCatalogSheld
        restShoppingCatalogSheldMockMvc.perform(get("/api/shopping-catalog-shelds/{id}", shoppingCatalogSheld.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shoppingCatalogSheld.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()))
            .andExpect(jsonPath("$.isDefault").value(DEFAULT_IS_DEFAULT.booleanValue()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }


    @Test
    @Transactional
    public void getShoppingCatalogSheldsByIdFiltering() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        Long id = shoppingCatalogSheld.getId();

        defaultShoppingCatalogSheldShouldBeFound("id.equals=" + id);
        defaultShoppingCatalogSheldShouldNotBeFound("id.notEquals=" + id);

        defaultShoppingCatalogSheldShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultShoppingCatalogSheldShouldNotBeFound("id.greaterThan=" + id);

        defaultShoppingCatalogSheldShouldBeFound("id.lessThanOrEqual=" + id);
        defaultShoppingCatalogSheldShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where label equals to DEFAULT_LABEL
        defaultShoppingCatalogSheldShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogSheldList where label equals to UPDATED_LABEL
        defaultShoppingCatalogSheldShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where label not equals to DEFAULT_LABEL
        defaultShoppingCatalogSheldShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogSheldList where label not equals to UPDATED_LABEL
        defaultShoppingCatalogSheldShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultShoppingCatalogSheldShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the shoppingCatalogSheldList where label equals to UPDATED_LABEL
        defaultShoppingCatalogSheldShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where label is not null
        defaultShoppingCatalogSheldShouldBeFound("label.specified=true");

        // Get all the shoppingCatalogSheldList where label is null
        defaultShoppingCatalogSheldShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLabelContainsSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where label contains DEFAULT_LABEL
        defaultShoppingCatalogSheldShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogSheldList where label contains UPDATED_LABEL
        defaultShoppingCatalogSheldShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where label does not contain DEFAULT_LABEL
        defaultShoppingCatalogSheldShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogSheldList where label does not contain UPDATED_LABEL
        defaultShoppingCatalogSheldShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogSheldList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogSheldList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the shoppingCatalogSheldList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate is not null
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.specified=true");

        // Get all the shoppingCatalogSheldList where lastUpdate is null
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogSheldList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogSheldList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogSheldList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultShoppingCatalogSheldShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogSheldList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultShoppingCatalogSheldShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDefaultIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDefault equals to DEFAULT_IS_DEFAULT
        defaultShoppingCatalogSheldShouldBeFound("isDefault.equals=" + DEFAULT_IS_DEFAULT);

        // Get all the shoppingCatalogSheldList where isDefault equals to UPDATED_IS_DEFAULT
        defaultShoppingCatalogSheldShouldNotBeFound("isDefault.equals=" + UPDATED_IS_DEFAULT);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDefaultIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDefault not equals to DEFAULT_IS_DEFAULT
        defaultShoppingCatalogSheldShouldNotBeFound("isDefault.notEquals=" + DEFAULT_IS_DEFAULT);

        // Get all the shoppingCatalogSheldList where isDefault not equals to UPDATED_IS_DEFAULT
        defaultShoppingCatalogSheldShouldBeFound("isDefault.notEquals=" + UPDATED_IS_DEFAULT);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDefaultIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDefault in DEFAULT_IS_DEFAULT or UPDATED_IS_DEFAULT
        defaultShoppingCatalogSheldShouldBeFound("isDefault.in=" + DEFAULT_IS_DEFAULT + "," + UPDATED_IS_DEFAULT);

        // Get all the shoppingCatalogSheldList where isDefault equals to UPDATED_IS_DEFAULT
        defaultShoppingCatalogSheldShouldNotBeFound("isDefault.in=" + UPDATED_IS_DEFAULT);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDefaultIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDefault is not null
        defaultShoppingCatalogSheldShouldBeFound("isDefault.specified=true");

        // Get all the shoppingCatalogSheldList where isDefault is null
        defaultShoppingCatalogSheldShouldNotBeFound("isDefault.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDeleted equals to DEFAULT_IS_DELETED
        defaultShoppingCatalogSheldShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the shoppingCatalogSheldList where isDeleted equals to UPDATED_IS_DELETED
        defaultShoppingCatalogSheldShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultShoppingCatalogSheldShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the shoppingCatalogSheldList where isDeleted not equals to UPDATED_IS_DELETED
        defaultShoppingCatalogSheldShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultShoppingCatalogSheldShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the shoppingCatalogSheldList where isDeleted equals to UPDATED_IS_DELETED
        defaultShoppingCatalogSheldShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        // Get all the shoppingCatalogSheldList where isDeleted is not null
        defaultShoppingCatalogSheldShouldBeFound("isDeleted.specified=true");

        // Get all the shoppingCatalogSheldList where isDeleted is null
        defaultShoppingCatalogSheldShouldNotBeFound("isDeleted.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByShoppingCatalogItemIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);
        ShoppingCatalogItem shoppingCatalogItem = ShoppingCatalogItemResourceIT.createEntity(em);
        em.persist(shoppingCatalogItem);
        em.flush();
        shoppingCatalogSheld.addShoppingCatalogItem(shoppingCatalogItem);
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);
        Long shoppingCatalogItemId = shoppingCatalogItem.getId();

        // Get all the shoppingCatalogSheldList where shoppingCatalogItem equals to shoppingCatalogItemId
        defaultShoppingCatalogSheldShouldBeFound("shoppingCatalogItemId.equals=" + shoppingCatalogItemId);

        // Get all the shoppingCatalogSheldList where shoppingCatalogItem equals to shoppingCatalogItemId + 1
        defaultShoppingCatalogSheldShouldNotBeFound("shoppingCatalogItemId.equals=" + (shoppingCatalogItemId + 1));
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogSheldsByShoppingCatalogCartIsEqualToSomething() throws Exception {
        // Get already existing entity
        ShoppingCatalogCart shoppingCatalogCart = shoppingCatalogSheld.getShoppingCatalogCart();
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);
        Long shoppingCatalogCartId = shoppingCatalogCart.getId();

        // Get all the shoppingCatalogSheldList where shoppingCatalogCart equals to shoppingCatalogCartId
        defaultShoppingCatalogSheldShouldBeFound("shoppingCatalogCartId.equals=" + shoppingCatalogCartId);

        // Get all the shoppingCatalogSheldList where shoppingCatalogCart equals to shoppingCatalogCartId + 1
        defaultShoppingCatalogSheldShouldNotBeFound("shoppingCatalogCartId.equals=" + (shoppingCatalogCartId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShoppingCatalogSheldShouldBeFound(String filter) throws Exception {
        restShoppingCatalogSheldMockMvc.perform(get("/api/shopping-catalog-shelds?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCatalogSheld.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].isDefault").value(hasItem(DEFAULT_IS_DEFAULT.booleanValue())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));

        // Check, that the count call also returns 1
        restShoppingCatalogSheldMockMvc.perform(get("/api/shopping-catalog-shelds/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShoppingCatalogSheldShouldNotBeFound(String filter) throws Exception {
        restShoppingCatalogSheldMockMvc.perform(get("/api/shopping-catalog-shelds?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShoppingCatalogSheldMockMvc.perform(get("/api/shopping-catalog-shelds/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShoppingCatalogSheld() throws Exception {
        // Get the shoppingCatalogSheld
        restShoppingCatalogSheldMockMvc.perform(get("/api/shopping-catalog-shelds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShoppingCatalogSheld() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        int databaseSizeBeforeUpdate = shoppingCatalogSheldRepository.findAll().size();

        // Update the shoppingCatalogSheld
        ShoppingCatalogSheld updatedShoppingCatalogSheld = shoppingCatalogSheldRepository.findById(shoppingCatalogSheld.getId()).get();
        // Disconnect from session so that the updates on updatedShoppingCatalogSheld are not directly saved in db
        em.detach(updatedShoppingCatalogSheld);
        updatedShoppingCatalogSheld
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .isDefault(UPDATED_IS_DEFAULT)
            .isDeleted(UPDATED_IS_DELETED);
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(updatedShoppingCatalogSheld);

        restShoppingCatalogSheldMockMvc.perform(put("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isOk());

        // Validate the ShoppingCatalogSheld in the database
        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeUpdate);
        ShoppingCatalogSheld testShoppingCatalogSheld = shoppingCatalogSheldList.get(shoppingCatalogSheldList.size() - 1);
        assertThat(testShoppingCatalogSheld.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testShoppingCatalogSheld.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testShoppingCatalogSheld.isIsDefault()).isEqualTo(UPDATED_IS_DEFAULT);
        assertThat(testShoppingCatalogSheld.isIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingShoppingCatalogSheld() throws Exception {
        int databaseSizeBeforeUpdate = shoppingCatalogSheldRepository.findAll().size();

        // Create the ShoppingCatalogSheld
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO = shoppingCatalogSheldMapper.toDto(shoppingCatalogSheld);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoppingCatalogSheldMockMvc.perform(put("/api/shopping-catalog-shelds")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogSheldDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCatalogSheld in the database
        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShoppingCatalogSheld() throws Exception {
        // Initialize the database
        shoppingCatalogSheldRepository.saveAndFlush(shoppingCatalogSheld);

        int databaseSizeBeforeDelete = shoppingCatalogSheldRepository.findAll().size();

        // Delete the shoppingCatalogSheld
        restShoppingCatalogSheldMockMvc.perform(delete("/api/shopping-catalog-shelds/{id}", shoppingCatalogSheld.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShoppingCatalogSheld> shoppingCatalogSheldList = shoppingCatalogSheldRepository.findAll();
        assertThat(shoppingCatalogSheldList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
