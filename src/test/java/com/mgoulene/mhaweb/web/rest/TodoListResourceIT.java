package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.TodoList;
import com.mgoulene.mhaweb.domain.Todo;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.domain.Workspace;
import com.mgoulene.mhaweb.repository.TodoListRepository;
import com.mgoulene.mhaweb.service.TodoListService;
import com.mgoulene.mhaweb.service.dto.TodoListDTO;
import com.mgoulene.mhaweb.service.mapper.TodoListMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.TodoListCriteria;
import com.mgoulene.mhaweb.service.TodoListQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TodoListResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class TodoListResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private TodoListRepository todoListRepository;

    @Mock
    private TodoListRepository todoListRepositoryMock;

    @Autowired
    private TodoListMapper todoListMapper;

    @Mock
    private TodoListService todoListServiceMock;

    @Autowired
    private TodoListService todoListService;

    @Autowired
    private TodoListQueryService todoListQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTodoListMockMvc;

    private TodoList todoList;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TodoListResource todoListResource = new TodoListResource(todoListService, todoListQueryService);
        this.restTodoListMockMvc = MockMvcBuilders.standaloneSetup(todoListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TodoList createEntity(EntityManager em) {
        TodoList todoList = new TodoList()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        // Add required entity
        Workspace workspace;
        if (TestUtil.findAll(em, Workspace.class).isEmpty()) {
            workspace = WorkspaceResourceIT.createEntity(em);
            em.persist(workspace);
            em.flush();
        } else {
            workspace = TestUtil.findAll(em, Workspace.class).get(0);
        }
        todoList.setWorkspace(workspace);
        return todoList;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TodoList createUpdatedEntity(EntityManager em) {
        TodoList todoList = new TodoList()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        // Add required entity
        Workspace workspace;
        if (TestUtil.findAll(em, Workspace.class).isEmpty()) {
            workspace = WorkspaceResourceIT.createUpdatedEntity(em);
            em.persist(workspace);
            em.flush();
        } else {
            workspace = TestUtil.findAll(em, Workspace.class).get(0);
        }
        todoList.setWorkspace(workspace);
        return todoList;
    }

    @BeforeEach
    public void initTest() {
        todoList = createEntity(em);
    }

    @Test
    @Transactional
    public void createTodoList() throws Exception {
        int databaseSizeBeforeCreate = todoListRepository.findAll().size();

        // Create the TodoList
        TodoListDTO todoListDTO = todoListMapper.toDto(todoList);
        restTodoListMockMvc.perform(post("/api/todo-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListDTO)))
            .andExpect(status().isCreated());

        // Validate the TodoList in the database
        List<TodoList> todoListList = todoListRepository.findAll();
        assertThat(todoListList).hasSize(databaseSizeBeforeCreate + 1);
        TodoList testTodoList = todoListList.get(todoListList.size() - 1);
        assertThat(testTodoList.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testTodoList.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createTodoListWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = todoListRepository.findAll().size();

        // Create the TodoList with an existing ID
        todoList.setId(1L);
        TodoListDTO todoListDTO = todoListMapper.toDto(todoList);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTodoListMockMvc.perform(post("/api/todo-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TodoList in the database
        List<TodoList> todoListList = todoListRepository.findAll();
        assertThat(todoListList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = todoListRepository.findAll().size();
        // set the field null
        todoList.setLabel(null);

        // Create the TodoList, which fails.
        TodoListDTO todoListDTO = todoListMapper.toDto(todoList);

        restTodoListMockMvc.perform(post("/api/todo-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListDTO)))
            .andExpect(status().isBadRequest());

        List<TodoList> todoListList = todoListRepository.findAll();
        assertThat(todoListList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = todoListRepository.findAll().size();
        // set the field null
        todoList.setLastUpdate(null);

        // Create the TodoList, which fails.
        TodoListDTO todoListDTO = todoListMapper.toDto(todoList);

        restTodoListMockMvc.perform(post("/api/todo-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListDTO)))
            .andExpect(status().isBadRequest());

        List<TodoList> todoListList = todoListRepository.findAll();
        assertThat(todoListList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTodoLists() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList
        restTodoListMockMvc.perform(get("/api/todo-lists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(todoList.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllTodoListsWithEagerRelationshipsIsEnabled() throws Exception {
        TodoListResource todoListResource = new TodoListResource(todoListServiceMock, todoListQueryService);
        when(todoListServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restTodoListMockMvc = MockMvcBuilders.standaloneSetup(todoListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restTodoListMockMvc.perform(get("/api/todo-lists?eagerload=true"))
        .andExpect(status().isOk());

        verify(todoListServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllTodoListsWithEagerRelationshipsIsNotEnabled() throws Exception {
        TodoListResource todoListResource = new TodoListResource(todoListServiceMock, todoListQueryService);
            when(todoListServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restTodoListMockMvc = MockMvcBuilders.standaloneSetup(todoListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restTodoListMockMvc.perform(get("/api/todo-lists?eagerload=true"))
        .andExpect(status().isOk());

            verify(todoListServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getTodoList() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get the todoList
        restTodoListMockMvc.perform(get("/api/todo-lists/{id}", todoList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(todoList.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getTodoListsByIdFiltering() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        Long id = todoList.getId();

        defaultTodoListShouldBeFound("id.equals=" + id);
        defaultTodoListShouldNotBeFound("id.notEquals=" + id);

        defaultTodoListShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTodoListShouldNotBeFound("id.greaterThan=" + id);

        defaultTodoListShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTodoListShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTodoListsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where label equals to DEFAULT_LABEL
        defaultTodoListShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the todoListList where label equals to UPDATED_LABEL
        defaultTodoListShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where label not equals to DEFAULT_LABEL
        defaultTodoListShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the todoListList where label not equals to UPDATED_LABEL
        defaultTodoListShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultTodoListShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the todoListList where label equals to UPDATED_LABEL
        defaultTodoListShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where label is not null
        defaultTodoListShouldBeFound("label.specified=true");

        // Get all the todoListList where label is null
        defaultTodoListShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllTodoListsByLabelContainsSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where label contains DEFAULT_LABEL
        defaultTodoListShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the todoListList where label contains UPDATED_LABEL
        defaultTodoListShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where label does not contain DEFAULT_LABEL
        defaultTodoListShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the todoListList where label does not contain UPDATED_LABEL
        defaultTodoListShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultTodoListShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTodoListShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultTodoListShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultTodoListShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultTodoListShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the todoListList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultTodoListShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate is not null
        defaultTodoListShouldBeFound("lastUpdate.specified=true");

        // Get all the todoListList where lastUpdate is null
        defaultTodoListShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultTodoListShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultTodoListShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultTodoListShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultTodoListShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultTodoListShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultTodoListShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllTodoListsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        // Get all the todoListList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultTodoListShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the todoListList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultTodoListShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllTodoListsByTodoIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);
        Todo todo = TodoResourceIT.createEntity(em);
        em.persist(todo);
        em.flush();
        todoList.addTodo(todo);
        todoListRepository.saveAndFlush(todoList);
        Long todoId = todo.getId();

        // Get all the todoListList where todo equals to todoId
        defaultTodoListShouldBeFound("todoId.equals=" + todoId);

        // Get all the todoListList where todo equals to todoId + 1
        defaultTodoListShouldNotBeFound("todoId.equals=" + (todoId + 1));
    }


    @Test
    @Transactional
    public void getAllTodoListsByOwnerIsEqualToSomething() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);
        Profil owner = ProfilResourceIT.createEntity(em);
        em.persist(owner);
        em.flush();
        todoList.addOwner(owner);
        todoListRepository.saveAndFlush(todoList);
        Long ownerId = owner.getId();

        // Get all the todoListList where owner equals to ownerId
        defaultTodoListShouldBeFound("ownerId.equals=" + ownerId);

        // Get all the todoListList where owner equals to ownerId + 1
        defaultTodoListShouldNotBeFound("ownerId.equals=" + (ownerId + 1));
    }


    @Test
    @Transactional
    public void getAllTodoListsByWorkspaceIsEqualToSomething() throws Exception {
        // Get already existing entity
        Workspace workspace = todoList.getWorkspace();
        todoListRepository.saveAndFlush(todoList);
        Long workspaceId = workspace.getId();

        // Get all the todoListList where workspace equals to workspaceId
        defaultTodoListShouldBeFound("workspaceId.equals=" + workspaceId);

        // Get all the todoListList where workspace equals to workspaceId + 1
        defaultTodoListShouldNotBeFound("workspaceId.equals=" + (workspaceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTodoListShouldBeFound(String filter) throws Exception {
        restTodoListMockMvc.perform(get("/api/todo-lists?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(todoList.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restTodoListMockMvc.perform(get("/api/todo-lists/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTodoListShouldNotBeFound(String filter) throws Exception {
        restTodoListMockMvc.perform(get("/api/todo-lists?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTodoListMockMvc.perform(get("/api/todo-lists/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTodoList() throws Exception {
        // Get the todoList
        restTodoListMockMvc.perform(get("/api/todo-lists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTodoList() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        int databaseSizeBeforeUpdate = todoListRepository.findAll().size();

        // Update the todoList
        TodoList updatedTodoList = todoListRepository.findById(todoList.getId()).get();
        // Disconnect from session so that the updates on updatedTodoList are not directly saved in db
        em.detach(updatedTodoList);
        updatedTodoList
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        TodoListDTO todoListDTO = todoListMapper.toDto(updatedTodoList);

        restTodoListMockMvc.perform(put("/api/todo-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListDTO)))
            .andExpect(status().isOk());

        // Validate the TodoList in the database
        List<TodoList> todoListList = todoListRepository.findAll();
        assertThat(todoListList).hasSize(databaseSizeBeforeUpdate);
        TodoList testTodoList = todoListList.get(todoListList.size() - 1);
        assertThat(testTodoList.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testTodoList.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingTodoList() throws Exception {
        int databaseSizeBeforeUpdate = todoListRepository.findAll().size();

        // Create the TodoList
        TodoListDTO todoListDTO = todoListMapper.toDto(todoList);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTodoListMockMvc.perform(put("/api/todo-lists")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(todoListDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TodoList in the database
        List<TodoList> todoListList = todoListRepository.findAll();
        assertThat(todoListList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTodoList() throws Exception {
        // Initialize the database
        todoListRepository.saveAndFlush(todoList);

        int databaseSizeBeforeDelete = todoListRepository.findAll().size();

        // Delete the todoList
        restTodoListMockMvc.perform(delete("/api/todo-lists/{id}", todoList.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TodoList> todoListList = todoListRepository.findAll();
        assertThat(todoListList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
