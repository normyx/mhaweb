package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.SpentSharingConfig;
import com.mgoulene.mhaweb.domain.SpentConfig;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.repository.SpentSharingConfigRepository;
import com.mgoulene.mhaweb.service.SpentSharingConfigService;
import com.mgoulene.mhaweb.service.dto.SpentSharingConfigDTO;
import com.mgoulene.mhaweb.service.mapper.SpentSharingConfigMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.SpentSharingConfigCriteria;
import com.mgoulene.mhaweb.service.SpentSharingConfigQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SpentSharingConfigResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class SpentSharingConfigResourceIT {

    private static final Integer DEFAULT_SHARE = 0;
    private static final Integer UPDATED_SHARE = 1;
    private static final Integer SMALLER_SHARE = 0 - 1;

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private SpentSharingConfigRepository spentSharingConfigRepository;

    @Autowired
    private SpentSharingConfigMapper spentSharingConfigMapper;

    @Autowired
    private SpentSharingConfigService spentSharingConfigService;

    @Autowired
    private SpentSharingConfigQueryService spentSharingConfigQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentSharingConfigMockMvc;

    private SpentSharingConfig spentSharingConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentSharingConfigResource spentSharingConfigResource = new SpentSharingConfigResource(spentSharingConfigService, spentSharingConfigQueryService);
        this.restSpentSharingConfigMockMvc = MockMvcBuilders.standaloneSetup(spentSharingConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharingConfig createEntity(EntityManager em) {
        SpentSharingConfig spentSharingConfig = new SpentSharingConfig()
            .share(DEFAULT_SHARE)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        // Add required entity
        SpentConfig spentConfig;
        if (TestUtil.findAll(em, SpentConfig.class).isEmpty()) {
            spentConfig = SpentConfigResourceIT.createEntity(em);
            em.persist(spentConfig);
            em.flush();
        } else {
            spentConfig = TestUtil.findAll(em, SpentConfig.class).get(0);
        }
        spentSharingConfig.setSpentConfig(spentConfig);
        // Add required entity
        Profil profil;
        if (TestUtil.findAll(em, Profil.class).isEmpty()) {
            profil = ProfilResourceIT.createEntity(em);
            em.persist(profil);
            em.flush();
        } else {
            profil = TestUtil.findAll(em, Profil.class).get(0);
        }
        spentSharingConfig.setProfil(profil);
        return spentSharingConfig;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharingConfig createUpdatedEntity(EntityManager em) {
        SpentSharingConfig spentSharingConfig = new SpentSharingConfig()
            .share(UPDATED_SHARE)
            .lastUpdate(UPDATED_LAST_UPDATE);
        // Add required entity
        SpentConfig spentConfig;
        if (TestUtil.findAll(em, SpentConfig.class).isEmpty()) {
            spentConfig = SpentConfigResourceIT.createUpdatedEntity(em);
            em.persist(spentConfig);
            em.flush();
        } else {
            spentConfig = TestUtil.findAll(em, SpentConfig.class).get(0);
        }
        spentSharingConfig.setSpentConfig(spentConfig);
        // Add required entity
        Profil profil;
        if (TestUtil.findAll(em, Profil.class).isEmpty()) {
            profil = ProfilResourceIT.createUpdatedEntity(em);
            em.persist(profil);
            em.flush();
        } else {
            profil = TestUtil.findAll(em, Profil.class).get(0);
        }
        spentSharingConfig.setProfil(profil);
        return spentSharingConfig;
    }

    @BeforeEach
    public void initTest() {
        spentSharingConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpentSharingConfig() throws Exception {
        int databaseSizeBeforeCreate = spentSharingConfigRepository.findAll().size();

        // Create the SpentSharingConfig
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);
        restSpentSharingConfigMockMvc.perform(post("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeCreate + 1);
        SpentSharingConfig testSpentSharingConfig = spentSharingConfigList.get(spentSharingConfigList.size() - 1);
        assertThat(testSpentSharingConfig.getShare()).isEqualTo(DEFAULT_SHARE);
        assertThat(testSpentSharingConfig.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createSpentSharingConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentSharingConfigRepository.findAll().size();

        // Create the SpentSharingConfig with an existing ID
        spentSharingConfig.setId(1L);
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentSharingConfigMockMvc.perform(post("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkShareIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingConfigRepository.findAll().size();
        // set the field null
        spentSharingConfig.setShare(null);

        // Create the SpentSharingConfig, which fails.
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);

        restSpentSharingConfigMockMvc.perform(post("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingConfigRepository.findAll().size();
        // set the field null
        spentSharingConfig.setLastUpdate(null);

        // Create the SpentSharingConfig, which fails.
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);

        restSpentSharingConfigMockMvc.perform(post("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigs() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharingConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getSpentSharingConfig() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get the spentSharingConfig
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/{id}", spentSharingConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(spentSharingConfig.getId().intValue()))
            .andExpect(jsonPath("$.share").value(DEFAULT_SHARE))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getSpentSharingConfigsByIdFiltering() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        Long id = spentSharingConfig.getId();

        defaultSpentSharingConfigShouldBeFound("id.equals=" + id);
        defaultSpentSharingConfigShouldNotBeFound("id.notEquals=" + id);

        defaultSpentSharingConfigShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSpentSharingConfigShouldNotBeFound("id.greaterThan=" + id);

        defaultSpentSharingConfigShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSpentSharingConfigShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share equals to DEFAULT_SHARE
        defaultSpentSharingConfigShouldBeFound("share.equals=" + DEFAULT_SHARE);

        // Get all the spentSharingConfigList where share equals to UPDATED_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.equals=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share not equals to DEFAULT_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.notEquals=" + DEFAULT_SHARE);

        // Get all the spentSharingConfigList where share not equals to UPDATED_SHARE
        defaultSpentSharingConfigShouldBeFound("share.notEquals=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share in DEFAULT_SHARE or UPDATED_SHARE
        defaultSpentSharingConfigShouldBeFound("share.in=" + DEFAULT_SHARE + "," + UPDATED_SHARE);

        // Get all the spentSharingConfigList where share equals to UPDATED_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.in=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share is not null
        defaultSpentSharingConfigShouldBeFound("share.specified=true");

        // Get all the spentSharingConfigList where share is null
        defaultSpentSharingConfigShouldNotBeFound("share.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share is greater than or equal to DEFAULT_SHARE
        defaultSpentSharingConfigShouldBeFound("share.greaterThanOrEqual=" + DEFAULT_SHARE);

        // Get all the spentSharingConfigList where share is greater than or equal to UPDATED_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.greaterThanOrEqual=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share is less than or equal to DEFAULT_SHARE
        defaultSpentSharingConfigShouldBeFound("share.lessThanOrEqual=" + DEFAULT_SHARE);

        // Get all the spentSharingConfigList where share is less than or equal to SMALLER_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.lessThanOrEqual=" + SMALLER_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsLessThanSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share is less than DEFAULT_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.lessThan=" + DEFAULT_SHARE);

        // Get all the spentSharingConfigList where share is less than UPDATED_SHARE
        defaultSpentSharingConfigShouldBeFound("share.lessThan=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share is greater than DEFAULT_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.greaterThan=" + DEFAULT_SHARE);

        // Get all the spentSharingConfigList where share is greater than SMALLER_SHARE
        defaultSpentSharingConfigShouldBeFound("share.greaterThan=" + SMALLER_SHARE);
    }


    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultSpentSharingConfigShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingConfigList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingConfigList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultSpentSharingConfigShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultSpentSharingConfigShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the spentSharingConfigList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate is not null
        defaultSpentSharingConfigShouldBeFound("lastUpdate.specified=true");

        // Get all the spentSharingConfigList where lastUpdate is null
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultSpentSharingConfigShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingConfigList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultSpentSharingConfigShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingConfigList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingConfigList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultSpentSharingConfigShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultSpentSharingConfigShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the spentSharingConfigList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultSpentSharingConfigShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllSpentSharingConfigsBySpentConfigIsEqualToSomething() throws Exception {
        // Get already existing entity
        SpentConfig spentConfig = spentSharingConfig.getSpentConfig();
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);
        Long spentConfigId = spentConfig.getId();

        // Get all the spentSharingConfigList where spentConfig equals to spentConfigId
        defaultSpentSharingConfigShouldBeFound("spentConfigId.equals=" + spentConfigId);

        // Get all the spentSharingConfigList where spentConfig equals to spentConfigId + 1
        defaultSpentSharingConfigShouldNotBeFound("spentConfigId.equals=" + (spentConfigId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentSharingConfigsByProfilIsEqualToSomething() throws Exception {
        // Get already existing entity
        Profil profil = spentSharingConfig.getProfil();
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);
        Long profilId = profil.getId();

        // Get all the spentSharingConfigList where profil equals to profilId
        defaultSpentSharingConfigShouldBeFound("profilId.equals=" + profilId);

        // Get all the spentSharingConfigList where profil equals to profilId + 1
        defaultSpentSharingConfigShouldNotBeFound("profilId.equals=" + (profilId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentSharingConfigShouldBeFound(String filter) throws Exception {
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharingConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentSharingConfigShouldNotBeFound(String filter) throws Exception {
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpentSharingConfig() throws Exception {
        // Get the spentSharingConfig
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpentSharingConfig() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        int databaseSizeBeforeUpdate = spentSharingConfigRepository.findAll().size();

        // Update the spentSharingConfig
        SpentSharingConfig updatedSpentSharingConfig = spentSharingConfigRepository.findById(spentSharingConfig.getId()).get();
        // Disconnect from session so that the updates on updatedSpentSharingConfig are not directly saved in db
        em.detach(updatedSpentSharingConfig);
        updatedSpentSharingConfig
            .share(UPDATED_SHARE)
            .lastUpdate(UPDATED_LAST_UPDATE);
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(updatedSpentSharingConfig);

        restSpentSharingConfigMockMvc.perform(put("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isOk());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeUpdate);
        SpentSharingConfig testSpentSharingConfig = spentSharingConfigList.get(spentSharingConfigList.size() - 1);
        assertThat(testSpentSharingConfig.getShare()).isEqualTo(UPDATED_SHARE);
        assertThat(testSpentSharingConfig.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSpentSharingConfig() throws Exception {
        int databaseSizeBeforeUpdate = spentSharingConfigRepository.findAll().size();

        // Create the SpentSharingConfig
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentSharingConfigMockMvc.perform(put("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpentSharingConfig() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        int databaseSizeBeforeDelete = spentSharingConfigRepository.findAll().size();

        // Delete the spentSharingConfig
        restSpentSharingConfigMockMvc.perform(delete("/api/spent-sharing-configs/{id}", spentSharingConfig.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
