package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.ShoppingCatalogCart;
import com.mgoulene.mhaweb.domain.ShoppingCatalogSheld;
import com.mgoulene.mhaweb.repository.ShoppingCatalogCartRepository;
import com.mgoulene.mhaweb.service.ShoppingCatalogCartService;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogCartMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogCartCriteria;
import com.mgoulene.mhaweb.service.ShoppingCatalogCartQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShoppingCatalogCartResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class ShoppingCatalogCartResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private ShoppingCatalogCartRepository shoppingCatalogCartRepository;

    @Autowired
    private ShoppingCatalogCartMapper shoppingCatalogCartMapper;

    @Autowired
    private ShoppingCatalogCartService shoppingCatalogCartService;

    @Autowired
    private ShoppingCatalogCartQueryService shoppingCatalogCartQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoppingCatalogCartMockMvc;

    private ShoppingCatalogCart shoppingCatalogCart;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoppingCatalogCartResource shoppingCatalogCartResource = new ShoppingCatalogCartResource(shoppingCatalogCartService, shoppingCatalogCartQueryService);
        this.restShoppingCatalogCartMockMvc = MockMvcBuilders.standaloneSetup(shoppingCatalogCartResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCatalogCart createEntity(EntityManager em) {
        ShoppingCatalogCart shoppingCatalogCart = new ShoppingCatalogCart()
            .label(DEFAULT_LABEL)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return shoppingCatalogCart;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCatalogCart createUpdatedEntity(EntityManager em) {
        ShoppingCatalogCart shoppingCatalogCart = new ShoppingCatalogCart()
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return shoppingCatalogCart;
    }

    @BeforeEach
    public void initTest() {
        shoppingCatalogCart = createEntity(em);
    }

    @Test
    @Transactional
    public void createShoppingCatalogCart() throws Exception {
        int databaseSizeBeforeCreate = shoppingCatalogCartRepository.findAll().size();

        // Create the ShoppingCatalogCart
        ShoppingCatalogCartDTO shoppingCatalogCartDTO = shoppingCatalogCartMapper.toDto(shoppingCatalogCart);
        restShoppingCatalogCartMockMvc.perform(post("/api/shopping-catalog-carts")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogCartDTO)))
            .andExpect(status().isCreated());

        // Validate the ShoppingCatalogCart in the database
        List<ShoppingCatalogCart> shoppingCatalogCartList = shoppingCatalogCartRepository.findAll();
        assertThat(shoppingCatalogCartList).hasSize(databaseSizeBeforeCreate + 1);
        ShoppingCatalogCart testShoppingCatalogCart = shoppingCatalogCartList.get(shoppingCatalogCartList.size() - 1);
        assertThat(testShoppingCatalogCart.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testShoppingCatalogCart.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createShoppingCatalogCartWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoppingCatalogCartRepository.findAll().size();

        // Create the ShoppingCatalogCart with an existing ID
        shoppingCatalogCart.setId(1L);
        ShoppingCatalogCartDTO shoppingCatalogCartDTO = shoppingCatalogCartMapper.toDto(shoppingCatalogCart);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoppingCatalogCartMockMvc.perform(post("/api/shopping-catalog-carts")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogCartDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCatalogCart in the database
        List<ShoppingCatalogCart> shoppingCatalogCartList = shoppingCatalogCartRepository.findAll();
        assertThat(shoppingCatalogCartList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogCartRepository.findAll().size();
        // set the field null
        shoppingCatalogCart.setLabel(null);

        // Create the ShoppingCatalogCart, which fails.
        ShoppingCatalogCartDTO shoppingCatalogCartDTO = shoppingCatalogCartMapper.toDto(shoppingCatalogCart);

        restShoppingCatalogCartMockMvc.perform(post("/api/shopping-catalog-carts")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogCartDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogCart> shoppingCatalogCartList = shoppingCatalogCartRepository.findAll();
        assertThat(shoppingCatalogCartList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingCatalogCartRepository.findAll().size();
        // set the field null
        shoppingCatalogCart.setLastUpdate(null);

        // Create the ShoppingCatalogCart, which fails.
        ShoppingCatalogCartDTO shoppingCatalogCartDTO = shoppingCatalogCartMapper.toDto(shoppingCatalogCart);

        restShoppingCatalogCartMockMvc.perform(post("/api/shopping-catalog-carts")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogCartDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingCatalogCart> shoppingCatalogCartList = shoppingCatalogCartRepository.findAll();
        assertThat(shoppingCatalogCartList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCarts() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList
        restShoppingCatalogCartMockMvc.perform(get("/api/shopping-catalog-carts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCatalogCart.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getShoppingCatalogCart() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get the shoppingCatalogCart
        restShoppingCatalogCartMockMvc.perform(get("/api/shopping-catalog-carts/{id}", shoppingCatalogCart.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shoppingCatalogCart.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getShoppingCatalogCartsByIdFiltering() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        Long id = shoppingCatalogCart.getId();

        defaultShoppingCatalogCartShouldBeFound("id.equals=" + id);
        defaultShoppingCatalogCartShouldNotBeFound("id.notEquals=" + id);

        defaultShoppingCatalogCartShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultShoppingCatalogCartShouldNotBeFound("id.greaterThan=" + id);

        defaultShoppingCatalogCartShouldBeFound("id.lessThanOrEqual=" + id);
        defaultShoppingCatalogCartShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where label equals to DEFAULT_LABEL
        defaultShoppingCatalogCartShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogCartList where label equals to UPDATED_LABEL
        defaultShoppingCatalogCartShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where label not equals to DEFAULT_LABEL
        defaultShoppingCatalogCartShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogCartList where label not equals to UPDATED_LABEL
        defaultShoppingCatalogCartShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultShoppingCatalogCartShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the shoppingCatalogCartList where label equals to UPDATED_LABEL
        defaultShoppingCatalogCartShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where label is not null
        defaultShoppingCatalogCartShouldBeFound("label.specified=true");

        // Get all the shoppingCatalogCartList where label is null
        defaultShoppingCatalogCartShouldNotBeFound("label.specified=false");
    }
                @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLabelContainsSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where label contains DEFAULT_LABEL
        defaultShoppingCatalogCartShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogCartList where label contains UPDATED_LABEL
        defaultShoppingCatalogCartShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where label does not contain DEFAULT_LABEL
        defaultShoppingCatalogCartShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the shoppingCatalogCartList where label does not contain UPDATED_LABEL
        defaultShoppingCatalogCartShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogCartList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogCartList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the shoppingCatalogCartList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate is not null
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.specified=true");

        // Get all the shoppingCatalogCartList where lastUpdate is null
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogCartList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogCartList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogCartList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        // Get all the shoppingCatalogCartList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultShoppingCatalogCartShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the shoppingCatalogCartList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultShoppingCatalogCartShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllShoppingCatalogCartsByShoppingCatalogSheldIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);
        ShoppingCatalogSheld shoppingCatalogSheld = ShoppingCatalogSheldResourceIT.createEntity(em);
        em.persist(shoppingCatalogSheld);
        em.flush();
        shoppingCatalogCart.addShoppingCatalogSheld(shoppingCatalogSheld);
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);
        Long shoppingCatalogSheldId = shoppingCatalogSheld.getId();

        // Get all the shoppingCatalogCartList where shoppingCatalogSheld equals to shoppingCatalogSheldId
        defaultShoppingCatalogCartShouldBeFound("shoppingCatalogSheldId.equals=" + shoppingCatalogSheldId);

        // Get all the shoppingCatalogCartList where shoppingCatalogSheld equals to shoppingCatalogSheldId + 1
        defaultShoppingCatalogCartShouldNotBeFound("shoppingCatalogSheldId.equals=" + (shoppingCatalogSheldId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShoppingCatalogCartShouldBeFound(String filter) throws Exception {
        restShoppingCatalogCartMockMvc.perform(get("/api/shopping-catalog-carts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCatalogCart.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restShoppingCatalogCartMockMvc.perform(get("/api/shopping-catalog-carts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShoppingCatalogCartShouldNotBeFound(String filter) throws Exception {
        restShoppingCatalogCartMockMvc.perform(get("/api/shopping-catalog-carts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShoppingCatalogCartMockMvc.perform(get("/api/shopping-catalog-carts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShoppingCatalogCart() throws Exception {
        // Get the shoppingCatalogCart
        restShoppingCatalogCartMockMvc.perform(get("/api/shopping-catalog-carts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShoppingCatalogCart() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        int databaseSizeBeforeUpdate = shoppingCatalogCartRepository.findAll().size();

        // Update the shoppingCatalogCart
        ShoppingCatalogCart updatedShoppingCatalogCart = shoppingCatalogCartRepository.findById(shoppingCatalogCart.getId()).get();
        // Disconnect from session so that the updates on updatedShoppingCatalogCart are not directly saved in db
        em.detach(updatedShoppingCatalogCart);
        updatedShoppingCatalogCart
            .label(UPDATED_LABEL)
            .lastUpdate(UPDATED_LAST_UPDATE);
        ShoppingCatalogCartDTO shoppingCatalogCartDTO = shoppingCatalogCartMapper.toDto(updatedShoppingCatalogCart);

        restShoppingCatalogCartMockMvc.perform(put("/api/shopping-catalog-carts")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogCartDTO)))
            .andExpect(status().isOk());

        // Validate the ShoppingCatalogCart in the database
        List<ShoppingCatalogCart> shoppingCatalogCartList = shoppingCatalogCartRepository.findAll();
        assertThat(shoppingCatalogCartList).hasSize(databaseSizeBeforeUpdate);
        ShoppingCatalogCart testShoppingCatalogCart = shoppingCatalogCartList.get(shoppingCatalogCartList.size() - 1);
        assertThat(testShoppingCatalogCart.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testShoppingCatalogCart.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingShoppingCatalogCart() throws Exception {
        int databaseSizeBeforeUpdate = shoppingCatalogCartRepository.findAll().size();

        // Create the ShoppingCatalogCart
        ShoppingCatalogCartDTO shoppingCatalogCartDTO = shoppingCatalogCartMapper.toDto(shoppingCatalogCart);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoppingCatalogCartMockMvc.perform(put("/api/shopping-catalog-carts")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCatalogCartDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCatalogCart in the database
        List<ShoppingCatalogCart> shoppingCatalogCartList = shoppingCatalogCartRepository.findAll();
        assertThat(shoppingCatalogCartList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShoppingCatalogCart() throws Exception {
        // Initialize the database
        shoppingCatalogCartRepository.saveAndFlush(shoppingCatalogCart);

        int databaseSizeBeforeDelete = shoppingCatalogCartRepository.findAll().size();

        // Delete the shoppingCatalogCart
        restShoppingCatalogCartMockMvc.perform(delete("/api/shopping-catalog-carts/{id}", shoppingCatalogCart.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShoppingCatalogCart> shoppingCatalogCartList = shoppingCatalogCartRepository.findAll();
        assertThat(shoppingCatalogCartList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
