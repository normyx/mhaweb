package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.ProfilData;
import com.mgoulene.mhaweb.repository.ProfilDataRepository;
import com.mgoulene.mhaweb.service.ProfilDataService;
import com.mgoulene.mhaweb.service.dto.ProfilDataDTO;
import com.mgoulene.mhaweb.service.mapper.ProfilDataMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.ProfilDataCriteria;
import com.mgoulene.mhaweb.service.ProfilDataQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProfilDataResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class ProfilDataResourceIT {

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private ProfilDataRepository profilDataRepository;

    @Autowired
    private ProfilDataMapper profilDataMapper;

    @Autowired
    private ProfilDataService profilDataService;

    @Autowired
    private ProfilDataQueryService profilDataQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProfilDataMockMvc;

    private ProfilData profilData;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProfilDataResource profilDataResource = new ProfilDataResource(profilDataService, profilDataQueryService);
        this.restProfilDataMockMvc = MockMvcBuilders.standaloneSetup(profilDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProfilData createEntity(EntityManager em) {
        ProfilData profilData = new ProfilData()
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return profilData;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProfilData createUpdatedEntity(EntityManager em) {
        ProfilData profilData = new ProfilData()
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return profilData;
    }

    @BeforeEach
    public void initTest() {
        profilData = createEntity(em);
    }

    @Test
    @Transactional
    public void createProfilData() throws Exception {
        int databaseSizeBeforeCreate = profilDataRepository.findAll().size();

        // Create the ProfilData
        ProfilDataDTO profilDataDTO = profilDataMapper.toDto(profilData);
        restProfilDataMockMvc.perform(post("/api/profil-data")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDataDTO)))
            .andExpect(status().isCreated());

        // Validate the ProfilData in the database
        List<ProfilData> profilDataList = profilDataRepository.findAll();
        assertThat(profilDataList).hasSize(databaseSizeBeforeCreate + 1);
        ProfilData testProfilData = profilDataList.get(profilDataList.size() - 1);
        assertThat(testProfilData.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testProfilData.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testProfilData.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createProfilDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = profilDataRepository.findAll().size();

        // Create the ProfilData with an existing ID
        profilData.setId(1L);
        ProfilDataDTO profilDataDTO = profilDataMapper.toDto(profilData);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProfilDataMockMvc.perform(post("/api/profil-data")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProfilData in the database
        List<ProfilData> profilDataList = profilDataRepository.findAll();
        assertThat(profilDataList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = profilDataRepository.findAll().size();
        // set the field null
        profilData.setLastUpdate(null);

        // Create the ProfilData, which fails.
        ProfilDataDTO profilDataDTO = profilDataMapper.toDto(profilData);

        restProfilDataMockMvc.perform(post("/api/profil-data")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDataDTO)))
            .andExpect(status().isBadRequest());

        List<ProfilData> profilDataList = profilDataRepository.findAll();
        assertThat(profilDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProfilData() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList
        restProfilDataMockMvc.perform(get("/api/profil-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(profilData.getId().intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProfilData() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get the profilData
        restProfilDataMockMvc.perform(get("/api/profil-data/{id}", profilData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(profilData.getId().intValue()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getProfilDataByIdFiltering() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        Long id = profilData.getId();

        defaultProfilDataShouldBeFound("id.equals=" + id);
        defaultProfilDataShouldNotBeFound("id.notEquals=" + id);

        defaultProfilDataShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultProfilDataShouldNotBeFound("id.greaterThan=" + id);

        defaultProfilDataShouldBeFound("id.lessThanOrEqual=" + id);
        defaultProfilDataShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultProfilDataShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the profilDataList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultProfilDataShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultProfilDataShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the profilDataList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultProfilDataShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultProfilDataShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the profilDataList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultProfilDataShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate is not null
        defaultProfilDataShouldBeFound("lastUpdate.specified=true");

        // Get all the profilDataList where lastUpdate is null
        defaultProfilDataShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultProfilDataShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the profilDataList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultProfilDataShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultProfilDataShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the profilDataList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultProfilDataShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultProfilDataShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the profilDataList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultProfilDataShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilDataByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        // Get all the profilDataList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultProfilDataShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the profilDataList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultProfilDataShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProfilDataShouldBeFound(String filter) throws Exception {
        restProfilDataMockMvc.perform(get("/api/profil-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(profilData.getId().intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restProfilDataMockMvc.perform(get("/api/profil-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProfilDataShouldNotBeFound(String filter) throws Exception {
        restProfilDataMockMvc.perform(get("/api/profil-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProfilDataMockMvc.perform(get("/api/profil-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProfilData() throws Exception {
        // Get the profilData
        restProfilDataMockMvc.perform(get("/api/profil-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProfilData() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        int databaseSizeBeforeUpdate = profilDataRepository.findAll().size();

        // Update the profilData
        ProfilData updatedProfilData = profilDataRepository.findById(profilData.getId()).get();
        // Disconnect from session so that the updates on updatedProfilData are not directly saved in db
        em.detach(updatedProfilData);
        updatedProfilData
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .lastUpdate(UPDATED_LAST_UPDATE);
        ProfilDataDTO profilDataDTO = profilDataMapper.toDto(updatedProfilData);

        restProfilDataMockMvc.perform(put("/api/profil-data")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDataDTO)))
            .andExpect(status().isOk());

        // Validate the ProfilData in the database
        List<ProfilData> profilDataList = profilDataRepository.findAll();
        assertThat(profilDataList).hasSize(databaseSizeBeforeUpdate);
        ProfilData testProfilData = profilDataList.get(profilDataList.size() - 1);
        assertThat(testProfilData.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testProfilData.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testProfilData.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProfilData() throws Exception {
        int databaseSizeBeforeUpdate = profilDataRepository.findAll().size();

        // Create the ProfilData
        ProfilDataDTO profilDataDTO = profilDataMapper.toDto(profilData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProfilDataMockMvc.perform(put("/api/profil-data")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProfilData in the database
        List<ProfilData> profilDataList = profilDataRepository.findAll();
        assertThat(profilDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProfilData() throws Exception {
        // Initialize the database
        profilDataRepository.saveAndFlush(profilData);

        int databaseSizeBeforeDelete = profilDataRepository.findAll().size();

        // Delete the profilData
        restProfilDataMockMvc.perform(delete("/api/profil-data/{id}", profilData.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProfilData> profilDataList = profilDataRepository.findAll();
        assertThat(profilDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
