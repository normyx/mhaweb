package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.MhawebApp;
import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.domain.User;
import com.mgoulene.mhaweb.domain.ProfilData;
import com.mgoulene.mhaweb.domain.Spent;
import com.mgoulene.mhaweb.domain.ShoppingCart;
import com.mgoulene.mhaweb.domain.Task;
import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.domain.TodoList;
import com.mgoulene.mhaweb.domain.TodoListTemplate;
import com.mgoulene.mhaweb.domain.Wallet;
import com.mgoulene.mhaweb.domain.Workspace;
import com.mgoulene.mhaweb.repository.ProfilRepository;
import com.mgoulene.mhaweb.service.ProfilService;
import com.mgoulene.mhaweb.service.dto.ProfilDTO;
import com.mgoulene.mhaweb.service.mapper.ProfilMapper;
import com.mgoulene.mhaweb.web.rest.errors.ExceptionTranslator;
import com.mgoulene.mhaweb.service.dto.ProfilCriteria;
import com.mgoulene.mhaweb.service.ProfilQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.mhaweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProfilResource} REST controller.
 */
@SpringBootTest(classes = MhawebApp.class)
public class ProfilResourceIT {

    private static final String DEFAULT_DISPLAY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private ProfilRepository profilRepository;

    @Autowired
    private ProfilMapper profilMapper;

    @Autowired
    private ProfilService profilService;

    @Autowired
    private ProfilQueryService profilQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProfilMockMvc;

    private Profil profil;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProfilResource profilResource = new ProfilResource(profilService, profilQueryService);
        this.restProfilMockMvc = MockMvcBuilders.standaloneSetup(profilResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Profil createEntity(EntityManager em) {
        Profil profil = new Profil()
            .displayName(DEFAULT_DISPLAY_NAME)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return profil;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Profil createUpdatedEntity(EntityManager em) {
        Profil profil = new Profil()
            .displayName(UPDATED_DISPLAY_NAME)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return profil;
    }

    @BeforeEach
    public void initTest() {
        profil = createEntity(em);
    }

    @Test
    @Transactional
    public void createProfil() throws Exception {
        int databaseSizeBeforeCreate = profilRepository.findAll().size();

        // Create the Profil
        ProfilDTO profilDTO = profilMapper.toDto(profil);
        restProfilMockMvc.perform(post("/api/profils")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDTO)))
            .andExpect(status().isCreated());

        // Validate the Profil in the database
        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeCreate + 1);
        Profil testProfil = profilList.get(profilList.size() - 1);
        assertThat(testProfil.getDisplayName()).isEqualTo(DEFAULT_DISPLAY_NAME);
        assertThat(testProfil.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createProfilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = profilRepository.findAll().size();

        // Create the Profil with an existing ID
        profil.setId(1L);
        ProfilDTO profilDTO = profilMapper.toDto(profil);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProfilMockMvc.perform(post("/api/profils")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Profil in the database
        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDisplayNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = profilRepository.findAll().size();
        // set the field null
        profil.setDisplayName(null);

        // Create the Profil, which fails.
        ProfilDTO profilDTO = profilMapper.toDto(profil);

        restProfilMockMvc.perform(post("/api/profils")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDTO)))
            .andExpect(status().isBadRequest());

        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = profilRepository.findAll().size();
        // set the field null
        profil.setLastUpdate(null);

        // Create the Profil, which fails.
        ProfilDTO profilDTO = profilMapper.toDto(profil);

        restProfilMockMvc.perform(post("/api/profils")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDTO)))
            .andExpect(status().isBadRequest());

        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProfils() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList
        restProfilMockMvc.perform(get("/api/profils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(profil.getId().intValue())))
            .andExpect(jsonPath("$.[*].displayName").value(hasItem(DEFAULT_DISPLAY_NAME)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProfil() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get the profil
        restProfilMockMvc.perform(get("/api/profils/{id}", profil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(profil.getId().intValue()))
            .andExpect(jsonPath("$.displayName").value(DEFAULT_DISPLAY_NAME))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getProfilsByIdFiltering() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        Long id = profil.getId();

        defaultProfilShouldBeFound("id.equals=" + id);
        defaultProfilShouldNotBeFound("id.notEquals=" + id);

        defaultProfilShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultProfilShouldNotBeFound("id.greaterThan=" + id);

        defaultProfilShouldBeFound("id.lessThanOrEqual=" + id);
        defaultProfilShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllProfilsByDisplayNameIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where displayName equals to DEFAULT_DISPLAY_NAME
        defaultProfilShouldBeFound("displayName.equals=" + DEFAULT_DISPLAY_NAME);

        // Get all the profilList where displayName equals to UPDATED_DISPLAY_NAME
        defaultProfilShouldNotBeFound("displayName.equals=" + UPDATED_DISPLAY_NAME);
    }

    @Test
    @Transactional
    public void getAllProfilsByDisplayNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where displayName not equals to DEFAULT_DISPLAY_NAME
        defaultProfilShouldNotBeFound("displayName.notEquals=" + DEFAULT_DISPLAY_NAME);

        // Get all the profilList where displayName not equals to UPDATED_DISPLAY_NAME
        defaultProfilShouldBeFound("displayName.notEquals=" + UPDATED_DISPLAY_NAME);
    }

    @Test
    @Transactional
    public void getAllProfilsByDisplayNameIsInShouldWork() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where displayName in DEFAULT_DISPLAY_NAME or UPDATED_DISPLAY_NAME
        defaultProfilShouldBeFound("displayName.in=" + DEFAULT_DISPLAY_NAME + "," + UPDATED_DISPLAY_NAME);

        // Get all the profilList where displayName equals to UPDATED_DISPLAY_NAME
        defaultProfilShouldNotBeFound("displayName.in=" + UPDATED_DISPLAY_NAME);
    }

    @Test
    @Transactional
    public void getAllProfilsByDisplayNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where displayName is not null
        defaultProfilShouldBeFound("displayName.specified=true");

        // Get all the profilList where displayName is null
        defaultProfilShouldNotBeFound("displayName.specified=false");
    }
                @Test
    @Transactional
    public void getAllProfilsByDisplayNameContainsSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where displayName contains DEFAULT_DISPLAY_NAME
        defaultProfilShouldBeFound("displayName.contains=" + DEFAULT_DISPLAY_NAME);

        // Get all the profilList where displayName contains UPDATED_DISPLAY_NAME
        defaultProfilShouldNotBeFound("displayName.contains=" + UPDATED_DISPLAY_NAME);
    }

    @Test
    @Transactional
    public void getAllProfilsByDisplayNameNotContainsSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where displayName does not contain DEFAULT_DISPLAY_NAME
        defaultProfilShouldNotBeFound("displayName.doesNotContain=" + DEFAULT_DISPLAY_NAME);

        // Get all the profilList where displayName does not contain UPDATED_DISPLAY_NAME
        defaultProfilShouldBeFound("displayName.doesNotContain=" + UPDATED_DISPLAY_NAME);
    }


    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultProfilShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the profilList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultProfilShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate not equals to DEFAULT_LAST_UPDATE
        defaultProfilShouldNotBeFound("lastUpdate.notEquals=" + DEFAULT_LAST_UPDATE);

        // Get all the profilList where lastUpdate not equals to UPDATED_LAST_UPDATE
        defaultProfilShouldBeFound("lastUpdate.notEquals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultProfilShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the profilList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultProfilShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate is not null
        defaultProfilShouldBeFound("lastUpdate.specified=true");

        // Get all the profilList where lastUpdate is null
        defaultProfilShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate is greater than or equal to DEFAULT_LAST_UPDATE
        defaultProfilShouldBeFound("lastUpdate.greaterThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the profilList where lastUpdate is greater than or equal to UPDATED_LAST_UPDATE
        defaultProfilShouldNotBeFound("lastUpdate.greaterThanOrEqual=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate is less than or equal to DEFAULT_LAST_UPDATE
        defaultProfilShouldBeFound("lastUpdate.lessThanOrEqual=" + DEFAULT_LAST_UPDATE);

        // Get all the profilList where lastUpdate is less than or equal to SMALLER_LAST_UPDATE
        defaultProfilShouldNotBeFound("lastUpdate.lessThanOrEqual=" + SMALLER_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate is less than DEFAULT_LAST_UPDATE
        defaultProfilShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the profilList where lastUpdate is less than UPDATED_LAST_UPDATE
        defaultProfilShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllProfilsByLastUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        // Get all the profilList where lastUpdate is greater than DEFAULT_LAST_UPDATE
        defaultProfilShouldNotBeFound("lastUpdate.greaterThan=" + DEFAULT_LAST_UPDATE);

        // Get all the profilList where lastUpdate is greater than SMALLER_LAST_UPDATE
        defaultProfilShouldBeFound("lastUpdate.greaterThan=" + SMALLER_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllProfilsByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        profil.setUser(user);
        profilRepository.saveAndFlush(profil);
        Long userId = user.getId();

        // Get all the profilList where user equals to userId
        defaultProfilShouldBeFound("userId.equals=" + userId);

        // Get all the profilList where user equals to userId + 1
        defaultProfilShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByProfilDataIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        ProfilData profilData = ProfilDataResourceIT.createEntity(em);
        em.persist(profilData);
        em.flush();
        profil.setProfilData(profilData);
        profilRepository.saveAndFlush(profil);
        Long profilDataId = profilData.getId();

        // Get all the profilList where profilData equals to profilDataId
        defaultProfilShouldBeFound("profilDataId.equals=" + profilDataId);

        // Get all the profilList where profilData equals to profilDataId + 1
        defaultProfilShouldNotBeFound("profilDataId.equals=" + (profilDataId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsBySpentIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        Spent spent = SpentResourceIT.createEntity(em);
        em.persist(spent);
        em.flush();
        profil.addSpent(spent);
        profilRepository.saveAndFlush(profil);
        Long spentId = spent.getId();

        // Get all the profilList where spent equals to spentId
        defaultProfilShouldBeFound("spentId.equals=" + spentId);

        // Get all the profilList where spent equals to spentId + 1
        defaultProfilShouldNotBeFound("spentId.equals=" + (spentId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByShoppingCartIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        ShoppingCart shoppingCart = ShoppingCartResourceIT.createEntity(em);
        em.persist(shoppingCart);
        em.flush();
        profil.addShoppingCart(shoppingCart);
        profilRepository.saveAndFlush(profil);
        Long shoppingCartId = shoppingCart.getId();

        // Get all the profilList where shoppingCart equals to shoppingCartId
        defaultProfilShouldBeFound("shoppingCartId.equals=" + shoppingCartId);

        // Get all the profilList where shoppingCart equals to shoppingCartId + 1
        defaultProfilShouldNotBeFound("shoppingCartId.equals=" + (shoppingCartId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByTaskIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        Task task = TaskResourceIT.createEntity(em);
        em.persist(task);
        em.flush();
        profil.addTask(task);
        profilRepository.saveAndFlush(profil);
        Long taskId = task.getId();

        // Get all the profilList where task equals to taskId
        defaultProfilShouldBeFound("taskId.equals=" + taskId);

        // Get all the profilList where task equals to taskId + 1
        defaultProfilShouldNotBeFound("taskId.equals=" + (taskId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByTaskProjectIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        TaskProject taskProject = TaskProjectResourceIT.createEntity(em);
        em.persist(taskProject);
        em.flush();
        profil.addTaskProject(taskProject);
        profilRepository.saveAndFlush(profil);
        Long taskProjectId = taskProject.getId();

        // Get all the profilList where taskProject equals to taskProjectId
        defaultProfilShouldBeFound("taskProjectId.equals=" + taskProjectId);

        // Get all the profilList where taskProject equals to taskProjectId + 1
        defaultProfilShouldNotBeFound("taskProjectId.equals=" + (taskProjectId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByTodoListIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        TodoList todoList = TodoListResourceIT.createEntity(em);
        em.persist(todoList);
        em.flush();
        profil.addTodoList(todoList);
        profilRepository.saveAndFlush(profil);
        Long todoListId = todoList.getId();

        // Get all the profilList where todoList equals to todoListId
        defaultProfilShouldBeFound("todoListId.equals=" + todoListId);

        // Get all the profilList where todoList equals to todoListId + 1
        defaultProfilShouldNotBeFound("todoListId.equals=" + (todoListId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByTodoListTemplateIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        TodoListTemplate todoListTemplate = TodoListTemplateResourceIT.createEntity(em);
        em.persist(todoListTemplate);
        em.flush();
        profil.addTodoListTemplate(todoListTemplate);
        profilRepository.saveAndFlush(profil);
        Long todoListTemplateId = todoListTemplate.getId();

        // Get all the profilList where todoListTemplate equals to todoListTemplateId
        defaultProfilShouldBeFound("todoListTemplateId.equals=" + todoListTemplateId);

        // Get all the profilList where todoListTemplate equals to todoListTemplateId + 1
        defaultProfilShouldNotBeFound("todoListTemplateId.equals=" + (todoListTemplateId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByWalletIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        Wallet wallet = WalletResourceIT.createEntity(em);
        em.persist(wallet);
        em.flush();
        profil.addWallet(wallet);
        profilRepository.saveAndFlush(profil);
        Long walletId = wallet.getId();

        // Get all the profilList where wallet equals to walletId
        defaultProfilShouldBeFound("walletId.equals=" + walletId);

        // Get all the profilList where wallet equals to walletId + 1
        defaultProfilShouldNotBeFound("walletId.equals=" + (walletId + 1));
    }


    @Test
    @Transactional
    public void getAllProfilsByWorkspaceIsEqualToSomething() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);
        Workspace workspace = WorkspaceResourceIT.createEntity(em);
        em.persist(workspace);
        em.flush();
        profil.addWorkspace(workspace);
        profilRepository.saveAndFlush(profil);
        Long workspaceId = workspace.getId();

        // Get all the profilList where workspace equals to workspaceId
        defaultProfilShouldBeFound("workspaceId.equals=" + workspaceId);

        // Get all the profilList where workspace equals to workspaceId + 1
        defaultProfilShouldNotBeFound("workspaceId.equals=" + (workspaceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProfilShouldBeFound(String filter) throws Exception {
        restProfilMockMvc.perform(get("/api/profils?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(profil.getId().intValue())))
            .andExpect(jsonPath("$.[*].displayName").value(hasItem(DEFAULT_DISPLAY_NAME)))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));

        // Check, that the count call also returns 1
        restProfilMockMvc.perform(get("/api/profils/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProfilShouldNotBeFound(String filter) throws Exception {
        restProfilMockMvc.perform(get("/api/profils?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProfilMockMvc.perform(get("/api/profils/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProfil() throws Exception {
        // Get the profil
        restProfilMockMvc.perform(get("/api/profils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProfil() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        int databaseSizeBeforeUpdate = profilRepository.findAll().size();

        // Update the profil
        Profil updatedProfil = profilRepository.findById(profil.getId()).get();
        // Disconnect from session so that the updates on updatedProfil are not directly saved in db
        em.detach(updatedProfil);
        updatedProfil
            .displayName(UPDATED_DISPLAY_NAME)
            .lastUpdate(UPDATED_LAST_UPDATE);
        ProfilDTO profilDTO = profilMapper.toDto(updatedProfil);

        restProfilMockMvc.perform(put("/api/profils")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDTO)))
            .andExpect(status().isOk());

        // Validate the Profil in the database
        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeUpdate);
        Profil testProfil = profilList.get(profilList.size() - 1);
        assertThat(testProfil.getDisplayName()).isEqualTo(UPDATED_DISPLAY_NAME);
        assertThat(testProfil.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProfil() throws Exception {
        int databaseSizeBeforeUpdate = profilRepository.findAll().size();

        // Create the Profil
        ProfilDTO profilDTO = profilMapper.toDto(profil);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProfilMockMvc.perform(put("/api/profils")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Profil in the database
        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProfil() throws Exception {
        // Initialize the database
        profilRepository.saveAndFlush(profil);

        int databaseSizeBeforeDelete = profilRepository.findAll().size();

        // Delete the profil
        restProfilMockMvc.perform(delete("/api/profils/{id}", profil.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
