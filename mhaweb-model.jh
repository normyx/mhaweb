entity Profil {
  displayName String required,
  lastUpdate LocalDate required
}
entity ProfilData {
  photo ImageBlob required,
  lastUpdate LocalDate required
}
entity ShoppingCart {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity ShoppingCatalogCart {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity ShoppingCatalogItem {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity ShoppingCatalogSheld {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required,
  isDefault Boolean required,
  isDeleted Boolean required
}
entity ShoppingItem {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required,
  checked Boolean required,
  quantity Float min(0),
  unit Unit
}
entity Spent {
  label String required minlength(2) maxlength(30),
  description String maxlength(200),
  amount Float required,
  spentDate LocalDate required,
  confirmed Boolean required,
  lastUpdate LocalDate required
}
entity SpentConfig {
  label String required minlength(5) maxlength(40),
  amount Float min(0),
  spentLabel String minlength(2) maxlength(20),
  lastUpdate LocalDate required
}
entity SpentSharing {
  amountShare Float required,
  share Integer required min(0),
  lastUpdate LocalDate required
}
entity SpentSharingConfig {
  share Integer required min(0),
  lastUpdate LocalDate required
}
entity Task {
  label String required minlength(5) maxlength(40),
  description String maxlength(4000),
  done Boolean required,
  dueDate LocalDate,
  lastUpdate LocalDate required
}
entity TaskProject {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity Todo {
  label String required minlength(5) maxlength(40),
  done Boolean required,
  lastUpdate LocalDate required
}
entity TodoList {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity TodoListTemplate {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity TodoTemplate {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity Wallet {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
entity Workspace {
  label String required minlength(5) maxlength(40),
  lastUpdate LocalDate required
}
enum Unit {
  QUANTITY,
  G,
  KG,
  L,
  ML
}

relationship OneToOne {
  Profil{user(login)} to User,
  Profil{profilData} to ProfilData
}
relationship OneToMany {
  Workspace{shoppingCart} to ShoppingCart{workspace(label) required},
  ShoppingCatalogSheld{shoppingCatalogItem} to ShoppingCatalogItem{shoppingCatalogSheld(label) required},
  ShoppingCatalogCart{shoppingCatalogSheld} to ShoppingCatalogSheld{shoppingCatalogCart(label) required},
  ShoppingCart{shoppingItem} to ShoppingItem{shoppingCart(label) required},
  Profil{spent} to Spent{spender(displayName)},
  Spent{spentSharing} to SpentSharing{spent(label)},
  TaskProject{task} to Task{taskProject(label)},
  Workspace{taskProject} to TaskProject{workspace(label)},
  TodoList{todo} to Todo{todoList(label) required},
  Workspace{todoList} to TodoList{workspace(label) required},
  Workspace{todoListTemplate} to TodoListTemplate{workspace(label) required},
  TodoListTemplate{todoTemplate} to TodoTemplate{todoListTemplate(label) required},
  Workspace{wallet} to Wallet{workspace(label) required}
}
relationship ManyToOne {
  ShoppingCart{shoppingCatalogCart(label)} to ShoppingCatalogCart,
  ShoppingItem{shoppingCatalofSheld(label) required} to ShoppingCatalogSheld,
  Spent{wallet(label)} to Wallet,
  SpentConfig{wallet(label) required} to Wallet,
  SpentSharing{sharingProfil(displayName)} to Profil,
  SpentSharingConfig{spentConfig(label) required} to SpentConfig,
  SpentSharingConfig{profil(displayName) required} to Profil
}
relationship ManyToMany {
  ShoppingCart{owner(displayName)} to Profil{shoppingCart},
  Task{owner} to Profil{task},
  TaskProject{owner(displayName)} to Profil{taskProject},
  TodoList{owner(displayName)} to Profil{todoList},
  TodoListTemplate{owner(displayName)} to Profil{todoListTemplate},
  Wallet{owner(displayName)} to Profil{wallet},
  Workspace{owner(displayName)} to Profil{workspace(label)}
}

dto Profil, ProfilData, ShoppingCart, ShoppingCatalogCart, ShoppingCatalogItem, ShoppingCatalogSheld, ShoppingItem, Spent, SpentConfig, SpentSharing, SpentSharingConfig, Task, TaskProject, Todo, TodoList, TodoListTemplate, TodoTemplate, Wallet, Workspace with mapstruct
paginate Profil, ShoppingCart, ShoppingCatalogCart, ShoppingCatalogItem, ShoppingCatalogSheld, ShoppingItem, Spent, SpentConfig, SpentSharing, SpentSharingConfig, Task, TaskProject, Todo, TodoList, TodoListTemplate, TodoTemplate, Wallet, Workspace with infinite-scroll
service Profil, ProfilData, ShoppingCart, ShoppingCatalogCart, ShoppingCatalogItem, ShoppingCatalogSheld, ShoppingItem, Spent, SpentConfig, SpentSharing, SpentSharingConfig, Task, TaskProject, Todo, TodoList, TodoListTemplate, TodoTemplate, Wallet, Workspace with serviceClass
filter Profil, ProfilData, ShoppingCatalogCart, ShoppingCatalogItem, ShoppingCatalogSheld, ShoppingItem, Spent, SpentConfig, SpentSharing, SpentSharingConfig, Task, TaskProject, Todo, TodoList, TodoListTemplate, TodoTemplate, Wallet, Workspace
