@echo off
set BASE_VERSION=0.9.1
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"

set "FULL_STAMP=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"
set "FULL_VERSION=%BASE_VERSION%-%FULL_STAMP%"
set "JAR_FILENAME=.\target\mhaweb-%FULL_VERSION%.jar"
set "APK_FILENAME=.\mha-%FULL_VERSION%.apk"
set "WWW_ZIP_FILENAME=mhaionic-www-%FULL_VERSION%.zip"
set "DUMP_FILEMANE=dump-%FULL_VERSION%.sql"
echo FULL_VERSION: "%FULL_VERSION%"
echo JAR_FILENAME: "%JAR_FILENAME%"
echo APK_FILENAME: "%APK_FILENAME%"
echo DUMP_FILEMANE: "%DUMP_FILEMANE%"

@setlocal

set ERROR_CODE=0

:buildJar
call mvnw.cmd versions:set -DnewVersion=%FULL_VERSION%
if ERRORLEVEL 1 goto error
call mvnw.cmd -Pprod clean verify -DskipTests
if ERRORLEVEL 1 goto error

:buildIonic
cd ..\mhaionic
call echo %FULL_VERSION% > RELEASE.md
if ERRORLEVEL 1 goto error
call ionic cordova build android --prod --release 
if ERRORLEVEL 1 goto error
call jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore msha.keystore -storepass cartel2000 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk msha
if ERRORLEVEL 1 goto error
del platforms\android\app\build\outputs\apk\release\msha.apk
if ERRORLEVEL 1 goto error
zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk platforms/android/app/build/outputs/apk/release/msha.apk
if ERRORLEVEL 1 goto error
move /Y platforms\android\app\build\outputs\apk\release\msha.apk %APK_FILENAME%
if ERRORLEVEL 1 goto error
call ionic build --prod
if ERRORLEVEL 1 goto error
call 7z a -tzip -r %WWW_ZIP_FILENAME% www\*.*
if ERRORLEVEL 1 goto error
cd ..\mhaweb


:preDeployApp
@REM Dump file
call "c:\Program Files\PostgreSQL\12\bin\pg_dump.exe" -w -U mha -h 192.168.0.71 -p 5432 -c mysharedhomeactivities > %DUMP_FILEMANE%
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %JAR_FILENAME% Mathieu@192.168.0.21:/repo/mha
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %DUMP_FILEMANE% Mathieu@192.168.0.21:/repo/mha
if ERRORLEVEL 1 goto error
del %DUMP_FILEMANE%
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %JAR_FILENAME% pi@192.168.0.72:/home/pi/mhapackages/
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %JAR_FILENAME% pi@192.168.0.72:/home/pi/mhapackages/mhaweb-current.jar
if ERRORLEVEL 1 goto error
cd ..\mhaionic
call pscp -pw cartel2000 %APK_FILENAME% Mathieu@192.168.0.21:/repo/mha
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %APK_FILENAME% Mathieu@192.168.0.21:/repo/mha/mha.apk
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %WWW_ZIP_FILENAME% Mathieu@192.168.0.21:/repo/mha
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %WWW_ZIP_FILENAME% pi@192.168.0.72:/home/pi/mhapackages/
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 %WWW_ZIP_FILENAME% pi@192.168.0.72:/home/pi/mhapackages/mhaionic-www-current.zip
if ERRORLEVEL 1 goto error
del %WWW_ZIP_FILENAME%
if ERRORLEVEL 1 goto error
del %APK_FILENAME%
if ERRORLEVEL 1 goto error
cd ..\mhaweb

:commitAll
call git add .
if ERRORLEVEL 1 goto error
call git commit -m "%FULL_VERSION%"
if ERRORLEVEL 1 goto error
cd ..\mhaionic
call git add .
if ERRORLEVEL 1 goto error
call git commit -m "%FULL_VERSION%"
if ERRORLEVEL 1 goto error
cd ..\mhaweb
goto end

:error
set ERROR_CODE=1

:end
@endlocal & set ERROR_CODE=%ERROR_CODE%

exit /B %ERROR_CODE%

